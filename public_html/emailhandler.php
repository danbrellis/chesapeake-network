#!/usr/local/bin/php -q
<?php
define('POSTIE_DEBUG', false);

if(defined('POSTIE_DEBUG') && POSTIE_DEBUG == true){
$email = 'Return-path: <dbrellis@allianceforthebay.org>
Received: from mail-ob0-f182.google.com ([209.85.214.182])
        by vme1235.sidushost.com with esmtps (TLSv1.2:AES128-GCM-SHA256:128)
        (Exim 4.84)
        (envelope-from <dbrellis@allianceforthebay.org>)
        id 1YW3RR-0004z7-Aq
        for 443-accept@chesapeakenetwork.org; Thu, 12 Mar 2015 09:46:25 -0400
Received: by obbnt9 with SMTP id nt9so15745596obb.12
        for <443-accept@chesapeakenetwork.org>; Thu, 12 Mar 2015 11:20:30 -0700 (PDT)
X-Google-DKIM-Signature: v=1; a=rsa-sha256; c=relaxed/relaxed;
        d=1e100.net; s=20130820;
        h=x-gm-message-state:mime-version:from:date:message-id:subject:to
         :content-type;
        bh=kdhViOkSXIheF1omcpIUnD2vJIVtckeQElw10Z1Qsis=;
        b=JEit8AU0Vg3Cn6MusZafMxhCYweivghnZRS5mApeOvAVZ3svwwh8b9B7IiQcLW1KRv
         sP/ACPCu4bfJ8PMoeZACB9W3ZnDk5XePy5dRoE4ZsxfjC803ghennZus+Ux9aA+XmPb2
         ieWvk5yO58uYBgL0hjYVx59UMQTHCd1eI6IhzmA8LrZDXo5+Taj0HIPIzmndBhX3yWC9
         MDuX+4HwQo+90yrXWlzMlnKQwegSJfA1K9QTaN2nujCpqsvJat+IiTTEMpHHdCCvzQih
         H9Krs/WZFhpt6BaBAheUffhN+6h0aAfR1ravOXsQzhIURBNEKN+LzQOHJIEESzs/BqTe
         8KWA==
X-Gm-Message-State: ALoCoQlmV0hzLSwgGdM70wmHNeg2GAHjkvLl5S8QGr5Z8e+YB2f8H1OvVrYqYJ3eY1SVH+hMjdQB
X-Received: by 10.182.117.226 with SMTP id kh2mr27351160obb.15.1426184430829;
 Thu, 12 Mar 2015 11:20:30 -0700 (PDT)
MIME-Version: 1.0
Received: by 10.202.220.7 with HTTP; Thu, 12 Mar 2015 11:20:10 -0700 (PDT)
From: Dan Brellis <dbrellis@allianceforthebay.org>
Date: Thu, 12 Mar 2015 14:20:10 -0400
Message-ID: <CAJUF53PePW+rmt3fjsWmiRKTMKbgQyBk_cNkbvFjFkA@mail.gmail.com>
Subject:
To: accept-443@chesapeakenetwork.org
Content-Type: multipart/alternative; boundary=089e015370ee62550a05111b700f

--089e015370ee62550a05111b700f
Content-Type: text/plain; charset=UTF-8

--
Dan Brellis
Alliance for the Chesapeake Bay
dbrellis@allianceforthebay.org | 443.949.0575

--089e015370ee62550a05111b700f
Content-Type: text/html; charset=UTF-8
Content-Transfer-Encoding: quoted-printable

<div dir=3D"ltr"><br clear=3D"all"><div><div class=3D"gmail_signature"><div=
 dir=3D"ltr"><div><br></div><div>--</div><div>Dan Brellis</div><div>Allianc=
e for the Chesapeake Bay<br></div><div><a href=3D"mailto:dbrellis@alliancef=
orthebay.org" target=3D"_blank">dbrellis@allianceforthebay.org</a> | 443.94=
9.0575</div></div></div></div>
</div>

--089e015370ee62550a05111b700f--';
}
else {
	//Listen to incoming e-mails
	$sock = fopen ("php://stdin", 'r');
	$email = '';
	
	//Read e-mail into buffer
	while (!feof($sock)) {
		$email .= fread($sock, 1024);
	}
	
	//Close socket
	fclose($sock);
}

//bring in the big boys
define('WP_USE_THEMES', false);
require('wp-blog-header.php');

//let it begin!
//cnea prefix = Chesapeake Network Email Annoucnements
cnea_preprocess_email($email);

function cnea_preprocess_email($email){
	require_once(get_stylesheet_directory() . '/includes/mimedecode.php');
	require_once('wp-content/plugins/postie/postie-functions.php');
	
    if (!function_exists('file_get_html'))
		require_once('wp-content/plugins/postie/simple_html_dom.php');

    //EchoInfo("Starting mail fetch");
    postie_environment();
    $wp_content_path = dirname(dirname(dirname(__FILE__)));
    DebugEcho("wp_content_path: $wp_content_path");
	
    if (file_exists($wp_content_path . DIRECTORY_SEPARATOR . "filterPostie.php")) {
        DebugEcho("found filterPostie.php in $wp_content_path");
        include_once ($wp_content_path . DIRECTORY_SEPARATOR . "filterPostie.php");
    }

    //EchoInfo(sprintf(__("There are %d messages to process", "postie"), count($emails)));

    if (function_exists('memory_get_usage')) {
        DebugEcho(__("memory at start of email processing:") . memory_get_usage());
    }

	$mimeDecodedEmail = DecodeMIMEMail($email); //decode email
	//mail('danbrellis@gmail.com', $mimeDecodedEmail->headers['to'], time() . $email); exit();

	if (property_exists($mimeDecodedEmail, "headers") && array_key_exists('from', $mimeDecodedEmail->headers)) {
		//make sure we haven't seen this email already
		if( false === get_transient( 'cnea_' .substr($mimeDecodedEmail->headers["message-id"], 0, 40) ) ) {
			$from = RemoveExtraCharactersInEmailAddress(trim($mimeDecodedEmail->headers["from"]));
			$from = apply_filters("postie_filter_email", $from);
	
			$toEmail = '';
			if (isset($mimeDecodedEmail->headers["to"])) {
				$tos = explode(',', $mimeDecodedEmail->headers["to"]);
				set_transient( 'cnea_' .substr($mimeDecodedEmail->headers["message-id"], 0, 40), 'true', 2 * HOUR_IN_SECONDS );
			}
	
			$replytoEmail = '';
			if (isset($mimeDecodedEmail->headers["reply-to"])) {
				$replytoEmail = $mimeDecodedEmail->headers["reply-to"];
			}
	
			$from = apply_filters("postie_filter_email2", $from, $tos, $replytoEmail);
			DebugEcho("ValidatePoster: post postie_filter_email2 $from");
		}
		else{
			DebugEcho(__("transient already set - duplicate email: ") . 'cnea_' .substr($mimeDecodedEmail->headers["message-id"], 0, 40));
			exit(0);
		};
	}
	
	if($from && $tos && is_array($tos)){
		foreach($tos as $to){
			$to = RemoveExtraCharactersInEmailAddress(trim($to));
			
			//first axe any unintended emails
			$blacklist = array('mailer-daemon@vme1235.sidushost.com', 'chesadmin@vme1235.sidushost.com', 'chesadmin-owner@chesapeakenetwork.org');
			if(in_array(strtolower($from), $blacklist) || in_array(strtolower($to), $blacklist)) exit(0);
			
			if (array_key_exists('subject', $mimeDecodedEmail->headers) && !empty($mimeDecodedEmail->headers['subject'])) {
				$subject = $mimeDecodedEmail->headers['subject'];
				$lwr_subject = strtolower($subject);
				if(strpos($lwr_subject, 'out of office') || strpos($lwr_subject, 'autoreply') || strpos($lwr_subject, 'automatic reply')) exit(0);
			}
			
			
			$email = new CN_Email($to, $from, $mimeDecodedEmail);
			
			//Check poster to see if a valid person
			$email->ValidatePoster();
			
			if (!empty($email->poster) && $email->PostEmail()) {
				exit(0);//done with no errors, phew
			}
		}
	}
}

function cnea_send_bounce($email){
	return;
}
function cnea_postie_post_before($details){
	
	$details['post_content'] = cnea_filter_RemoveEmptyTags($details['post_content']);
	
	return $details;
}
//add_filter('postie_post_before', 'cnea_postie_post_before', 10, 2);

/**
 * Looks at the content and remove empty tags
 * @param string
 * @param array - a list of patterns to determine if it is a sig block
 */
function cnea_filter_RemoveEmptyTags($content) {
	$html = LoadDOM($content);
	if ($html !== false) {
		
		$found = false;
		$matches = array();
		if (preg_match($pattern, trim($html->plaintext), $matches)) {
			DebugEcho("filter_RemoveSignatureWorker: signature found in base, removing");
			DebugDump($matches);
			$found = true;
			$i = stripos($html->innertext, $matches[1]);
			$presig = substr($html->innertext, 0, $i);
			DebugEcho("filter_RemoveSignatureWorker sig new text: '$presig'");
			$html->innertext = $presig;
		} else {
			//DebugEcho("filter_RemoveSignatureWorker: no matches {preg_last_error()} '$pattern' $html->plaintext");
			//DebugDump($matches);
		}
	
		foreach ($html->children() as $e) {
			//DebugEcho("sig: " . $e->plaintext);
			if (!$found && preg_match($pattern, trim($e->plaintext))) {
				DebugEcho("filter_RemoveSignatureWorker signature found: removing");
				$found = true;
			}
			if ($found) {
				$e->outertext = '';
			} else {
				$found = filter_RemoveSignatureWorker($e, $pattern);
			}
		}
		return $found;
		
		
		
		
		
		
		
		
		
		filter_RemoveSignatureWorker($html->root, $pattern);
		$content = $html->save();
	}
	return $content;
}

class CN_Email{
	var $to = false;
	var $to_slug = false;
	var $from = false;
	var $poster = false; //user id
	var $user = false; //wp user object
	var $post_details = false;
	var $post_id = false;
	var $is_reply = false;
	var $can_reply = false;
	var $rt_post = false; //reply-to post obj
	var $group = false;
	
	var $outofoffice = false;
	var $mimeDecodedEmail;
	var $config;
	
	public function __construct($to, $from, $email){
		$this->mimeDecodedEmail = $email;
		$this->config = config_Read();
		//extract($config);
		if (!array_key_exists('maxemails', $this->config)) {
			$this->config['maxemails'] = 0;
		}
		$this->to = $to;
		$this->from = $from;
	}
	
	/**
	 * This function can be used to send confirmation or rejection emails
	 * It accepts an object containing the entire message
	 */
	function MailToRecipients($mail_content, $testEmail = false, $recipients = array(), $returnToSender = false, $reject = true, $postid = null) {
		DebugEcho("MailToRecipients: send mail");
		if ($testEmail) {
			return false;
		}
	
		$myemailadd = get_option("admin_email");
		$blogname = get_option("blogname");
	
		if (count($recipients) == 0) {
			DebugEcho("MailToRecipients: no recipients");
			return false;
		}
		DebugEcho("MailToRecipients: " . implode(',', $recipients));
		$to = array_pop($recipients);
	
		$from = trim($mail_content->headers["from"]);
		$subject = $mail_content->headers['subject'];
		if ($returnToSender) {
			DebugEcho("MailToRecipients: return to sender $returnToSender");
			array_push($recipients, $from);
		}
	
		$headers = "From: Wordpress <" . $myemailadd . ">\r\n";
		foreach ($recipients as $recipient) {
			$recipient = trim($recipient);
			if (!empty($recipient)) {
				$headers .= "Cc: " . $recipient . "\r\n";
			}
		}
	
		DebugEcho("To: $to");
		DebugEcho($headers);
		
		$posturl = '';
		if ($postid != null) {
			if($reject) wp_delete_post($postid, true);
			else $posturl = get_permalink($postid);
		}
	
		// Set email subject
		if ($reject){
		  return true; //added in to avoid reject messages for autoresponders;
      //@todo create a check so that a rejection email is only sent for bonafide rejections
			if (is_array($mail_content->ctype_parameters) && array_key_exists('boundary', $mail_content->ctype_parameters) && $mail_content->ctype_parameters['boundary']) {
				$boundary = $mail_content->ctype_parameters["boundary"];
			} else {
				$boundary = uniqid("B_");
			}
			//$headers.="Content-Type: multipart/alternative; boundary=$boundary" . "\r\n";
				
			if($reject == 'invald_content') {
				DebugEcho("MailToRecipients: sending reject mail for invalid content");
				$alert_subject = $blogname . " [Reject]: Invalid Post Title and/or Content";
				$message = "An unauthorized message has been sent to ".$blogname.".\n\n";
				$message .= "Sender: ".$from."\n";
				$message .= "Subject: ".$subject."\n";
				$message .= "\n\nError: A message title and message body are required to post to the ".$blogname;
				$message .= "\n\nI know- rejection hurts. You can avoid these sorts of things by using our new announcement form on the website where you'll have more control: " . get_bloginfo( 'url' ) . '/announcements/add/';
				
			} elseif($reject == 'invalid_group'){
				DebugEcho("MailToRecipients: sending reject mail for invalid group");
				$alert_subject = $blogname . " [Reject]: Unrecognized recipient: " . $this->to;
				$message = "An unauthorized message has been sent to ".$blogname.".\n\n";
				$message .= "Sender: ".$from."\n";
				$message .= "Subject: ".$subject."\n";
				$message .= "Error: The email address ". $this->to ." doesn't exist.";
				$message .= "\n\nI know- rejection hurts. You can avoid these sorts of things by using our new announcement form on the website where you'll have more control: " . get_bloginfo( 'url' ) . '/announcements/add/';
			} 
			elseif($reject == 'invalid_poster'){
				DebugEcho("MailToRecipients: sending reject mail for invalid poster");
				$alert_subject = $blogname . " [Reject]: Unauthorized Post Attempt from $from";
				$message = "An unauthorized message has been sent to $blogname.\n\n";
				$message .= "Sender: ".$from."\n";
				$message .= "Subject: ".$subject."\n";
				$message .= "Error: Membership is required to add annoucements.";
				$message .= "\n\nI know- rejection hurts. If you would like to join the " . $blogname . ", you can create an account here: " . get_bloginfo( 'url' );
			}
			elseif($reject == 'invalid_permissions'){
				DebugEcho("MailToRecipients: sending reject mail for invalid permissions");
				$alert_subject = $blogname . " [Reject]: Permissions Error Responding to $subject";
				$message = "An unauthorized message has been sent to $blogname.\n\n";
				$message .= "Sender: ".$from."\n";
				$message .= "Subject: ".$subject."\n";
				$message .= "Error: Either you do not have permission to reply or replies are closed on this post.";
				$message .= "\n\nI know- rejection hurts. There's a lot less risk of these things happening on the actual website: " . get_bloginfo( 'url' );
			}
			else { //default - no clue what happened
				DebugEcho("MailToRecipients: Something went wrong!");
				$alert_subject = $blogname . " [Error]: Message Not Delivered";
				$message = "An unknown error has occured while processing your email.\n";
				$message .= "Sender: ".$from."\n";
				$message .= "Subject: ".$subject;
				$message .= "\n\nWell, this is awkward. Something weird happened. Why don't we skip this whole thing and use our new online form: " . get_bloginfo( 'url' ) . '/announcements/add/';
			}
			
			$message .= "\n\n\nThe original content of the email has been attached.\n\n";
			//$mailtext = "--$boundary\r\n";
			//$mailtext .= "\n";
			
			$mailtext = $message . "\n";
			if ($mail_content->parts) {
				$mailparts = $mail_content->parts;
			} else {
				$mailparts[] = $mail_content;
			}
			foreach ($mailparts as $part) {
				$mailtext .= "--$boundary\r\n";
				if (array_key_exists('content-type', $part->headers)) {
					$mailtext .= "Content-Type: " . $part->headers["content-type"] . "\n";
				}
				if (array_key_exists('content-transfer-encoding', $part->headers)) {
					$mailtext .= "Content-Transfer-Encoding: " . $part->headers["content-transfer-encoding"] . "\n";
				}
				if (array_key_exists('content-disposition', $part->headers)) {
					$mailtext .= "Content-Disposition: " . $part->headers["content-disposition"] . "\n";
				}
				if (array_key_exists('from', $part->headers)) {
					$mailtext .= "From: " . $part->headers["from"] . "\n";
				}
				if (array_key_exists('to', $part->headers)) {
					$mailtext .= "To: " . $part->headers["to"] . "\n";
				}
				$mailtext .= "\n";
				if (property_exists($part, 'body')) {
					$mailtext .= $part->body;
				}
			}
			DebugEcho("Rejection Email Text: ");
			DebugDump($mailtext);
			
		} else { //success
			$group = groups_get_group(array('group_id' => $this->group)); 
			$alert_subject = "Successfully posted to the ".$group->name." group";
			$mailtext = "Woohoo! Your post '".$subject."' has been successfully sent to the ".$group->name." group <".$posturl.">.\n\n";
			$mailtext .= "Feel good? Want to make this experience even better next time? Try posting through our new announcement form on the website where you'll have more control: " . get_bloginfo( 'url' ) . '/announcements/add/';
			DebugEcho("MailToRecipients: $alert_subject\n$mailtext");
		}
		
		DebugEcho("MailToRecipients: To $to");
		DebugDump($headers);
		return wp_mail($to, $alert_subject, $mailtext, $headers);
	}
	
	//returns false || user_id
	public function ValidatePoster() {
		$test_email = null;
		extract($this->config);
		$this->poster = $poster = NULL;
	
		$resentFrom = "";
		if (property_exists($this->mimeDecodedEmail, "headers") && array_key_exists('resent-from', $this->mimeDecodedEmail->headers)) {
			$resentFrom = RemoveExtraCharactersInEmailAddress(trim($this->mimeDecodedEmail->headers["resent-from"]));
		}
		
		//See if the email address is one of the special authorized ones
		if (!empty($this->from)) {
			DebugEcho("Confirming Access For $from ");
			$user = get_user_by('email', $this->from);
			if ($user !== false) {
				$user_ID = $user->ID;
			}
		} else {
			$user_ID = "";
		}
		
		if (!empty($user_ID)) {
			$this->user = new WP_User($user_ID);
			if ($this->user->has_cap("post_via_postie")) {
				DebugEcho("$user_ID has 'post_via_postie' permissions");
				$this->poster = $user_ID;
	
				DebugEcho("ValidatePoster: pre postie_author $poster");
				$this->poster = apply_filters("postie_author", $this->poster);
				DebugEcho("ValidatePoster: post postie_author $poster");
			} else {
				DebugEcho("$user_ID does not have 'post_via_postie' permissions");
				$user_ID = "";
			}
		}
		if (empty($user_ID) && ($turn_authorization_off || isEmailAddressAuthorized($this->from, $authorized_addresses) || isEmailAddressAuthorized($resentFrom, $authorized_addresses))) {
			DebugEcho("ValidatePoster: looking up default user $admin_username");
			$this->user = get_user_by('login', $admin_username);
			if ($this->user === false) {
				//EchoInfo("Your 'Default Poster' setting '$admin_username' is not a valid WordPress user (2)");
				$this->poster = 1;
			} else {
				$this->poster = $this->user->ID;
				DebugEcho("ValidatePoster: pre postie_author (default) $poster");
				$this->poster = apply_filters("postie_author", $this->poster);
				DebugEcho("ValidatePoster: post postie_author (default) $poster");
			}
			DebugEcho("ValidatePoster: found user '$poster'");
		}
	
		if (!$this->poster) { //send error illegal poster message
			EchoInfo('Invalid sender: ' . htmlentities($from) . "! Not adding email!");
			if ($forward_rejected_mail) {
				$admin_email = get_option("admin_email");
				if ($this->MailToRecipients($this->mimeDecodedEmail, $test_email, array($admin_email), $return_to_sender, 'invalid_poster')) {
					EchoInfo("A copy of the message has been forwarded to the administrator.");
				} else {
					EchoInfo("The message was unable to be forwarded to the adminstrator.");
				}
			}
			return false;
		}
		wp_set_current_user($this->poster); //needed by some wp functions who check permissions for the logged in user
	}
	
	/**
	 * This is the main handler for all of the processing
	 */
	public function PostEmail() {
		global $chesnet;
		postie_disable_revisions();
		extract($this->config);
		$success = $test_email = false;
		
		//get the "to" slug
		$parts = explode('@', $this->to);
		DebugEcho("Recipient Address Parts: count($parts)");
		if(count($parts) !== 2){
			EchoInfo('Invalid recipient!');
			if ($forward_rejected_mail) {
				if ($this->MailToRecipients($this->mimeDecodedEmail, $test_email, array($this->from, get_option("admin_email")), false, 'invalid_group'))
					EchoInfo("A copy of the message has been forwarded to the sender.");
				else 
					EchoInfo("The message was unable to be forwarded to the sender.");
			}
			return false;
		}
		$this->to_slug = strtolower(trim($parts[0]));
		if (strpos($this->to_slug, 'reply-to-') !== false){
			$rt_post_id = str_replace('reply-to-', '', $this->to_slug);
			$rt_post = get_post($rt_post_id);
			
			if($rt_post && $rt_post->post_type !== 'topic') { //no love for bbpress yet
				$this->is_reply = true;
				$this->rt_post = $rt_post;
				
				//check if post has comments open
				if($rt_post->comment_status == 'open')
					$this->can_reply = true; //this may change when we get the group
			}
		}
		//action-post_id
		//accept-1234567
		elseif(strpos($this->to_slug, 'accept-') !== false || strpos($this->to_slug, 'reject-') !== false){
			DebugEcho("Accept or reject post");
			$to_slug_pieces = explode('-', $this->to_slug); //array('accept', '1234567');
			$this->post_id = array_pop($to_slug_pieces); // 1234567
			
			$action = $to_slug_pieces[0];
			DebugEcho("Post ID: $this->post_id");
			DebugEcho("Action: $action");

			//verify post id
			$post = get_post($this->post_id);
			$group = cn_get_post_group($post);
			$this->group = $group->id;
			$chesnet->cn_set_group_id = $this->group;
			DebugEcho("Group");
			DebugDump($group);
			
			if($post && !empty($this->group)){
				DebugEcho("Valid post and group");

				//make sure sender is a mod/admin
				$mod_ids = cn_mod_get_group_approvers($this->group);
				if(in_array($this->poster, $mod_ids)){
					//approve or reject
					if ( 'accept' == $action && $post ) {
						// Accept the pending announcement
						$post_args = array(
							'ID' => $this->post_id,
							'post_status' => 'publish'
						);
						
						if ( !wp_update_post( $post_args ) )
							//send error email
							$this->MailToRecipients($this->mimeDecodedEmail, $test_email, array($this->from), false, 'mod_request');
						else 
							do_action( 'after_groups_admin_announcement_moderation_approved', $this->group, $this->post_id );
						
						return;
			
					} elseif ( 'reject' == $action && $post ) {	
						// Reject the request
						$cat = cn_get_cat_from_group($this->group);
						
						if ( !wp_remove_object_terms( $this->post_id, (int)$cat->term_id, 'category' ) )
							//send error email
							$this->MailToRecipients($this->mimeDecodedEmail, $test_email, array($this->from), false, 'mod_request');
						else 
							do_action( 'after_groups_admin_announcement_moderation_rejected', $this->group, $this->post_id );
						
						return;
					}
				}
				
				return false;
			}
		}
		//else, just a regular post
			
		/* in order to do attachments correctly, we need to associate the
		  attachments with a post. So we add the post here, then update it */
		$tmpPost = array('post_title' => 'tmptitle', 'post_content' => 'tmpPost', 'post_status' => 'draft');
		$this->post_id = wp_insert_post($tmpPost);
		DebugEcho("tmp post id is $this->post_id");
		
		$this->CreatePost();
		
		if($this->outofoffice === true){
			wp_delete_post($this->post_id, true);
			return false;
		}

		if(empty($this->post_details['post_content']) || empty($this->post_details['post_name'])){
			EchoInfo('Invalid post title and/or content!');
			if ($forward_rejected_mail) {
				if ($this->MailToRecipients($this->mimeDecodedEmail, $test_email, array($this->from, get_option("admin_email")), false, 'invalid_content', $this->post_id))
					EchoInfo("A copy of the message has been forwarded to the sender.");
				else 
					EchoInfo("The message was unable to be forwarded to the sender.");
			}
			return false;
		}
		
		$this->setGroup();
		if(empty($this->group)){
			EchoInfo('Invalid post title and/or content!');
			if ($forward_rejected_mail) {
				if ($this->MailToRecipients($this->mimeDecodedEmail, $test_email, array($this->from, get_option("admin_email")), false, 'invalid_group', $this->post_id))
					EchoInfo("A copy of the message has been forwarded to the sender.");
				else 
					EchoInfo("The message was unable to be forwarded to the sender.");
			}
			return false;
		}
		
		if($this->is_reply && !$this->can_reply){
			EchoInfo('User unable to reply to existing post!');
			if ($forward_rejected_mail) {
				if ($this->MailToRecipients($this->mimeDecodedEmail, $test_email, array($this->from, get_option("admin_email")), false, 'invalid_permissions'))
					EchoInfo("A copy of the message has been forwarded to the sender.");
				else 
					EchoInfo("The message was unable to be forwarded to the sender.");
			}
			return false;
		}
		
		$this->post_details['post_status'] = 'pending'; //need to do this to allow meta to be added before activity is created
		$post_status = groups_get_groupmeta($this->group, 'cn_moderate_new_announcements') == 'yes' ? 'pending' : 'publish'; //@todo move to class.group-moderation plugin
	
		$this->post_details = apply_filters('postie_post', $this->post_details);
		$this->post_details = apply_filters('postie_post_before', $this->post_details);
	
		DebugEcho(("Post postie_post filter"));
		DebugDump($this->post_details);
	
	
		if (empty($this->post_details)) {
			//@todo send notification email
			
			// It is possible that the filter has removed the post, in which case, it should not be posted.
			// And if we created a placeholder post (because this was not a reply to an existing post),
			// then it should be removed
			if (!$this->is_reply) {
				wp_delete_post($this->post_id, true);
				EchoInfo("postie_post filter cleared the post, not saving.");
				if ($forward_rejected_mail) {
					if ($this->MailToRecipients($this->mimeDecodedEmail, $test_email, array($this->from, get_option("admin_email")), false, 'invalid_content', $this->post_id))
						EchoInfo("A copy of the message has been forwarded to the sender.");
					else 
						EchoInfo("The message was unable to be forwarded to the sender.");
				}
				return false;
				
			}
		} else {
			DisplayEmailPost($this->post_details);
	
			$post_id = PostToDB($this->post_details, $this->is_reply, $custom_image_field, new CN_PostiePostModifiers());
			if($post_id) {
				$success = true;
				$this->post_id = $post_id;
				add_post_meta($this->post_id, '_cn_post_sharing', 'yes', true);
				if($post_status == 'publish') wp_update_post( array('ID' => $post_id, 'post_status' => $post_status) );
			}
			else {
				EchoInfo("Unkown error while saving post.");
				if ($forward_rejected_mail) {
					if ($this->MailToRecipients($this->mimeDecodedEmail, $test_email, array($this->from, get_option("admin_email")), false, true, $this->post_id))
						EchoInfo("A copy of the message has been forwarded to the sender.");
					else 
						EchoInfo("The message was unable to be forwarded to the sender.");
				}
				return false;
			}
			
			if ($confirmation_email != '') {
				if ($confirmation_email == 'sender') {
					$recipients = array($this->post_details['email_author']);
				} elseif ($confirmation_email == 'admin') {
					$recipients = array(get_option("admin_email"));
				} elseif ($confirmation_email == 'both') {
					$recipients = array($this->post_details['email_author'], get_option("admin_email"));
				}
				$this->MailToRecipients($this->mimeDecodedEmail, $test_email, $recipients, false, false, $this->post_id);
				
			}
		}
		postie_disable_revisions(true);
		DebugEcho("Done");
		
		return $success;
	}
	
	//set up email details
	public function CreatePost() {
		$fulldebug = IsDebugMode();
		$fulldebugdump = false;
	
		extract($this->config);
	
		$attachments = array(
			"html" => array(), //holds the html for each image
			"cids" => array(), //holds the cids for HTML email
			"image_files" => array() //holds the files for each image
		);
	
		if (array_key_exists('message-id', $this->mimeDecodedEmail->headers)) {
			DebugEcho("Message Id is :" . htmlentities($this->mimeDecodedEmail->headers["message-id"]));
			if ($fulldebugdump) {
				DebugDump($this->mimeDecodedEmail);
			}
		}
	
		filter_PreferedText($this->mimeDecodedEmail, $this->config['prefer_text_type']);
		if ($fulldebugdump) {
			DebugDump($this->mimeDecodedEmail);
		}
	
		$content = GetContent($this->mimeDecodedEmail, $attachments, $this->post_id, $this->poster, $this->config);
		if ($fulldebug) {
			DebugEcho("CreatePost: '$content'");
			DebugDump($attachments);
		}
	
		$subject = GetSubject($this->mimeDecodedEmail, $content, $this->config);
		$lwr_subject = strtolower($subject);
		if(strpos($lwr_subject, 'out of office') || strpos($lwr_subject, 'autoreply') || strpos($lwr_subject, 'automatic reply')) 
			$this->outofoffice = true;
	
		filter_RemoveSignature($content, $this->config);
		if ($fulldebug) {
			DebugEcho("post sig: $content");
		}
	
		$post_excerpt = tag_Excerpt($content, $this->config);
		if ($fulldebug) {
			DebugEcho("post excerpt: $content");
		}
	
		$postAuthorDetails = getPostAuthorDetails($subject, $content, $this->mimeDecodedEmail);
		if ($fulldebug) {
			DebugEcho("post author: $content");
		}
	
		$message_date = NULL;
		if (array_key_exists("date", $this->mimeDecodedEmail->headers) && !empty($this->mimeDecodedEmail->headers["date"])) {
			$cte = "";
			$cs = "";
			if (property_exists($this->mimeDecodedEmail, 'content-transfer-encoding') && array_key_exists('content-transfer-encoding', $this->mimeDecodedEmail->headers)) {
				$cte = $this->mimeDecodedEmail->headers["content-transfer-encoding"];
			}
			if (property_exists($this->mimeDecodedEmail, 'ctype_parameters') && array_key_exists('charset', $this->mimeDecodedEmail->ctype_parameters)) {
				$cs = $this->mimeDecodedEmail->ctype_parameters["charset"];
			}
			$message_date = HandleMessageEncoding($cte, $cs, $this->mimeDecodedEmail->headers["date"], $message_encoding, $message_dequote);
		}
		//$message_date = tag_Date($content, $message_date);
	
		list($post_date, $post_date_gmt, $delay) = filter_Delay($content, $message_date, $this->config['time_offset']);
		if ($fulldebug) {
			DebugEcho("post date: $content");
		}
	
		filter_Ubb2HTML($content);
		if ($fulldebug) {
			DebugEcho("post ubb: $content");
		}
	
		//do post type before category to keep the subject line correct
		//$post_type = tag_PostType($subject, $postmodifiers, $config);
		if ($fulldebug) {
			DebugEcho("post type: $content");
		}
		
		/*$post_categories = tag_Categories($subject, $config['default_post_category'], $config['category_match'], $post_id);
		if ($fulldebug) {
			DebugEcho("post category: $content");
		}*/
	
		$post_tags = tag_Tags($content, $config['default_post_tags']);
		if ($fulldebug) {
			DebugEcho("post tag: $content");
		}
	
		/*$comment_status = tag_AllowCommentsOnPost($content);
		if ($fulldebug) {
			DebugEcho("post comment: $content");
		}*/
	
		/*$post_status = tag_Status($content, $post_status);
		if ($fulldebug) {
			DebugEcho("post status: $content");
		}*/
	
		if ($this->config['converturls']) {
			$content = filter_Videos($content, $this->config['shortcode']); //videos first so linkify doesn't mess with them
			if ($fulldebug) {
				DebugEcho("post video: $content");
			}
	
			$content = filter_Linkify($content);
			if ($fulldebug) {
				DebugEcho("post linkify: $content");
			}
		}
	
		filter_VodafoneHandler($content, $attachments);
		if ($fulldebug) {
			DebugEcho("post vodafone: $content");
		}
	
		filter_ReplaceImageCIDs($content, $attachments, $this->config);
		if ($fulldebug) {
			DebugEcho("post cid: $content");
		}
	
		$customImages = tag_CustomImageField($content, $attachments, $this->config);
		if ($fulldebug) {
			DebugEcho("post custom: $content");
		}
	
		
		if (!$this->is_reply) {
			DebugEcho("Not a reply");
			$id = $this->post_id;
			if ($this->config['add_meta'] == 'yes') {
				DebugEcho("Adding meta");
				if ($this->config['wrap_pre'] == 'yes') {
					DebugEcho("Adding <pre>");
					$content = $postAuthorDetails['content'] . "<pre>\n" . $content . "</pre>\n";
					$content = "<pre>\n" . $content . "</pre>\n";
				} else {
					$content = $postAuthorDetails['content'] . $content;
					$content = $content;
				}
			} else {
				if ($this->config['wrap_pre'] == 'yes') {
					DebugEcho("Adding <pre>");
					$content = "<pre>\n" . $content . "</pre>\n";
				}
			}
		} else {
			DebugEcho("Reply detected");
			// strip out quoted content
			$lines = explode("\n", $content);
			$newContents = '';
			foreach ($lines as $line) {
				if (preg_match("/^>.*/i", $line) == 0 &&
						preg_match("/^(from|subject|to|date):.*?/i", $line) == 0 &&
						preg_match("/^-+.*?(from|subject|to|date).*?/i", $line) == 0 &&
						preg_match("/^on.*?wrote:$/i", $line) == 0 &&
						preg_match("/^-+\s*forwarded\s*message\s*-+/i", $line) == 0) {
					$newContents.="$line\n";
				}
			}
			$content = $newContents;
			wp_delete_post($this->post_id, true);
			$this->post_id = $id = $this->rt_post->ID;
		}
	
		if ($delay != 0 && $post_status == 'publish') {
			$post_status = 'future';
		}
	
		filter_Newlines($content, $this->config);
		if ($fulldebug) {
			DebugEcho("post newline: $content");
		}
	
		filter_Start($content, $this->config);
		if ($fulldebug) {
			DebugEcho("post start: $content");
		}
	
		filter_End($content, $this->config);
		if ($fulldebug) {
			DebugEcho("post end: $content");
		}
	
		//if ($config['prefer_text_type'] == 'plain') {
		filter_ReplaceImagePlaceHolders($content, $attachments["html"], $this->config, $id, $this->config['image_placeholder'], true);
		if ($fulldebug) {
			DebugEcho("post body ReplaceImagePlaceHolders: $content");
		}
	
		if ($post_excerpt) {
			filter_ReplaceImagePlaceHolders($post_excerpt, $attachments["html"], $this->config, $id, "#eimg%#", false);
			DebugEcho("excerpt: $post_excerpt");
			if ($fulldebug) {
				DebugEcho("post excerpt ReplaceImagePlaceHolders: $content");
			}
		}
		//}
	
		/* if no subject or content, we'll bail
		if (trim($subject) == "") {
			$subject = $config['default_title'];
			DebugEcho("post parsing subject is blank using: " . $config['default_title']);
		}
		*/
	
		$this->post_details = array(
			'post_author' => $this->poster,
			'comment_author' => $postAuthorDetails['author'],
			'comment_author_url' => $postAuthorDetails['comment_author_url'],
			'user_ID' => $postAuthorDetails['user_ID'],
			'email_author' => $postAuthorDetails['email'],
			'post_date' => $post_date,
			'post_date_gmt' => $post_date_gmt,
			'post_content' => $content,
			'post_title' => $subject,
			'post_type' => $post_type, /* Added by Raam Dev <raam@raamdev.com> */
			'ping_status' => get_option('default_ping_status'),
			'tags_input' => $post_tags,
			'comment_status' => get_option('default_comment_status'),
			'post_name' => sanitize_title($subject),
			'post_excerpt' => $post_excerpt,
			'ID' => $id,
			'customImages' => $customImages,
		);
		DebugEcho("post_details: ");
		DebugDump($this->post_details);
	}
	
	public function setGroup(){
		global $chesnet;
		if($this->is_reply === true){ //if it's a reply, we get the group differently
			//get the grou pfrom the post id
			$group = cn_get_post_group($this->post_id);
			if($group){
				$this->group = $group->id;
				$chesnet->cn_set_group_id = $this->group;
				//check if poster is a member
				$this->can_reply = $this->can_reply ? bp_group_is_member($group) : false;
			}
		}
		else {
			$slug = $this->to_slug;
			DebugEcho("recipient slug: " . $this->to_slug);
			//@todo fix this some home so it's not hardcoded!
			$state_slugs = array(
				strtolower('Maryland')		=> 'Maryland',
				strtolower('Pennsylvania')	=> 'Pennsylvania', 
				strtolower('Virginia')		=> 'Virginia',
				strtolower('WestVirginia')	=> 'West Virginia',
				strtolower('NewYork')		=> 'New York',
				strtolower('NewYork1')		=> 'New York',
				strtolower('Delaware')		=> 'Delaware',
				strtolower('DELAWARE1')		=> 'Delaware',
				strtolower('DistrictofColumbia')	=> 'DC');
			
			//first check if the slug is a state group
			if(isset($state_slugs[$slug])){
				DebugEcho("Recipient is a state: " . $state_slugs[$slug]);
				$this->group = cn_get_group_from_cat((int)$this->config['default_post_category']);
				$chesnet->cn_set_group_id = $this->group;
				$this->post_details['post_category'] = array((int)$this->config['default_post_category']);
				$this->post_details['tags_input'][] = $state_slugs[$slug];
			}
			else { //otherwise, lookup the slug to get the group
				DebugEcho("Recipient is not a state.");
				$args = array(
					'user_id'			=> $this->poster,
					'meta_query'		=> array(
											array(
												'key'	=> 'group_email_address',
												'value'	=> $this->to
											)
										   ),
					'populate_extras'	=> false,
					'show_hidden'       => true
				);
				$groups = BP_Groups_Group::get($args);
				DebugEcho("BP_Groups_Group::get ");
				DebugDump($groups);
				if($groups['total'] > 0){
					$this->group = $groups['groups'][0]->id; //just get the first one
					$chesnet->cn_set_group_id = $this->group;
					$cat = cn_get_cat_from_group($this->group);
					$this->post_details['post_category'] = array((int)$cat->term_id);
				}
			}
			DebugEcho("Group ID: " . $this->group);
			DebugEcho("Category ID: ");
			DebugDump($this->post_details['post_category']);
		}// end if this is a reply
	}
	
	public function GetReplyTo($to){
		
	}
	
}

class CN_PostiePostModifiers {

    function apply($postid) {
		return;
    }

}

?>