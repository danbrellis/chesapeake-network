<?php

/**
 * No Topics Feedback Part
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<div data-alert class="alert-box warning">
            <h4>Hey... where's the rest?</h4>
            <p style="margin-bottom:0">As we transfer data from the old platform to the new one, we're taking baby steps. If it seems like something is missing, we'll likely be added it over the next few weeks. If you need something in particular, feel free to ask.</p>
        </div>
