<?php

// Don't load directly
if ( !defined( 'ABSPATH' ) ) {
	die('-1');
}


/**
 * The Submit and Edit job form
 */
?>
	<div class="row">
    	<div class="large-14 columns post-title">
			<label for="post_title"><?php _e('Position Title', 'chesnet'); ?> <small class="req"><?php _e('(required)', 'chesnet'); ?></small>
			<input type="text" tabindex="0" id="post_title" name="post_title" value="<?php echo isset($_SESSION['post_title']) ? esc_attr($_SESSION['post_title']) : esc_attr($post->post_title); ?>" /></label>
        </div>
	</div>
    
	<div class="row">
    	<div class="large-14 columns cn-post-content">
			<label for="cnpostcontent"><?php _e('Job Description', 'chesnet'); ?> <small class="req"><?php _e('(required)', 'chesnet'); ?></small>
			<?php $this->cn_formContentEditor( isset($_SESSION['post_content']) ? $_SESSION['post_content'] : $post->post_content, true ); ?></label>
        </div>
	</div>

    <div class="row">
    	<div class="large-7 columns">
    		<label for="cn_jobs_org"><?php _e('Enter the hiring organization.', 'chesnet'); ?>
    		<input type="text" id="cn_jobs_org" name="cn_jobs_org" value="<?php echo isset($_SESSION['cn_jobs_org']) ? esc_attr($_SESSION['cn_jobs_org']) : esc_attr($cn_jobs_org); ?>" /></label>
        </div>
        <div class="large-7 columns">
        	<label for="cn_jobs_orgweb"><?php _e("Enter the hiring organization's website.", 'chesnet'); ?>
            	<input type="text" id="cn_jobs_orgweb" name="cn_jobs_orgweb" value="<?php echo isset($_SESSION['cn_jobs_orgweb']) ? esc_attr($_SESSION['cn_jobs_orgweb']) : esc_attr($cn_jobs_orgweb); ?>" aria-describedby="jobs_org_web_help" />
                <span id="jobs_org_web_help"><?php _e('(must begin with http:// or https://)', 'chesnet'); ?></span>
            </label>
        </div>
    </div>
    <div class="panel">    
    <?php
	$uploadtxt = __('Upload', 'chesnet');
	$jobs_announcement_file = isset($_SESSION['uploads']['cn_jobs_announcement_file']) ? $_SESSION['uploads']['cn_jobs_announcement_file'] : $cn_jobs_announcement_file['file'];
	
	if($jobs_announcement_file && $jobs_announcement_file !== 'none'): $uploadtxt = __('Change', 'chesnet'); ?>
        <div class="row">
        	<div class="large-14 columns">
        		<label class="left inline"><strong><?php echo basename($jobs_announcement_file); ?></strong> <?php _e('is currently attached.', 'chesnet'); ?></label> <span href="#" id="delete__<?php echo $post->ID; ?>__cn_jobs_announcement_file" class="cn_delete_file delete button tiny alert right hide-if-no-js" title="<?php _e('Delete file', 'chesnet'); ?>"><i class="fi-x imgWrapLeft"></i><?php _e('Delete', 'chesnet'); ?></span>
                <?php wp_nonce_field('cn_delete_file_nonce', 'cn_delete_file_nonce'); ?>
                
                <div class="hide-if-js">
                	<input type="checkbox" name="cn_delete_file" value="1" id="cn_delete_file" /><label for="cn_delete_file"><?php _e('No attachment file.', 'chesnet'); ?></label>
                </div>
			</div>
        </div>
    <?php else: $jobs_announcement_file = '';
	endif; ?>
    
    
        <div class="row">
            <div class="large-14 columns">
                <label for="cn_jobs_announcement_file"><?php _e('Provide an attachment with the job announcement.', 'chesnet'); ?></label>
                <div class="row collapse">
                    <div class="small-11 columns">
                        <input class="hide-if-no-js" type="text" placeholder="<?php echo $jobs_announcement_file ? basename($jobs_announcement_file) : __('Choose File', 'chesnet'); ?>" disabled="disabled" />
                    </div>
                    <div class="small-3 columns left">
                        <div class="fileUpload button info postfix">
                            <?php echo $uploadtxt; ?>
                            <input type="file" id="cn_jobs_announcement_file" name="cn_jobs_announcement_file" value="" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="large-14 columns">
                <label><?php _e('Or, enter the website for the position announcement.', 'chesnet'); ?>
                    <input type="text" id="cn_jobs_announcement_website" name="cn_jobs_announcement_website" value="<?php echo isset($_SESSION['cn_jobs_announcement_website']) ? esc_attr($_SESSION['cn_jobs_announcement_website']) : esc_attr($cn_jobs_announcement_website); ?>" aria-describedby="job_announcement_web_help" />
                    <span id="job_announcement_web_help"><?php _e('(must begin with http:// or https://)', 'chesnet'); ?></span>
                </label>
            </div>
        </div>
    </div>
        
    <div class="row">
    	<div class="large-14 columns">
    		<label for="cn_jobs_apply_by_date"><?php _e('Enter the deadline for receiving applications.', 'chesnet'); ?>
            	<input type="text" id="cn_jobs_apply_by_date" name="cn_jobs_apply_by_date" value="<?php echo isset($_SESSION['cn_jobs_apply_by_date']) ? esc_attr($_SESSION['cn_jobs_apply_by_date']) : $cn_jobs_apply_by_date; ?>" aria-describedby="job_apply_by_format" class="datepickerclass" />
                <span id="job_apply_by_format"><?php _e('(YYYY-MM-DD)', 'chesnet'); ?></span>
            </label>
        </div>
    </div>