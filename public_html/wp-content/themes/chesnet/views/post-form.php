<?php

// Don't load directly
if ( !defined( 'ABSPATH' ) ) {
	die('-1');
}


/**
 * The Submit and Edit job form
 */
?>
	<div class="row">
    	<div class="large-14 columns post-title">
            <label for="post_title"><?php _e('Message Title', 'chesnet'); ?> <small class="req"><?php _e('(required)', 'chesnet'); ?></small>
            <input type="text" tabindex="0" id="post_title" name="post_title" value="<?php echo isset($_SESSION['post_title']) ? esc_attr($_SESSION['post_title']) : esc_attr($post->post_title); ?>" /></label>
        </div>
	</div>
    
	<div class="row">
    	<div class="large-14 columns cn-post-content">
            <label for="cnpostcontent"><?php _e('Message Description', 'chesnet'); ?> <small class="req"><?php _e('(required)', 'chesnet'); ?></small>
            <?php $this->cn_formContentEditor( isset($_SESSION['post_content']) ? $_SESSION['post_content'] : $post->post_content, true ); ?></label>
        </div>
	</div>
    
    <!-- @todo add an upload field for attachments maybe -->