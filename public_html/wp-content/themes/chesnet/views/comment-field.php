<?php global $chesnet, $bp; ?>
    	<div class="left">
            <a href="<?php echo $bp->loggedin_user->domain; ?>" title="<?php echo $bp->loggedin_user->fullname; ?>">
                <?php echo bp_core_fetch_avatar ( array( 'item_id' => $bp->loggedin_user->id, 'type' => 'thumb', 'class' => 'imgWrapLeft', 'width' => 32, 'height' => 32 ) ); ?>
            </a>
        </div>
		<div class="hide-over">
			<?php echo $chesnet->cn_formContentEditor( isset($_POST['comment']) ? $_POST['comment'] : '', false, 'comment' ); ?>
		</div>