<?php

/*
 * Enter post title, description, and meta data
 */

// Don't load directly
if ( !defined( 'ABSPATH' ) ) {
	die('-1');
}

//capture the class
//now we need to differentiate between post_types
echo '<h3>' . __('2. Compose your message', 'chesnet') . '</h3>';
$this->get_post_form_template( $post, $_SESSION['post_type'], 'views' );

do_action( 'cn_after_step2_form' );

?>