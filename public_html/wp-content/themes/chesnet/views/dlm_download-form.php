<?php

// Don't load directly
if ( !defined( 'ABSPATH' ) ) {
	die('-1');
}


/**
 * The Submit and Edit download form
 */
 
//get all dlm versions
if(isset($post->ID)) $versions = get_posts( array(
	'post_parent'		=> $post->ID,
	'post_type'			=> 'dlm_download_version',
	'orderby'			=> 'menu_order post_modified',
	'order'				=> 'ASC',
	'post_status'		=> 'any',
	'posts_per_page'	=> -1) );
?>

    <?php //echo $this->cn_get_tagit_form($post->ID, $this->get_dlm_tax($this->cn_set_group_id), $_SESSION['newtag'][$this->get_dlm_tax($this->cn_set_group_id)] ? esc_attr($_SESSION['newtag'][$this->get_dlm_tax($this->cn_set_group_id)]) : '' ); ?>
    
	<div class="row">
    	<div class="large-14 columns post-title">
			<label for="post_title"><?php _e('Resource Title', 'chesnet'); ?> <small class="req"><?php _e('(required)', 'chesnet'); ?></small><input type="text" tabindex="0" id="post_title" name="post_title" value="<?php echo isset($_SESSION['post_title']) ? esc_attr($_SESSION['post_title']) : esc_attr($post->post_title); ?>" /></label>
        </div>
	</div>
    
	<div class="row">
    	<div class="large-14 columns cn-post-content">
			<label for="cnpostcontent"><?php _e('Resource Description', 'chesnet'); ?> <small class="req"><?php _e('(required)', 'chesnet'); ?></small>
			<?php $this->cn_formContentEditor( isset($_SESSION['post_content']) ? $_SESSION['post_content'] : $post->post_content, false ); ?></label>
        </div>
	</div>
    
	<?php
    //@todo make version icon the dashicon 'backup' icon to be consistent with how resource meta data are viewed in single-page view
    echo '<h3><i class="fi-page-add imgWrapLeft"></i>'.__('Add Version', 'chesnet').'</h3>';
    $this->get_post_form_template( null, 'dlm_download_version', 'views' );
	 
    if(isset($versions)): ?>
    	<h3><i class="fi-page imgWrapLeft"></i><?php _e('Previous Versions', 'chesnet'); ?></h3>
        <?php if(!is_super_admin() && !current_user_can( 'administrator' )): ?>
        <div class="panel">
            <div class="row">
                <div class="small-4 medium-2 columns"><span data-tooltip aria-haspopup="true" class="has-tip" title="<?php _e('The version with the <strong>lowest</strong> priority will be downloaded by default.', 'chesnet'); ?>"><strong><?php _e('Priority', 'chesnet'); ?></strong></span></div>
                <div class="small-4 medium-2 columns"><strong><?php _e('Version', 'chesnet'); ?></strong></div>
                <div class="small-6 medium-10 columns"><strong><?php _e('File', 'chesnet'); ?></strong></div>
            </div>
            
            <?php //each existing one
            foreach($versions as $child) $this->get_post_form_template( $child, 'dlm_download_version', 'views' ); ?>
		</div>
        <?php else: foreach($versions as $child) $this->get_post_form_template( $child, 'dlm_download_version', 'views' ); ?>
        <?php endif; ?>
    <?php endif; ?>