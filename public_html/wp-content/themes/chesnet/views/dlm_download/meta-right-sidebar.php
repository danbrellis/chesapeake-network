<?php
/**
 * Template file for dlm_download post_type meta information on right sidebar
 */
global $dlm_download, $post, $chesnet;

//var_dump($dlm_download);
$versions = $dlm_download->get_file_versions();
$previous_versions = '';
$group = $chesnet->cn_the_set_group;
 
 
?>

<div class="sidebar-module sidebar-module-inset panel">
    <h4><?php printf('%s %s', cn_get_activity_type(get_post_type(), 'plural_name'), __('Details', 'chesnet')); ?><br /><small><?php  printf(__('in <span class="groups">%s</span> group', 'chesnet'), $group->name); ?></small></h4>
    <hr />
    <h5><a href="<?php $dlm_download->the_download_link(); ?>" rel="nofollow" target="_blank"><i class="fi-download imgWrapLeft"></i><?php _e( 'Download', 'chesnet' ); ?></a></h5>
    
    <?php 
	// Get previous versions
	if ( sizeof( $versions ) > 1 ) {
		array_shift( $versions );

		$previous_versions = ' <a href="#" data-dropdown="previous-versions" aria-controls="previous-versions" aria-expanded="false" class="dropdown imgWrapRight" title="' . __( 'Previous versions', 'chesnet' ) . '"><span class="dashicons dashicons-backup"></span> '.__('Versions', 'chesnet') .'</a><ul id="previous-versions" data-dropdown-content class="f-dropdown" aria-hidden="true" tabindex="-1">';

		foreach ( $versions as $version ) {
			$dlm_download->set_version( $version->id );
			$version_post = get_post( $version->id );

			$previous_versions .= '<li><a href="' . $dlm_download->get_the_download_link() . '" target="_blank">' . sprintf( __( 'Version %s', 'chesnet' ), $dlm_download->get_the_version_number() ) . ' (' . date_i18n( get_option( 'date_format' ), strtotime( $version_post->post_date ) ) . ')</a></li>';
		}

		$dlm_download->set_version();

		$previous_versions .= '</ul>';
	}
	
	//Labels
	$label_list = $chesnet->get_dlm_label_list($post->ID, $chesnet->cn_set_group_id);
	
	//Download Meta
	$download_meta = array(
		'filename' => array(
			'name'     => __( 'File Name', 'chesnet' ),
			'value'    => $dlm_download->get_the_filename(),
			'title'	   => $dlm_download->get_the_filename()
		),
		'version' => array(
			'name'     => __( 'Version', 'chesnet' ),
			'value'    => $dlm_download->get_the_version_number() . $previous_versions
		),
		'filetype' => array(
			'name'     => __( 'Type', 'chesnet' ),
			'value'    => '<span class="filetype">' . $dlm_download->get_the_filetype() . '</span>'
		),
		'filesize' => array(
			'name'     => __( 'Size', 'chesnet' ),
			'value'    => $dlm_download->get_the_filesize()
		),
		'date' => array(
			'name'     => __( 'Date added', 'chesnet' ),
			'value'    => date_i18n( get_option( 'date_format' ), strtotime( $dlm_download->post->post_date ) )
		),
		'downloaded' => array(
			'name'     => __( 'Downloaded', 'chesnet' ),
			'value'    => sprintf( _n( '1 time', '%d times', $dlm_download->get_the_download_count(), 'chesnet' ), $dlm_download->get_the_download_count() )
		),
		'labels' => array(
			'name'     => __( 'Labels', 'chesnet' ),
			'value'    => $label_list
		),
	);

	$download_meta = apply_filters( 'dlm_page_addon_download_meta', $download_meta );
	echo '<div class="p-flex-table meta-p-flex-table">';
	foreach ( $download_meta as $meta ) :
		if ( empty( $meta['value'] ) )
			continue;
		?>
		<div class="ft-tr">
			<div class="ft-td-a"><?php echo $meta['name']; ?></div>
			<div class="ft-td-b"<?php echo isset($meta['title']) ? ' title="'.$meta['title'].'"' : ''; ?>><?php echo $meta['value']; ?></div>
		</div>
	<?php endforeach;
	echo '</div>';
	?>
    
    
    
</div>