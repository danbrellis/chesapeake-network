<?php

//message content for email to group admin/mods notifying them that a new message is awaiting approval
//message is sent in plain text (no html)

?>

<p><?php printf(__("A new %s has been added to the group '%s' and is awaiting approval. A portion of the message is below for your review.", 'chesnet'), cn_get_activity_type($p->post_type, 'singular_name'), $group->name); ?></p>
<p><?php printf(__("See all pending announcements for the group: %s", 'chesnet'), bp_get_group_admin_permalink( $group ) . $this->slug ); ?>
<p><?php _e('Approve', 'chesnet'); ?><br />
<?php _e('Reply to this email.', 'chesnet'); ?><br />
<?php printf(__('Or go to: %s', 'chesnet'), cn_get_group_pending_announcement_approve_link($p->ID)); ?><br />
</p>
<p><?php _e('Reject', 'chesnet'); ?><br />
<?php printf(__('Send a blank email to: %s', 'chesnet'), $reject); ?><br />
<?php printf(__('Or go to: %s', 'chesnet'), cn_get_group_pending_announcement_reject_link($p->ID)); ?><br />
</p>

<?php _ex( '------------------------------------------------------', 'chesnet', 'email-horizontal-rule', 'chesnet' ); ?>

<p><?php printf(__("Written by: %s (%s)", 'chesnet'),  get_the_author_meta( 'display_name', $p->post_author), bp_core_get_user_domain($p->post_author)); ?></p>
<p><?php printf(__("Title: %s", 'chesnet'),  get_the_title( $p->ID )); ?></p>

<p><?php echo $p->post_content; ?></p>