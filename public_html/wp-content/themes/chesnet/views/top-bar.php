<?php if ( is_front_page() ): ?>
    <div class="top-bar site-banner show-for-medium-up" title="<?php _e('Photo courtesy of Chesapeake Bay Program.', 'chesnet'); ?>"<?php $ran = rand(1, 97); if($ran % 3 == 0): ?> style="background-position:center <?php echo $ran; ?>%"<?php endif; ?> id="cn-site-banner"><?php _e('Photo courtesy of Chesapeake Bay Program.', 'chesnet'); ?></div>
<?php endif; ?>
<div class="top-bar-container show-for-medium-up contain-to-grid" role="navigation">    
    <nav class="top-bar" data-topbar role="navigation">
        <ul class="title-area">
        	<li class="site-name">
            	<h1><a href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>"><span class="screen-reader-text"><?php bloginfo('name'); ?></span><img src="<?php echo esc_url( get_theme_mod( 'chesnet_logo' ) ); ?>" alt="<?php bloginfo('name'); ?>" /><div class="logo-circle-bg"></div></a></h1>
            </li>
             <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
            <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
		</ul>
          
          
          <h1 class="hidden-for-small-up"><a href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a></h1>
                <section class="top-bar-section">
            <?php cn_top_bar_l(); ?>
            <?php cn_top_bar_r(); ?>
        </section>
    </nav>
</div>

