<?php

/*
 * The select post type fields
 */

// Don't load directly
if ( !defined( 'ABSPATH' ) ) {
	die('-1');
}

//capture the class
global $chesnet;

//the first step in adding a post is to select the post type
//So only one is selected, we'll use radio buttons
//This will let jquery make them fancy looking later

//get all post types that a user can add
$pts = get_post_types(array('user_can_add' => true), 'objects');
if($pts && is_array($pts)){
	echo '<h3 class="subheader">1. ' . __('Select the message type', 'chesnet') . '</h3>';
	echo '<ul class="small-block-grid-2 medium-block-grid-4 large-block-grid-3 ">';
	foreach($pts as $pt): ?>
    	<li>
        	<div class="btn-switch <?php echo $pt->name; ?> large">
                <input type="radio" name="pt" id="<?php echo $pt->name; ?>" value="<?php echo $pt->name; ?>" <?php checked($_SESSION['pt'], $pt->name); ?> /><label for="<?php echo $pt->name; ?>"><?php echo $pt->labels->singular_name; ?></label>
            </div>
        </li>


    <?php endforeach;
	echo '</ul>';
}


?>