<?php

// Don't load directly
if ( !defined( 'ABSPATH' ) ) {
	die('-1' );
}

/**
 * The Download Versions Box
 */

?>

<?php //if( WP_DEBUG ) echo '<small>[DEBUG] Path to this template: '.__FILE__.'</small>'; ?>

<?php
if ( is_string( $files ) ) {
	$files = array_filter( (array) json_decode( $files ) );
} elseif ( is_array( $files ) ) {
	$files = array_filter( $files );
} else {
	$files = array();
}

$uploadtxt = __('Upload', 'chesnet');
$version_ID = (isset($post) && isset($post->ID)) ? $post->ID : 'new_dlm_download_version';
if(isset($_SESSION['uploads']) && isset($_SESSION['uploads'][$version_ID . '-dlm_new_upload'])) $version_file = $_SESSION['uploads'][$version_ID . '-dlm_new_upload'];
//if(is_super_admin()) var_dump($version_ID);
//@todo clean up this whole thing (and the upload form for job announcements), especially how it works with and without javascript

?>

<?php if($version_ID == 'new_dlm_download_version' || current_user_can( 'administrator' ) || is_super_admin()): ?>
    <div id="dlmDetails-<?php echo $version_ID; ?>" class="inside dlmForm panel">
        <?php if(isset($version_file) && $version_file !== 'none'): $uploadtxt = __('Change', 'chesnet'); ?>
            <div class="row">
                <div class="large-14 columns">
                    <label class="left inline"><strong><?php echo basename($version_file); ?></strong> <?php _e('is currently attached.', 'chesnet'); ?></label> <span href="#" id="delete__<?php echo $version_ID; ?>__<?php echo $version_ID; ?>-dlm_new_upload" class="cn_delete_file delete hide-if-no-js button tiny alert right" title="<?php _e('Delete file', 'chesnet'); ?>"><i class="fi-x imgWrapLeft"></i><?php _e('Delete', 'chesnet'); ?></span>
                    <?php wp_nonce_field('cn_delete_file_nonce', 'cn_delete_file_nonce'); ?>
                    <div class="hide-if-js">
                        <input type="checkbox" name="cn_delete_file" value="1" id="cn_delete_file" /><label for="cn_delete_file"><?php _e('No attachment file.', 'chesnet'); ?></label>
                    </div>
                </div>
            </div>
        <?php
        else: $version_file = '';
        endif; ?>
        
        <div class="row">
            <div class="large-14 columns">
            
                <div class="row collapse">
                    <div class="small-11 columns">
                        <input class="hide-if-no-js" type="text" placeholder="<?php echo $version_file ? basename($version_file) : __('Choose File', 'chesnet'); ?>" disabled="disabled" />
                    </div>
                    <div class="small-3 columns left">
                        <div class="fileUpload button info postfix">
                            <?php echo $uploadtxt; ?>
                            <input type="file" name="<?php echo $version_ID; ?>-dlm_new_upload" />
                        </div>
                    </div>
                </div>
            
            </div>
        </div>
        <div class="row">
            <div class="large-14 columns">
                <label><?php _e('Or enter multiple files for mirrors, one file url per line.', 'chesnet'); ?>
                    <textarea name="dlm_download_version[<?php echo $version_ID; ?>][files]"><?php echo isset($_SESSION['dlm_download_version'][$version_ID]['files']) ? $_SESSION['dlm_download_version'][$version_ID]['files'] : esc_textarea( implode( "\r", $files ) ); ?></textarea>
                </label>
            </div>
        </div>
    
        <div class="row">
            <div class="large-7 columns">
                <label><?php _e('Version', 'chesnet'); ?>
                    <input type="text" name="dlm_download_version[<?php echo $version_ID; ?>][version]" value="<?php echo isset($_SESSION['dlm_download_version'][$version_ID]['version']) ? $_SESSION['dlm_download_version'][$version_ID]['version'] : $version; ?>" aria-describedby="version-id-helper-txt" />
                    <p id="version-id-helper-txt"><?php _e('An identifier to differentiate between versions (eg "with-edits").', 'chesnet'); ?></p>
                </label>
                
            </div>
            <div class="large-7 columns">
                <label><?php _e('Priority', 'chesnet'); ?>
                    <input type="text" class="file_menu_order" name="dlm_download_version[<?php echo $version_ID; ?>][menu_order]" value="<?php echo isset($_SESSION['dlm_download_version'][$version_ID]['menu_order']) ? $_SESSION['dlm_download_version'][$version_ID]['menu_order'] : isset($post->menu_order) ? $post->menu_order : ''; ?>" aria-describedby="priority-helper-txt" />
                    <p id="priority-helper-txt"><?php _e('The version with the <strong>lowest</strong> priority will be downloaded by default.', 'chesnet'); ?></p>
                </label>
                
            </div>
        </div>
    </div>
<?php else: //can't edit ?>
	<div id="dlmDetails-<?php echo $version_ID; ?>" class="row">
		<div class="small-4 medium-2 columns">
        	<input type="text" class="file_menu_order" name="dlm_download_version[<?php echo $version_ID; ?>][menu_order]" value="<?php echo isset($_SESSION['dlm_download_version'][$version_ID]['menu_order']) ? $_SESSION['dlm_download_version'][$version_ID]['menu_order'] : isset($post->menu_order) ? $post->menu_order : ''; ?>" />
        </div>
        <div class="small-4 medium-2 columns"><?php echo $version; ?></div>
        <div class="small-6 medium-10 columns"><?php echo implode( "\r", $files); ?></div>
        <hr />
	</div>
<?php endif; ?>