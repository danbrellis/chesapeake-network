<?php

// Don't load directly
if ( !defined( 'ABSPATH' ) ) {
	die('-1');
}

global $bgc_locale;
if(isset($_SESSION['event-hour'])) $bgc_event_hour = $_SESSION['event-hour'];
if(isset($_SESSION['event-minute'])) $bgc_event_minute = $_SESSION['event-minute'];
if(isset($_SESSION['event-ampm'])) $bgc_event_ampm = $_SESSION['event-ampm'];
?>
	<div class="row">
    	<div class="large-14 columns post-title">
    		<label for="post_title"><?php _e('Event Title', 'chesnet'); ?> <small class="req"><?php _e('(required)', 'chesnet'); ?></small>
            <input name="post_title" id="post_title" value="<?php echo isset($_SESSION['post_title']) ? esc_attr($_SESSION['post_title']) : esc_attr($post->post_title); ?>" type="text"></label>
        </div>
    </div>
    
    <div class="row">
    	<div class="medium-7 columns">
        	<label for="event-date"><?php _e('Date', 'chesnet'); ?> <small class="req"><?php _e('(required)', 'chesnet'); ?></small>
    		<input name="event-date" id="event-date" class="datepickerclass" value="<?php echo isset($_SESSION['event-date']) ? esc_attr($_SESSION['event-date']) : esc_attr($bgc_event_date); ?>" type="text"></label>
        </div>
        <div class="medium-7 columns">
        	<div class="row">
            	<div class="small-4 medium-offset-2 columns">
                    <label for="event-time"><?php _e('Time', 'chesnet'); ?></label>
                    <select name="event-hour" id="event-hour">
                        <?php
                        if ($bgc_locale['time_format']==24) $bgc_locale['time_format'] = 23;
                        for ($i = 1; $i <= $bgc_locale['time_format']; $i++) {
                            $hour_check = ($i == $bgc_event_hour) ? ' selected="selected"' : '';
                            echo '<option value="'.$i.'"'.$hour_check.'>'.$i."</option>\n";
                        }
                        ?>
                    </select>
                </div>
                <div class="small-4 small-offset-1 medium-offset-0 columns">
                    <label class="hidden"><?php _e('Time', 'chesnet'); ?></label>
                    <select name="event-minute" id="event-minute">
                        <option value="00"<?php echo ($bgc_event_minute=='00') ? ' selected="selected"' : ''; ?>>:00</option>
                        <option value="15"<?php echo ($bgc_event_minute=='15') ? ' selected="selected"' : ''; ?>>:15</option>
                        <option value="30"<?php echo ($bgc_event_minute=='30') ? ' selected="selected"' : ''; ?>>:30</option>
                        <option value="45"<?php echo ($bgc_event_minute=='45') ? ' selected="selected"' : ''; ?>>:45</option>
                    </select>
                </div>
                <div class="small-4 small-offset-1 medium-offset-0 columns">
                    <?php if ($bgc_locale['time_format']==12) : ?>
                        <label class="hidden"><?php _e('Time', 'chesnet'); ?></label>
                        <select name="event-ampm" id="event-ampm">
                            <option value="am"<?php echo ($bgc_event_ampm=='am') ? ' selected="selected"' : ''; ?>>am</option>
                        <option value="pm"<?php echo ($bgc_event_ampm=='pm') ? ' selected="selected"' : ''; ?>>pm</option>
                        </select>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
    	<div class="large-14 columns event-loc">
        	<label for="event-loc"><?php _e('Location', 'chesnet'); ?>
    		<input name="event-loc" id="event-loc" class="geocompletei" value="<?php echo isset($_SESSION['event-loc']) ? esc_attr($_SESSION['event-loc']) : esc_attr($bgc_event_location); ?>" type="text"></label>
            <input name="event-map" id="event-map" class="show-map" value="1" type="checkbox"<?php if(isset($_SESSION['event-map']) && $_SESSION['event-map'] == 1) echo ' checked="checked"'; elseif($bgc_event_map) echo ' checked="checked"'; ?> aria-describedby="event-map-desc" /><label for="event-map"><?php _e('Show Map?', 'chesnet'); ?></label>
            <span id="event-map-desc"><?php _e('(Note: Location must be an address)', 'chesnet'); ?></span>
            <div id="event-loc-map"></div>
        </div>
    </div>

    <div class="row">
    	<div class="large-14 columns cn-post-content">
        	<label for="cnpostcontent"><?php _e('Description', 'chesnet'); ?> <small class="req"><?php _e('(required)', 'chesnet'); ?></small>
			<?php $this->cn_formContentEditor( isset($_SESSION['post_content']) ? $_SESSION['post_content'] : $post->post_content, true ); ?></label>
        </div>
    </div>
    
    

    