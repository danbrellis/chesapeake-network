<?php

/*
 * The select post type fields
 */

// Don't load directly
if ( !defined( 'ABSPATH' ) ) {
	die('-1');
}

?>

<?php //<p><a href="<?php echo $this->cn_getUrl( 'list', null, null, $post->post_type, null ); ? >">Cancel</a></p> ?>
<form id="cn-form-post-form" class="form step<?php echo $_SESSION['step']; ?>" action="<?php echo remove_query_arg( 'cs' ); ?>" method="post" enctype="multipart/form-data">
	<?php include($this->getTemplatePath( 'views', 'cn-step'.$_SESSION['step']. '-form.php' ));
	
	$this->cn_formSpamControl();
	if($edit && $post->ID): ?>
        <input type="hidden" name="ID" value="<?php echo $post->ID; ?>" />
	<?php endif; ?>
    <input type="hidden" name="cn_action" value="<?php echo isset($_SESSION['ID']) ? 'edit' : 'add'; ?>" />
    <input type="hidden" name="gid" id="group_id" value="<?php echo $this->cn_set_group_id; ?>" />
    <input type="hidden" name="post_type" value="<?php echo $post->post_type; ?>" />
    <input type="hidden" name="post_author" value="<?php echo $current_user->ID; ?>" />
    <input type="hidden" name="prev_step" value="<?php echo $_SESSION['step']; ?>" />
    <input type="hidden" name="step" value="<?php echo $_SESSION['step']+1; ?>" />
    <?php wp_nonce_field( 'cn_form_submission', 'cn_form_submission_nonce' ); ?>
    <div class="clearfix">
		<?php if($_SESSION['step'] > 1): ?><input type="submit" class="button secondary left" name="cn-post-form-back" value="<?php _e('Go Back', 'chesnet'); ?>" /><?php endif; ?>
    	<input type="submit" class="right button" name="cn-post-form" value="<?php echo $_SESSION['step'] < 3 ? _e('Next', 'chesnet') : _e('Save &amp; Share', 'chesnet'); ?>" />
     </div>
     <a href="<?php echo esc_url(home_url()); ?>"><?php _e('Cancel', 'chesnet'); ?></a>
    
   	
<?php do_action( 'cn_submission_form_bottom' ); ?>
</form>

<?php ?>