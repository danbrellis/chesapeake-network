<?php

/*
 * Select groups and sharing options
 */

// Don't load directly
if ( !defined( 'ABSPATH' ) ) {
	die('-1');
}

global $bp;

if(empty($_SESSION['cn_post_sharing'])) $_SESSION['cn_post_sharing'] = 'yes';

//show sharing options

//first show the groups
//if gid is set, then we will highlight that group as already selected and offer other groups this can be shared to
//otherwise, show the CN group
$taxonomy_names = get_object_taxonomies( $_SESSION['post_type'] );

//only let a user select a group if the post can go to a group
//also, topics can only be added to one group, so we show the group it's already in or the group referred or group 1

//get all the groups a user could add the post to
$possible_groups = array();

$input_type = 'radio'; //at this point, only can add a post to 1 group (due to handling moderation priveledges and comments
switch($_SESSION['post_type']){
	case 'cn_job': //no group option, just CN
		$possible_groups = groups_get_groups( array( 'include' => array(1), 'per_page' => 1, 'show_hidden' => true ) );
		$_SESSION['cn_group_ids'] = array(1);
		break;
	case 'topic':
		if(isset($_SESSION['ID'])){ //just the group the topic is already in
			$forum_id = bbp_get_topic_forum_id($_SESSION['ID']);
			$forum_group_ids = bbp_get_forum_group_ids($forum_id);
			$possible_groups = groups_get_groups( array( 'include' => $forum_group_ids, 'per_page' => 1, 'show_hidden' => true ) );
		}
		break;
	case 'bgc_event':
		if(isset($_SESSION['ID'])){ //just the group the event is already in
			$cats = wp_get_post_categories($_SESSION['ID']);
			if(isset($cats) && is_array($cats)) {
				$event_group_ids = array();
				foreach($cats as $cat_id) $event_group_ids[] = $this->cn_get_group_from_cat($cat_id);
			}
			$possible_groups = groups_get_groups( array( 'include' => $event_group_ids, 'per_page' => null, 'show_hidden' => true ) );
		}
		break;
	default:
		$possible_groups = $this->cn_get_user_groups();
} 
if(empty($possible_groups)) $possible_groups = $this->cn_get_user_groups();

//filter group array
$possible_groups = apply_filters('cn_possible_groups', $possible_groups, $_SESSION);
$possible_groups['groups'] = array_values($possible_groups['groups']);
?>

<?php echo '<h3>3. ' . __('Group, tag &amp; share your message', 'chesnet') . '</h3>';

//if there is just 1 group_id, just show that (no choices)
echo '<div class="row"><div class="large-14 columns"><h4>' . ($_SESSION['cn_action'] == 'edit' ? __('Save in: ', 'chesnet') : __('Add to: ', 'chesnet')) . '</h4>';

	if(count($possible_groups['groups']) == 1 ){
		echo '<div class="btn-switch chk" title="'.$possible_groups['groups'][0]->name.'"><input value="1" type="radio" id="cn_post_group-'.$possible_groups['groups'][0]->id.'" name="cn_group_ids[]" checked="checked"/><label for="cn_post_group-'.$possible_groups['groups'][0]->id.'">'.$possible_groups['groups'][0]->name.'</label></div>';
	}
	else{ //more than 1, so we give the option
		$group = null;
		
	
		$possible_group_ids = array();
		foreach($possible_groups['groups'] as $possible_group) $possible_group_ids[] = $possible_group->id;
		
		echo '<div class="row"><div class="large-14 columns">';
		
		echo $this->cn_get_user_groups_chk($_SESSION['cn_group_ids'], array('include' => $possible_group_ids), $input_type);
	
		echo '<input type="hidden" name="cn_group_ids_option" value="1" />';
		
	}
echo '</div></div>';
?>

<?php

//add tag selectors to post
//use radio buttons
//no ability to add new tags

//first, make sure post_tag is enabled for this post type
//$taxonomy_names = get_object_taxonomies( $_SESSION['post_type'] );
if(isset($taxonomy_names) && in_array('post_tag', $taxonomy_names)):
	echo '<h4>' . sprintf(__('Tag Your %s:', 'chesnet'), $post_type_obj->labels->singular_name) . '</h4>';
	printf( '<p>' . __('Tagging will help ensure your %s is delivered to those who want to see it. Click each tag that is relevant to your post.', 'chesnet') . '</p>', $post_type_obj->labels->singular_name); ?>
    <div class="row">
		<div class="large-14 columns">
			<?php  //list the tags to select
			$selected_tag_ids = array();
			if(isset($_SESSION['cn_post_tag_ids'])) $selected_tag_ids = $_SESSION['cn_post_tag_ids'];
			elseif(isset($_SESSION['cn_post_tag_ids_option']) && $_SESSION['cn_post_tag_ids_option'] == 1) $selected_tag_ids = array();
			elseif(isset($_SESSION['ID'])) $selected_tag_ids = wp_get_post_tags( $_SESSION['ID'], array( 'fields' => 'ids' ) );
			echo $this->cn_get_post_tag_checkbox($selected_tag_ids);
			echo '<input type="hidden" name="cn_post_tag_ids_option" value="1" />'; ?>
        </div>
    </div>
<?php endif; ?>

<?php //Provide the option to not send to any groups
echo '<h4>' . __('Email Options:', 'chesnet') . '</h4>';
printf( '<p>' . __('After saving your %s, would you like to notify members via email of your announcement?', 'chesnet') . '</p>', $post_type_obj->labels->singular_name); ?>
<div class="row">
	<div class="large-14 columns">
    	<?php //@todo incorporate reminders
		$reminders = 'inactive'; if(isset($_SESSION['ID']) && $reminders == 'active'): //figure out reminders left
			//get reminder meta for each group
			$groupreminder = array();
			foreach($possible_groups['groups'] as $pg) $groupreminder[$pg->id] = get_post_meta($_SESSION['ID'], '_cn_reminders_left_group_' . $pg->id, true);
			//w/o jquery, just use the lowest reminder number available
			$remindersleft = min($groupreminder);
			?>
			<div data-alert class="alert-box info">
				<?php $agreement = ($remindersleft == 1) ? __('time', 'you can remind members of this post by email X more time(s)', 'chesnet') : __('times', 'you can remind members of this post by email X more time(s)', 'chesnet'); ?>
					<span class="hide-if-js"><?php printf(__('You can remind members of this %s by email %d more %s.', 'chesnet'), $post_type_obj->labels->singular_name, $remindersleft, $agreement); ?></span>
			</div>
		<?php endif; ?>
    	<ul class="large-block-grid-2 small-block-grid-1">
        	<li>
            	<div class="btn-switch chk" title="<?php _e('Don&rsquo;t send an email.', 'chesnet'); ?>"><input type="radio" name="cn_post_sharing" id="no-email" value="no" <?php checked($_SESSION['cn_post_sharing'], 'no'); ?> /><label for="no-email"><?php _e('Don&rsquo;t send an email.', 'chesnet'); ?></label></div>
            </li>
            <li>
            	<div class="btn-switch chk" title="<?php _e('Yes! Send an email.', 'chesnet'); ?>"><input type="radio" name="cn_post_sharing" id="send-email" value="yes" <?php checked($_SESSION['cn_post_sharing'], 'yes'); ?> /><label for="send-email"><?php _e('Yes! Send an email.', 'chesnet'); ?></label></div>
            </li>
        </ul>
    </div>
</div>