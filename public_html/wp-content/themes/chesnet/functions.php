<?php

/**
 * Plugin Name: CHESNET Theme Code
 * Version: 1.0
 * Author: Dan Brellis
 * License: GPL2
**/

/*
 * Chesapeake Network Mailing Lists
 *
 * @todo
 *
 
 First, create tables for emails: outgoing (fields include headers, html/text, email body, group_id, date to send) & sent (fields include date sent, opened, date received, email body, corresponding post ID)
 
 Whenever an activity is created, check to see if there should be a new email sent (see if post's metabox is checked to send to group). If it is, add it to the outgoing table in the database. This can happen in the user's same session (just one new database row).
 
 Afterwards, call an SSH script to do the actual sending and record each individual email sent in the mail_sent table. This script sends an email to each member in the group that is subscribed to receive "all emails".
 
 Later, at, say 4am, a daily cron job will go through the mail_outgoing table and get a list of all emails that were scheduled to be sent in the past 24 hours (aka, since the last cron job) and group them by mailing list (aka bp group). This will then compose a digest email and send to those in the groups that are subscribed to daily digest emails.
 
 *
 * okay, break
 *
 */
 
/**
 * GIT Framework
 *
 
 Git repo is at bitbucket.org - https://danbrellis@bitbucket.org/danbrellis/chesapeake-network.git (pwd is kjhgfdsa)
 
 --Gitting Started--
 * git init in project folder :: git init
 * create origin repo at bitbucket :: git remote add origin https://danbrellis@bitbucket.org/danbrellis/chesapeake-network.git
 
 --When Updating A Plugin--
 * create a new branch in remote dev site :: git checkout -b pu
 * In Wordpress admin, update the ONE plugin
 * stage and commit the newly updated files :: git commit -a -m 'Update Plugin: Name of Plugin (9.9.9)'
 * switch to master :: git checkout master
 * merge pu to master :: git merge pu
 * push master to remote :: git push origin master
 * in local file copy, make a backup of any plygin files that you altered
 * git fetch :: git fetch
 * bring in the remote master :: git pull origin master
 * create a pu branch :: git checkout -b pu
 * make any changes you'd like to the plugin
 * stage and commit those changes :: git commit -a -m 'Modify Plugin: Name of Plugin (9.9.9) "message of what changes you made to the plugin"'
 * switch to master :: git checkout master
 * merge pu to master :: git merge pu
 * push master to remote :: git push origin master
 * in remote dev site git, pull the new master :: git pull origin master
 * in live dev site git, pull the new master :: git fetch && git pull origin master
 
 --In Local/Dev Sites-- 
 * create dev branch (main working branch) :: git checkout -b [name_of_new_branch] 
 * optionally make new branches for individual features
 * make edits to dev site files locally and ftp to test
 * once we're happy, push current branch to origin and merge current branch to dev (locally)
 * push dev to origin
 * merge dev to master (locally)
 * push master to origin
 * Make edits and commits to dev branch

 * push dev to repo
 * on test site pull dev branch from repo
 * make sure it's a

/**
HERE - spinner.gif was added for invites. Now, investigate the rest of the invite anyone plugin and upload versino to live site

 ** Would be nice to fix one day
 * On profile page, use jquery to change url of different tabs being displayed: http://localhost/chesnet/members/admin/profile/
   foundation gives callbacks: http://foundation.zurb.com/docs/components/tabs.html#callbacks
 * Format private messages to make use of side bar here http://localhost/chesnet/members/admin/messages/
   List all messages & search on left sidebar, show content on main content panel
   Deactivated on live site
 * Fix autocomplete when selecting a user while composing a private message
   (some js error with jquery and foundation) http://localhost/chesnet/members/admin/messages/compose/
 * multiple addresses is being addressed for profiles:
   https://geomywp.com/support/forums/topic/multiple-addresses-per-buddypress-user/#post-14380
 * Tags have come a long way, but aren't finished. Need to make it so that when no javascript, we can filer the
   activity stream via $_GET parameters. Unfortunately, this isn't so easy because of a cookie issue that persists when js is off
   Also, if we can update the URL via js when the activity is filtered, that'd be nice.
 * When a pending post is approved, remove all notifications related to approving that post
   When a pending post is rejected, send a notification to the author and also allow the moderator to write a message 
   explaining why (could use native messages component!)
   Also, if a post is pending after 1 week, add reminder notification
   And show an alert to mods on the group page's header
 * add bulk resource upload
 * all the todo's in the emailhandler.php email piping script
   And, add setting to only set admins/mods send emails to the group and not members or anyone on the 
   network even if not in the group.
 * create a new post stauts caled 'rejected' for posts that are rejected from a group
 * disable wordpress cron (DISABLE_WP_CRON) and use directadmin cron to run wp cron
 * add members/invite members to groups
   can now invite users to group (still need to add autocomplete search)
 * transfer wikis
 * transfer emails
 * bug - when you update a dlm_download, a new activity item is generated
 * When adding a link in the frontend "add announcements" wp_editor, the model is missing crucial stylesheet info
 * reset password bug, not working/sending a new password email
 * Implement BP-Activity-Autoload (https://github.com/sbrajesh/bp-activity-autoloader). Code already added in assets/js/chesnet.js
 * Incorporate native (as of BP 2.4) group & member cover photos instead of custom ones)
   
 **Fix before we launch
 * Add funder logos to sidebar
**/

class CHESNET {
	
	//Variables
	public $supported_types = array(
		'application/pdf', //Adobe PDF
		'text/plain', //Plain TXT
		'application/msword', //MS Word - DOC
		'application/vnd.oasis.opendocument.text', //ODT
		'application/vnd.openxmlformats-officedocument.wordprocessingml.document' //MS Word - DOCX
	);
	
	public $currentPost;
	
	public $cn_set_group_id;
	public $cn_the_set_group;
	
	public $cn_the_set_activity_id;
	
	public $wp_editor_placeholder;
	
	public $showing_activity_stream = false;
	
	public $error_codes;
	
	public $user_can_add = array('post', 'topic', 'dlm_download', 'bgc_event', 'cn_job');
	
	private $recaptcha_sitekey = '6LdzpwMTAAAAAIQ8a1QrowurleZZXlFYZV03FXLn';
	private $recaptcha_secretkey = '6LdzpwMTAAAAALbbYhPz5vVyL3ZImv-wJfnHkzaA';
	
	//initiate the class
	function __construct() {
		// Various clean up functions
		get_template_part('library/cleanup');
		include_once(get_stylesheet_directory() . '/library/constants.php');
		
		//Theme Styles
		add_action( 'customize_register', array($this, 'theme_customizer') );
		if(class_exists('wp_debug')) add_action( 'foundationPress_before_footer', array('wp_debug', 'debug_info') );
		//add_action( 'foundationPress_before_footer', array($this, 'add_joyride') );
		add_action( 'login_enqueue_scripts', array($this, 'login_enqueue_scripts') );
		add_filter( 'login_headerurl', array($this, 'login_headerurl') );
		add_filter( 'login_headertitle', array($this, 'login_headertitle') );
		get_template_part('library/foundation');
		
		//Scripts & Styles
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ) );
		add_action( 'wp_enqueue_scripts', array($this, 'wp_enqueue_scripts') );
		add_action( 'admin_footer', array($this, 'print_inline_scripts') );
		//add_action( 'wp_footer', array($this, 'print_inline_scripts') );
		
		//Text Domain
		add_action( 'load_textdomain', array($this, 'load_textdomain'), 10, 2 );
	
		//Add Support
		if(function_exists( 'add_theme_support' )){
			add_theme_support( 'post-thumbnails' );
			set_post_thumbnail_size( 150, 150 ); // default Post Thumbnail dimensions  
			
			add_post_type_support( 'page', 'excerpt' );
			add_post_type_support( 'post', 'excerpt' );
			
			add_theme_support('menus');
			add_theme_support('automatic-feed-links');
			add_theme_support('post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat'));
		}
		add_action('after_switch_theme', array($this, 'after_switch_theme') );
		
		//WP-Admin
		add_action( 'post_edit_form_tag', array($this, 'post_edit_form_tag') );
		add_action( 'admin_init', array($this, 'admin_init') );
		add_action( 'after_setup_theme', array($this, 'after_setup_theme') );
		
		//Admin Notices
		add_action( 'admin_notices', array($this, 'admin_notices') );
		
		//Paths & Redirects
		//add_action( 'pre_get_posts', array($this, 'alter_archive_query') );
		add_action( 'pre_get_posts', array($this, 'pre_get_posts') );
		
		add_action('delete_option_rewrite_rules', array($this, 'add_rewrite_rules'));
		$current_rules = get_option('rewrite_rules');
		if(empty($current_rules)) {
		  $this->add_rewrite_rules();
		}
		
		add_action( 'wp_router_generate_routes', array($this, 'wp_router_generate_routes') );
		//Allows for CPT to be listed by author
		add_rewrite_rule("jobs/([^/]+)/list/page/?([2-9][0-9]*)",
			"index.php?post_type=cn_job&author_name=\$matches[1]&paged=\$matches[2]", 'top');
		add_rewrite_rule("jobs/([^/]+)/list/?",
			"index.php?post_type=cn_job&author_name=\$matches[1]", 'top');
		//add_rewrite_rule("resources/([^/]+)/list/page/?([2-9][0-9]*)",
			//"index.php?post_type=dlm_download&author_name=\$matches[1]&paged=\$matches[2]", 'top');
		//add_rewrite_rule("resources/([^/]+)/list/?",
			//"index.php?post_type=dlm_download&author_name=\$matches[1]", 'top');
			
		add_rewrite_rule("events/([^/]+)/list/page/?([2-9][0-9]*)",
			"index.php?post_type=bgc_event&author_name=\$matches[1]&paged=\$matches[2]", 'top');
		add_rewrite_rule("events/([^/]+)/list/?",
			"index.php?post_type=bgc_event&author_name=\$matches[1]", 'top');
		
		add_filter( 'post_link', array($this, 'append_query_string'), 10, 2 );
		add_filter( 'post_type_link', array($this, 'append_query_string'), 10, 2 );
		add_filter( 'post_type_link', array($this, 'cpt_link'), 10, 4 );
		add_filter( 'cn_getUrl', array($this, 'append_query_string'), 10, 2 );
		add_filter( 'term_link', array($this, 'term_link_filter'), 10, 3 );
		
		add_action( 'wp_insert_post_parent', array($this, 'wp_insert_post_set_global_post_data'), 10, 4 );
		add_action( 'wp_unique_post_slug_is_bad_flat_slug', array($this, 'wp_unique_post_slug_is_bad_flat_slug'), 10, 3 );
				
		//Media
		if(function_exists( 'add_image_size' )){ 
			//add_image_size( 'cclc-banner', 1140, 293, true );
		}
		add_filter( 'upload_mimes', array($this, 'upload_mimes'), 1, 1 );
		
		//Navigation
		get_template_part('library/navigation');
		get_template_part('library/menu-walker');
		
		//Sidebars & Widgets
		get_template_part('library/widget-areas');
		require(get_stylesheet_directory() . '/library/widgets.php');
		
		//Search
		//add_filter( 'pre_get_posts', array($this, 'SearchFilter') );
		//include_once(get_stylesheet_directory() . '/bp-global-unified-search-master/functions.php');
		
		//Custom Post Types & Taxonomies
		add_action( 'init', array($this, 'create_post_type') );
		add_action( 'init', array($this, 'register_taxonomy') );
		add_action( 'wp', array($this, 'register_dlm_tag_tax') );
		add_filter( 'dlm_cpt_dlm_download_args', array($this, 'dlm_cpt_dlm_download_args'), 10, 1 );
		add_filter( 'bbp_register_topic_post_type', array($this, 'bbp_register_topic_post_type'), 10, 1);
		add_filter( 'category_link', array($this, 'cn_category_link'), 10, 2 );
		
		//Meta Boxes
		add_action( 'add_meta_boxes', array($this, 'add_meta_boxes') );
		add_action( 'save_post', array($this, 'save_metadata') );
		
		//Content Code
		add_filter( 'get_image_tag_class', array($this, 'image_tag_class') );
		add_action( 'bp_before_archive', array($this, 'cn_more_add_my_links'), 10 );
		add_action( 'bp_before_blog_home', array($this, 'cn_more_add_my_links'), 10 );
		add_action( 'cn_after_step2_form', array($this, 'cn_after_step2_form') );
		add_action( 'cn_submission_form_bottom', array($this, 'cn_submission_form_bottom') ); 
		
		add_filter( 'cn_before_save_post_form', array($this, 'before_post_updated'), 10, 1 );
		add_filter( 'postie_post_before', array($this, 'before_post_updated'), 10, 1 );
		add_action( 'cn_after_process_post_form', array($this, 'cn_after_process_post_form'), 10, 4 );
		add_filter( 'redirect_after_multistep_form_save', array($this, 'redirect_after_multistep_form_save'), 10, 3 );
		add_action( 'cn_before_content', array($this, 'cn_before_content'), 10 );
		//add_action( 'cn_page_before_entry_content', array($this, 'template_notices'), 10 );
		//add_action( 'cn_post_before_entry_content', array($this, 'template_notices'), 10 );
		add_action( 'cn_post_before_entry_content', array($this, 'show_single_post_meta'), 10 );
		add_shortcode( 'glyphicon', array($this, 'glyphicon_shortcode') );
		add_filter( 'the_editor', array($this, 'the_editor') );
		//add_filter( 'excerpt_more', array($this, 'excerpt_more') );
		//add_filter( 'oembed_result', array($this, 'oembed_result'), 10, 3);
		//add_filter( 'the_content', array($this, 'the_content_filter') );
		add_filter( "comments_open", array($this, "comments_open"), 10, 2 );
		
		//AJAX
		add_action( 'wp_ajax_cn_set_to_delete_file_and_meta', array($this, 'cn_set_to_delete_file_and_meta') );
		add_action( 'wp_ajax_nopriv_cn_set_to_delete_file_and_meta', array($this, 'cn_set_to_delete_file_and_meta') );
		include_once(get_stylesheet_directory() . '/includes/ajax.php');
		remove_action( 'wp_head', 'bp_core_add_ajax_url_js' );
		add_filter('ajax_query_attachments_args', array($this, 'ajax_query_attachments_args') );
		
		//BuddyPress
		require(get_stylesheet_directory() . '/includes/class.group-resources.php');
		require(get_stylesheet_directory() . '/includes/class.group-moderation.php');
		//require(get_stylesheet_directory() . '/includes/class.group-news.php');
		if(!is_admin()){
			add_action( 'init', array($this, 'cn_set_current_group_id'), 11 );
			add_action( 'init', array($this, 'cn_set_current_activity_id'), 11 );
		}
		define('BP_EMBED_DISABLE_ACTIVITY', true);
		remove_action( 'bp_notification_settings', 'bp_activity_screen_notification_settings', 1 );
		remove_action( 'transition_post_status', 'bp_blogs_catch_transition_post_status', 10 );
		add_action( 'bp_init', array($this, 'bp_init'), 9 );
		include_once(get_stylesheet_directory() . '/buddypress/activity/templates/my-activity-sidebar.php' );
		add_filter( 'bp_get_group_join_button', array($this, 'bp_get_group_join_button'), 10, 1 );
		add_filter( 'bp_get_group_create_button', array($this, 'bp_get_group_create_button'), 10, 1 );
		add_filter( 'bp_get_send_message_button_args', array($this, 'bp_get_send_message_button_args'), 10, 1 );
		add_action( 'bp_setup_nav', array($this, 'bp_setup_nav'), 999 );
		/* these should be fixed soon via https://buddypress.trac.wordpress.org/ticket/6503 */
		add_filter( 'bp_get_displayed_user_nav_settings', '__return_false' );
		//add_filter( 'bp_get_options_nav_public', '__return_false' );
		//add_filter( 'bp_get_options_nav_edit', '__return_false' );
		//add_filter( 'bp_get_options_nav_change-avatar', '__return_false' );
		add_filter( 'bp_get_options_nav_admin', '__return_false' );
		add_filter( 'bp_get_options_nav_nav-hierarchy', '__return_false' );
		add_filter( 'bp_get_options_nav_change-cover', '__return_false' );
		add_filter( 'bp_get_options_nav_nav-notifications', '__return_false' );
		add_filter( 'bp_get_options_nav_request-membership', '__return_false' );
		add_filter( 'bp_get_options_nav_notifications', '__return_false' );
		add_filter( 'bp_get_options_nav_nav-invite-anyone', '__return_false' );
		add_action( 'bp_core_validate_user_signup', array($this, 'bp_core_validate_user_signup') );
		add_action( 'bp_core_activated_user', array($this, 'bp_core_activated_user'), 10, 3 );
		add_action( 'bp_group_header_actions', array($this, 'bp_group_header_actions') );
		add_action( 'bp_member_header_actions', array($this, 'bp_member_header_actions') );
		add_filter( 'bp_group_gravatar_default', array($this, 'bp_group_gravatar_default'), 10, 1 );
		add_filter( 'bp_gravatar_url', array($this, 'bp_gravatar_url'), 10, 1 );
		add_action( 'groups_join_group', array($this, 'groups_join_group'), 10, 2 );
		add_action( 'bp_after_group_settings_creation_step', array($this, 'group_settings_email_address') );
		add_action( 'bp_after_group_settings_admin', array($this, 'group_settings_email_address') );
		add_action( 'groups_group_after_save', array($this, 'groups_group_after_save') );
		add_filter( 'bp_get_group_class', array($this, 'bp_get_group_class') );
		add_action( 'bp_before_profile_edit_content', array($this, 'add_visibility_switch_to_profile_field_name') );
		add_action( 'bp_before_signup_profile_fields', array($this, 'add_visibility_switch_to_profile_field_name') );
		add_filter( 'bp_ajax_querystring', array( $this, 'activity_querystring_filter' ), 12, 2 );
		add_action( 'bp_before_directory_activity_list', array($this, 'add_tag_it_field') );
		add_action( 'bp_before_group_activity_content', array($this, 'add_tag_it_field') );
		add_action( 'bp_before_member_activity_content', array($this, 'add_tag_it_field') );
		add_action( 'bp_after_activity_loop', array($this, 'bp_after_activity_loop') );
		add_action( 'save_post_post', array($this, 'add_DirectAdmin_email_alias'), 10, 3 );
		add_action( 'save_post_dlm_download', array($this, 'add_DirectAdmin_email_alias'), 10, 3 );
		add_action( 'save_post_topic', array($this, 'add_DirectAdmin_email_alias'), 10, 3 );
		add_action( 'save_post_cn_job', array($this, 'add_DirectAdmin_email_alias'), 10, 3 );
		add_action( 'save_post_bgc_event', array($this, 'add_DirectAdmin_email_alias'), 10, 3 );
		
		//RSS Feeds
		add_filter( 'bp_get_activity_thread_permalink', array($this, 'bp_get_activity_thread_permalink') );
		add_filter( 'bp_get_activity_feed_item_title', array($this, 'bp_get_activity_feed_item_title') );
		add_action( 'bp_activity_feed_prefetch', array($this, 'bp_activity_feed_prefetch') );
		
		//X-Profile & Users
		add_action( 'bp_signup_pre_validate', array($this, 'bp_signup_pre_validate') );
		add_action( 'bp_before_registration_submit_buttons', array($this, 'bp_before_registration_submit_buttons') );
		
		//Plugins
		add_filter( 'bp_group_hierarchy_available_parent_groups', array($this, 'cn_bp_group_hierarchy_available_parent_groups'), 10, 2 );
		
		add_action( 'groups_before_delete_group', array($this, 'cn_groups_before_delete_group'), 10, 3 );
		add_action( 'groups_create_group', array($this, 'groups_create_group'), 10, 3 );
		
		add_filter( 'ass_block_group_activity_types', array($this, 'cn_ass_block_group_activity_types'), 10, 3 );
		add_filter( 'ass_this_activity_is_important', array($this, 'cn_ass_this_activity_is_important'), 10, 2 );
		add_filter( 'ass_get_post_from_activity', array($this, 'cn_ass_get_post_from_activity'), 10, 1 );
		add_filter( 'ass_after_group_notification', array($this, 'cn_ass_after_group_notification'), 10, 1 );
		add_filter( 'ass_filter_message_content', array($this, 'cn_ass_filter_message_content'), 10, 4 );
		remove_action( 'bp_group_header_meta', 'ass_group_subscribe_button' );
		remove_action( 'bp_directory_groups_actions', 'ass_group_subscribe_button' );
		add_action( 'bp_directory_groups_actions', array($this, 'ass_group_subscribe_button') );
		remove_action( 'bp_after_group_settings_admin', 'ass_default_subscription_settings_form' );
		remove_action( 'bp_after_group_settings_creation_step', 'ass_default_subscription_settings_form' );
		add_action( 'bp_after_group_settings_admin', array($this, 'ass_default_subscription_settings_form') );
		add_action( 'bp_after_group_settings_creation_step', array($this, 'ass_default_subscription_settings_form') );
		
		add_filter( 'manage_edit-dlm_download_columns', array( $this, 'dlm_download_columns' ) );
		add_filter( 'dlm_do_not_force', '__return_true' );
		
		add_filter( 'save_post_topic', array($this, 'save_post_topic'), 10, 1 );
		add_filter( 'bbp_get_topic_edit_url', array($this, 'bbp_get_topic_edit_url'), 10, 2 );
		//add_filter( 'bp_get_new_group_enable_forum', array($this, 'bp_get_new_group_enable_forum'), 10, 1);
		add_action( 'after_groups_admin_announcement_moderation_rejected', array($this, 'remove_topic_from_group'), 10, 2 );
		
		add_filter( 'bp_group_calendar_create_event_url', array($this, 'bp_group_calendar_create_event_url'), 10, 4 );
		remove_action( 'bp_after_group_settings_creation_step', 'bp_group_calendar_settings');
		remove_action( 'bp_after_group_settings_admin', 'bp_group_calendar_settings' );
		
		remove_action( 'bp_member_plugin_options_nav', 'bp_docs_member_create_button' );
		
		add_filter( 'invite_anyone_send_friend_requests_on_acceptance', '__return_false' );
		add_filter( 'invite_anyone_is_large_network', '__return_true' );
		if (defined('BP_INVITE_ANYONE_SLUG'))
			require(get_stylesheet_directory() . '/includes/class.group-invite-anyone.php');
		
		require(get_stylesheet_directory() . '/includes/class.objectcoverphoto.php');
		require(get_stylesheet_directory() . '/includes/class.httpsocket.php');
	}
	
	/**
	 ** Scripts & Styles
	 **/
	function admin_enqueue_scripts(){
		/* Le Scripts */
		wp_register_script( 'chesnet-admin', get_stylesheet_directory_uri() . '/assets/js/chesnet-admin.js', array('jquery', 'jquery-ui-core', 'jquery-ui-datepicker'), '1.0.0', true );
		wp_enqueue_script('chesnet-admin');
		
		/* Le Styles */
		wp_register_style('jquery-ui', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css');
	    wp_enqueue_style( 'jquery-ui' );  
	}
	
	function wp_enqueue_scripts(){
		$min = SCRIPT_DEBUG === true ? '' : '.min';
		/* Le Fonts */
		//wp_enqueue_script( 'typekit-ejs7hox', '//use.typekit.net/ejs7hox.js' ); //added in header.php
		
		/* Le Scripts */
		wp_register_script( 'chesnet', get_stylesheet_directory_uri() . '/assets/js/chesnet.js', array('jquery', 'jquery-ui-core', 'jquery-ui-widget', 'jquery-ui-position', 'jquery-ui-autocomplete', 'foundation', 'jquery-geocomplete'), '1.0.0', true );
		
		wp_register_script( 'googlemaps', '//maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places', array('jquery') );
		wp_register_script( 'recaptcha', 'https://www.google.com/recaptcha/api.js' );
		
		//JQuery & Foundation
		if( !is_admin()){
			
			wp_deregister_script('jquery');
			wp_register_script('jquery', "//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery".$min.".js", array(), '1.11.1', false);
			
			wp_deregister_script('jquery-ui-core');
			wp_register_script('jquery-ui-core', "//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js", array('jquery'), '1.10.4', false);
			
			wp_register_script( 'modernizr', get_stylesheet_directory_uri() . '/js/modernizr/modernizr'.$min.'.js', array(), '1.0.0', false );
			wp_register_script( 'fastclick', get_stylesheet_directory_uri() . '/js/fastclick/lib/fastclick.js', array('modernizr'), '1.0.3', false );
			wp_register_script( 'foundation', get_stylesheet_directory_uri() . '/js/foundation/js/foundation.min.js', array('jquery', 'jquery-ui-core'), '1.0.0', true );
			wp_register_script( 'foundation-joyride', get_stylesheet_directory_uri() . '/js/foundation/js/foundation/foundation.joyride.js', array('jquery'), '5.4.3', true );
			wp_register_script( 'tagit', get_stylesheet_directory_uri() . '/assets/aehlke-tag-it/js/tag-it'.$min.'.js', array('jquery', 'jquery-ui-core'), '2.0', true ); //https://github.com/aehlke/tag-it
			wp_register_script( 'jquery-history', get_stylesheet_directory_uri() . '/assets/jquery-history-master/scripts/jquery.history'.$min.'.js', array('jquery'), '1.0.3', true );
			wp_register_script( 'jquery-sparkle-core', get_stylesheet_directory_uri() . '/assets/jquery-sparkle/core.string.js', array('jquery'), '1.0.0', true );
			wp_register_script( 'jquery-geocomplete', get_stylesheet_directory_uri() . '/assets/jquery-geocomplete/jquery.geocomplete'.$min.'.js', array('jquery', 'googlemaps'), '1.6.4', true );
			wp_register_script('jquery-cookie', '//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie'.$min.'.js', array('jquery'), '1.4.1', true);
			
			
			wp_enqueue_script('jquery');
			wp_enqueue_script('jquery-ui-core');
			
			// enqueue modernizr, jquery and foundation in parent theme: library/enqueue-scripts.php
			wp_enqueue_script('modernizr');
			wp_enqueue_script('fastclick');
			wp_enqueue_script('foundation');
			wp_enqueue_script('foundation-joyride');
			
			//wp_enqueue_script('jquery-history');

			wp_enqueue_script('jquery-sparkle-core');
			wp_enqueue_script('jquery-geocomplete');
			wp_enqueue_script('jquery-cookie');
			
		}
		
		//Localize
		wp_localize_script( 'chesnet', 'cn_data', $this->localize_script_chesnet() );
		
		//Enqueue
		wp_enqueue_script('chesnet');
		wp_enqueue_script('tagit');
		wp_enqueue_script('googlemaps');
		wp_enqueue_script('recaptcha');
		
		/* Le Styles */
		wp_enqueue_style( 'chesnet', get_stylesheet_uri() );
		wp_enqueue_style( 'tagit', get_stylesheet_directory_uri() . '/assets/aehlke-tag-it/css/jquery.tagit.css' );
		wp_enqueue_style( 'jquery-ui-flick', '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/flick/jquery-ui.css' );
		wp_enqueue_style( 'dashicons' );
	}
	
	function localize_script_chesnet(){
		global $post;
		$local = array();
		
		if($post) $local['post_id'] = $post->ID;
		
		//post tags for activity filtering
		$tags_condensed = $tags_array = array();
		$tags_args = array(
			'hide_empty'	=> false,
			'get'			=> 'all',
			'orderby'		=> 'name'
		);
		$tags_array = get_tags( $tags_args );
		$props = array("term_group","name");
		usort($tags_array, function($a, $b) use ($props) {
			if($a->$props[0] == $b->$props[0])
				return $a->$props[1] > $b->$props[1] ? 1 : -1;
			return $a->$props[0] > $b->$props[0] ? 1 : -1;
		});
		foreach($tags_array as $tag) $tags_condensed[] = $tag->name;
		$local['activity_tags'] = $tags_condensed;
		$local['tagit_placeholder'] = __('Filter activity by state.', 'chesnet');

		//data for google maps
		if(is_single() && $post->post_type == 'bgc_event'){
			if(get_post_meta($post->ID, '_bgc_event_map', true) == 1){
				$loc = get_post_meta($post->ID, '_bgc_event_location', true);
				$local['bgc_map']['loc'] = esc_attr($loc);
				$local['bgc_map']['canvas'] = 'gmap_for_' . $post->ID;
			}
		}
		
		$local['announcement_link'] = add_query_arg('gid', $this->cn_set_group_id, get_bloginfo('url') . '/announcement/add');
		$local['post_by_email_confirm_txt'] = __("Did you know?\r\nYou can post to this or any group by using our new online form which gives you more control over how your posts are displayed to the group. With the online form, you can tag your messages by state and add additional data, like an event start time, the hiring organization for a job annoucement, and more.\r\nIf you'd like to use the online form (we recommend you do), click 'Cancel'. Otherwise, click 'Okay' to continue to post to the group via email.", 'chesnet');;
		return $local;
	}
	
	//output inline scripts
	function print_inline_scripts() {
		$post = $this->cn_get_post(); ?>
        
		<script type="text/javascript" >
        jQuery(document).ready(function($) { <?php
		
		//JS code for AJAX request to delete file and meta data from a post
		//if($post->post_type == 'cn_job' || $post->post_type == 'dlm_download'):
			$ajax_nonce = wp_create_nonce("cn_delete_file"); ?>
			
			$('.cn_delete_file').on( "click", function(){
				var btn = $(this);
				var post_ID = $('#post_ID').val();
				var btn_id = btn.attr('id'); //ie delete__92__cn_jobs_announcement_file
				var btn_data = btn_id.substring(8); //ie 92__cn_jobs_announcement_file
				var btn_data_array = btn_data.split('__');
				
				var data = {
					action: 'cn_set_to_delete_file_and_meta',
					security: '<?php echo $ajax_nonce; ?>',
					delete_file: true,
					key: '_' + btn_data_array[1],
					post_id: btn_data_array[0]
				};
				//console.log(data);
				$.post(ajaxurl, data, function(response) {
					var r = $.parseJSON( response );
					if(r.type == 'success'){
						//delete file text
						//console.log(btn);
						btn.parent('div').css('background-color', 'rgb(255, 170, 170)');
						btn.parent('div').fadeOut("slow");			
					}
				});
			});
		<?php //endif; ?>
		
		});
        </script>
        <?php
	}
	
	/**
	 ** Languages
	 **/
	function load_textdomain($domain, $mofile){
		if ('bbpress' === $domain && str_replace('/', '\\', plugin_dir_path($mofile)) === str_replace('/', '\\', WP_PLUGIN_DIR.'/bbpress/languages/')) {
			load_textdomain('bbpress', WP_LANG_DIR.'/bbpress/'.$domain.'-'.get_locale().'.mo');
		}
		if ('download-monitor' === $domain && str_replace('/', '\\', plugin_dir_path($mofile)) === str_replace('/', '\\', WP_PLUGIN_DIR.'/download-monitor/languages/')) {
			load_textdomain('download_monitor', WP_LANG_DIR.'/download-monitor/'.$domain.'-'.get_locale().'.mo');
		}
		if ('buddypress' === $domain && str_replace('/', '\\', plugin_dir_path($mofile)) === str_replace('/', '\\', WP_PLUGIN_DIR.'/buddypress/languages/')) {
			load_textdomain('buddypress', WP_LANG_DIR.'/buddypress/'.$domain.'-'.get_locale().'.mo');
		}
	}
	
	function load_textdomain_mofile( $mofile, $domain='' ) {
		//only do for the plugins/themes you want
		if ('download-monitor' === $domain) {
			return WP_LANG_DIR . '/'.$domain.'/'.str_replace('-','_',$domain).'-'.get_locale().'.mo';
		}
		elseif('bbpress' === $domain) {
			return WP_LANG_DIR . '/'.$domain.'/'.str_replace('-','_',$domain).'-'.get_locale().'.mo';
		}
		elseif('buddypress' === $domain) {
			return WP_LANG_DIR . '/'.$domain.'/'.str_replace('-','_',$domain).'-'.get_locale().'.mo';
		}
	
		return $mofile;
		/*
		if (!in_array($domain, array('download_monitor')))
			return $mofile;

		$pathinfo = pathinfo($mofile);
		$custom_mofile = WP_CONTENT_DIR . '/languages/' . 'download_monitor-en_EN.mo';
		if (file_exists($custom_mofile)) 			
			return ($custom_mofile); 
		else  
			return $mofile;
		*/
	}

	
	/**
	 ** Admin Notices
	 **/
	 
	// Adds admin notices to backend post editting
	// Use $this->add_admin_notices() to add a notice
	function admin_notices(){
		//print the message
		global $post;
		$notice = get_option('cn_admin_notice');
		if (empty($notice)) return;
		
		foreach($notice as $pid => $type){
			if($post->ID == $pid || !$pid){
				foreach($type as $t => $ms){
					foreach($ms as $m) echo '<div class="'.$t.'"><p>'.$m.'</p></div>';
				}
				unset($notice[$pid]);
				update_option('cn_admin_notice',$notice);
			}
		}
	}
	
	//Sets wp_option cn_admin_notice
	// @param str $type The class for the notice, wp uses 'error' || 'warning'.
	// @param str $message The notice message to display. 
	function add_admin_notices($post_id=false, $type, $message){
		$id = (int) $post_id;
		$notice = get_option('cn_admin_notice');
		$notice[$id][$type][] = $message;
		update_option('cn_admin_notice',$notice);
	}
	
	//Sets WP_Error for front-end notices
	//Uses WP_Error Class
	// @param str $message The notice message to display.
	// @param str $code The class for the notice, theme uses 'success' || error' || 'warning'.
	// @param arr $data 
	// @return WP_Error object
	function set_output_message($message, $code = 'success', $data = false) {
	  return new WP_Error($code, $message, $data);
	}
	
	//Displays WP_Errors for front-end notices
	//Uses WP_Error Class
	// @param obj $wp_error_obj WP_Error Class object
	function show_output_message($wp_error_obj){
		if ( is_wp_error($wp_error_obj) ) {
			foreach ((array)$wp_error_obj->errors as $message_type => $message){
				echo '<div class="'.$message_type.'">'.implode("\n\r",$message).'</div>';
			}
			//return $wp_error_obj->get_error_message();
		}
	}
	
	/**
	 ** Custom Taxonomies & Post Types
	 **/
	function create_post_type() {
		$job_labels = array(
			'name' => 'Job Postings',
			'singular_name' => 'Job Posting',
			'add_new' => 'Add New',
			'add_new_item' => 'Add New Job Posting',
			'edit_item' => 'Edit Job Posting',
			'new_item' => 'New Job Posting',
			'all_items' => 'All Job Postings',
			'view_item' => 'View Job Posting',
			'search_items' => 'Search Job Postings',
			'not_found' =>  'No jobs found',
			'not_found_in_trash' => 'No jobs found in Trash', 
			'parent_item_colon' => '',
			'menu_name' => 'Jobs'
		);
		$job_args = array(
			'labels' => $job_labels,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true, 
			'show_in_menu' => true, 
			'query_var' => true,
			'rewrite' => array( 'slug' => 'jobs' ),
			'capability_type' => 'post',
			'has_archive' => true, 
			'hierarchical' => false,
			'taxonomies' => array('category', 'post_tag'),
			'menu_position' => null,
			'supports' => array( 'title', 'editor', 'author', 'excerpt', 'revisions', 'custom-fields' )
		); 
	    register_post_type( 'cn_job', $job_args );
		
		//Define which post types users can add (as messages/announcements)
		// post, dlm_download, cn_job, bgc_event, topic (forum topic), @todo: add bp_doc?
		global $wp_post_types;
		foreach($wp_post_types as $pt)
			$pt->user_can_add = in_array($pt->name, $this->user_can_add) ? true : false;
	}

	//Filter arguments for Downloads (dlm_downloads) post type created by Download Monitor Plugin.
	function dlm_cpt_dlm_download_args($args){
		$args = array(
			'labels' => array(
					'name' 					=> __( 'Resources', 'chesnet' ),
					'singular_name' 		=> __( 'Resource', 'chesnet' ),
					'add_new' 				=> __( 'Add New', 'chesnet' ),
					'add_new_item' 			=> __( 'Add Resource', 'chesnet' ),
					'edit' 					=> __( 'Edit', 'chesnet' ),
					'edit_item' 			=> __( 'Edit Resource', 'chesnet' ),
					'new_item' 				=> __( 'New Resource', 'chesnet' ),
					'view' 					=> __( 'View Resource', 'chesnet' ),
					'view_item' 			=> __( 'View Resource', 'chesnet' ),
					'search_items' 			=> __( 'Search Resources', 'chesnet' ),
					'not_found' 			=> __( 'No Resources found', 'chesnet' ),
					'not_found_in_trash' 	=> __( 'No Resources found in trash', 'chesnet' ),
					'parent' 				=> __( 'Parent Resource', 'chesnet' )
				),
			'description' => __( 'This is where you can create and manage downloads for your site.', 'download_monitor' ),
			'public' 				=> true,
			'show_ui' 				=> true,
			'capability_type' 		=> 'post',
			'capabilities' => array(
				//'publish_posts' 		=> 'manage_downloads',
				//'edit_posts' 			=> 'manage_downloads',
				//'edit_others_posts' 	=> 'manage_downloads',
				//'delete_posts' 			=> 'manage_downloads',
				//'delete_others_posts'	=> 'manage_downloads',
				//'read_private_posts'	=> 'manage_downloads',
				//'edit_post' 			=> 'manage_downloads',
				//'delete_post' 			=> 'manage_downloads',
				//'read_post' 			=> 'manage_downloads'
			),
			'publicly_queryable' 	=> true,
			'exclude_from_search' 	=> false,
			'hierarchical' 			=> false,
			'rewrite' 				=> false,
			'query_var' 			=> 'resource',
			'supports' 				=> apply_filters( 'dlm_cpt_dlm_download_supports', array( 'title', 'editor', 'author', 'excerpt', 'thumbnail', 'custom-fields', 'comments' ) ),
			'taxonomies'			=> array('category', 'post_tag'),
			'has_archive' 			=> true,
			'show_in_nav_menus' 	=> false
		);
		return $args;
	}
	
	function bbp_register_topic_post_type ($args){
		$args['taxonomies'] = array('category', 'post_tag');
		return $args;
	}
	
	//Unregister taxonomies
	function register_taxonomy(){
		register_taxonomy('dlm_download_category', array()); //clear this from dlm plugin
		register_taxonomy('dlm_download_tag', array()); //clear this from dlm plugin
		register_taxonomy('topic-tag', array()); //clear this from bbpress plugin
		
		//if(is_admin()) register_taxonomy($this->get_dlm_tax(1), array());
		//will need to do this whenever viewing a group taxonomy in the admin screen
		if(!is_admin()) register_taxonomy($this->get_dlm_tax($this->cn_set_current_group_id()), array(), array('hierarchical' => true));
	}
	
	function register_dlm_tag_tax(){
		global $post;
		if(!is_admin()){
			if($post && in_array($post->post_type, $this->user_can_add))
				$this->cn_set_current_group_id($this->cn_get_gid_from_post_id($post->ID));

			if($this->cn_set_group_id) register_taxonomy($this->get_dlm_tax($this->cn_set_group_id), array(), array('hierarchical' => true));
		}
	}
	
	/**
	 ** Meta Boxes
	 **/
	function add_meta_boxes() {
		$jobs_boxes = array(
			//hiring organization name and website
			array('sectionid' => 'hiring_organization',
				 'title' => __('Hiring Organization', 'chesnet'),
				 'inner_func_suffix' => 'hiring_org',
				 'context' => 'side'),
			//position attachemnt file, website
			array('sectionid' => 'more_info',
				 'title' => __('Further Reading', 'chesnet'),
				 'inner_func_suffix' => 'more_info',
				 'context' => 'normal'),
			//contact info, how to apply, application status, apply by date
			array('sectionid' => 'apply',
				 'title' => __('Apply', 'chesnet'),
				 'inner_func_suffix' => 'apply',
				 'context' => 'side')
		);
		foreach ($jobs_boxes as $job_box){
			add_meta_box(
				'cn_' . $job_box['sectionid'],
				$job_box['title'],
				array($this, 'cn_inner_jobs_' . $job_box['inner_func_suffix']),
				'cn_job',
				$job_box['context']
			);
		}
		//for BP Group Calendar Plugin
		add_meta_box(
			'bgc_more_info',
			__( 'More Event Information', 'chesnet' ),
			array($this, 'bgc_event_info_mb'),
			'bgc_event',
			'normal',
			'high'
		);
		
		//for Activity Sending
		foreach($this->user_can_add as $pt){
			add_meta_box(
				'cn_announcement_sharing',
				__( 'Announcement Sharing', 'chesnet' ),
				array($this, 'cn_announcement_sharing_mb'),
				$pt,
				'side'
			);
		}
	}
	
	//Meta boxes for cn_job cpt
	function cn_inner_jobs_hiring_org( $post ) {
		// Add an nonce field so we can check for it later.
		wp_nonce_field( 'cn_inner_jobs_hiring_org', 'cn_inner_jobs_hiring_org_nonce' );
	
		$org = get_post_meta( $post->ID, '_cn_jobs_org', true );
		$orgweb = get_post_meta( $post->ID, '_cn_jobs_orgweb', true );
		
		echo '<p><label for="cn_jobs_org">Enter the hiring organization.</label>';
		echo '<input type="text" id="cn_jobs_org" name="cn_jobs_org" value="' . esc_attr( $org ) . '" style="width:99%;" /></p>';
		
		echo '<p><label for="cn_jobs_orgweb">Enter the hiring organization\'s website.</label>';
		echo '<input type="text" id="cn_jobs_orgweb" name="cn_jobs_orgweb" value="' . esc_attr( $orgweb ) . '" style="width:99%;" /></p>';
	}
	
	function cn_inner_jobs_more_info( $post ) {
		wp_nonce_field( 'cn_inner_jobs_more_info', 'cn_inner_jobs_more_info_nonce' );
	
		$file = get_post_meta( $post->ID, '_cn_jobs_announcement_file', true );
		$website = get_post_meta( $post->ID, '_cn_jobs_announcement_website', true );
	
		if($file) {
			echo "<div><strong>" . basename($file['file']) . '</strong> is currently attached.';
			echo '  <span id="delete_cn_jobs_announcement_file" class="cn_delete_file delete button hide-if-no-js" title="Delete file">Delete</span>
				<div class="hide-if-js"><input type="checkbox" name="cn_delete_file" value="1" id="cn_delete_file" /> <label for="cn_delete_file">No attachment file.</label></div></div>';
		}
		echo '<p><label for="cn_jobs_announcement_file">Provide an attachment with the job announcement (max 1).</label><br />';
		echo '<input type="file" id="cn_jobs_announcement_file" name="cn_jobs_announcement_file" value="" /></p>';
		
		echo '<p><label for="cn_jobs_announcement_website">Enter the website for the position announcement (must begin with http:// or https://).</label>';
		echo '<input type="text" id="cn_jobs_announcement_website" name="cn_jobs_announcement_website" value="' . esc_attr( $website ) . '" style="width:99%;" /></p>';
	}
	
	function cn_inner_jobs_apply( $post ) {
		wp_nonce_field( 'cn_inner_jobs_apply', 'cn_inner_jobs_apply_nonce' );
	
		$apply_by = get_post_meta( $post->ID, '_cn_jobs_apply_by_date', true );
		$status = get_post_meta( $post->ID, '_cn_jobs_status', true );
		
		echo '<p><label for="cn_jobs_apply_by_date">Enter the deadline for receiving applications.</label><br />';
		echo '<input type="text" id="cn_jobs_apply_by_date" name="cn_jobs_apply_by_date" value="'; if($apply_by) echo date('Y-m-d', strtotime($apply_by)); echo '" size="25" /><br />';
		echo '<span class="description">(YYYY-MM-DD)</span></p>';
		
		echo '<p><label for="cn_jobs_status">Job position status.</label><br />';
		echo '<select name="cn_jobs_status" id="cn_jobs_status">';
		echo '<option value="open"'; if($status == 'open') echo ' selected="selected"'; echo '>Open</option>';
		echo '<option value="closed"'; if($status == 'closed') echo ' selected="selected"'; echo '>Closed</option>';
		echo '</select></p>';
	}
	
	function bgc_event_info_mb( $post ){
		global $wpdb, $current_user, $bp, $bgc_locale;
		
		wp_nonce_field( 'bgc_event_info', 'bgc_event_info_nonce' );
		
		//variables
		$event_time = get_post_meta( $post->ID, '_bgc_event_time', true );
		$event_location = get_post_meta( $post->ID, '_bgc_event_location', true );
		$event_map = get_post_meta( $post->ID, '_bgc_event_map', true ) == 1 ? ' checked="checked"' : '';
		
		//check for valid date/time
		$tmp_date = strtotime($event_time) + ( get_option( 'gmt_offset' ) * HOUR_IN_SECONDS );
		if ($tmp_date) {
			$event_date = date('Y-m-d', $tmp_date);
			$event_hour = date('g', $tmp_date);
			$event_minute = date('i', $tmp_date);
			$event_ampm = date('a', $tmp_date);
		} else {
			$event_hour = 7;
		}
		?>
        
		<p>
            <label for="event-date"><?php _e('Date', 'groupcalendar'); ?>
            <input name="event-date" id="event-date" value="<?php echo $event_date; ?>" type="text"></label>
        </p>

		<p>
        	<label for="event-time"><?php _e('Time', 'groupcalendar'); ?> *
                <select name="event-hour" id="event-hour">
                    <?php if ($bgc_locale['time_format']==24)
                    $bgc_locale['time_format'] = 23;
                    for ($i = 1; $i <= $bgc_locale['time_format']; $i++) {
                        $hour_check = ($i == $event_hour) ? ' selected="selected"' : '';
                        echo '<option value="'.$i.'"'.$hour_check.'>'.$i."</option>\n";
                    } ?>
                </select>
                <select name="event-minute" id="event-minute">
                    <option value="00"<?php echo ($event_minute=='00') ? ' selected="selected"' : ''; ?>>:00</option>
                    <option value="15"<?php echo ($event_minute=='15') ? ' selected="selected"' : ''; ?>>:15</option>
                    <option value="30"<?php echo ($event_minute=='30') ? ' selected="selected"' : ''; ?>>:30</option>
                    <option value="45"<?php echo ($event_minute=='45') ? ' selected="selected"' : ''; ?>>:45</option>
                </select>
                <?php if ($bgc_locale['time_format']==12) : ?>
                    <select name="event-ampm" id="event-ampm">
                        <option value="am"<?php echo ($event_ampm=='am') ? ' selected="selected"' : ''; ?>>am</option>
                        <option value="pm"<?php echo ($event_ampm=='pm') ? ' selected="selected"' : ''; ?>>pm</option>
                    </select>
				<?php endif; ?>
            </label>
        </p>

		<p>
        	<label for="event-loc"><?php _e('Location', 'groupcalendar'); ?></label>
            <input name="event-loc" id="event-loc" value="<?php echo $event_location; ?>" type="text" style="width:98%">
        </p>

		<p>
        	<label for="event-map"><?php _e('Show Map Link?', 'groupcalendar'); ?>
				<input name="event-map" id="event-map" value="1" type="checkbox"<?php echo $event_map; ?> />
      			<small><?php _e('(Note: Location must be an address)', 'groupcalendar'); ?></small>
      		</label>
        </p>
		<?php
	}
	
	function cn_announcement_sharing_mb( $post ){
		//on publish, do we share this announcement with the world?
		wp_nonce_field( 'cn_post_sharing', 'cn_post_sharing_nonce' );
	
		$cn_post_sharing = get_post_meta( $post->ID, '_cn_post_sharing', true ); ?>
        
        <p>
            <label for="cn_post_sharing"><input type="checkbox" id="cn_post_sharing" name="cn_post_sharing" value="yes" <?php checked($cn_post_sharing, 'yes'); ?> /> <?php _e('Send to groups on publish?', 'chesnet'); ?></label><br />
            <span class="description"><?php _e('Save before changes are put into effect.', 'chesnet'); ?></span>
        </p>
        
        <?php
		//how many reminders are left for each group?
		$cats = wp_get_object_terms( $post->ID, 'category' );
		if(is_wp_error($cats) || empty($cats)) $cats = array(get_category( 1 ) ); //default is group 1
		
		?>
        <strong><?php _e('Reminders Left', 'chesnet'); ?></strong>
        <ul>

        	<?php foreach($cats as $cat):
			$group_id = $this->cn_get_group_from_cat($cat->term_id);
			$reminders = get_post_meta($post->ID, 'cn_reminders_left_group_' . $group_id, true);
			if(!$reminders){
				add_post_meta($post->ID, '_cn_reminders_left_group_' . $group_id, 3, true);
				$reminders = 3; //default is 3
			} ?>
                <li><label for="cn_reminders_left_group_<?php echo $group_id; ?>"><input type="text" maxlength="1" size="1" value="<?php echo $reminders ?>" name="cn_reminders_left_group_<?php echo $group_id; ?>" /> <?php echo $cat->name; ?></label></li>
            <?php endforeach; ?>
        </ul>
        
        <?php
	}
	
	// When the post is saved, saves our custom data in wp-admin
	function save_metadata( $post_id ) {
		if(!is_admin()) return;
		global $post;
		if(empty($post)) return;
		// If this is an autosave, our form has not been submitted, so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return $post_id;
		
		// Check if our nonce is set & valid
		if ( isset( $_POST['cn_post_sharing'] ) ) {
			if ( wp_verify_nonce( $_POST['cn_post_sharing_nonce'], 'cn_post_sharing' ) ) {
				
				// Check the user's permissions.
				if( current_user_can( 'edit_post', $post_id ) && isset( $_POST['post_type'] ) ){
					if ( isset( $_POST['cn_post_sharing'] ) )
						update_post_meta( $post_id, '_cn_post_sharing', 'yes' );
					else update_post_meta( $post_id, '_cn_post_sharing', 'no' );
					
					$cats = wp_get_object_terms( $post->ID, 'category' );
					if(is_wp_error($cats) || empty($cats)) $cats = array(get_category( 1 ) ); //default is group 1
					foreach($cats as $cat){
						$group_id = $this->cn_get_group_from_cat($cat->term_id);
						$reminders = intval(sanitize_text_field( $_POST['cn_reminders_left_group_' . $group_id] ));
						update_post_meta( $post_id, '_cn_reminders_left_group_' . $group_id, $reminders );
					}
				}
			}
		}
		
		if($post->post_type == 'cn_job'){
			// Check the user's permissions.
			if ( !current_user_can( 'edit_post', $post_id ) ) return $post_id;
			
			// Check if our nonces are set.
			if ( isset( $_POST['cn_inner_jobs_hiring_org_nonce'] ) && wp_verify_nonce( $_POST['cn_inner_jobs_hiring_org_nonce'], 'cn_inner_jobs_hiring_org' ) ){
				$org = sanitize_text_field( $_POST['cn_jobs_org'] );
				$orgweb = sanitize_text_field( $_POST['cn_jobs_orgweb'] );
				
				update_post_meta( $post_id, '_cn_jobs_org', $org );
				update_post_meta( $post_id, '_cn_jobs_orgweb', $orgweb );
			}
			
			if ( isset( $_POST['cn_inner_jobs_more_info_nonce'] ) && wp_verify_nonce( $_POST['cn_inner_jobs_more_info_nonce'], 'cn_inner_jobs_more_info' ) ){
				$announcement_website = esc_url( $_POST['cn_jobs_announcement_website'], array('http','https'), false );
				$this->cn_do_handle_job_file($post_id);
				update_post_meta( $post_id, '_cn_jobs_announcement_website', $announcement_website );
			}
			
			if ( isset( $_POST['cn_inner_jobs_apply_nonce'] ) && wp_verify_nonce( $_POST['cn_inner_jobs_apply_nonce'], 'cn_inner_jobs_apply' ) ){
				update_post_meta( $post_id, '_cn_jobs_status', $_POST['cn_jobs_status'] );
				
				$date = '';
				$apply_by = sanitize_title_for_query($_POST['cn_jobs_apply_by_date']); //YYYY-MM-DD
				
				if($apply_by && substr_count($apply_by, '-') == 2){
					if ($dt = DateTime::createFromFormat('Y-m-d', $apply_by)) {
						
						if(checkdate($dt->format('m'),$dt->format('d'),$dt->format('Y')) && 
						   ( ($dt->format('Y') - 10) < date('Y', strtotime($post->post_date)) ) && 
						   ( date('Y', $post->post_date) < ($dt->format('Y') + 10) )
						   ){
							//it's a date
							$date = $dt->format('Y-m-d');
						}
					}
				}
				
				//if(empty($date)) $date = $this->cn_jobs_apply_by_date_calc($post_id);
				update_post_meta( $post_id, '_cn_jobs_apply_by_date', $date );
			}
			
			return $post_id;
		}
		elseif($post->post_type == 'bgc_event'){
			// Check if our nonce is set.
			if ( !isset( $_POST['bgc_event_info_nonce'] ) || !wp_verify_nonce( $_POST['bgc_event_info_nonce'], 'bgc_event_info' ) )
				return $post_id;
			
			// Check the user's permissions.
			if ( ! current_user_can( 'edit_post', $post_id ) ) return $post_id;
		
			/* OK, its safe for us to save the data now. */
			
			// Sanitize user input.
			$tmp_date = $_POST['event-date'].' '.$_POST['event-hour'].':'.$_POST['event-minute'].$_POST['event-ampm'];
			$tmp_date = strtotime($tmp_date);
			//check for valid date/time
			if ($tmp_date && strtotime($_POST['event-date']) && strtotime($_POST['event-date']) != -1) {
				$event_date = gmdate( 'Y-m-d H:i:s', ( $tmp_date - ( get_option( 'gmt_offset' ) * HOUR_IN_SECONDS ) ) ); //assumed date entered in timezone offset. Subtract the offset to get GMT for storage.
			}
	
			$event_location = esc_attr(strip_tags(trim($_POST['event-loc'])));
			$event_map = ($_POST['event-map']==1) ? 1 : 0;
			
			// Update the meta field in the database.
			if($event_date) update_post_meta($post_id, '_bgc_event_time', $event_date);
			update_post_meta($post_id, '_bgc_event_location', $event_location);
			update_post_meta($post_id, '_bgc_event_map', $event_map);
		}
	}
	

	//Set meta tags for posts
	// @return arr The meta tags for the post type
	function cn_metaTags($post_type){
		$cn_job = array(
			'_cn_jobs_status',
			'_cn_jobs_org',
			'_cn_jobs_orgweb',
			'_cn_jobs_announcement_website',
			'_cn_jobs_announcement_file', //saving handled by cn_do_handle_job_file(), but need to keep for retrieving the meta data
			'_cn_jobs_apply_by_date'
		);
		
		$dlm_download_version = array(
			'_files',
			'_version',
			'_filesize'
		);
		
		$bgc_event = array(
			'_bgc_event_time',
			'_bgc_event_location',
			'_bgc_event_map'
		);
		
		$topic = array(
			'_bbp_forum_id',
			'_bbp_topic_id'
		);
		
		$post = $dlm_download = array();
		
		return $$post_type;
	}
	
	/**
	 ** Theme Options
	 **/
	function after_switch_theme(){
		global $wp_roles;
		$wp_roles->remove_cap( 'author', 'delete_published_posts' ); //added so users can't delete media uploads
	}
	
	function theme_customizer( $wp_customize ) {
		$wp_customize->add_section( 'chesnet_logo_section' , array(
			'title'       => __( 'Logo', 'chesnet' ),
			'priority'    => 30,
			'description' => 'Upload a logo to replace the default site name and description in the header',
		) );
		
		$wp_customize->add_setting( 'chesnet_logo' );
		
		$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'chesnet_logo', array(
			'label'    => __( 'Logo', 'chesnet' ),
			'section'  => 'chesnet_logo_section',
			'settings' => 'chesnet_logo',
		) ) );
	}
	
	function add_joyride(){
		if((is_home() || is_front_page()) && is_user_logged_in()): ?>

        <!-- At the bottom of your page but inside of the body tag -->
        <ol class="joyride-list" data-joyride>
            <li data-text="Next" data-options="prev_button: false">
                <h4>Welcome!</h4>
                <p>Hello and welcome to the newly designed Chesapeake Network.</p>
                <p>Here is just a quick tour to help you best get around. Over the next few days we'll be sending out more details about the new site.</p>
            </li>
            <li data-id="activity-container" data-text="Next" data-prev-text="Prev" data-options="tip_location:right">
                <h4>1. The Activity Stream</h4>
                <p>Whenever a member posts an announcement, it shows up here. This is composed of all announcements in your groups. Click 'Read More' to see the full post, comment, and favorite it.</p>
            </li>
            <li data-id="activity-actions-container" data-button="Next" data-prev-text="Prev" data-options="tip_location:right;tip_animation:fade">
                <h4>2. Filter The Activity Stream</h4>
                <p>We've broken down any annoucement a member can send into 5 message types. Click on any one of them (or a combination) to filter the activity stream to the right.</p>
            </li>
            <li data-id="activity-tagit" data-button="Next" data-prev-text="Prev" data-options="tip_location:bottom;tip_animation:fade">
                <h4>2. Filter The Activity Stream</h4>
                <p>Additionally, you can add to your filter by state. For example, you can sort announcements by events in DC.</p>
            </li>
            <li data-id="menu-item-31" data-button="Next" data-prev-text="Prev" data-options="tip_location:bottom;tip_animation:fade">
                <h4>3. Post An Announcement</h4>
                <p>The way we share information has been transformed on the Network. To give you control over the message, you can use the new step-by-step form.</p>
                <p>This form lets you pick the message type (events, job postings, etc) and relevant State tags. Without it, none of the filters we talked about earlier are useful.</p>
            </li>
            <li data-button="End" data-prev-text="Prev">
                <h4>That's It For Now</h4>
                <p>Thank you for your patience as we got the site ready. Please keep in mind that some of the data from the old site hasn't made it over here yet. Don't worry, it's not lost, we'll be adding it soon.</p>
                <p>More feature highlights and tutorials are coming soon. Thanks again and enjoy.</p><p>Dan Brellis<br />Alliance for the Chesapeake Bay</p>
            </li>
        </ol>
        
        <?php //elseif(): ?>
        
        <?php endif;
	}
	
	function login_enqueue_scripts(){ 
		$logo = esc_url( get_theme_mod( 'chesnet_logo' ) ); ?>
		<style type="text/css">
            body.login div#login h1 a {
                background-image: url(<?php echo $logo; ?>);
				background-size: 320px;
				width: auto;
				height: 136px;
            }
        </style>
	<?php }
	
	function login_headerurl($login_header_url){
		return get_bloginfo('url');
	}
	
	function login_headertitle($login_header_title){
		return get_bloginfo('description');
	}
	
	/**
	 ** BuddyPress
	 **/
	
	//on validating input during user signup 
	function bp_core_validate_user_signup($result){
		if(is_wp_error($result['errors'])){
			if ( array_key_exists( 'user_name', $result['errors']->errors ) ){
				$temp = $result['errors']->errors;
				unset( $temp['user_name'] );
				$result['errors']->errors = $temp;
			}
			//replace above with this when WP 4.1.0 is available!
			//$result['errors']->remove('user_name');
		}

		if(!empty($result['user_email']) && empty($result['errors']->errors['user_email']))	{
			$result['user_name'] = md5($result['user_email']);
			$_POST['signup_username'] = $result['user_name'];
		}
		
		return $result;
	}
	
	//after user is activated, immediately change username to user id
	//thanks https://github.com/sbrajesh/bp-username-changer/blob/master/bp-change-username.php
	function bp_core_activated_user($user_id, $key, $user){
		global $wpdb, $bp;
		
		$lnid = xprofile_get_field_id_from_name('Last Name');
		if(isset($user['meta']["field_" . $lnid])){
			$prefix = substr($user['meta']["field_" . $lnid], 0, 1);
		}
		if(!$prefix) $prefix = 'm';
		
		$new_user_name = strtolower($prefix . str_pad($user_id, 3, '0', STR_PAD_LEFT));
		
		$user_info = get_userdata($user_id);
		$old_login = $user_info->user_login;
		
		wp_cache_delete( $new_user_name, 'userlogins' );
		wp_cache_delete( $old_login, 'userlogins' );
		
		if(!validate_username($new_user_name) || username_exists($new_user_name)) return; //oh well
		
		/* now, update the user_login / user_nicename in the database */
		wp_update_user( array(
			'ID' => $user_id,
			'user_login' => $new_user_name,
			'user_nicename' => sanitize_title( $new_user_name )
		) );
		// manually update user_login
		$wpdb->update( $wpdb->users, array( 'user_login' => $new_user_name ), array( 'ID' => $user_id ), array( '%s' ), array( '%d' ) );
		//@todo do we need to update wp_signups?
		
		// delete object cache
		clean_user_cache( $user_id );
		wp_cache_delete( $user_id, 'users' );
		wp_cache_delete( 'bp_core_userdata_' . $user_id, 'bp' );
		wp_cache_delete( 'bp_user_username_' . $user_id, 'bp' );
		wp_cache_delete( 'bp_user_domain_' . $user_id, 'bp' );
		
	}
	
	//sets the global current group variable
	//this is necessary so that add/edit/view pages can still reference a group
	//returns the set group ID
	function cn_set_current_group_id($gid=null){
		global $bp;
		
		if($gid && $this->cn_group_exists_by_id(intval($gid))){
			$this->cn_set_group_id = (int)$gid;
			return (int)$gid;
		}
		
		if(isset($_GET['gid'])){ //$_GET is in the URL
			$gid = (int)$_GET['gid'];
			if($this->cn_group_exists_by_id($gid)){
				$this->cn_set_group_id = $gid;
				return $gid;
			}
		}
		if(isset($_POST['gid'])){ //$_POST is from a form
			$gid = (int)$_POST['gid'];
			if($this->cn_group_exists_by_id($gid)){
				$this->cn_set_group_id = $gid;
				return $gid;
			}
		}
		if(isset($bp->groups->current_group->id)){
			$this->cn_set_group_id = (int)$bp->groups->current_group->id;
			return $this->cn_set_group_id;
		}
		
		$this->cn_set_group_id = 1;
		return 1;
	}
	
	//sets the global current activity variable
	function cn_set_current_activity_id($aid=null){
		global $bp, $post;
		if($aid){
			if(cn_activity_exists_by_id($aid)){
				$this->cn_the_set_activity_id = (int)$aid;
				return;
			}
		}
		if(isset($_GET['aid'])){ //$_GET is in the URL
			$aid = (int)$_GET['aid'];
			if(cn_activity_exists_by_id($aid)){
				$this->cn_the_set_activity_id = $aid;
				return;
			}
		}
		if(isset($_POST['aid'])){ //$_POST is from a form
			$aid = (int)$_POST['aid'];
			if(cn_activity_exists_by_id($aid)){
				$this->cn_the_set_activity_id = $aid;
				return;
			}
		}
		if($post && in_array($post->post_type, $this->user_can_add)){
			$filter = array();
			$filter['object'] = 'groups';
			$filter['secondary_id'] = $post->ID;
			if($this->cn_set_group_id) $filter['primary_id'] = $this->cn_set_group_id;
			
			$a = bp_activity_get( 
				array(
					'filter' => $filter
				)
			);

			if(isset($a['activities']) && count($a['activities'])){
				$this->cn_the_set_activity_id = $a['activities'][0]->id;
				return;
			}
		}
		$this->cn_the_set_activity_id = false;
	}
	
	//checks if the supplied id is a valid buddypress group
	function cn_group_exists_by_id($gid, $table_name = false){
		global $wpdb, $bp;
		
		if ( empty( $table_name ) && isset($bp->groups->table_name) )
			$table_name = $bp->groups->table_name;
			
		return $table_name ? $wpdb->get_var( $wpdb->prepare( "SELECT id FROM {$table_name} WHERE id = %d", $gid ) ) : false;
	}
	
	function bp_init() {
		global $bp;
		$bp->activity->directory_title = __('Activity Stream', 'chesnet');
		$bp->groups->directory_title = __('Network Groups', 'chesnet');

		if(isset($bp->groups->current_group->id) && $bp->groups->current_group->id == 1) remove_action( 'bp_group_header_actions', 'bp_group_join_button' );
	}
		
	function bp_get_group_join_button( $button ) {
		//if group is 1, return false
		if(isset($button['wrapper_id'])){
			$group_id = str_replace('groupbutton-', '', $button['wrapper_id']);
			if((int) $group_id == 1) return false;
		}
		
		if(did_action( 'bp_before_group_header' ) && !did_action( 'bp_after_group_header' )) {
			
			unset($button['wrapper_class']);
			unset($button['wrapper_id']);
			$button['wrapper'] = 'li';
			return $button;
		}
		else return $button;
	}
	
	function bp_get_group_create_button( $button ) {
		//may need to put some conditions in here to make sure 
		//we only make it a tiny button on the group directory title 
		//called in buddypress/groups/groups-loop.php in theme
		$button['link_class'] = 'button tiny right';
		return $button;
	}
	
	function bp_groups_directory_header( $title ) {
		return $title . ' ' . bp_get_group_create_button();
	}
	
	function bp_setup_nav() {
		global $bp;
		if ( bp_is_group() ) {
			//bp_core_remove_subnav_item( bp_get_current_group_slug(), 'members' ); //eliminates page too, hidden with bp_get_options_nav... filter
			//bp_core_remove_subnav_item( bp_get_current_group_slug(), 'admin' ); //eliminates page too, hidden with bp_get_options_nav... filter
			bp_core_remove_subnav_item( bp_get_current_group_slug(), 'hierarchy' );
			//bp_core_remove_subnav_item(  bp_get_current_group_slug(), 'notifications' ); //eliminates page too, hidden with bp_get_options_nav... filter
			//bp_core_remove_subnav_item(  bp_get_current_group_slug(), 'request-membership' ); //eliminates page too, hidden with bp_get_options_nav... filter
			//bp_core_remove_subnav_item(  'bp_docs', 'my-account-bp_docs-create' );
		}
		if( bp_is_user() ) {
			//bp_core_remove_nav_item('settings');  //eliminates page too, hidden with bp_get_displayed_user_nav... filter
		}
	}
	
	//altered for theme's purpose from bp_group_list_admins() in bp-groups/bp-groups-template.php
	function bp_group_list_admins( $group = false ) {
		global $groups_template;
	
		if ( empty( $group ) ) {
			$group =& $groups_template->group;
		}
	
		// fetch group admins if 'populate_extras' flag is false
		if ( empty( $group->args['populate_extras'] ) ) {
			$query = new BP_Group_Member_Query( array(
				'group_id'   => $group->id,
				'group_role' => 'admin',
				'type'       => 'first_joined',
			) );
	
			if ( ! empty( $query->results ) ) {
				$group->admins = $query->results;
			}
		}
	
		if ( ! empty( $group->admins ) ) { ?>
			<ul id="group-admins" class="subtle-sidenav">
				<?php foreach( (array) $group->admins as $admin ) { ?>
					<li>
						<a href="<?php echo bp_core_get_user_domain( $admin->user_id, $admin->user_nicename, $admin->user_login ) ?>">
						<?php echo bp_core_fetch_avatar( 
							array( 
								'item_id' => $admin->user_id, 
								'email' => $admin->user_email, 
								'type' => 'thumb',
								'width' => 20,
								'height' => 20,
								'html' => true,
								'class' => 'left imgWrapLeft',
								'alt' => sprintf( 
									__( 'Profile picture of %s', 'chesnet' ),
									bp_core_get_user_displayname( $admin->user_id ) 
								) 
							)
						);
						echo bp_core_get_user_displayname( $admin->user_id ); ?></a>
					</li>
				<?php } ?>
			</ul>
		<?php } else { ?>
			<span class="activity"><?php _e( 'No Admins', 'chesnet' ) ?></span>
		<?php } ?>
	<?php
	}
	
	function bp_group_list_mods( $group = false ) {
		global $groups_template;
	
		if ( empty( $group ) ) {
			$group =& $groups_template->group;
		}
	
		// fetch group mods if 'populate_extras' flag is false
		if ( empty( $group->args['populate_extras'] ) ) {
			$query = new BP_Group_Member_Query( array(
				'group_id'   => $group->id,
				'group_role' => 'mod',
				'type'       => 'first_joined',
			) );
	
			if ( ! empty( $query->results ) ) {
				$group->mods = $query->results;
			}
		}
	
		if ( ! empty( $group->mods ) ) : ?>
	
			<ul id="group-mods" class="subtle-sidenav">
	
				<?php foreach( (array) $group->mods as $mod ) { ?>
	
					<li>
						<a href="<?php echo bp_core_get_user_domain( $mod->user_id, $mod->user_nicename, $mod->user_login ) ?>">
						<?php echo bp_core_fetch_avatar( 
							array( 
								'item_id' => $mod->user_id, 
								'email' => $mod->user_email, 
								'type' => 'thumb',
								'width' => 20,
								'height' => 20,
								'html' => true,
								'class' => 'left imgWrapLeft',
								'alt' => sprintf( 
									__( 'Profile picture of %s', 'chesnet' ),
									bp_core_get_user_displayname( $mod->user_id ) 
								) 
							)
						);
						echo bp_core_get_user_displayname( $mod->user_id ); ?></a>
                        </a>
					</li>
	
				<?php } ?>
	
			</ul>
	
	<?php else : ?>
	
			<span class="activity"><?php _e( 'No Mods', 'chesnet' ) ?></span>
	
	<?php endif;
	
	}
	
	function format_group_list_for_widget($groups, $before='', $after=''){
		$r = '';
		$cn = '';
		if($groups && is_array($groups)){
			foreach($groups as $group){
				$item = '';
				$group_url = bp_get_group_permalink($group);
				//get group avatar
				$avatar_args = array(
					'item_id'	=> $group->id,
					'object'	=> 'group',
					'type'		=> 'thumb',
					'width'		=> 20,
					'height'	=> 20,
					'html'		=> true
				);
				$avatar = bp_core_fetch_avatar($avatar_args);
				$item .= $before . '<a href="'.$group_url.'" title="'.$group->name.'">';
				$item .= $avatar;
				$item .= $group->name . '</a>';
				$item .= $after;
				
				if($group->id == 1) $cn = $item;
				else $r .= $item;
			}
		}
		return $cn . $r;
	}
	
	//buttons for .button-group in page-banner
	function bp_member_header_actions(){
		if(bp_is_my_profile()) printf( '<li><a href="%s" class="group-button">%s</a></li>', bp_loggedin_user_domain() . 'profile/edit/', __('Update Information', 'chesnet'));
	}
	
	function bp_group_header_actions(){
		$ass_group_subscribe_button_group_banner = $this->ass_group_subscribe_button_group_banner();
		if($ass_group_subscribe_button_group_banner)
			echo '<li>' . $ass_group_subscribe_button_group_banner . '</li>';
	}
	
	//add whatever classes you want to the xprofile group container
	function add_css_classes_to_field($new_classes){
		if($new_classes && is_array($new_classes)){
			add_filter( 'bp_field_css_classes', function($classes) use (&$new_classes) { 
				return array_merge($new_classes, $classes);
			} );
		}
	}
	
	//add visibility switches when editting profile
	//currently requires dropdown items to be created elsewhere
	function bp_get_the_profile_field_name($field_name){
		global $field;
		if($field->visibility_level == 'public') $icon = 'unlock';
		elseif($field->visibility_level == 'loggedin') $icon = 'lock';
		else $icon = 'x';
		
		$m = sprintf( __( 'This field can be seen by: <strong>%s</strong>.', 'chesnet' ), bp_get_the_profile_field_visibility_level_label() );
		
		if ( bp_current_user_can( 'bp_xprofile_change_field_visibility' ) ){
			$m .= '<br />' . __('Click the visibililty icon to change its setting.', 'chesnet');
			return '<span data-tooltip aria-haspopup="true" class="has-tip tip-top" title="'.$m.'"><i class="fi-'.$icon.' imgWrapLeft" data-dropdown="visdrop-'.bp_get_the_profile_field_input_name().'" aria-controls="visdrop-'.bp_get_the_profile_field_input_name().'" aria-expanded="false"></i>' . $field_name . '</span>';
		}
		else{
			return '<span data-tooltip aria-haspopup="true" class="has-tip tip-top" title="'.$m.'"><i class="fi-'.$icon.' imgWrapLeft"></i>' . $field_name . '</span>';
		}
	}
	
	function add_visibility_switch_to_profile_field_name(){
		add_filter( 'bp_get_the_profile_field_name', array($this, 'bp_get_the_profile_field_name') );
	}
	
	//filter for message button args
	function bp_get_send_message_button_args($button){
		unset($button['wrapper_class']);
		unset($button['wrapper_id']);
		$button['wrapper'] = 'li';
		$button['link_class'] = 'group-button send-message';
		return $button;
		
		return array(
			'id'                => 'private_message',
			'component'         => 'messages',
			'must_be_logged_in' => true,
			'block_self'        => true,
			'wrapper_id'        => 'send-private-message',
			'link_href'         => bp_get_send_private_message_link(),
			'link_title'        => __( 'Send a private message to this user.', 'buddypress' ),
			'link_text'         => __( 'Private Message', 'buddypress' ),
			'link_class'        => 'send-message'
		);
		return $args;
	}
	
	//Set default gravatar for groups
	function bp_group_gravatar_default($user_default){
		$group_gravatar_default = get_option( 'avatar_default_group' );
		return !$group_gravatar_default ? $user_default : urldecode($group_gravatar_default);
	}
	function group_default_avatar(){
		?>
        <input name="avatar_default_group" type="text" id="avatar_default_group" value="<?php echo urldecode(get_option( 'avatar_default_group' )); ?>" class="regular-text ltr" placeholder="ie 'identicon' or 'http://example.org/gravatar/groups.jpg'"><br /><span class="description"><?php _e('Leave blank to use deafult avatar (above).', 'chesnet'); ?></span>
        <?php
	}
	
	//change gravatar host for URL
	function bp_gravatar_url($host){
		return 'http://www.gravatar.com/avatar/';
	}
	
	//run after a user joins a group successfully
	function groups_join_group($group_id, $user_id){
		//make the user subscribe to the group's forum board
		$forum_ids = bbp_get_group_forum_ids($group_id);
		if(isset($forum_ids) && is_array($forum_ids)) {
			foreach($forum_ids as $forum_id) bbp_add_user_forum_subscription($user_id, $forum_id);
		}
	}
	
	//filter the group classes
	function bp_get_group_class($classes){
		$hidden = array_search('hidden', $classes);
		if($hidden) $classes[$hidden] = 'hidden-group';
		return $classes;
	}
	
	//Filter buddypress ajax activity querystring
	public function activity_querystring_filter( $query_string = '', $object = '' ) {
		if( $object != 'activity' )
			return $query_string;

		// You can easily manipulate the query string
		// by transforming it into an array and merging
		// arguments with these default ones
		$args = wp_parse_args( $query_string, array(
			'action'  => false,
			'type'    => false,
			'user_id' => false,
			'page'    => 1
		) );

		/* tagged item */
		$tagged_action = 'bp_tagged_item';
		$actions = explode(',', $args['action']);
		$tags = array();
		
		foreach($actions as $key => $value){
			if(strpos($value, $tagged_action) !== false){
				$tag_str = str_replace($tagged_action . ':', '', $value);
				$tags_ar = explode(';', $tag_str);
				$tags = array_merge($tags, $tags_ar);
				unset($actions[$key]);
			}
		}
		if(isset($_GET['afilter']) && $_GET['afilter'] == 1 && isset($_GET['tags'])){
			$get_tags = explode(',', $_GET['tags']);
			$tags = array_merge($tags, $get_tags);
		}
		
		
		//clean out our custom action from $args['action'] && $args['type']
		$args['action'] = implode(',', $actions);
		$args['type'] = $args['action'];

		if(!empty($tags)){
			// on user's profile, shows the most favorited activities for displayed user
			if( bp_is_user() )
				$args['user_id'] = bp_displayed_user_id();
	 
			// Add activity id filter
			/* this is to make it so that you can select a meta query OR message type (opposite of what we want)
			add_filter( 'bp_activity_get_where_conditions', array($this, 'bp_activity_get_where_conditions'), 10, 5 );
			$args['meta_query'] = array(
				'relation' => 'OR',
				array(
					'key'     => 'activity_tag',
					'value'   => array('maryland'),
					'compare' => 'IN'
				),
				array(
					'key'     => 'foobar',
					'compare' => 'NOT EXISTS'
				)
			);
			*/
			
			//get the tag ids
			$args['meta_query'] = array();
			$args['meta_query']['relation'] = 'AND';
			
			foreach(array_unique($tags) as $tag){
				$term = get_term_by( 'name', $tag, 'post_tag' );
				$args['meta_query'][$term->term_id] = array (
					'key'     => 'post_tag_id',
					'value'   => $term->term_id,
					'compare' => '='
				);
				//$tag_ids[] = (int)$term->term_id;
			}
			
			
			
			
			//used if we want an activity with any (not all) of the tags
			/*$args['meta_query'] = array(
				'relation' => 'AND',
				array(
					'key'     => 'post_tag_id',
					'value'   => $tag_ids,
					'type'    => 'numeric',
					'compare' => '='
				),
			);*/
			

			$query_string = empty( $args ) ? $query_string : $args;
		}
//var_dump($query_string);
		return $query_string;
	}
	
	//used in above function to filter the bp activity where conditions
	function bp_activity_get_where_conditions($where_conditions, $r, $select_sql, $from_sql, $join_sql){
		//echo '<pre>'; var_dump($where_conditions); echo '</pre>';

		if($where_conditions['filter_sql'] && $where_conditions['meta_query_sql']){
			
			$meta_query_sql = $where_conditions['meta_query_sql'];
			$filter_sql = $where_conditions['filter_sql'];
			unset($where_conditions['meta_query_sql'], $where_conditions['filter_sql']);
			$where_conditions['filter_sql'] = "((" . $meta_query_sql . ") OR ( " . $filter_sql . "))";
			
		}
		//echo '<pre>'; var_dump($where_conditions); echo '</pre>';
		return $where_conditions;
	}
	
	function add_tag_it_field(){ ?>
        <div class="cn-tagit" id="activity-tagit">
    		<input type="text" class="cn-tagit-input bp-filter-activity-by-tags" id="activity-tag-filter" placeholder="<?php _e('Filter activity by tag.', 'chesnet'); ?>" />
    	</div> <?php 
	}
	
	function bp_after_activity_loop(){ ?>
    	<div data-alert class="alert-box warning">
            <h4>Hey... where's the rest?</h4>
            <p style="margin-bottom:0">As we transfer data from the old platform to the new one, we're taking baby steps. If it seems like something is missing, we'll likely be added it over the next few weeks. If you need something in particular, feel free to ask.</p>
        </div>
    <?php
	}
	
	//adds the accept & reject email aliases when a post is created via the DirectAdmin API
	function add_DirectAdmin_email_alias($post_id, $post, $update){
		$sock = $this->open_DA_socket();
		$this->add_email_alias($sock, 'accept-' . $post_id);
		$this->add_email_alias($sock, 'reject-' . $post_id);
	}
	
	/**
	 ** X-Profile & Users
	 **/
	//validate recaptcha on registration
	function bp_signup_pre_validate(){
		global $bp;
		$error = true;
		
		if(isset($_POST['g-recaptcha-response'])){
			$captcha = $_POST['g-recaptcha-response'];
			if($captcha){
				$response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$this->recaptcha_secretkey."&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
				$r = json_decode($response);
				if($r->success === true) $error = false;
			}
		}
		
		if($error){
			$bp->signup->errors['g-recaptcha'] = __('Please check the captcha box to prove you are a human', 'chesnet');
			bp_core_add_message( __('Please check the captcha box to prove you are a human', 'chesnet'), 'error' );
		}

	}
	
	function bp_before_registration_submit_buttons(){
		echo '<div class="g-recaptcha" style="margin-bottom:14px" data-sitekey="'.$this->recaptcha_sitekey.'"></div>';
	}
	
	//replacement function for bp_profile_get_visibility_radio_buttons() to format like foundation
	function cn_bp_profile_visibility_radio_buttons( $args = '' ) {
		echo $this->cn_bp_profile_get_visibility_radio_buttons( $args );
	}
		function cn_bp_profile_get_visibility_radio_buttons($args){
			// Parse optional arguments
			$r = bp_parse_args( $args, array(
				'field_id'     => bp_get_the_profile_field_id(),
				'before'       => '<ul class="radio">',
				'after'        => '</ul>',
				'before_radio' => '<li class="%s">',
				'after_radio'  => '</li>',
				'class'        => 'bp-xprofile-visibility'
			), 'xprofile_visibility_radio_buttons' );
	
			// Empty return value, filled in below if a valid field ID is found
			$retval = '';
	
			// Only do-the-do if there's a valid field ID
			if ( ! empty( $r['field_id'] ) ) :
	
				// Start the output buffer
				ob_start();
	
				// Output anything before
				echo $r['before']; ?>
	
				<?php if ( bp_current_user_can( 'bp_xprofile_change_field_visibility' ) ) : ?>
	
					<?php foreach( bp_xprofile_get_visibility_levels() as $level ) : ?>
	
						<?php printf( $r['before_radio'], esc_attr( $level['id'] ) ); ?>
	
						<input type="radio" id="<?php echo esc_attr( 'see-field_' . $r['field_id'] . '_' . $level['id'] ); ?>" name="<?php echo esc_attr( 'field_' . $r['field_id'] . '_visibility' ); ?>" value="<?php echo esc_attr( $level['id'] ); ?>" <?php checked( $level['id'], bp_get_the_profile_field_visibility_level() ); ?> /><label for="<?php echo esc_attr( 'see-field_' . $r['field_id'] . '_' . $level['id'] ); ?>"><span class="field-visibility-text"><?php echo esc_html( $level['label'] ); ?></span></label>
	
						<?php echo $r['after_radio']; ?>
	
					<?php endforeach; ?>
	
				<?php endif;
	
				// Output anything after
				echo $r['after'];
	
				// Get the output buffer and empty it
				$retval = ob_get_clean();
			endif;
	
			return apply_filters( 'bp_profile_get_visibility_radio_buttons', $retval, $r, $args );
		}
			
	/**
	 ** WP Routes and Callbacks
	 **/
	function wp_router_generate_routes( WP_Router $router ) {
		remove_filter('wp_title', 'bp_modify_page_title');
		remove_filter('wp_title', 'bbp_title');
		$router->add_route( 'cn-add-announcement', array(
			'path' => '^' . 'announcement(s?)' . '/' . 'add' . '$',
			'query_vars' => array(),
			'page_callback' => array($this, 'add_announcement_callback'),
			'page_arguments' => array(),
			'access_callback' => true,
			'title' => __('Add Announcement', 'chesnet'),
			'title_callback' => array($this, 'announcement_page_title'),
			'title_arguments' => array(false, __('Add ', 'chesnet')),
			'template' => array(
				 'page.php',
				dirname( __FILE__ ) . '/page.php'
			)
		) );
		
		$router->add_route( 'cn-edit-announcement', array(
			'path' => '^' . 'announcement(s?)' . '/' . 'edit' . '/(.*?)$',
			'query_vars' => array(
				 'id' => 2
			),
			'page_callback' => array($this, 'edit_announcement_callback'),
			'page_arguments' => array(
				 'id'
			),
			'access_callback' => true,
			'title' => __('Edit Announcement', 'chesnet'),
			'title_callback' => array($this, 'announcement_page_title'),
			'title_arguments' => array(false, __('Edit ', 'chesnet')),
			'template' => array(
				 'page.php',
				dirname( __FILE__ ) . '/page.php'
			)
		) );
		
		$router->add_route('cn-add-job', array(
			'path' => '^' . 'jobs' . '/' . 'add' . '$', 
			'query_vars' => array(
				'cn_post_type' => 'cn_job'
			),
			'page_callback' => array($this, 'add_announcement_callback'),
			'page_arguments' => array(
				'cn_post_type'
			),
			'access_callback' => TRUE,
			'title' => __('Add Job Announcement', 'chesnet'),
			'title_callback' => array($this, 'announcement_page_title'),
			'title_arguments' => array('cn_job', __('Add ', 'chesnet')),
			'template' => array(
				 'page.php',
				dirname( __FILE__ ) . '/page.php'
			)
		));
		
		$router->add_route( 'cn-edit-job', array(
			'path' => '^' . 'jobs' . '/' . 'edit' . '/(.*?)$',
			'query_vars' => array(
				 'cn_job_id' => 1
			),
			'page_callback' => array($this, 'edit_announcement_callback'),
			'page_arguments' => array(
				 'cn_job_id'
			),
			'access_callback' => true,
			'title' => __('Edit Job Announcement', 'chesnet'),
			'title_callback' => array($this, 'announcement_page_title'),
			'title_arguments' => array('cn_job', __('Edit ', 'chesnet')),
			'template' => array(
				 'page.php',
				dirname( __FILE__ ) . '/page.php'
			)
		) );
		
		$router->add_route('cn-add-news', array(
			'path' => '^' . 'posts' . '/' . 'add' . '$',
			'query_vars' => array(
				'post_type' => 'post'
			),
			'page_callback' => array($this, 'add_announcement_callback'),
			'page_arguments' => array(
				'post_type'
			),
			'access_callback' => TRUE,
			'title' => __('Add Post', 'chesnet'),
			'title_callback' => array($this, 'announcement_page_title'),
			'title_arguments' => array('post', __('Add ', 'chesnet')),
			'template' => array(
				 'page.php',
				dirname( __FILE__ ) . '/page.php'
			)
		));
		
		$router->add_route( 'cn-edit-news', array(
			'path' => '^' . 'posts' . '/' . 'edit' . '/(.*?)$',
			'query_vars' => array(
				 'cn_news_id' => 1
			),
			'page_callback' => array($this, 'edit_announcement_callback'),
			'page_arguments' => array(
				 'cn_news_id'
			),
			'access_callback' => true,
			'title' => __('Edit Post', 'chesnet'),
			'title_callback' => array($this, 'announcement_page_title'),
			'title_arguments' => array('post', __('Edit ', 'chesnet')),
			'template' => array(
				 'page.php',
				dirname( __FILE__ ) . '/page.php'
			)
		) );
		
		$router->add_route('cn-add-dlm', array(
			'path' => '^' . 'resources' . '/' . 'add' . '$',
			'query_vars' => array(
				'post_type' => 'dlm_download'
			),
			'page_callback' => array($this, 'add_announcement_callback'),
			'page_arguments' => array(
				'post_type'
			),
			'access_callback' => TRUE,
			'title' => __('Add Resource', 'chesnet'),
			'title_callback' => array($this, 'announcement_page_title'),
			'title_arguments' => array('dlm_download', __('Add ', 'chesnet')),
			'template' => array(
				 'page.php',
				dirname( __FILE__ ) . '/page.php'
			)
		));
		
		$router->add_route( 'cn-edit-dlm', array(
			'path' => '^' . 'resources' . '/' . 'edit' . '/(.*?)$',
			'query_vars' => array(
				 'dlm_id' => 1
			),
			'page_callback' => array($this, 'edit_announcement_callback'),
			'page_arguments' => array(
				 'dlm_id'
			),
			'access_callback' => true,
			'title' => __('Edit Resource', 'chesnet'),
			'title_callback' => array($this, 'announcement_page_title'),
			'title_arguments' => array('dlm_download', __('Edit ', 'chesnet')),
			'template' => array(
				 'page.php',
				dirname( __FILE__ ) . '/page.php'
			)
		) );
		
		$router->add_route('cn-add-topic', array(
			'path' => '^' . 'questions' . '/' . 'add' . '$',
			'query_vars' => array(
				'pt' => 'topic'
			),
			'page_callback' => array($this, 'add_announcement_callback'),
			'page_arguments' => array(
				'pt'
			),
			'access_callback' => TRUE,
			'title' => __('Add Question', 'chesnet'),
			'title_callback' => array($this, 'announcement_page_title'),
			'title_arguments' => array('topic', __('Add ', 'chesnet')),
			'template' => array(
				 'page.php',
				dirname( __FILE__ ) . '/page.php'
			)
		));
		
		$router->add_route( 'cn-edit-topic', array(
			'path' => '^' . 'questions' . '/' . 'edit' . '/(.*?)$',
			'query_vars' => array(
				 'topic_id' => 1
			),
			'page_callback' => array($this, 'edit_announcement_callback'),
			'page_arguments' => array(
				 'topic_id'
			),
			'access_callback' => true,
			'title' => __('Edit Questions', 'chesnet'),
			'title_callback' => array($this, 'announcement_page_title'),
			'title_arguments' => array('topic', __('Edit ', 'chesnet')),
			'template' => array(
				 'page.php',
				dirname( __FILE__ ) . '/page.php'
			)
		) );
		
		$router->add_route('cn-add-event', array(
			'path' => '^' . 'events' . '/' . 'add' . '$',
			'query_vars' => array(
				'post_type' => 'bgc_event'
			),
			'page_callback' => array($this, 'add_announcement_callback'),
			'page_arguments' => array(
				'post_type'
			),
			'access_callback' => TRUE,
			'title' => __('Add Calendar Event', 'chesnet'),
			'title_callback' => array($this, 'announcement_page_title'),
			'title_arguments' => array('bgc_event', __('Add ', 'chesnet')),
			'template' => array(
				 'page.php',
				dirname( __FILE__ ) . '/page.php'
			)
		));
		
		$router->add_route( 'cn-edit-event', array(
			'path' => '^' . 'events' . '/' . 'edit' . '/(.*?)$',
			'query_vars' => array(
				 'event_id' => 1
			),
			'page_callback' => array($this, 'edit_announcement_callback'),
			'page_arguments' => array(
				 'event_id'
			),
			'access_callback' => true,
			'title' => __('Edit Calendar Event', 'chesnet'),
			'title_callback' => array($this, 'announcement_page_title'),
			'title_arguments' => array('bgc_event', __('Edit ', 'chesnet')),
			'template' => array(
				 'page.php',
				dirname( __FILE__ ) . '/page.php'
			)
		) );
		
		//add_filter('wp_title', 'bp_modify_page_title');
	}
	
	//Callback Functions for Router Pages
	function add_announcement_callback($cn_post_type=null){
		if (!isset($_SESSION)) session_start();
		if(isset($_SESSION['ID'])) session_unset();
		$this->cn_removeFilters();
		$pt = false;
		
		if(in_array($cn_post_type, $this->user_can_add)) $pt = $cn_post_type;
		return $this->do_multistep_post_form(null, $pt);
	}
	function edit_announcement_callback($id){
		if (!isset($_SESSION)) session_start();
		$this->cn_removeFilters();
		if ( $post = get_post($id) ){
			if(in_array($post->post_type, $this->user_can_add)){
				if(isset($_SESSION['ID']) && $_SESSION['ID'] !== (int)$id) session_unset();
				$this->currentPost = $post;
				return $this->do_multistep_post_form($id, $post->post_type);
			}
		}
		return 'Not found.';
	}
	
	function add_job_callback( ) {
		$this->cn_removeFilters();
		return $this->do_post_form(null, 'cn_job');
	}
	
	function edit_job_callback( $id ) {
		$this->cn_removeFilters();
		
		if ( get_post_type($id) == 'cn_job' ){
			$this->currentPost = get_post($id);
			return $this->do_post_form( $id, 'cn_job' );
		}
		else return 'Not found.';
	}
	
	function add_news_callback( ) {
		$this->cn_removeFilters();
		return $this->do_post_form(null, 'post');
	}
	
	function edit_news_callback( $id ) {
		$this->cn_removeFilters();
		
		if ( get_post_type($id) == 'post' ){
			$this->currentPost = get_post($id);
			return $this->do_post_form( $id, 'post' );
		}
		else return 'Not found.';
	}
	
	function add_dlm_callback( ) {
		$this->cn_removeFilters();
		return $this->do_post_form(null, 'dlm_download');
	}
	
	function edit_dlm_callback( $id ) {
		$this->cn_removeFilters();
		
		if ( get_post_type($id) == 'dlm_download' ){
			$this->currentPost = get_post($id);
			return $this->do_post_form( $id, 'dlm_download' );
		}
		else return 'Not found.';
	}
	
	//Callback Function for Router Titles
	function announcement_page_title($pt = false, $before = false, $after = false){
		if(empty($_SESSION['step']) || (isset($_SESSION['step']) && $_SESSION['step'] == 1)) return false;
		
		$post_type = (!$pt && isset($_SESSION['post_type'])) ? $_SESSION['post_type'] : $pt;
		
		return $before . cn_get_activity_type($post_type, 'singular_name') . $after;
	}
	
	/**
	 ** Paths & Redirects
	 **/
	 
	//Get a file's path
	// @param str $template_path The path, excluding leading and ending slashes.
	// @param str $template_file The file name, with extension.
	function getTemplatePath( $template_path, $template_file ) {
		if ( file_exists( get_stylesheet_directory() . '/' . $template_path . '/' . $template_file ) ) {
			return get_stylesheet_directory() . '/' . $template_path . '/' . $template_file ;
		}
	}
	
	//Alter the main query
	function pre_get_posts($query){
		return;
		if ( $query->is_main_query() && !is_admin() ) {
			if(is_super_admin() && $query->query_vars['post_type'] == 'dlm_download'){
				echo '<pre>';
				var_dump($query);
				echo '</pre>';
			}
		}
	}
	
	//add rewrite rules for custom post type
	//thank you http://vocecommunications.com/blog/2010/11/adding-rewrite-rules-for-custom-post-types/
	function add_rewrite_rules() {
		global $wp_rewrite;
		
		/* Don't need this check because pretty permalinks are required by buddypress
		if('' == get_option('permalink_structure') || !$post_type_args->publicly_queryable) {
		  return; //only continue if using permalink structures and post type is publicly queryable
		}
		*/
		
		//For dlm_download post type
		$permalink_prefix = 'resources';
		$permalink_structure = '%year%/%monthnum%/%day%/%resource%/';
		$query_var = 'resource';
		
		//register the rewrite tag to use for the post type
		$wp_rewrite->add_rewrite_tag('%'.$query_var.'%', '([^/]+)', $query_var . '=');
		
		//we use the WP_Rewrite class to generate all the endpoints WordPress can handle by default.
		$rewrite_rules = $wp_rewrite->generate_rewrite_rules($permalink_prefix.'/'.'%year%/%monthnum%/%day%/%'.$query_var.'%/', EP_ALL, true, true, true, true, true);
		
		//build a rewrite rule from just the prefix to be the base url for the post type
		$rewrite_rules = array_merge($wp_rewrite->generate_rewrite_rules($permalink_prefix), $rewrite_rules);
		$rewrite_rules[$permalink_prefix.'/?$'] = 'index.php?paged=1';
		foreach($rewrite_rules as $regex => $redirect) {
			if(strpos($redirect, 'attachment=') === false) {
				//add the post_type to the rewrite rule
				$redirect .= '&post_type=' . 'dlm_download';
			}
		
		    //turn all of the $1, $2,... variables in the matching regex into $matches[] form
		    if(0 < preg_match_all('@\$([0-9])@', $redirect, $matches)) {
				for($i = 0; $i < count($matches[0]); $i++) {
					$redirect = str_replace($matches[0][$i], '$matches['.$matches[1][$i].']', $redirect);
				}
		    }
			//add the rewrite rule to wp_rewrite
			$wp_rewrite->add_rule($regex, $redirect, 'top');
		}
	}
	
	//If the page is an archive for a post, set the query parameters
	function alter_archive_query( $query ){
		if ( is_post_type_archive('cn_job') && isset($query->query['post_type']) && $query->query['post_type']  == 'cn_job' ) {
			 if(isset($_GET['author'])){
				 $author = $_GET['author'];
			 	 $query->set( 'post_author', get_current_user_id() );
			 }
		}
	}
	
	function append_query_string( $url, $post=false ) {
		if(is_admin()) return $url;
		if ( $post instanceof WP_Post ) {
			if($post->post_type == 'bp_doc') return $url;
		}
		return add_query_arg( 'gid', $this->cn_set_group_id, $url );
		
		//maybe we don't need this?
		$taxonomies = get_object_taxonomies( $post->post_type );
		if ( in_array('category', $taxonomies) ) {
			$url = add_query_arg( 'gid', $this->cn_set_group_id, $url );
		}
		return $url;
	}
	
	function cpt_link($permalink, $post){
		//thank you http://vocecommunications.com/blog/2010/11/adding-rewrite-rules-for-custom-post-types/
		if(('dlm_download' == $post->post_type) && '' != $permalink && !in_array($post->post_status, array('draft', 'pending', 'auto-draft')) ) {
			$permalink_prefix = 'resources';
			$permalink_structure = '%year%/%monthnum%/%day%/%resource%/';
			$query_var = 'resource';
			
			$rewritecode = array(
				'%year%',
				'%monthnum%',
				'%day%',
				'%hour%',
				'%minute%',
				'%second%',
				'%post_id%',
				'%author%',
				'%'.$query_var.'%'
			);
		
			$author = '';
			if ( strpos($permalink_structure, '%author%') !== false ) {
				$authordata = get_userdata($post->post_author);
				$author = $authordata->user_nicename;
			}
		
			$unixtime = strtotime($post->post_date);
			$date = explode(" ",date('Y m d H i s', $unixtime));
			$rewritereplace = array(
				$date[0],
				$date[1],
				$date[2],
				$date[3],
				$date[4],
				$date[5],
				$post->ID,
				$author,
				$post->post_name,
			);
			$permalink = str_replace($rewritecode, $rewritereplace, '/'.$permalink_prefix.'/'.$permalink_structure);
			$permalink = user_trailingslashit(home_url($permalink));
		}
		
		return $this->append_query_string($permalink, $post);
	}
	
	function term_link_filter($url, $term, $taxonomy){
		if($taxonomy == 'post_tag'){
			//direct to 
			$server = strpos($_SERVER['REQUEST_URI'], 'admin-ajax') !== false ? $_SERVER['HTTP_REFERER'] : false;
			$base = bp_is_activity_component() ? $server : get_bloginfo('url');
			$old_tags = array();
			if(isset($_GET['afilter']) && $_GET['afilter'] == 1 && isset($_GET['tags'])){
				$old_tags = explode(',', $_GET['tags']);
			}
			$old_tags[] = $term->name;
			$url = add_query_arg(
				array(
					'afilter' => '1',
					'tags' => implode(',', array_unique(array_map("urlencode", $old_tags)))
				),
				$base
			);
		}
		return $url;
	}
	
	//simply used to set the post details to a global variable inside the wp_insert_post function
	//these variables are used later in wp_unique_post_slug_is_bad_flat_slug
	function wp_insert_post_set_global_post_data($pp, $post_ID, $corrected_args, $user_args){
		$this->wp_insert_post_post_ID = $post_ID;
		$this->wp_insert_post_post_args = $corrected_args;
		
		return $pp;
	}
	
	function wp_unique_post_slug_is_bad_flat_slug($return, $slug, $post_type){
		//return true if anotehr post on the same day, month & year exists
		global $wpdb;
		
		//only for dlm_downloads
		if($post_type == 'dlm_download'){
			$check_sql = "SELECT post_name FROM $wpdb->posts WHERE post_name = %s AND post_type = %s AND ID != %d AND DATE_FORMAT(post_date,'%%Y-%%m-%%d') = %s";
			$return = $wpdb->get_var( $wpdb->prepare( $check_sql, $slug, $post_type, $this->wp_insert_post_post_ID, date('Y-m-d', $this->wp_insert_post_post_args['post_date']) ) );
		}
		
		return $return;
	}
	
	/**
	 ** WP Admin
	 **/
	 
	//Change post form in backend to allow file uploads
	function post_edit_form_tag() {  
		echo ' enctype="multipart/form-data"';  
	} 
	
	//block access to backend  and hide admin bar for non administrator roles
	function admin_init() {
		//Settings
		add_settings_field( 
			'avatar_default_group',
			__('Group Default Avatar', 'chesnet'),
			array($this, 'group_default_avatar'),
			'discussion',
			'avatars'
		);
		register_setting( 'discussion', 'avatar_default_group', 'urlencode' );
		
		//Block backend to non admins
		if (!current_user_can('manage_options') && !defined('DOING_AJAX') ){
			wp_redirect(site_url());
			exit();
		}
	}
	
	function after_setup_theme() {
		//hide toolbar on front-end, save for admin
		if (!current_user_can('administrator') && !is_admin())
			show_admin_bar(false);
	}
	
	
	/**
	 ** Posts, Pages, Forms, Images & Other Content
	 **/
	
	//allow additional file types to be uploaded
	function upload_mimes($mime_types){
		$mime_types['pub'] = 'application/x-mspublisher'; //Microsoft Publisher
		$mime_types['wmf'] = 'image/x-wmf'; //Windows MetaFile
		$mime_types['htm'] = 'text/html'; //Hypertext Markup Language
		$mime_types['html'] = 'text/html'; //Hypertext Markup Language
		$mime_types['kml'] = 'application/vnd.google-earth.kml+xml'; //Google Earth Keyhole Markup Language
		$mime_types['kmz'] = 'application/vnd.google-earth.kmz'; //Google Earth Keyhole Markup Language Archive
		$mime_types['eps'] = 'application/postscript'; //Encapsulated PostScript
		$mime_types['ai'] = 'application/postscript'; //Adobe Illustrator file
		$mime_types['indd'] = 'application/x-indesign'; //Adobe Indesign file
		$mime_types['psd'] = 'application/octet-stream'; //Adobe Photoshop file
		$mime_types['swf'] = 'application/x-shockwave-flash'; //Shockwave Flash
		return $mime_types;
	}

	
	//modifys the html class for the image when added to a post/page
	function image_tag_class($class_name){
		$class_name = $class_name . ' img-responsive';
		return $class_name;
	}
	
	//Adds links to add a post, view archive of $post_type, and view current user's archive of $post_type
	//So called because it adds hyperlinks to "more", "my" and "add".
	function cn_more_add_my_links(){
		$post_type = get_query_var('post_type');
		switch ($post_type) {
			case 'cn_job':
				$lay = "Job Announcement";
				break;
			case 'dlm_download':
				$lay = "Download";
				break;
			default:
			   $lay = "Message";
			   $post_type = 'post';
		}
		$mine = $this->cn_getUrl( 'list', null, null, $post_type, get_current_user_id() );
		$all = $this->cn_getUrl( 'list', null, null, $post_type, null ); ?>
		
		<p><a href="<?php echo $mine; ?>">My <?php echo $lay; ?>s</a> <a href="<?php echo $all; ?>">All <?php echo $lay; ?>s</a></p>
		<p><a href="<?php echo $this->cn_getUrl( 'add', null, null, $post_type, null ); ?>">Add <?php echo $lay; ?></a></p>
		
		<?php
	}
	
	//Frontend add/edit post type form
	//this is a multistep form
	function do_multistep_post_form( $id = null, $post_type = null ) {
		global $bp, $current_user;

		//$id = 126;
		if (!isset($_SESSION)) session_start();
		
		$output		= '';
		$show_form	= true;
		$post_saved = false;
		
		if(isset($_GET['cs']) && $_GET['cs'] == 'true') session_unset();
		
		if($id && !isset($_SESSION['ID'])) session_unset();
		
		if($id && isset($_SESSION['ID'])) $id = $_SESSION['ID'];
		if($id && isset($_GET['mid']) && $_GET['mid'] !== $id){
			unset($id);
			session_unset();
		}
		if(isset($_GET['mid'])) $id = $_GET['mid'];
		
		if($post_type && isset($_SESSION['pt']) && $_SESSION['pt'] !== $post_type){
			session_unset();
		}

		if(isset($_GET['mid'])) $id = $_GET['mid'];
		
		$this->cn_set_current_group_id();
		$_SESSION['gid'] = $this->cn_set_group_id;
		
		//first validate any submitted data
		foreach ($_POST as $key => $value) {
			if($key == 'cn-post-form' || $key == 'cn-post-form-back') continue;
			$_SESSION[$key] = $value;
		}
		
		if(isset($_POST['cn-post-form-back']) && $_POST['cn-post-form-back'] && wp_verify_nonce( $_POST['cn_form_submission_nonce'], 'cn_form_submission' )){
			$step = (int)$_SESSION['prev_step'] - 1;
			$prev_step = $step > 1 ? $step - 1 : 1;
			$p_t = $_SESSION['post_type'];
			if($step == 1) session_unset();
			$_SESSION['step'] = $step;
			$_SESSION['prev_step'] = $prev_step;
			$_SESSION['post_type'] = $p_t;
		}

		//priority for post_type: $_SESSION['pt'] > $_SESSION['post_type'] > $post_type
		$true_pt = '';
		if(isset($_SESSION['pt']) && in_array($_SESSION['pt'], $this->user_can_add))
			$true_pt = $_SESSION['pt'];
		elseif(isset($_SESSION['post_type']) && in_array($_SESSION['post_type'], $this->user_can_add))
			$true_pt = $_SESSION['post_type'];
		elseif($post_type && in_array($post_type, $this->user_can_add))
			$true_pt = $post_type;
		
		$step = isset($_SESSION['step']) ? $_SESSION['step'] : '';
		if(empty($true_pt)) $step = 1;
		else $step = ($step == 1 && $post_type) ? 2 : $step;
		$_SESSION['pt'] = $_SESSION['post_type'] = $post_type = $true_pt;
		$_SESSION['step'] = $step; unset($step);

		//Determine which integer to assign to the step
		if (empty($_SESSION['step'])) {
			$_SESSION['step'] = 2; //if the post_type is set, but the step isn't, we'll defaul to step 2
		}

		//Set up $post object
		if ( $id ) {
			$post_to_edit = get_post( intval( $id ) );
			if($post_to_edit){
				global $post;
				$old_post = $post;
				
				$post = $post_to_edit;
				$post_type = $post->post_type;
				$edit = true;
				if($_SESSION['step'] == 1) $_SESSION['step'] = 2;
				$_SESSION['ID'] = $post->ID;
				$this->currentPost = get_post($post);
			}
			else unset($id);
		}
		
		if(!$id) {
			$post = new stdClass();
			if ( isset( $_POST[ 'post_title' ] ) )
				$post->post_title = $_POST[ 'post_title' ];
			else $post->post_title = '';
			if ( isset( $_POST[ 'post_content' ] ) )
				$post->post_content = $_POST[ 'post_content' ];
			else $post->post_content = '';
			$post->post_type = $post_type;
			$edit = false;
		}
		$_SESSION['post_type'] = $post_type;
		$_SESSION['step'] = (int)$_SESSION['step'];
		if($_SESSION['step'] > 1) $_SESSION['prev_step'] = $_SESSION['step'] - 1;
		
		//Run through our permission checks
		if ( $edit && !$post->ID ) {
			//if we're editing a post but the id is bad, bail
			bp_core_add_message( $post_type_obj->labels->singular_name . ' ' . __('not found.', 'chesnet'), 'error' );
			$show_form = false;
		}

		// login check
		if ( !is_user_logged_in() ) return $this->cn_login_form($post->post_type); //@todo: allow ability to post w/o a login?
		// security check
		if ( $edit && !$this->cn_userCanEdit( $post ) ) {
			bp_core_add_message( sprintf(__('You do not have permission to edit this %s', 'chesnet'), $post_type_obj->labels->singular_name), 'error' );
			$show_form = false;
			return $output;
		}
		
		//$this->loadScripts = true;
		do_action( 'cn_before_submission_page', $post_type );
		$output .= '<div id="cn-post-form" class="form">';
		
		if ($post_type) {
			$post_type_obj = get_post_type_object( $post_type );
			//to see if we can put this post in a category, first check if a category was selected in $_POST
			//otherwise, see if it has 'category' taxonomy enabled
			$taxonomy_names = get_object_taxonomies( $post_type );
			if ( isset($_SESSION[ 'cn_group_ids' ]) ) $group_ids = $_SESSION['cn_group_ids'];
			elseif(in_array('category', $taxonomy_names)){
				$cat_slugs = wp_get_post_categories($id, array('fields' => 'slugs'));
				if($cat_slugs && is_array($cat_slugs)){
					foreach($cat_slugs as $cat_slug){
						
						$group_ids[] = $this->cn_gid_from_cat_slug($cat_slug);
					}
				}
			}
			if(empty($group_ids) && empty($_SESSION['cn_group_ids_option'])) $group_ids[] = $this->cn_set_group_id;
			$_SESSION['cn_group_ids'] = $group_ids;
			if(!$group_ids) $_SESSION['cn_group_ids'] = array();
		}
		else $post_type_obj = false;
		
		if(isset($_POST['cn_post_tag_ids_option']) && empty($_POST['cn_post_tag_ids'])) $_SESSION['cn_post_tag_ids'] = array();
		if(isset($_POST['cn_group_ids_option']) && empty($_POST['cn_group_ids'])) $_SESSION['cn_group_ids'] = array();
			
		//if(empty($_POST['cn_group_ids']) && isset($_SESSION['cn_group_ids_option'])) $_SESSION['cn_group_ids'] = array();
		
		//process form
		if(isset($_POST['cn-post-form']) && wp_verify_nonce( $_POST['cn_form_submission_nonce'], 'cn_form_submission' )){ 
			if(isset($_SESSION[ 'cnpostcontent' ])) {
				$_SESSION[ 'post_content' ] = $_SESSION[ 'cnpostcontent' ]; //wp_editor doesn't support underscores in the id
			}
			
			//validate STEP variables
			$step_check = call_user_func_array(array($this, 'validate_post_form_step' . $_SESSION['prev_step']), array($post_type_obj));
			
			if(!$step_check){
				$_SESSION['step'] = (int)$_SESSION['prev_step'];
				$_SESSION['prev_step'] = $_SESSION['step'] > 1 ? $_SESSION['step'] - 1 : 1;
			}
			
			if($_SESSION['step'] == 4){ //process final form
				do_action('cn_before_save_post_form', $_SESSION, $post_type);
				$_SESSION = apply_filters('cn_before_save_post_form', $_SESSION);
				
				if ( $id ) {
					if( $this->cn_updatePost( $id, $_SESSION ) ) {
						$post_saved = true;
						bp_core_add_message( $post_type_obj->labels->singular_name . ' ' . __('updated.', 'chesnet') );
						$output .= $this->cn_getEditButton( get_post( $id ), $post_type_obj->labels->edit_item );
						$output .= ' | <a href="' . $this->cn_getUrl( 'add', null, null, $post_type ) . '">' . $post_type_obj->labels->add_new_item . '</a>';
						$show_form = false;
					} else {
						bp_core_add_message( sprintf( __('There was a problem saving your %s, please try again', 'chesnet'), $post_type_obj->labels->singular_name), 'error' );
					}
				} else {
					if ( is_user_logged_in() || ( isset( $_POST[ 'naes' ] ) && $_POST[ 'naes' ] == 1 && empty( $_POST[ 'aes' ] ) ) ) {
						$_SESSION[ 'post_status' ] = 'pending'; //default - this may change depending on the group selected
	
						$id = $this->cn_createPost($_SESSION);
	
						if ( $id ) {
							$post_saved = true;
							bp_core_add_message( sprintf(__('%s submitted.', 'chesnet'), $post_type_obj->labels->singular_name) );
							
							$post = get_post( $id );
							if ( $this->cn_userCanEdit( $post ) && is_user_logged_in() )
								$output .= $this->cn_getEditButton( $post, $post_type_obj->labels->edit_item ) . ' ';
	
							$output .= ' | <a href="' . $this->cn_getUrl( 'add', null, null, $post_type ) . '">' . $post_type_obj->labels->add_new_item . '</a>';
	
							$show_form = false;
						} else {
							bp_core_add_message( sprintf( __('There was a problem submitting your %s, please try again', 'chesnet'), $post_type_obj->labels->singular_name), 'error' );
						}
					} else {
						if ( ( isset( $_POST[ 'naes' ] ) && $_POST[ 'naes' ] != 1 ) || !isset( $_POST[ 'naes' ] ) ) {
							bp_core_add_message(__('Please check the box below to prove you are not an evil spammer', 'chesnet'), 'warning');
						} else {
							bp_core_add_message(sprintf('There was a problem submitting your %s, please try again', $post_type_obj->labels->singular_name), 'error');
						}
					}
				}
				
				do_action('cn_after_process_post_form', $_SESSION, $post_type, $post_saved, $id);
			}
			else {
				if($_SESSION['step'] > 3) $_SESSION['step'] = (int)$_SESSION['prev_step'];
			}
		}
		elseif($_SESSION['step'] > 3) {
			session_unset();
			$show_form = true;
			return $this->do_multistep_post_form();
		}
					
		if ( $edit && ($this->cn_userCanEdit( $post ) || is_user_logged_in()) )
			$output .= '<div style="clear:left"></div>';
			
		if($post_type_obj) $show_form = apply_filters( $post_type_obj->name . '_show_form', $show_form );
		else $show_form = apply_filters( 'new_post_show_form', $show_form );
		if ( $show_form ) {
			//get data from $_POST and override core function
			do_action( 'cn_before_step'.$_SESSION['step'].'_form' );
			ob_start();
			
			include($this->getTemplatePath( 'views', 'cn-post-form.php' ));
			
			$output .= ob_get_clean();
		}
		else{
			if($post_saved && $id) {
				$this->cn_set_current_group_id($this->cn_get_gid_from_post_id($id));
				
				session_unset(); //clear session data to avoid duplicate entries from 'refresh'
				
				//rediret to show newly created post
				$redirect_url = get_post_permalink($id);
				
				wp_redirect( apply_filters('redirect_after_multistep_form_save', $redirect_url, $id, $post->post_type) );
				
				exit();
			}
		}
		
		$output .= '</div>';

		wp_reset_query();
		if(WP_DEBUG === true && false){
			echo '<pre>';
			var_dump($_SESSION);
			echo '</pre><br><br>';
			var_dump($_FILES);
		}
		return $output;
	}
	
	function validate_post_form_step1($pto = false){
		if($_SESSION['post_type'] && in_array($_SESSION['post_type'], $this->user_can_add))
			return true;
		else{
			bp_core_add_message( __('You must select a message type', 'chesnet'), 'error' );
			return false;
		}
	}
	
	function validate_post_form_step2($post_type_obj){
		$error = 0;
		if ( empty( $_SESSION[ 'post_title' ] ) ) {
			bp_core_add_message( sprintf( __('The %s title is required', 'chesnet'), $post_type_obj->labels->singular_name), 'error' );
			$error = 1;
		}
		if ( empty( $_SESSION[ 'post_content' ] ) ){
			bp_core_add_message( sprintf( __('The %s description is required', 'chesnet'), $post_type_obj->labels->singular_name), 'error' );
			$error = 1;
		}
		
		//file upload
		if($_FILES && is_array($_FILES)){
			foreach($_FILES as $input_name => $file_data){
				if($file_data['error'] == 4 && $file_data['name'] == ''){
					//unset($_SESSION['uploads'][$input_name]);
					continue;	
				}
				//check for errors
				if($file_data['error'] == 0){ //no error
					//move the file to the tmp_uploads folder in wp-content/uploads
					$tmp_name = $file_data['tmp_name'];
					$name = $file_data['name'];
					$name_ar = explode('.',$name);
					$ext = array_pop($name_ar);
					if (!file_exists($this->getUploadsDir('basedir') . '/tmp-uploads')) {
						mkdir($this->getUploadsDir('basedir') . '/tmp-uploads', 0777, true);
					}
					$new_name = $this->getUploadsDir('basedir') . '/tmp-uploads/' . sanitize_title(implode('.',$name_ar)) . '.' . $ext;
					if(move_uploaded_file($tmp_name, $new_name)){
						//store new file path into session
						$_SESSION['uploads'][$input_name] = $new_name;
					}
					else{
						bp_core_add_message( __('File could not be stored to the server.', 'chesnet'), 'warning' );
						$error = 1;
					}
				} else{
					bp_core_add_message( __('File Upload Error: ', 'chesnet') . $this->error_codes[$file_data['error']], 'warning' );
					$error = 1;
				}
			}
		}
		//unset($_FILES);
		
		//Validate metadata
		if($post_type_obj->name == 'bgc_event'){
			//make sure date is set and in valid format
			if(empty($_SESSION['event-date'])){
				bp_core_add_message( __('The event date is required', 'chesnet'), 'warning' );
				$error = 1;
			} else{
				$date = $_SESSION['event-date'];
				if($date && substr_count($date, '-') == 2){
					if ($dt = DateTime::createFromFormat('Y-m-d', $date)) {
						if(checkdate($dt->format('m'),$dt->format('d'),$dt->format('Y')) && 
						   ( ($dt->format('Y') - 11) < date('Y') )
						   ){
							//it's a date
							$valid_date = $dt->format('Ymd');		
						}
					}
				}
				if($date && !$valid_date){
					bp_core_add_message( __('The date must be in a valid format (YYYY-MM-DD) and not exceed 10 years from today', 'chesnet'), 'warning' );
					$error = 1;
				}
			}
			//if location isn't set, eliminate event-map
			if(empty($_SESSION['event-loc'])) unset($_SESSION['event-map']);
		}
		elseif($post_type_obj->name == 'cn_job'){
			$date = $_SESSION['cn_jobs_apply_by_date'];
			if($date && substr_count($date, '-') == 2){
				if ($dt = DateTime::createFromFormat('Y-m-d', $date)) {
					if(checkdate($dt->format('m'),$dt->format('d'),$dt->format('Y')) && 
					   ( ($dt->format('Y') - 11) < date('Y') )
					   ){
						//it's a date
						$valid_date = $dt->format('Ymd');		
					}
				}
				
			}
			if($date && !$valid_date){
				bp_core_add_message( __('The date must be in a valid format (YYYY-MM-DD) and not exceed 10 years from today', 'chesnet'), 'warning' );
				$error = 1;
			}
			if(!empty($_SESSION['cn_jobs_announcement_website'])){
				if (filter_var($_SESSION['cn_jobs_announcement_website'], FILTER_VALIDATE_URL) == false){
					bp_core_add_message( __('Please enter a valid URL for the announcement website', 'chesnet'), 'warning' );
					$error = 1;
				}
			}
			if(!empty($_SESSION['cn_jobs_orgweb'])){
				if (filter_var($_SESSION['cn_jobs_orgweb'], FILTER_VALIDATE_URL) == false){
					bp_core_add_message( __('Please enter a valid URL for the organization website', 'chesnet'), 'warning' );
					$error = 1;
				}
			}
			
			
		}
		
		if($error == 1) return false;
		else return true;
	}
	
	function validate_post_form_step3($post_type_obj){
		if(empty($_SESSION['cn_post_sharing'])){
			bp_core_add_message( sprintf(__('Please select a sharing option for your %s.', 'chesnet'), $post_type_obj->labels->singular_name), 'warning' );
			return false;
		}
		
		if(empty($_SESSION['cn_group_ids'])){
			bp_core_add_message( sprintf(__('You must select a group to which your %s is posted.', 'chesnet'), $post_type_obj->labels->singular_name), 'warning' );
			return false;
		}
		
		return true;
	}
	
	//Action before post form is submitted
	function before_post_updated($args){
		//make sure all <a> tags open in a new tab
		if(isset($args['post_content']) && $args['post_content']){
			$args['post_content'] = preg_replace('/<a (href=".*?").*?>/', '<a $1 target="_blank">', stripslashes($args['post_content']));
		}
		
		return $args;
	}
			
	//Action after post form is submitted
	function cn_after_process_post_form($form_info, $post_type, $post_saved, $id=null){
		if($post_type == 'dlm_download'){
			if($post_saved == true && $id){
				$dlm_download_version = $form_info['dlm_download_version'];
				if($dlm_download_version && is_array($dlm_download_version)){
					foreach($dlm_download_version as $version_id => $version_data){
						$upload = isset($form_info['uploads'][$version_id . '-dlm_new_upload']) ? $form_info['uploads'][$version_id . '-dlm_new_upload'] : false;
						$cn_update_dlm_version = $this->cn_update_dlm_version($version_id, $version_data, $upload, $id);
					}
				}
				
				//delete transients of version ids to account for menu_order-ing
				delete_transient( 'dlm_file_version_ids_' . $id );
			}
		}
		
		if($post_type == 'topic'){
			if($post_saved == true && $id){
				$post = get_post($id);
				if($post){
					$forum_id = bbp_get_topic_forum_id( $id );
					//update topic's forum's meta
					bbp_update_forum( array(
						'forum_id'           => $forum_id,
						'last_topic_id'      => $id,
						'last_active_id'     => $id,
						'last_active_time'   => $post->post_date
					) );
				}
			}
		}
		
	}
	
	//add media button dropdown html
	function cn_after_step2_form(){ ?>
    	<div id="media_btn_explain" data-dropdown-content class="f-dropdown content open medium" aria-hidden="false" tabindex="-1">
          <strong><?php _e('Add images and other attachments to your post.', 'chesnet'); ?></strong>
        </div>
    <?php
	}
	
	//add to the bottom of the post form
	function cn_submission_form_bottom($post = null){
		if(!$post) $post = $this->cn_get_post();
		if($post) {
			echo '<input type="hidden" name="post_ID" id="post_ID" value="'.$post->ID.'" />';
		
			if($post->post_type == 'dlm_download'){
				$this->cn_set_current_group_id();
				echo '<input type="hidden" name="gid" id="group_id" value="'.$this->cn_set_group_id.'" />';
			}
		}
	}
	
	//Implement SPAM Control on forms
	function cn_formSpamControl(){
		return;
	}
	
	//Displays wp_editor textarea
	function cn_formContentEditor( $content=null, $visual=false, $name='cnpostcontent', $args=false ) {
		// if the admin wants the rich editor, and they are using WP 3.3, show the WYSIWYG, otherwise default to just a text box
		$defaults = array(
			'wpautop' => true,
			'media_buttons' => true,
			'editor_class' => 'frontend',
			'textarea_rows' => 5,
			'tabindex' => 2
		);
		$settings = wp_parse_args($args, $defaults);
		if ( $visual && function_exists( 'wp_editor' ) ) {
			wp_editor(  (isset($content) ? html_entity_decode(stripslashes($content)) : '') , $name, $settings );
		} else {
			?><textarea tabindex="2" id="<?php echo $name; ?>" name="<?php echo $name; ?>" placeholder="<?php echo $this->wp_editor_placeholder; ?>"><?php if ( isset( $content ) ) echo esc_textarea( $content ); ?></textarea><?php
		}
	}
	
	//Filter the wp_editor html
	function the_editor($editor){
		$placeholder = $this->wp_editor_placeholder;
		return $placeholder ? str_replace('<textarea', '<textarea placeholder="'.$placeholder.'"', $editor) : $editor;
	}
	
	///Caller function for post details
	function cn_formPostDetails( $post = null, $post_type ) {
		if ( isset( $post->ID ) && $post->ID ) {
			$this->cn_PostChooserBox( $post, $post->post_type );
		} else {
			$this->cn_PostChooserBox( null, $post_type );
		}
		if ( !$post ) {
			if ( isset( $old_post_id ) ) {
				$post->ID = $old_post_id;
			}
		}
	}
	
	///Adds a style chooser to the write post page (sets up meta fields)
	function cn_PostChooserBox($post = null, $post_type) {
		if(isset($post->ID)){
			$postId = $post->ID;
		}else{
			$postId = 0;
		}
		
		if($post->post_type) $post_type = $post->post_type;
			
		$metaTags = $this->cn_metaTags($post_type);
		if(!$metaTags || !is_array($metaTags)) return;
		
		foreach ( $metaTags as $tag ) {
			if ( $postId ) { //if there is a post AND the post has been saved at least once.
			
				// Sort the meta to make sure it is correct for recurring events
				
				$meta = get_post_meta($postId,$tag);
				$tag = substr($tag, 1);
				sort($meta);
				if (isset($meta[0])) { $$tag = $meta[0]; }
			} else {
				$cleaned_tag = str_replace('_cn_','',$tag);
				
				//allow posted data to override default data
				if( isset($_POST['cn_'.$cleaned_tag]) ){
					$$tag = stripslashes_deep($_POST['cn_'.$cleaned_tag]);
				}else{
					$$tag = "";
				}
			}
		}
		
		if($post_type == 'cn_job') $cn_meta_box_template = $this->getTemplatePath( 'post-type-forms', 'job-meta-box.php' );
		elseif($post_type == 'dlm_download_version') $cn_meta_box_template = $this->getTemplatePath( 'post-type-forms', 'dlm-meta-box.php' );
		include( $cn_meta_box_template );
	}
	
	
	function get_post_form_template($post = null, $post_type, $folder){
		if(isset($post->ID)) $post_id = $post->ID;
		else $post_id = 0;

		if($post && $post->post_type) $post_type = $post->post_type;
			
		$metaTags = $this->cn_metaTags($post_type);
		
		if($metaTags && is_array($metaTags)){
			foreach ( $metaTags as $tag ) {
				if ( $post_id ) { //if there is a post AND the post has been saved at least once.
					$meta = get_post_meta($post_id,$tag);
					$tag = ltrim($tag, '_');
					if (isset($meta[0])) { $$tag = $meta[0]; }
				} else {
					$tag = ltrim($tag, '_');
					$$tag = "";
				}
			}
		}
		
		//meta data fixing
		if($post_type == 'bgc_event'){
			//check for valid date/time
			$tmp_date = strtotime($bgc_event_time) + ( get_option( 'gmt_offset' ) * HOUR_IN_SECONDS );
			
			if ($tmp_date && !empty($bgc_event_time)) {
				$bgc_event_date = date('Y-m-d', $tmp_date);
				$bgc_event_hour = date('g', $tmp_date);
				$bgc_event_minute = date('i', $tmp_date);
				$bgc_event_ampm = date('a', $tmp_date);
			} else {
				$tmp_date = time();
				$tmp_date = (ceil($tmp_date / (15 * 60)) * (15 * 60)) + ( get_option( 'gmt_offset' ) * HOUR_IN_SECONDS );
				if(empty($_SESSION['event-date']) && isset($_GET['event-date'])) $_SESSION['event-date'] = $_GET['event-date'];
				else $bgc_event_date = date('Y-m-d', $tmp_date);
				$bgc_event_hour = date('g', $tmp_date);
				$bgc_event_minute = date('i', $tmp_date);
				$bgc_event_ampm = date('a', $tmp_date);
			}
			
			
			$bgc_event_map = $bgc_event_map == 1 ? ' checked="checked"' : '';
			//however
			if($_POST && empty($_POST['event-map'])) $bgc_event_map = '';
		}
		elseif($post_type == 'cn_job'){
			if(empty($_SESSION['uploads']['cn_jobs_announcement_file']) && isset($cn_jobs_announcement_file['file'])) $_SESSION['uploads']['cn_jobs_announcement_file'] = $cn_jobs_announcement_file['file'];
			if(empty($cn_jobs_announcement_file) || !is_array($cn_jobs_announcement_file)) $cn_jobs_announcement_file['file'] = '';
			$cn_jobs_apply_by_date = $cn_jobs_apply_by_date ? esc_attr(date('Y-m-d', strtotime($cn_jobs_apply_by_date))) : '';
		}
		
		include( $this->getTemplatePath( $folder, $post_type . '-form.php' ) );
		
	}
	
	//Displays a list of checkboxes or radio inputs for the groups a user is in
	function cn_get_user_groups_chk($selected=false, $args = false, $input = 'radio'){
		$groups = $this->cn_get_user_groups( $args );
		if(!$selected) $selected = array();
		elseif(!is_array($selected)) $selected = array($selected);
		
		if(!empty($groups)){
			$ul = '<ul class="large-block-grid-3 medium-block-grid-4 small-block-grid-2">';
			$html = '';
			$group_ids = array(1);
			$include_cn = false;
			foreach($groups['groups'] as $group) {
				if(in_array($group->id, $selected)) $group_ids[] = (int)$group->id;
				if($group->id == 1){
					$include_cn = true;
					continue;
				}
				else {
					$checked = in_array($group->id,$selected) ? ' checked="checked"' : '';
					$html .= '<li><div class="btn-switch chk" title="'.$group->name.'"><input value="'.$group->id.'" type="'.$input.'" id="cn_post_group-'.$group->id.'" name="cn_group_ids[]"'.$checked.' /><label for="cn_post_group-'.$group->id.'">'.$group->name.'</label></li>';
				}
			}
			if($include_cn){
				$cn_checked = in_array('1',$selected) ? ' checked="checked"' : '';
				$html = '<li><div class="btn-switch chk" title="'.get_bloginfo('name').'"><input value="1" type="'.$input.'" id="cn_post_group-1" name="cn_group_ids[]"'.$cn_checked.' /><label for="cn_post_group-1">'.get_bloginfo('name').'</label></li>' . $html;
			}
			$html .= '</ul><input type="hidden" name="cn_previous_groups_user" value="'.implode(',',$group_ids).'" />';
			$html .= '<input type="hidden" name="cn_previous_groups" value="'.implode(',',$selected).'" />';
			return '<div class="post-groups">' . $ul . $html . '</div>';
		}
		else return;
	}
	
	//@deprecated in lieu of cn_get_user_groups_chk()
	//Displays a list of checkboxes for the groups a user is in
	function cn_get_user_groups_checkbox($selected=false, $args = false){
		$groups = $this->cn_get_user_groups( $args );
		if(!$selected) $selected = array();
		elseif(!is_array($selected)) $selected = array($selected);
		
		if(!empty($groups)){
			$html = '<ul class="large-block-grid-3 medium-block-grid-4 small-block-grid-2">';
			$group_ids = array(1);
			$include_cn = false;
			foreach($groups['groups'] as $group) {
				if(in_array($group->id, $selected)) $group_ids[] = (int)$group->id;
				if($group->id == 1){
					$include_cn = true;
					continue;
				}
				else {
					$checked = in_array($group->id,$selected) ? ' checked="checked"' : '';
					$html .= '<li><div class="btn-switch chk" title="'.$group->name.'"><input value="'.$group->id.'" type="checkbox" id="cn_post_group-'.$group->id.'" name="cn_group_ids[]"'.$checked.' /><label for="cn_post_group-'.$group->id.'">'.$group->name.'</label>';
				}
			}
			if($include_cn){
				$cn_checked = in_array('1',$selected) ? ' checked="checked"' : '';
				$html = '<input value="1" type="checkbox" id="cn_post_group-1" name="cn_group_ids[]"'.$cn_checked.' /><label for="cn_post_group-1">'.get_bloginfo('name').'</label>' . $html;
			}
			$html .= '</ul><input type="hidden" name="cn_previous_groups_user" value="'.implode(',',$group_ids).'" />';
			$html .= '<input type="hidden" name="cn_previous_groups" value="'.implode(',',$selected).'" />';
			return '<div class="post-groups">' . $html . '</div>';
		}
		else return;
	}
	
	//@deprecated in lieu of cn_get_user_groups_chk()
	//Displays a list of radios for the groups a user is in
	function cn_get_user_groups_radio($selected=false, $args = false){
		$groups = $this->cn_get_user_groups( $args );
		if(!$selected) $selected = '';
		elseif(is_array($selected)) $selected = (int)$selected[0];
		if(!empty($groups)){
			$html = '';
			$group_ids = array(1);
			$include_cn = false;
			foreach($groups['groups'] as $group) {
				if($group->id == $selected) $group_ids[] = (int)$group->id;
				if($group->id == 1){
					$include_cn = true;
					continue;
				}
				else {
					$checked = $group->id == $selected ? ' checked="checked"' : '';
					$html .= '<label for="cn_post_group-'.$group->id.'">'.$group->name.'<input value="'.$group->id.'" type="radio" id="cn_post_group-'.$group->id.'" name="cn_group_ids[]"'.$checked.' /></label>'."\r\n";
				}
			}
			if($include_cn){
				$cn_checked = '1' == $selected ? ' checked="checked"' : '';
				$html = '<label for="cn_post_group-1">'.get_bloginfo('name').'</label><input value="1" type="radio" id="cn_post_group-1" name="cn_group_ids[]"'.$cn_checked.' />' . $html;
			}
			$html .= '<input type="hidden" name="cn_previous_groups_user" value="'.implode(',',$group_ids).'" />';
			$html .= '<input type="hidden" name="cn_previous_groups" value="'.$selected.'" />';
			return '<div class="post-groups">' . $html . '</div>';
		}
		else return;
	}
	
	//returns an array of groups for the current user
	function cn_get_user_groups($args = false){
		$user_id = bp_loggedin_user_id() ? bp_loggedin_user_id() : false;
		if(!$user_id) return false;
		$defaults = array(
			'type'            => false,    // active, newest, alphabetical, random, popular, most-forum-topics or most-forum-posts
			'order'           => 'DESC',   // 'ASC' or 'DESC'
			'orderby'         => 'name', // date_created, last_activity, total_member_count, name, random
			'user_id'         => $user_id, // Pass a user_id to limit to only groups that this user is a member of
			'include'         => false,    // Only include these specific groups (group_ids)
			'exclude'         => false,    // Do not include these specific groups (group_ids)
			'search_terms'    => false,    // Limit to groups that match these search terms
			'meta_query'      => false,    // Filter by groupmeta. See WP_Meta_Query for syntax
			'show_hidden'     => true,    // Show hidden groups to non-admins
			'per_page'        => false,    // The number of results to return per page
			'page'            => false,    // The page to return if limiting per page
			'populate_extras' => false,    // Fetch meta such as is_banned and is_member
		);
		$args = wp_parse_args( $args, $defaults );
		$groups = groups_get_groups( $args );
		return $groups;
	}
	
	//Displays a list of radios for the available post_tag taxonomy
	function cn_get_post_tag_checkbox($selected=false, $args = false){
		$html = '';
		$tags_grouped = array();
		if(!$selected) $selected = array();
		elseif(!is_array($selected)) $selected = array($selected);

		$defaults = array(
			'hide_empty'	=> false,
			'get'			=> 'all'
		);
		$args = wp_parse_args( $args, $defaults );
		$tags_array = get_tags( $defaults );
		$tag_group_labels = get_option( 'tag_group_labels', array() );

		if($tags_array && is_array($tags_array)){
			foreach($tags_array as $tag) $tags_grouped[$tag->term_group][] = $tag;
			
			foreach($tags_grouped as $tag_group => $tag_obj_arr){
				if(is_array($tag_obj_arr)){
					$html .= '<fieldset><legend>';
					$html .= $tag_group == 0 ? '' : $tag_group_labels[$tag_group]; 
					$html .= '</legend><ul class="small-block-grid-2 medium-block-grid-4 large-block-grid-3 ">';
    				foreach($tag_obj_arr as $tag_obj){
						$checked = in_array($tag_obj->term_id, $selected) ? ' checked="checked"' : '';
						$html .= '<li><div class="btn-switch chk" title="'.$tag_obj->name.'"><input value="'.$tag_obj->term_id.'" type="checkbox" id="cn_post_tag_id-'.$tag_obj->term_id.'" name="cn_post_tag_ids[]"'. $checked .' /><label for="cn_post_tag_id-'.$tag_obj->term_id.'">'.$tag_obj->name.'</label></div></li>';
					}
  					$html .= '</ul></fieldset>';
				}
				
			}
		}
		return $html;
	}
	
	//Displays the html and content for the tagsinput fields
	function cn_get_tagit_form($post_id = null, $taxonomy, $tags = null){
		$output = '';
		if(taxonomy_exists($taxonomy)){
			if(!$tags) $value = $post_id ? implode(',', wp_get_object_terms( $post_id, $taxonomy, array('fields'=> 'names'))) : '';
			else $value = $tags;
			$output .= '<div class="tagsdiv" id="post_tag">
				<div class="ajaxtag">
					<label class="screen-reader-text" for="new-tag-'.$taxonomy.'">'. __('Label resource', 'chesnet') . ' <span class="howto">'. __('(seperate with commas)', 'chesnet') . '</span></label>
					<p><input type="text" id="cn-tagit-input" name="newtag['.$taxonomy.']" class="newtag form-input-tip" size="16" autocomplete="off" value="' . $value . '" />
				</div>
			</div>';
		}
		else $output = false;
		return $output;
	}
	
	function comments_open( $open, $post_id ) {
		$post = get_post( $post_id );
		if(!$post) return false;
		
		$no_commenting = array('wp_router_page', 'cn_job', 'forum', 'topic', 'reply', 'page', 'bp_doc');
		
		if ( in_array($post->post_type, $no_commenting) )
			$open = false;
		
		//comments closed if post isn't published
		if($post->post_status !== 'publish')
			$open = false;
		
		return $open;
	}
	
	function comment_form( $args = array(), $post_id = null ) {
		if ( null === $post_id )
			$post_id = get_the_ID();
	 
		$commenter = wp_get_current_commenter();
		$user = wp_get_current_user();
		$user_identity = $user->exists() ? $user->display_name : '';
	 
		$args = wp_parse_args( $args );
		if ( ! isset( $args['format'] ) )
			$args['format'] = current_theme_supports( 'html5', 'comment-form' ) ? 'html5' : 'xhtml';
	 
		$req      = get_option( 'require_name_email' );
		$aria_req = ( $req ? " aria-required='true'" : '' );
		$html5    = 'html5' === $args['format'];
	 
		$required_text = sprintf( ' ' . __('Required fields are marked %s', 'chesnet'), '<span class="required">*</span>' );
		
		ob_start();
		include $this->getTemplatePath( 'views', 'comment-field.php' );
		$comment_field = ob_get_clean();
	 
		$defaults = array(
			'comment_field'        => $comment_field,
			'must_log_in'          => '<p class="must-log-in">' . sprintf( __( 'You must be <a href="%s">logged in</a> to post a comment.' ), wp_login_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '</p>',
			'logged_in_as'         => '<p class="logged-in-as">' . sprintf( __( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>' ), get_edit_user_link(), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '</p>',
			'comment_notes_before' => '<p class="comment-notes">' . __( 'Your email address will not be published.' ) . ( $req ? $required_text : '' ) . '</p>',
			'comment_notes_after'  => '<p class="form-allowed-tags">' . sprintf( __( 'You may use these <abbr title="HyperText Markup Language">HTML</abbr> tags and attributes: %s' ), ' <code>' . allowed_tags() . '</code>' ) . '</p>',
			'id_form'              => 'commentform',
			'id_submit'            => 'submit',
			'name_submit'          => 'submit',
			'title_reply'          => __( 'Leave a Reply' ),
			'title_reply_to'       => __( 'Leave a Reply to %s' ),
			'cancel_reply_link'    => __( 'Cancel reply' ),
			'label_submit'         => __( 'Post Comment' ),
			'submit_field'		   => '<p class="form-submit"><input name="%s" type="submit" id="%s" value="%s" /></p>',
			'format'               => 'xhtml',
		);
	 
		$args = wp_parse_args( $args, apply_filters( 'comment_form_defaults', $defaults ) );
	 
		?>
		<?php if ( comments_open( $post_id ) ) : ?>
            <?php
            do_action( 'comment_form_before' );
            ?>
            <div id="respond" class="comment-respond clearfix">
                <div class="reply-header clearfix">
                	<strong id="reply-title" class="left comment-reply-title"><?php comment_form_title( $args['title_reply'], $args['title_reply_to'] ); ?></strong>
                	<div class="right"><?php cancel_comment_reply_link( $args['cancel_reply_link'] ); ?></div>
                </div>
                <?php if ( get_option( 'comment_registration' ) && !is_user_logged_in() ) : ?>
                    <?php echo $args['must_log_in']; ?>
                    <?php do_action( 'comment_form_must_log_in_after' );  ?>
                <?php else : ?>
                    <form action="<?php echo site_url( '/wp-comments-post.php' ); ?>" method="post" id="<?php echo esc_attr( $args['id_form'] ); ?>" class="comment-form"<?php echo $html5 ? ' novalidate' : ''; ?>>
                        <?php do_action( 'comment_form_top' ); ?>
                        <?php if ( is_user_logged_in() ) : ?>
                            <?php echo apply_filters( 'comment_form_logged_in', $args['logged_in_as'], $commenter, $user_identity ); ?>
                            <?php do_action( 'comment_form_logged_in_after', $commenter, $user_identity ); ?>
                        <?php else : ?>
                            <?php echo $args['comment_notes_before']; ?>
                            <?php do_action( 'comment_form_before_fields' );
                            foreach ( (array) $args['fields'] as $name => $field ) {
                                echo apply_filters( "comment_form_field_{$name}", $field ) . "\n";
                            }
                            do_action( 'comment_form_after_fields' ); ?>
                        <?php endif; ?>
                        <?php 
						echo apply_filters( 'comment_form_field_comment', $args['comment_field'] );
                        echo $args['comment_notes_after'];
                        printf($args['submit_field'],
							esc_attr( $args['name_submit'] ),
							esc_attr( $args['id_submit'] ),
							esc_attr( $args['label_submit'] )
						);
                        comment_id_fields( $post_id );
                        do_action( 'comment_form', $post_id );
                        ?>
                    </form>
                <?php endif; ?>
            </div><!-- #respond -->
            <?php do_action( 'comment_form_after' );
        else :
            do_action( 'comment_form_comments_closed' );
        endif;
	}
	
	function template_notices(){
		if(!did_action('template_notices')) do_action( 'template_notices' );
	}
	
	function show_single_post_meta(){
		global $post;
		
		if($post->post_type == 'bgc_event'):
			//get event date and time
			$date_display = '';
			$tmp_date = get_post_meta($post->ID, '_bgc_event_time', true);
			$event_datetime_ts = strtotime($tmp_date) + ( get_option( 'gmt_offset' ) * HOUR_IN_SECONDS );
			$event_day_ts = strtotime('today', $event_datetime_ts);
			$event_datetime = date('Y-m-d H:i:s', $event_datetime_ts);
			$now = strtotime('today');
			if($event_day_ts == $now) { //event is today
				$date_display = sprintf(__('%s at %s', 'chesnet'), date('l', $event_datetime_ts), date('g:ia', $event_datetime_ts));
				$m = __('This event is today.', 'chesnet');
			}
			elseif($event_day_ts > $now){ //event is upcoming
				$from_now = $this->date_difference($event_day_ts, $now);
				if($event_day_ts < strtotime('+ 6 days')){
					//event is within the week, but hasn't already passed
					$date_display = sprintf(__('%s at %s', 'chesnet'), date('l', $event_datetime_ts), date('g:ia', $event_datetime_ts));
					if($from_now == 1) $m = __('1 day from now.', 'chesnet');
					else $m = sprintf(__('%d days from now.', 'chesnet'), $from_now);
				}
				elseif($event_day_ts < strtotime('+ 364 days')) {
					$date_display = sprintf(__('%s at %s', 'chesnet'), date('j F', $event_datetime_ts), date('g:ia', $event_datetime_ts));
					$m = sprintf(__('%d days from now.', 'chesnet'), $from_now);
				}
				else {
					$date_display = sprintf(__('%s at %s', 'chesnet'), date('j F Y', $event_datetime_ts), date('g:ia', $event_datetime_ts));
					$m = sprintf(__('%d days from now.', 'chesnet'), $from_now);
				}
			}
			else { //event is over
				$date_display = date('j F Y');
				$m = __('This event is over', 'chesnet');
			}
			
			//Get location
			$loc = get_post_meta($post->ID, '_bgc_event_location', true);
			?>
            <div class="post-meta clearfix panel widget">
                <div class="row"<?php if(get_post_meta($post->ID, '_bgc_event_map', true) == 1 && $loc) echo ' data-equalizer'; ?>>
                    <div class="hide-if-no-js">
                         <div id="gmap_for_<?php echo $post->ID; ?>"  data-equalizer-watch></div>
                    </div>
                    <div class="small-14 columns" data-equalizer-watch>
                        <ul class="no-bullet">
                            <li><i class="fi-clock imgWrapLeft left"></i><div class="hide-over"><?php echo $date_display; ?><br /><span class="size16"><?php echo $m; ?></span></div></li>
                            <?php if($loc): ?>
                                <li><i class="fi-marker imgWrapLeft left"></i>
                                    <div class="event-location hide-over">
                                        <?php echo esc_attr( $loc ); ?></span><br />
                                        <a href="http://maps.google.com/?q=<?php echo esc_attr( $loc ); ?>" title="<?php _e('View in Google Maps', 'chesnet'); ?>" id="gmap_link_for_<?php echo $post->ID; ?>" target="_blank"><?php _e('map', 'chesnet'); ?></a>
                                     </div>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </div>
               </div>
           </div>
			
		<?php
		elseif($post->post_type == 'cn_job'):
			//hiring org (and website), applyby date, job status, announcement file/website
			$org = get_post_meta($post->ID, '_cn_jobs_org', true);
			$org_web = get_post_meta($post->ID, '_cn_jobs_orgweb', true);
			$applyby = get_post_meta($post->ID, '_cn_jobs_apply_by_date', true);
			$status = get_post_meta($post->ID, '_cn_jobs_status', true);
			$announcement = get_post_meta($post->ID, '_cn_jobs_announcement_website', true);
			if(empty($announcement)) $announcement = get_post_meta($post->ID, '_cn_jobs_announcement_file', true);
			
			$job_meta = array();
			if($org){
				$org_html = $org_web ? '<a href="'.$org_web.'" target="_blank">'.esc_attr($org).'</a>' : esc_attr($org);
				$job_meta[0] =sprintf('<i class="fi-torso-business imgWrapLeft left"></i><dl><dd>%s</dd><dt>%s</dt></dl>', $org_html, __('Hiring Organization', 'chesnet'));
			}
			if($announcement){
				$ann_type = __('WEB', 'chesnet');
				if(is_array($announcement)){
					$ann_url = $announcement['url'];
					$pos = strpos($ann_url, '.');
					if($pos !== false) $ann_type = strtoupper(substr(strrchr($ann_url, "."), 1));
				}
				else  $ann_url = $announcement;
				
				$job_meta[1] = sprintf('<i class="fi-link imgWrapLeft left"></i><dl><dd><a href="%s" target="_blank">%s</a></dd><dt>%s</dt></dl>', $ann_url, __('Job Announcement', 'chesnet'), sprintf(__('External %s Resource', 'chesnet'), $ann_type));
			}
			if($applyby){
				$job_meta[2] = sprintf('<i class="fi-calendar imgWrapLeft left"></i><dl><dd>%s</dd><dt>%s</dt></dl>', date('j F Y', strtotime($applyby)), __('Job Closing Date', 'chesnet'));
			}
			if($status){
				$job_meta[3] = sprintf('<i class="fi-%s imgWrapLeft left"></i><dl><dd>%s</dd><dt>%s</dt></dl>', $status == 'open' ? 'check' : 'x', ucwords($status), __('Job Status', 'chesnet'));
			}
			$job_meta = array_values($job_meta);
			if(count($job_meta) > 0):
			?>
        	<div class="post-meta clearfix panel widget">
            	<div class="row">
                	<div class="small-14 large-7 columns">
                    	<ul class="no-bullet">
                            <li><?php echo $job_meta[0]; ?></li>
                            <?php if(isset($job_meta[1])): ?>
                            	<li class="uls"><?php echo $job_meta[1]; ?></li>
                            <?php endif; ?>
                        </ul>
                    </div>
                    <?php if(isset($job_meta[2])): ?>
                    <div class="small-14 large-7 columns">
                    	<ul class="no-bullet">
                            <li><?php echo $job_meta[2]; ?></li>
                            <?php if(isset($job_meta[3])): ?>
                            	<li><?php echo $job_meta[3]; ?></li>
                            <?php endif; ?>
                        </ul>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
            <?php endif;
        endif;
	}
	
	function cn_before_content(){
		global $post, $bp;
		
		//if we're viewing a single post, verify OR correct the global cn_set_group_id
		
		if(is_single()){
			$gid = ''; //this is teh group id we will compare the user's permissions to
			
			//if the gid is set, make sure it makes sense by cross examining the post cat
			$post_cats = $group_ids = array();
			$post_cats = wp_get_post_categories($post->ID, array('fields' => 'slugs'));
			foreach($post_cats as $cat_slug){
				$group_ids[] = $this->cn_gid_from_cat_slug($cat_slug);
			}
			if(!empty($group_ids)){
				if(isset($this->cn_set_group_id))
					if(in_array($this->cn_set_group_id, $group_ids)) $gid = $this->cn_set_group_id;
				else { //reset the global gid
					if(count($group_ids) > 1) {
						if(in_array(1, $group_ids)) $gid = $this->cn_set_current_group_id(1); //if CN gorup is an option, default to that
						else { //find the first group that the user is in and use that one
							//get all the groups
							$groups = groups_get_groups( array(
								'type'              => 'active',
								'include'           => $group_ids,
								'show_hidden'       => true,
								'per_page'          => null,
								'populate_extras'   => false,
								'update_meta_cache' => false
							));
							foreach ($groups['groups'] as $group) {
								if(!empty($gid)) continue;
								
								//if user is logged in, pick one of those
								if(isset($bp->loggedin_user->id)){
									if(groups_is_user_member( $bp->loggedin_user->id, $group->id )){
										$gid = $this->cn_set_current_group_id($group->id);
										continue;
									}
								}
								//if not, pick the first public one
								if( bp_get_group_status( $group ) == 'public' ) {
									$gid = $this->cn_set_current_group_id($group->id);
									continue;
								}
								//otherwise, default to first one
								$gid = $this->cn_set_current_group_id($group->id);
							}
						}
					}
					else {
						$gid = $this->cn_set_current_group_id($group_ids[0]);
					}
				}
			}
		}

	}
	
	function post_entry_meta() {
		global $post, $chesnet;
		
		do_action('cn_before_post_entry_meta', $post);
		//annoying bug fix
		if($post->post_status !== 'publish') $post->post_date = $post->post_modified;
		
		$member_id = $post->post_author; ?>
		<div class="entry-meta clearfix">
			<div class="author-box">
				<a class="left" href="<?php echo bp_core_get_user_domain( $member_id ); ?>" title="<?php echo bp_core_get_user_displayname( $member_id ); ?>">
					<?php echo bp_core_fetch_avatar ( array( 'item_id' => $member_id, 'type' => 'thumb', 'class' => 'imgWrapLeft' ) ); ?>
				</a>
				<div class="hide-over">
					<span class="byline author"><?php echo bp_core_get_userlink( $member_id ); ?></span> &middot; <time class="updated" datetime="<?php echo get_the_time('c'); ?>" title="<?php printf(__('%s at %s', 'chesnet'), get_the_time('l, F jS, Y'), get_the_time()); ?>"><?php printf(__('Posted %s', 'chesnet'), bp_core_time_since( $post->post_date, bp_core_current_time( false, 'timestamp' ) )); ?></time><br />
					<span class="inline groups"><?php printf(__('in %s.', 'chesnet'), $chesnet->cn_get_the_category_list( true, ', ' )); ?></span>
					<?php echo $chesnet->_get_the_tag_list(sprintf('&nbsp;<span data-tooltip aria-haspopup="true" class="has-tip tip-top" title=\'%s: ', __('Tagged with', 'chesnet')), ', ', '\'><i class="fi-pricetag-multiple size18"></i></span>', false); ?>
					
				</div>
			</div>
		</div>
		<?php
	}
	
	function content_nav( $nav_id=false ) {
		global $wp_query;
		if($nav_id === false) $nav_id = '';
	
		if ( !empty( $wp_query->max_num_pages ) && $wp_query->max_num_pages > 1 ) : ?>
	
			<div id="<?php echo $nav_id; ?>" class="navigation">
				<div class="alignleft"><?php next_posts_link( __( '&larr; Previous Entries', 'chesnet' ) ); ?></div>
				<div class="alignright"><?php previous_posts_link( __( 'Next Entries &rarr;', 'chesnet' ) ); ?></div>
			</div><!-- #<?php echo $nav_id; ?> -->
	
		<?php endif;
	}
	
	function wp_query_pagination($query, $args = array()){
		echo $this->get_wp_query_pagination($query, $args);
	}
	function get_wp_query_pagination($query, $args){
		extract($args);
		$ret = '';
		$cont = sprintf('<div %s class="pagination">', isset($cont_id) && !empty($cont_id) ? $cont_id : '');

		$big = 999999999; // need an unlikely integer
		$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
		$pag_links = paginate_links( array(
			'base' 			=> str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'format' 		=> '?paged=%#%',
			'current' 		=> max( 1, $paged ),
			'total' 		=> $query->max_num_pages,
			'prev_text'     => __('&laquo; Previous', 'chesnet'),
			'next_text'     => __('Next &raquo;', 'chesnet'),
			'type'           => 'list'
		) );
		
		$ret .= $cont;
		$ret .= '<div class="right">';
		$ret .= $pag_links;
        $ret .='</div>
		<div class="pag-count">';
		$ret .= sprintf(__('Viewing %s', 'chesnet'), $query->post_count == $query->found_posts ? __('all ', 'chesnet') . $query->post_count : $query->post_count . __(' of ', 'chesnet') . $query->found_posts);
		$ret .= '</div></div>';
    	return $ret;
	}
	
	/**
	 ** Processors Front End
	 **/

	function cn_saveMeta($id, $data, $post = null) {
		if(!isset($post->post_type)){
			$post = get_post($id);
		}
		if(WP_DEBUG === true) var_dump($data); //exit();
		
		//Add sharing meta
		if(isset($data['cn_post_sharing'])) add_post_meta($id, '_cn_post_sharing', $data['cn_post_sharing'], true);
		
		//set the post_status based on moderation setting
		//only if they are sending an email
		if($data['cn_post_sharing'] == 'yes'){
			$moderate = groups_get_groupmeta(cn_get_gid_from_post_id($id), 'cn_moderate_new_announcements');
			$data['post_status'] = $moderate == 'yes' ? 'pending' : 'publish';
		}
		else $data['post_status'] = 'publish';
		
		if($data['post_status'] == 'publish') wp_update_post( array('ID' => $id, 'post_status' => 'publish') );
		
		//do the meta data
		$metaTags = $this->cn_metaTags($post->post_type);
		if(!$metaTags || !is_array($metaTags)) return;
		
		//custom meta saving
		if($post->post_type == 'cn_job'){
			//fix job closing date formatting
			//if(!$data['cn_jobs_apply_by_date']) $data['cn_jobs_apply_by_date'] = $this->cn_jobs_apply_by_date_calc($id); //not required for now
			
			$data['cn_jobs_org'] = sanitize_text_field( $data['cn_jobs_org'] );
			$data['cn_jobs_orgweb'] = esc_url( $data['cn_jobs_orgweb'], array('http','https'), false );
			$data['cn_jobs_announcement_website'] = esc_url( $data['cn_jobs_announcement_website'], array('http','https'), false );
			$data['cn_jobs_status'] = strtotime($data['cn_jobs_apply_by_date']) > strtotime(time()) ? 'open' : 'closed';
			
			if(isset($data['uploads']['cn_jobs_announcement_file'])){
				$file_results = $this->cn_save_file_to_wp($data['uploads']['cn_jobs_announcement_file']);
				if(empty($file_results['error'])){
					//delete old file if exists and delete old meta
					$old_file = get_post_meta( $id, '_cn_jobs_announcement_file', true );
					if($old_file) unlink($old_file['file']);
					update_post_meta($id, '_cn_jobs_announcement_file', $file_results);
				}
			}
		}
		elseif($post->post_type == 'bgc_event'){
			$tmp_date = $data['event-date'].' '.$data['event-hour'].':'.$data['event-minute'].$data['event-ampm'];
			$tmp_date = strtotime($tmp_date);
			//check for valid date/time
			if ($tmp_date && strtotime($data['event-date']) && strtotime($data['event-date']) != -1) {
				$data['bgc_event_time'] = gmdate( 'Y-m-d H:i:s', ( $tmp_date - ( get_option( 'gmt_offset' ) * HOUR_IN_SECONDS ) ) ); //assumed date entered in timezone offset. Subtract the offset to get GMT for storage.
			}
	
			$data['bgc_event_location'] = esc_attr(strip_tags(trim($data['event-loc'])));
			$data['bgc_event_map'] = ($data['event-map']==1) ? 1 : 0;
		}
		elseif($post->post_type == 'topic'){
			$topic_id = $post->ID;
			
			//if this is a new topic, we need to get the forum id from the group id
			if ( $data['cn_action'] == 'add' ) {
				//post category should never be multiple
				//but if it is, we'll make sure to just go with [0]
				$cat_ids = wp_get_post_categories($post->ID);
				if(empty($cat_ids)) $group_id = 1; //default to main group
				else{
					$group_id = $this->cn_get_group_from_cat($cat_ids[0]);
				}
				$forum_ids = bbp_get_group_forum_ids( $group_id );
				// Get the first forum ID
				if ( !empty( $forum_ids ) ) {
					$forum_id = (int) is_array( $forum_ids ) ? $forum_ids[0] : $forum_ids;
				}
				else{ //need to add a forum for the group
					$BBP_Forums_Group_Extension = new BBP_Forums_Group_Extension();
					$group = $BBP_Forums_Group_Extension->toggle_group_forum( $group_id, true );
					// Set the default forum status
					switch ( $group->status ) {
						case 'hidden'  :
							$status = bbp_get_hidden_status_id();
							break;
						case 'private' :
							$status = bbp_get_private_status_id();
							break;
						case 'public'  :
						default        :
							$status = bbp_get_public_status_id();
							break;
					}
			
					$forum_id = bbp_insert_forum(
						array(
							'post_parent'    => bbp_get_group_forums_root_id(), // forum ID
							'post_status'    => $status,
							'post_type'      => bbp_get_forum_post_type(),
							'post_author'    => bbp_get_current_user_id(),
							'post_content'   => $group->description,
							'post_title'     => $group->name,
							'comment_status' => 'closed'
						),
						array(
							'reply_count'          => 0,
							'topic_count'          => 0,
							'topic_count_hidden'   => 0,
							'total_reply_count'    => 0,
							'total_topic_count'    => 0,
							'last_topic_id'        => $topic_id,
							'last_reply_id'        => $topic_id,
							'last_active_id'       => $topic_id,
							'last_active_time'     => $post->post_date,
							'forum_subforum_count' => 0,
						) );
					bbp_add_forum_id_to_group( $group_id, $forum_id );
					bbp_add_group_id_to_forum( $forum_id, $group_id );
				}
				//shold have a forum_id somehow by now
				//update meta using bbpress function
				bbp_update_topic( $topic_id, $forum_id );
				
				bbp_notify_forum_subscribers( $topic_id, $forum_id );
			}
			else{
				//update meta using bbpress function
				$forum_id = bbp_get_topic_forum_id( $topic_id );
				bbp_update_topic( $topic_id, $forum_id, null, null, true );
				bbp_update_topic_last_active_time( $topic_id, $post->post_modified );
			}
			if($post->post_parent !== $forum_id) wp_update_post( array( 'ID' => $post->ID, 'post_parent' => $forum_id));
			$bbp_topic_id = $topic_id;
			$bbp_forum_id = $forum_id;
		}
		
		//update meta fields
		foreach ( $metaTags as $tag ) {
			if($post->post_type == 'cn_job' && $tag == '_cn_jobs_announcement_file') continue; //handled above by cn_do_handle_job_file()
			$htmlElement = ltrim( $tag, '_' );
			if ( isset( $data[$htmlElement] ) ) {
				if ( is_string($data[$htmlElement]) )
					$data[$htmlElement] = filter_var($data[$htmlElement], FILTER_SANITIZE_STRING);
	
				update_post_meta( $id, $tag, $data[$htmlElement] );
			}
		}
		
		//delete any meta
		if(isset($data['meta_to_delete']) && is_array($data['meta_to_delete'])){
			foreach($data['meta_to_delete'] as $key) delete_post_meta($id, $key);
		}
	}
	
	// Update & save an existing post
	function cn_updatePost( $id, $args ) {
		$args['ID'] = $id;
		unset($args['post_author']); //if an administrator edits the post, we don't want to change the author
		$args = $this->add_tax_to_post_array($args, $id);
		
		if(wp_update_post($args)) {
			$this->cn_saveMeta($id, $args, get_post( $id ) );
			return $id;
		}
		else return false;
	}

	//Create a new post
	function cn_createPost($args) {
		
		$defaults = array(
			'post_type' => 'post'
		);
		$args = wp_parse_args( $args, $defaults);
		$args = $this->add_tax_to_post_array($args);
		if(WP_DEBUG === true) var_dump($args);// exit();		
		
		remove_filter( 'post_link', array($this, 'append_query_string'), 10 );
		remove_filter( 'post_type_link', array($this, 'append_query_string'), 10 );
		$id = wp_insert_post($args, true);	
		add_filter( 'post_link', array($this, 'append_query_string'), 10, 2 );
		add_filter( 'post_type_link', array($this, 'append_query_string'), 10, 2 );

		if( !is_wp_error($id) ) {
			$this->cn_saveMeta($id, $args, get_post( $id ) );
			return $id;
		}
		else return false;	
	}
	
	//Delete a post
	function cn_deletePost($id, $force_delete = false) {
		//delete files and meta
		return wp_delete_post($id, $force_delete);		
	}
	
	//add taxonomic data to post array
	function add_tax_to_post_array($data, $id = false ){
		//first, groups (via category taxonomy
		$taxonomies = get_object_taxonomies( $data['post_type'] );
		if ( in_array('category', $taxonomies) && isset($data['cn_group_ids'])){
			$group_ids = $terms = $cn_previous_groups_user = $cn_previous_groups = array();
			
			//add the user selected groups to group_ids
			$group_ids = empty($data['cn_group_ids']) ? array() : $data['cn_group_ids'];
			
			//add any groups that the user couldn't see and the post was in
			//removed because we can only add to 1 post to 1 group (aka category)
			/*if(isset($data['cn_previous_groups_user'])) $cn_previous_groups_user = explode(',',$data['cn_previous_groups_user']);
			if(isset($data['cn_previous_groups'])) $cn_previous_groups = explode(',', $data['cn_previous_groups']);
			
			foreach($cn_previous_groups_user as $g) {
				if (($key = array_search($g, $cn_previous_groups)) !== false) {
					unset($cn_previous_groups[$key]);
				}
			}
			$group_ids = array_merge($cn_groups, $cn_previous_groups);*/

			//only 1 item in array, but we'll keep the foreach incase we can add multiple group options later
			foreach($group_ids as $gid){ 
				$term = term_exists($this->cn_cat_slug_from_gid($gid), 'category');
				$terms[] = (int)$term['term_id'];
				
				//also add sharing reminder meta
				if($data['cn_action'] == 'add') add_post_meta($id, '_cn_reminders_left_group_' . $gid, 3, true);
				/* @todo develop reminder system
				elseif($data['cn_post_sharing'] == 'yes'){ //editting and post_sharing means it's a reminder to groups

					$activity_args = array(
						'type' => cn_get_activity_type($post->post_type, 'reminder')
					);
					$activity_id = cn_bp_do_record_activity($gid, $post, $activity_args);
					if(!$activity_id) cn_catch_error(new WP_Error('cn_bp_activity_error', __('Error adding reminder activity.', 'chesnet')));
				}*/
			}
			$data['post_category'] = $terms;
		}
		
		//next, tagsm via post_tag taxonomy
		if ( in_array('post_tag', $taxonomies) && isset($data['cn_post_tag_ids_option'])){
			$submitted_tags = empty($_SESSION['cn_post_tag_ids']) ? array() : $_SESSION['cn_post_tag_ids'];
			if(!empty($submitted_tags)){
				$new_tag_ids = array();
				foreach($submitted_tags as $submitted_tag_id){
					$term_exists = term_exists( (int)$submitted_tag_id, 'post_tag' );
					if(isset($term_exists['term_id'])) $new_tag_ids[] = (int)$term_exists['term_id'];
				}
				//add the new tags
				$data['tags_input'] = $new_tag_ids;
			}
			else { //delete all teh tags
				$data['tags_input'] = null;
			}
		}
		return $data;
	}
	
	//returns an array(file, url, type): http://codex.wordpress.org/Function_Reference/wp_handle_sideload
	function cn_save_file_to_wp($upload, $time = null){
		if(!file_exists($upload)) return array('error' => __('The uploaded file does not exist', 'chesnet'));
	
		if ( !function_exists( 'wp_handle_upload' ) ) require_once( ABSPATH . 'wp-admin/includes/file.php' );
		
		$upload_overrides = array( 'test_form' => false, 'test_size' => true, 'test_upload' => true );
		// array based on $_FILE as seen in PHP file uploads
		$file = array(
			'name' => basename($upload), // ex: wp-header-logo.png
			'type' => $this->_mime_content_type($upload),
			'tmp_name' => $upload,
			'error' => 0,
			'size' => filesize($upload),
		);
		
		// move the temporary file into the uploads directory
		$file_results = wp_handle_sideload( $file, $upload_overrides, $time );
		if($file_results['file']) str_replace('\\', '/', $file_results['file']);
		return $file_results;
	}
	
	/**
	 ** RSS Feeds
	 **/
	function bp_get_activity_thread_permalink($link){
		//because RSS feeds don't like "&" that aren't escaped, we need to run esc_url on bp_get_activity_thread_permalink() when it's called in BP_Activity_Feed (buddypress plugin: bp-activity/classes/class-bp-activity-feed.php)
		return esc_url($link);
	}
	
	function bp_get_activity_feed_item_title($title){
		global $activities_template;
		
		//Because we store just meta data in the activity action, there is nothing there for bp_get_activity_feed_item_title() to display, so we use the post's title instead.
		if($activities_template->activity->component == 'groups'){
			if($activities_template->activity->secondary_item_id != 0){
				if($activities_template->activity->type == 'entry_comment'){ //either a comment
					$title = strip_tags( ent2ncr( trim( convert_chars( bp_create_excerpt( get_comment_text($activities_template->activity->secondary_item_id), 70, array( 'ending' => " [&#133;]" ) ) ) ) ) );
				}
				else { //or a post
					$title = get_the_title($activities_template->activity->secondary_item_id);
				}
			}
		}
		return $title;
	}
	
	//alter activity feed class variables before feed is fetched
	function bp_activity_feed_prefetch($feed){
		if(isset($_GET['max']) && intval($_GET['max']) !== 0) $feed->__set('max', absint($_GET['max']));
	}
	
	/**
	 ** AJAX
	 **/
	
	function ajax_query_attachments_args( $query ) {
		if ( !current_user_can( 'edit_others_posts' ) ) {
			global $current_user;
			$query['author'] = $current_user->ID;
		}
		return $query;
	}
	
	/**
	 ** Plugins
	 **/
	 
	/* Jobs Plugin */
	
	//Handle file upload and meta addition.
	//Looks for $_FILES['cn_jobs_announcement_file']['name']
	//If a file is uploaded, add the meta for the URL and delete any old file present.
	//If cn_delete_file is set, delete the current file and meta
	//return arr $upload : Array with the file path, file url and any errors.
	function cn_do_handle_job_file($post_id){
		if(!empty($_FILES['cn_jobs_announcement_file']['name'])) {
		//a file was uploaded
			 
			// Get the file type of the upload  
			$arr_file_type = wp_check_filetype(basename($_FILES['cn_jobs_announcement_file']['name']));  
			$uploaded_type = $arr_file_type['type'];  
			// Check if the type is supported. If not, throw an error.  
			if(in_array($uploaded_type, $this->supported_types)) {  
	  
				// Use the WordPress API to upload the file  
				$upload = wp_upload_bits($_FILES['cn_jobs_announcement_file']['name'], null, file_get_contents($_FILES['cn_jobs_announcement_file']['tmp_name']));
				$upload['file'] = str_replace('\\', '/', $upload['file']);
		  
				if(isset($upload['error']) && $upload['error'] != 0) {  
					$message = "There was an error uploading your file. The error is: " . $upload['error'];
					//admin error
					if(is_admin()) $this->add_admin_notices($post_id, 'error', $message);
					else $cn_message = $this->set_output_message($message, 'error');
				} else { 
					//delete old file if exists and delete old meta
					$old_file = get_post_meta( $post_id, '_cn_jobs_announcement_file', true );
					if($old_file) unlink($old_file['file']);
					update_post_meta($post_id, '_cn_jobs_announcement_file', $upload);
				} // end if/else  
	  
			} else {  
				$message = __("The file type that you've uploaded is not supported.", "chesnet");
				//admin error
				if(is_admin()) $this->add_admin_notices($post_id, 'error', $message);
				else $cn_message = $this->set_output_message($message, 'error');
			} // end if/else
			return $upload;          
		} // end if  
		elseif(isset($_POST['cn_delete_file']) && $_POST['cn_delete_file'] == 1){
			$this->cn_do_delete_file_and_meta($post_id,'_cn_jobs_announcement_file');
		}
	}
	
	//AJAX call to delete file and meta data from a post in admin backend
	function cn_set_to_delete_file_and_meta() {
		global $wpdb;
		if (!isset($_SESSION)) session_start();
		check_ajax_referer( 'cn_delete_file_nonce', 'security' );
		
		$return = array();
		
		$post_id = intval($_POST['post_id']);
		
		$delete_file = $_POST['delete_file'] == 'true' ? true : false;
		$this->cn_set_delete_file_and_meta($post_id, sanitize_text_field($_POST['key']), $delete_file);
		
		$return['type'] = 'success';
		echo json_encode($return);
		die();
	}
	
	//
	function cn_set_delete_file_and_meta($post_id, $key, $delete_file=true){
		if (!isset($_SESSION)) session_start();
		if($delete_file){
			$old_file = get_post_meta( $post_id, $key, true );
			if($old_file && file_exists($old_file['file'])) $_SESSION['files_to_delete'][] = $old_file['file'];
		}
		$_SESSION['meta_to_delete'][] = $key;
		$_SESSION['uploads'][ltrim($key, '_')] = 'none';
	}
	
	//Actually does the deleting of the file and the meta key/value pair
	function cn_do_delete_file_and_meta($post_id, $key, $delete_file=true){
		$success = false;
		if($delete_file){
			$old_file = get_post_meta( $post_id, $key, true );
			if($old_file && file_exists($old_file['file'])) $success = unlink($old_file['file']);
		}
		if($success == true || $delete_file == false) delete_post_meta( $post_id, $key );
		return $success;
	}
		
	/* Downloads Plugin */
	
	function get_dlm_tax($group_id){
		return 'cn_dlm_tag_group_' . $group_id;
	}
		
	/*
	 * Handles adding or updating a dlm_download_version.
	 *
	 * @since 1.0
	 * @author dbrellis
	 * @param $version_id int The ID of the dlm_download_version post.
	 * @param $data arr Data for the dlm_download_version post as well as the meta data.
	 * @param $new_file arr Array from $_FILES[] that holds a new file to be added to the version.
	 * @param $download_id int The ID of the dlm_download post.
	 * @return $new_stuff arr Array of new dlm_download_version id and the attachment id 
	 *
	 */
	function cn_update_dlm_version($version_id=null, $data, $upload=false, $download_id){
		$edit = false;
		$new_stuff = array();
		
		if($version_id){
			//check if the supplied version ID is a valid dlm_download_version
			$version = get_post($version_id);
			if($version && $version->post_type == 'dlm_download_version') $edit = true;
		}
		
		//return if there is nothign to change
		if(empty($data['version']) && empty($data['files']) && (!$upload || !file_exists($upload))) return;
		
		//a file is in place and it's a new version!
		$version_args = array(
			'post_title'    => 'Download #' . $download_id . ' File Version',
			'post_content'  => '',
			'post_status'   => 'publish',
			'post_author'   => get_current_user_id(),
			'post_parent'   => $download_id,
			'post_type'     => 'dlm_download_version',
			'menu_order'	=> $data['menu_order']
		);
		//fix the files
		$data_files = $data['files'];
		unset($data['files']);
		$data['files'] = array();
		if(!empty($data_files)){
			$files = preg_split('/\s+/', $data_files);

			$protocols = array('http', 'https', 'ftp', 'ftps', 'news', 'irc', 'gopher', 'nntp', 'feed' );
			foreach($files as $file){
				$data['files'][] = esc_url_raw( $file, $protocols );
			}
		}
		if($upload){
			$file_results = $this->cn_save_file_to_wp($upload);
					
			if (!empty($file_results['error'])) {
				bp_core_add_message($file_results['error'], 'warning');
			} else {
				$local_url = $file_results['url'];
				$wp_filetype = $file_results['type'];
				$filename = $file_results['file'];
				$wp_upload_dir = wp_upload_dir();
				$attachment = array(
					'guid' => $local_url,
					'post_mime_type' => $wp_filetype,
					'post_title' => sanitize_title(basename($filename)),
					'post_content' => '',
					'post_status' => 'inherit',
					'post_parent' => $download_id,
					'post_author'  => get_current_user_id()
				);
				$attach_id = wp_insert_attachment( $attachment, $filename);
				$new_stuff['attachment_id'] = $attach_id;
				require_once(ABSPATH . 'wp-admin/includes/image.php');
				$attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
				wp_update_attachment_metadata( $attach_id, $attach_data );
				
				//add file url to $version_data['_files'][]
				array_unshift($data['files'], $local_url);
			}
		}
		$data['files'] = array_filter($data['files']);
		//get filesize of first file
		$data['filesize'] = $GLOBALS['download_monitor']->get_filesize($data['files'][0]);
		$new_version_data = array_merge($data, $version_args);
			
		$new_stuff['version_id'] = $edit ? $this->cn_updatePost( $version_id, $new_version_data ) : $this->cn_createPost($new_version_data);
		return $new_stuff;	
	}
	
	//custom columns on admin screen
	function dlm_download_columns( $columns ) {
		unset($columns["download_cat"]);
		unset($columns["download_tag"]);
		
		return $columns;
	}
	
	//get labels for a download
	function get_dlm_labels($dlm_id, $group_id, $args = null){
		$label_tax_name = $this->get_dlm_tax($group_id);
		$labels = array();
		
		//get all terms in this taxonomy
		$labels = wp_get_post_terms($dlm_id, $label_tax_name, $args);
		
		if(is_wp_error($labels) ){
			$this->cn_catch_error($labels);
			$labels = false;	
		}
		
		return $labels;
	}
	
	//returns a comma seperated list of labels for a given dlm in a group
	function get_dlm_label_list($dlm_id, $group_id, $glue = ', ', $links = true){
		global $bp;
		$group = groups_get_group( array( 'group_id' => $group_id ) );
		//get labels
		$labels = $this->get_dlm_labels($dlm_id, $group_id);
		if($labels !== false && is_array($labels)){
			$r = array();
			foreach($labels as $label){
				$llink = cn_resources_get_label_link($label, $group);
				$r[] = $links ? sprintf('<a href="%s">%s</a>', $llink, $label->name) : $label->name;
			}
			$r = implode($glue, $r);
			return $r;
		}
		return false;
	}
		
	/* Group Category Plugin */
	 
	//Creates a new category item whenever a group is created
	function groups_create_group($group_id, $member, $group){
		//Set up args for category creation
		$slug = $this->cn_cat_slug_from_gid($group_id);
		
		$args = array('description' => 'Posts associated with the group ' . $group->name, 
					  'slug' => $slug, 
					  'parent' => 0);
		
		//create new category for 'post' post types
		$my_cat_id = wp_insert_term($group->name, 'category', $args);
	}
	
	//Deletes the category associated with a group before group deletion
	function cn_groups_before_delete_group($group_id){
		global $wpdb;
		
		//this one is a bit harder
		//get the privacy settings for the group
		$group = groups_get_group(array('group_id' => $group_id));
		
		//if the privacy settings are for group members only, 
		if($group->status !== 'public'){
			//get the posts in the category
			$posts = array();
			$cat_slug = $this->cn_cat_slug_from_gid($group->id);
			$cat = get_term_by( 'slug', $cat_slug, 'category' );
			$args = array('category' => $cat->term_id,
						  'post_status' => 'publish',
						  'posts_per_page' => -1);
			$posts = get_posts( $args );
			
			//set it to 'private'
			$set_private_ids = array();
			foreach($posts as $post) $set_private_ids[] = $post->ID;
			$wpdb->query( 
				"UPDATE $wpdb->posts
				 SET post_status = 'private'
				 WHERE ID IN " . implode(',', $set_private_ids )
			);
		}
		
		//just put them in category 1 by default
		wp_delete_category( $cat->term_id );
	}
	
	//returns an array of the groups a post is in (via its categories)
	function get_post_groups($post){
		return cn_get_post_groups($post);
	}
	
	//return the group a post is in (via its category)
	function cn_get_post_group($post){
		return cn_get_post_group($post);
	}
	
	//returns the group id from a given post
	function cn_get_gid_from_post_id($post_id){
		return cn_get_gid_from_post_id($post_id);
	}
	
	//Gets the group ID from a category ID or object
	function cn_get_group_from_cat($cat_id){
		return cn_get_group_from_cat($cat_id);
	}
	
	//Gets the category slug from a group ID
	function cn_cat_slug_from_gid($gid){
		return cn_cat_slug_from_gid($gid);
	}
	
	//Gets the group ID from a category slug
	function cn_gid_from_cat_slug($slug){
		return cn_gid_from_cat_slug($slug);
	}
	
	//Gets the category term (object) from a group ID.
	function cn_get_cat_from_group($gid){
		return cn_get_cat_from_group($gid);
	}
	
	//checks if a post is in a group by cross referencing the post's category
	function cn_is_post_in_group($post, $group_id=false){
		return cn_is_post_in_group($post, $group_id=false);
	}
	
	/* Group Hierarchy Plugin */
	
	//Removes the Main Group from the list of hierarchy available groups
	//and renames the No Parent option to the Site Name
	function cn_bp_group_hierarchy_available_parent_groups($display_groups, $this_group=false){
		$new_groups = array();
		foreach($display_groups as $group) {
			if($group->id == 1 || $group->name == get_bloginfo('name')) continue;
			if($group->id == 0) $group->name = get_bloginfo('name');
			$new_groups[] = $group;
		}
		return $new_groups;
	}
		
	/* Group Email Subscription Plugin */
	 
	//Includes an activity item for group subscription by type
	function cn_ass_this_activity_is_important($bool, $type){
		$important_actions = array(
			'bp_job_created',
			'bp_topic_created',
			'bp_dlm_created',
			'bp_bgc_created',
			'bp_news_created'
		);
		if(in_array($type,$important_actions)) return true;
	}
	
	//Blocks an activity item for group subscription by type
	function cn_ass_block_group_activity_types($bool, $type, $content){
		$blocked_actions = array(
			'bp_doc_created',
			'bp_doc_edited',
			'group_created'
		);
		if(in_array($type,$blocked_actions)) return false;
		else return true;
	}
		
	//Returns a post object from an activity or false if the sharing option is set to no
	function cn_ass_get_post_from_activity($secondary_item_id){
		$post = get_post($secondary_item_id);
		if(!$post) return false;
		else{
			return get_post_meta($post->ID, '_cn_post_sharing', true) == 'yes' ? $post : false;
		}
	}
	
	//Run after ass_group_notification_activity() has sent it's emails/recorded to digest
	function cn_ass_after_group_notification($activity){
		//update the reminders left meta for the post
		$post = get_post($activity->secondary_item_id);
		$group_id = $activity->item_id;
		if(!$post || !$group_id) return;
		
		$reminders = (int)get_post_meta($post->ID, '_cn_reminders_left_group_'.$group_id, true);
		update_post_meta($post->ID, '_cn_reminders_left_group_'.$group_id, $reminders-1);
	}
	
	//filter the content of the message
	function cn_ass_filter_message_content($message, $activity, $post, $group){
		$content = $post->post_content;
		$content = apply_filters('the_content', $content);
		$content = str_replace(']]>', ']]&gt;', $content);
		$activity_permalink = ( isset( $activity->primary_link ) && $activity->primary_link != bp_core_get_user_domain( $activity->user_id ) ) ? $activity->primary_link : bp_get_group_permalink( $group );
		$meta = array();
		
		if($post->post_type == 'cn_job'){
			$org = get_post_meta($post->ID, '_cn_jobs_org', true);
			$org_web = get_post_meta($post->ID, '_cn_jobs_orgweb', true);
			$applyby = get_post_meta($post->ID, '_cn_jobs_apply_by_date', true);
			$announcement = get_post_meta($post->ID, '_cn_jobs_announcement_website', true);
			if(empty($announcement)) $announcement = get_post_meta($post->ID, '_cn_jobs_announcement_file', true);
			
			if($org) $meta[__('Hiring Organization', 'chesnet')] = $org_web ? '<a href="'.$org_web.'" target="_blank">'.esc_attr($org).'</a>' : esc_attr($org);
			if($announcement){
				if(is_array($announcement)) $ann_url = $announcement['url'];
				else $ann_url = $announcement;
				
				$meta[__('Job Announcement', 'chesnet')] = '<a href="'.$ann_url.'" target="_blank">'.$ann_url.'</a>';
			}
			if($applyby){
				$meta[__('Job Closing Date', 'chesnet')] = date('j F Y', strtotime($applyby));
			}
		}
		elseif($post->post_type == 'bgc_event'){
			$date_display = '';
			$tmp_date = get_post_meta($post->ID, '_bgc_event_time', true);
			$event_datetime_ts = strtotime($tmp_date) + ( get_option( 'gmt_offset' ) * HOUR_IN_SECONDS );
			$meta[__('Date', 'chesnet')] = sprintf(__('%s at %s', 'chesnet'), date('j F Y', $event_datetime_ts), date('g:ia', $event_datetime_ts));
			
			//Get location
			$loc = get_post_meta($post->ID, '_bgc_event_location', true);
			if($loc) $meta[__('Location', 'chesnet')] = $loc;
		}
		elseif($post->post_type == 'dlm_download'){
			$dlm_download = new DLM_Download($post->ID);
			if($dlm_download){
				$meta[__('Download', 'chesnet')] = '<a href="'.$dlm_download->get_the_download_link().'" rel="nofollow" target="_blank">'.$dlm_download->get_the_filename().'</a>';
				$label_list = $this->get_dlm_label_list($post->ID, $group->id);
				if($label_list) $meta[__('Labels', 'chesnet')] = $this->get_dlm_label_list($post->ID, $group->id);
			}
		}
		
		ob_start(); ?>
			<p><?php printf( __('%s added a %s in %s:', 'chesnet'), bp_core_get_userlink( $post->post_author ), cn_get_activity_type($post->post_type, 'new_name'), $group->name); ?></p>
            
            <?php foreach($meta as $key => $value) echo '<strong>' . $key . '</strong>: ' . $value . '<br />';
			echo '<br />'; ?>
            
			<?php echo $content; ?>
            
            <?php echo '<p>---------------------------------------------------------------</p>'; ?>
            
            <p><?php printf(__('To view or reply, log in and go to: %s', 'chesnet'), $activity_permalink); ?></p>
		<?php $message = ob_get_clean();
		
		return $message;
	}
	
	function ass_group_subscribe_button_group_banner(){
		global $bp, $groups_template;
	
		if( ! empty( $groups_template ) ) {
			$group =& $groups_template->group;
		}
		else {
			$group = groups_get_current_group();
		}
	
		if ( !is_user_logged_in() || !empty( $group->is_banned ) || !$group->is_member )
			return;
	
		// if we're looking at someone elses list of groups hide the subscription
		if ( bp_displayed_user_id() && ( bp_loggedin_user_id() != bp_displayed_user_id() ) )
			return;
	
		$group_status = ass_get_group_subscription_status( bp_loggedin_user_id(), $group->id );

		switch($group_status) {
			case false:
				$group_status = NULL;
				$icon = 'no-alt';
				break;
			case 'supersub':
				$icon = 'yes';
				break;
			default:
				$icon = 'minus';
		}
		
		$status_desc = __('Notifications', 'chesnet');
		$gemail_icon_class = '<span class="dashicons dashicons-'.$icon.'"></span>';
		$sep = '';
		
		$status = ass_subscribe_translate( $group_status );
		
		$not_link = bp_get_group_permalink( $bp->groups->current_group ) . 'notifications';
		
		return '<a href="' . $not_link . '" class="group-button" title="'.sprintf(__('Your email status is %s', 'chesnet'), $status).'">'.$gemail_icon_class . $status_desc.'</a>';
		
		/*
		
		@todo: make a dropdown button to select your email settings and update via ajax ?>
        
        <li><a href="#" data-dropdown="bp-group-notifications" aria-controls="bp-group-notifications" aria-expanded="false" class="group-button" title="<?php printf(__('Your email status is %s', 'chesnet'), $status); ?>"><?php echo $gemail_icon_class . $status_desc; ?></a><br>
            <ul id="bp-group-notifications" data-dropdown-content class="f-dropdown" aria-hidden="true" tabindex="-1">
                <li><a class="group-sub" id="no-<?php echo $group->id; ?>"><?php _e('No Email', 'bp-ass') ?></a> <?php _e('I will read this group on the web', 'bp-ass') ?></li>
                <li><a class="group-sub" id="sum-<?php echo $group->id; ?>"><?php _e('Weekly Summary', 'bp-ass') ?></a> <?php _e('Get a summary of topics each', 'bp-ass') ?> <?php echo ass_weekly_digest_week(); ?></li>
                <li><a class="group-sub" id="dig-<?php echo $group->id; ?>"><?php _e('Daily Digest', 'bp-ass') ?></a> <?php _e('Get the day\'s activity bundled into one email', 'bp-ass') ?></li>
                <li><?php if ( ass_get_forum_type() ) : ?>
                    <a class="group-sub" id="sub-<?php echo $group->id; ?>"><?php _e('New Topics', 'bp-ass') ?></a> <?php _e('Send new topics as they arrive (but no replies)', 'bp-ass') ?><br>
                <?php endif; ?></li>
                <li><a class="group-sub" id="supersub-<?php echo $group->id; ?>"><?php _e('All Email', 'bp-ass') ?></a> <?php _e('Send all group activity as it arrives', 'bp-ass') ?></li>
            </ul>
        </li>
                
		<?php
		
		*/
		
	}
	
	// this adds the ajax-based subscription option in the group header, or group directory
	function ass_group_subscribe_button() {
		global $bp, $groups_template;
	
		if( ! empty( $groups_template ) ) {
			$group =& $groups_template->group;
		}
		else {
			$group = groups_get_current_group();
		}
	
		if ( !is_user_logged_in() || !empty( $group->is_banned ) || !$group->is_member )
			return;
	
		// if we're looking at someone elses list of groups hide the subscription
		if ( bp_displayed_user_id() && ( bp_loggedin_user_id() != bp_displayed_user_id() ) )
			return;
	
		$group_status = ass_get_group_subscription_status( bp_loggedin_user_id(), $group->id );
	
		if ( $group_status == 'no' )
			$group_status = NULL;
	
		$status_desc = __('Your email status is ', 'bp-ass');
		$link_text = __('change', 'bp-ass');
		$gemail_icon_class = ' gemail_icon';
		$sep = '';
	
		if ( !$group_status ) {
			//$status_desc = '';
			$link_text = __('Get email updates', 'bp-ass');
			$gemail_icon_class = '';
			$sep = '';
		}
	
		$status = ass_subscribe_translate( $group_status );
		?>

		<div class="group-subscription-div">
			<span class="group-subscription-status-desc"><?php echo $status_desc; ?></span>
			<span class="group-subscription-status<?php echo $gemail_icon_class ?>" id="gsubstat-<?php echo $group->id; ?>"><?php echo $status; ?></span> <?php echo $sep; ?>
			(<a id="gsublink-<?php echo $group->id; ?>" data-options="align:bottom" data-dropdown="gsubopt-<?php echo $group->id; ?>" aria-controls="gsubopt-<?php echo $group->id; ?>" aria-expanded="false" title="<?php _e('Change your email subscription options for this group.','chesnet');?>"><?php echo $link_text; ?></a>)
			<span class="ajax-loader" id="gsubajaxload-<?php echo $group->id; ?>"></span>
		</div>
		<ul id="gsubopt-<?php echo $group->id; ?>" class="f-dropdown small drop-bottom" data-dropdown-content aria-hidden="true" tabindex="-1">
			<li><a class="group-sub" id="no-<?php echo $group->id; ?>"><?php _e('No Email', 'bp-ass') ?><br /><?php _e('I will read this group on the web', 'bp-ass') ?></a></li>
			<li><a class="group-sub" id="sum-<?php echo $group->id; ?>"><?php _e('Weekly Summary', 'bp-ass') ?><br /><?php _e('Get a summary of topics each', 'bp-ass') ?> <?php echo ass_weekly_digest_week(); ?></a></li>
			<li><a class="group-sub" id="dig-<?php echo $group->id; ?>"><?php _e('Daily Digest', 'bp-ass') ?><br /><?php _e('Get the day\'s activity bundled into one email', 'bp-ass') ?></a></li>
	
			<?php if ( ass_get_forum_type() ) : ?>
				<li><a class="group-sub" id="sub-<?php echo $group->id; ?>"><?php _e('New Topics', 'bp-ass') ?><br /><?php _e('Send new topics as they arrive (but no replies)', 'bp-ass') ?></a></li>
			<?php endif; ?>
	
			<li><a class="group-sub" id="supersub-<?php echo $group->id; ?>"><?php _e('All Email', 'bp-ass') ?><br /><?php _e('Send all group activity as it arrives', 'bp-ass') ?></a></li>
		</ul>
	
		<?php
	}
	
	// create the default subscription settings during group creation and editing
	function ass_default_subscription_settings_form() {
		?>
		<h4><?php _e('Email Subscription Defaults', 'bp-ass'); ?></h4>
		<p><?php _e('When new users join this group, their default email notification settings will be:', 'bp-ass'); ?></p>
		<div class="row">
        	<div class="columns">
				<input class="wllabel" type="radio" name="ass-default-subscription" value="no" <?php ass_default_subscription_settings( 'no' ) ?> />
                <label><?php _e( 'No Email<br />(users will read this group on the web - good for any group - the default)', 'bp-ass' ) ?></label>
            </div>
        </div>
        <div class="row">
        	<div class="columns">
				<input class="wllabel" type="radio" name="ass-default-subscription" value="sum" <?php ass_default_subscription_settings( 'sum' ) ?> />
				<label><?php _e( 'Weekly Summary Email<br />(the week\'s topics - good for large groups)', 'bp-ass' ) ?></label>
            </div>
        </div>
        <div class="row">
        	<div class="columns">
            	<input class="wllabel" type="radio" name="ass-default-subscription" value="dig" <?php ass_default_subscription_settings( 'dig' ) ?> />
				<label><?php _e( 'Daily Digest Email<br />(all daily activity bundles in one email - good for medium-size groups)', 'bp-ass' ) ?></label>
            </div>
        </div>
		<?php if ( ass_get_forum_type() && false ) : //@todo disabled for now- figure out what this does for me ?>
            <label><input type="radio" name="ass-default-subscription" value="sub" <?php ass_default_subscription_settings( 'sub' ) ?> />
            <?php _e( 'New Topics Email (new topics are sent as they arrive, but not replies - good for small groups)', 'bp-ass' ) ?></label>
        <?php endif; ?>
        <div class="row">
        	<div class="columns">
            	<input class="wllabel" type="radio" name="ass-default-subscription" value="supersub" <?php ass_default_subscription_settings( 'supersub' ) ?> />
                <label><?php _e( 'All Email<br />(send emails about everything - recommended only for working groups)', 'bp-ass' ) ?></label>
            </div>
		</div>
		<hr />
		<?php
	}
	
	/* BBPress Plugin */
	
	//updates the category of a post to match the forum id
	//moving from third to test
	function save_post_topic($post_id){

		if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
			return $post_id;
		
		// If this is a revision, get real post ID
		if ( $parent_id = wp_is_post_revision( $post_id ) ) 
			$post_id = $parent_id;
		
		//get forum id (parent id)
		$old_forum_id = bbp_get_topic_forum_id($post_id);
		$forum_id = wp_get_post_parent_id($post_id);
		
		
		if($old_forum_id == $forum_id) return $post_id; //no changes

		//get groups for forum id
		$group_ids = array();
		$group_ids = bbp_get_forum_group_ids($forum_id);
		
		if($forum_id && $group_ids){
			//for each group id, get the corresponding category id, then add the post to those cats
			$cat_ids = array();
			foreach($group_ids as $group_id){
				$cat = $this->cn_get_cat_from_group($group_id);
				$cat_ids[] = $cat->term_id;
			}
			$cat_ids = array_map( 'intval', $cat_ids );
			$cat_ids = array_unique( $cat_ids );
			
			$term_taxonomy_ids = wp_set_object_terms( $post_id, $cat_ids, 'category' );
	
			if ( is_wp_error( $term_taxonomy_ids ) ) {
				$this->cn_catch_error($term_taxonomy_ids);
			}
			return $post_id;
		}
		else { //no handy function to remove a topic from all forums
			$post = get_post($post_id);
			bbp_update_topic_forum_id($post_id); //first update the forum id
			bbp_update_topic($post_id, 0, false, $post->post_author);
			
			//update old forum to remove meta
			bbp_update_forum(array(
				'forum_id' => $old_forum_id,
			));
			
			wp_set_object_terms( $post_id, null, 'category' );
			
		}
	}	
	
	//uses cn edit form url
	function bbp_get_topic_edit_url($url, $topic_id){
		if(!is_admin()){
			$url = $this->cn_getUrl( 'edit', $topic_id, null, 'topic' );
			$url = add_query_arg( 'cs', 'true', $url );
		}
		return $url;
	}
	
	//Filter redirect after multistep form is saved
	function redirect_after_multistep_form_save($redirect_url, $post_id, $post_type){
		if($post_type == 'topic'){
			$post = get_post($post_id);
			if($post->post_status !== 'publish'){
				$BBP_Forums_Group_Extension = new BBP_Forums_Group_Extension();
				
				$forum_id = bbp_get_topic_forum_id( $post_id );
				$group_ids = bbp_get_forum_group_ids( $forum_id );
				
				$group = groups_get_group( array( 'group_id' => $group_ids[0] ) );
				$redirect_url = trailingslashit( bp_get_group_permalink( $group ) ) . trailingslashit( $BBP_Forums_Group_Extension->slug );
			}
		}
		elseif($post_type == 'dlm_download') $redirect_url = get_manage_dlm_labels_url($post_id);
		
		return $redirect_url;
	}
	
	//removing a topic from all it's forums
	function remove_topic_from_group($group_id, $topic_id){
		$post = get_post($topic_id);
		$old_forum_id = bbp_get_topic_forum_id($post_id);

		$post_id = wp_update_post( array(
			'ID'			=> $topic_id,
			'post_parent'	=> 0
		) );
		
		bbp_update_topic_forum_id($topic_id); //first update the forum id
		bbp_update_topic($post_id, 0, false, $post->post_author);
		
		//update old forum to remove meta
		bbp_update_forum(array(
			'forum_id' => $old_forum_id,
		));
	}
	
	/* Group Calendar */
	function bp_group_calendar_widget_create_event($date = null){
		global $bp; ?>
        <div class="bp-widget">
		<h4><?php _e('Create Event', 'chesnet'); ?></h4>
        <?php
		$url = $this->cn_getUrl( 'add', null, null, 'bgc_event', null );
		
		$default_date = '';
		if ( !empty($date['year']) && !empty($date['month']) && !empty($date['day']) ) {
			$timestamp = strtotime($date['month'].'/'.$date['day'].'/'.$date['year']);
			if ($timestamp >= time()){
				$default_date = date('Y-m-d', $timestamp);
				$url = add_query_arg('event-date', $default_date, $url);
			}
		}
		//get group id
		$gid = $bp->groups->current_group->id;
		if(!empty($gid)) $url = add_query_arg('gid', $gid, $url);
		
		$url = add_query_arg('cs', 'true', $url);
		?>
        <a href="<?php echo $url; ?>" class="button"><?php _e('Add new event', 'chesnet'); ?></a>
        </div>
        
        <?php
	}
	
	function bp_group_calendar_create_event_url($url, $event_id, $edit=false, $group_id){
		if($edit){
			$url = $this->cn_getUrl('edit', $event_id, null, 'bgc_event');
			if($group_id) $url = add_query_arg('gid', $group_id, $url);
			$url = add_query_arg('cs', 'true', $url);
		}
		return $url;
	}
	
	/* Invite Anyone */
	//filters the users available to be invited to a group
	//may not need this since we want to display all the users and then disable the option to invite those that are already members or have alreay been invited
	//add_action( 'pre_user_query', array('CHESNET', 'invite_anyone_pre_user_query') );
	function invite_anyone_pre_user_query( $query ) {
		//exclude already invited members from the list
		$group_members = array();
		
		if ( bp_group_has_invites(array('group_id' => bp_get_group_id())) ) {
			global $invites_template;
			while ( bp_group_invites() ) {
				bp_group_the_invite();
				$group_members[] = $invites_template->invite->user->id;
			}
		}
		if(!empty($group_members)) $query->exclude = (isset($query->exclude) && is_array($query->exclude)) ? array_merge($query->exclude, $group_members) : $group_members;
	}

	
	/**
	 ** Email Handler
	 **/	
	function group_settings_email_address(){ ?>
        <h4><?php _e('Mailing List Settings', 'chesnet'); ?></h4>
        <p><?php _e('Email addresses members can use to send announcements to this Group from their email program (enter up to two).', 'chesnet'); ?></p>
        <div class="row">
        <?php //get group emails
        global $wpdb, $bp;
		$group = $bp->groups->current_group;
		$domain = 'chesapeakenetwork.org';
        $emails = groups_get_groupmeta($bp->groups->current_group->id, 'group_email_address', false);
        
        if(!$emails) $emails = array();
        $max_emails = count($emails) < 2 ? 2 : count($emails);
        
        for($i = 0; $i < $max_emails; $i++){
            
            $email = isset($emails[$i]) ? preg_replace( "/^([^@]+)(@.*)$/", "$1", $emails[$i]) : '';
			
			//create a default
			if((!$email || $email == '') && bp_get_groups_current_create_step() && $i == 0){
				
				$table = $wpdb->prefix . 'bp_groups_groupmeta';
				
				$email = $group->slug;
				$j = 2;
				do {
					$address = sanitize_email($email . '@' . $domain);
					
					$duplicate = $wpdb->get_results( "SELECT group_id FROM $table WHERE `meta_key` = 'group_email_address' AND `meta_value` = '{$address}'" );

					if(!$duplicate) {
						break;
					}
					$email = $group->slug . '-' . $j;
					$j++;
					
				} while ($duplicate);
				
			}
			
            $n = $i + 1;
            ?>
            <div class="medium-7 columns left">
                <div class="row collapse">
                    <label for="group-email-address-<?php echo $n; ?>"><?php printf(__('Email Address %d', 'chesnet'), $n); ?></label>
                    <div class="small-6 columns">
                        <input type="text" id="group-email-address-<?php echo $n; ?>" value="<?php echo $email; ?>" name="group-email-address[]">
                    </div>
                    <div class="small-8 columns">
                        <span class="postfix" title="&#64;chesapeakenetwork.org">&#64;chesapeakenetwork.org</span>
                    </div>
                </div>
            </div>
        <?php } 
        
        //only super admins can add multiple emails
        if(is_super_admin()): ?>
            <div class="medium-7 columns left">
                <label>&nbsp;</label><span class="button" id="group-admin-add-addt-email-field"><?php _e('Add email address', 'chesnet'); ?></span>
            </div>
        <?php endif; ?>
        </div>
        <p><small><?php _e('Contact the Network Administrator to request more than two group mailing list emails.', 'chesnet'); ?></small></p>
        
        <hr />

	<?php }
	
	function groups_group_after_save($group){
		global $wpdb;
		//update group email addresses
		if ( isset($_POST['group-email-address']) ) {
			//$old_emails = array('announcements@chesapeakenetwork.org', 'maryland@chesapeakenetwork.org', 'md@chesapeakenetwork.org')
			$old_emails = groups_get_groupmeta($group->id, 'group_email_address', false);
			$domain = 'chesapeakenetwork.org';
			//$_POST['group-email-address'] = array('announcements', 'maryland', 'virginia')
			$new_emails = array();
			foreach($_POST['group-email-address'] as $e){
				$new_emails[] = sanitize_email($e . '@' . $domain);
			}
			$emails_to_delete = array_diff($old_emails, $new_emails);
			$emails_to_add = array_filter(array_diff($new_emails, $old_emails));
			
			$sock = $this->open_DA_socket();
			
			if(!empty($emails_to_delete) && is_array($emails_to_delete)) {
				foreach($emails_to_delete as $email_to_delete) groups_delete_groupmeta( $group->id, 'group_email_address', $email_to_delete);
				$this->delete_email_alias($sock, $emails_to_delete);
			}
	
			if(!empty($emails_to_add) && is_array($emails_to_add)) {
				$table = $wpdb->prefix . 'bp_groups_groupmeta';
				foreach($emails_to_add as $email_to_add){
					//make sure we're not adding a duplicate
					$duplicate = false;
					$duplicate = $wpdb->get_results( "SELECT group_id FROM $table WHERE `meta_key` = 'group_email_address' AND `meta_value` = '{$email_to_add}'" );
					if($duplicate){
						bp_core_add_message(sprintf(__("The email address you entered '%s' is being used by another group.", 'chesnet'), $email_to_add), 'error');
						continue;
					}
					else {
						groups_add_groupmeta( $group->id, 'group_email_address', $email_to_add );
						$this->add_email_alias($sock, $email_to_add);
					}
				}
			}
		}
	}
	
	//Opens a new socket to DirectAdmin and return the socket object
	public function open_DA_socket(){
		$Socket = new HTTPSocket; 
		$Socket->connect("tcp://68.69.2.178", 2222); 
		$Socket->set_login(DA_USER, DA_PASSWORD); 
		$Socket->set_method('POST'); 			
		
		return $Socket;
	}
	
	//accepts 1 user (user or email address) to be forwarded to 1 address
	function add_email_alias($Socket, $user, $email = '"|/home/chesadmin/domains/chesapeakenetwork.org/public_html/emailhandler.php"'){
		$domain = 'chesapeakenetwork.org';
		$user_parts = explode('@', $user);
		$user = $user_parts[0];
		$Socket->query('/CMD_API_EMAIL_FORWARDERS', 
			array( 
				'domain' => $domain, 
				'action' => 'create', 
				'user' => $user, 
				'email' => $email
			)
		);
		//var_dump($Socket->result);
		if(strpos($Socket->result, 'error=0') === false){
			mail(get_option("admin_email"), '['.get_bloginfo('name').'] Error creating group email alias', print_r($Socket->result, true));
		}
	}
	
	//accepts an array or str of user(s) to be deleted
	function delete_email_alias($Socket, $users){
		$domain = 'chesapeakenetwork.org';
		$select = array();
		if(is_array($users)){
			$i = 0;
			foreach($users as $user){
				$user_parts = explode('@', $user);
				$user = $user_parts[0];
				$select['select' . $i] = $user;
				$i++;
			}
		}
		else {
			$user_parts = explode('@', $users);
			$user = $user_parts[0];
			$select['select0'] = $user;	
		}
		$defaults = array( 
			'domain' => $domain, 
			'action' => 'delete',
		);
		$q_args = array_merge($defaults, $select);
		$Socket->query('/CMD_API_EMAIL_FORWARDERS', 
			$q_args
		);
		//var_dump($Socket->result);
		if(strpos($Socket->result, 'error=0') === false){
			mail(get_option("admin_email"), '['.get_bloginfo('name').'] Error creating group email alias', "There was an error creating an alias for the group '{$group->name}' (ID: {$group->id})." . "\r\n" . "\r\n" . print_r($Socket->result, true));
		}
	}
	
	/**
	 ** Helpers
	 **/
	
	//Save errors and also email them to site admin
	function cn_catch_error($wp_error){
		if(is_wp_error($wp_error)) $message = date('d M Y - G:i:s') . "\n\r" . $wp_error->get_error_message();
		mail(get_bloginfo('admin_email'), 'Catchin\' Errors', $message);
	}
	 
	//Returns a post object, either the global post or the currentPost variable, set when adding a WP_Route
	function cn_get_post(){
		$post = $this->currentPost;
		if(!$post) global $post;
		
		return $post;
	}
	
	//Returns path for wp-content/uploads
	function getUploadsDir($key = null, $time = null){
		$upload_dir = wp_upload_dir($time);
		return $key ? $upload_dir[$key] : $upload_dir;
	}
	
	//Returns an array of all the post type objects that have the native category taxonomy
	//which in this theme is used to associate a post with a BP group
	function get_post_types_with_tax($taxonomy = 'category'){
		$post_types = get_post_types( '', 'objects' );
		$pts = array();
		foreach ( $post_types as $post_type ) {
			if(in_array($taxonomy, $post_type->taxonomies))
				$pts[] = $post_type;
		}
		return $pts;
	}
	
	//Unhook content filters from the content
	function cn_removeFilters() {
		remove_filter( 'the_content', 'wpautop' );
		remove_filter( 'the_content', 'wptexturize' );
	}
		
	///Display the login form and new account link
	function cn_login_form($post_type = null){
		do_action( 'cn_before_login_form' );
		do_action( 'cn_'.$post_type.'_before_login_form' );
		$output = '<p>' .'Please log in first.' . '</p>';
		$output .= wp_login_form( array( 'echo' => false ) );
		$output .= '<div class="register">';
		$output .= wp_register( '', '', false );
		$output .= '</div>';
		return $output;
	}
	 	
	/*
	 * get the URL for a specific action
	 *
	 * @param str $action The action being performed
	 * @param int $id The id of the post, if $action is set to 'edit'
	 * @param int $page The pagination number
	 * @param str $post_type The post type being used
	 * @param int $author The author id for posts
	 * @return str The url
	 */
	function cn_getUrl( $action, $id = null, $page = null, $post_type = null, $author = null ) {
		$author = get_userdata(intval($author));
		$url = '';
		if($id && !$post_type) $post_type = get_post_type($id);
		if ( '' == get_option( 'permalink_structure' ) ) {
			// pretty permalinks off
			$this->add_admin_notices('', 'error', 'Pretty permalinks must be enabled.');
			return;
		}
		else {
			if ( $id ) {
				if ( $post_type == 'cn_job' ) $url = home_url() . '/' .  'jobs' . '/' . $action . '/' . $id . '/';
				elseif ( $post_type == 'post' )  $url = home_url() . '/' .  'posts' . '/' . $action . '/' . $id . '/';
				elseif ( $post_type == 'dlm_download' )  $url = home_url() . '/' .  'resources' . '/' . $action . '/' . $id . '/';
				elseif ( $post_type == 'topic' )  $url = home_url() . '/' .  'questions' . '/' . $action . '/' . $id . '/';
				elseif ( $post_type == 'bgc_event' )  $url = home_url() . '/' .  'events' . '/' . $action . '/' . $id . '/';
			}
			elseif( $action == 'list') {
				if(!$author) {
					if($post_type == 'cn_job') $url = home_url() . '/' .  'jobs' . '/';
					elseif($post_type == 'post') $url = get_permalink( get_option('page_for_posts' ) );
					elseif($post_type == 'bgc_event') $url = home_url() . '/' .  'events' . '/';
					elseif($post_type == 'topic') $url = home_url() . '/' .  'topics' . '/';
					elseif($post_type == 'dlm_download') $url = home_url() . '/' .  'resources' . '/';
				}
				elseif($page > 1) {
					if($post_type == 'cn_job') $url = home_url() . '/' .  'jobs' . '/' . $author->user_login . '/' . $action . '/' . 'page' . '/' . $page . '/';
					elseif($post_type == 'post') $url = get_author_posts_url($author->ID);
					elseif($post_type == 'dlm_download') $url = home_url() . '/' .  'resources' . '/' . $author->user_login . '/' . $action . '/' . 'page' . '/' . $page . '/';
					elseif($post_type == 'topic') $url = bp_core_get_user_domain($author->ID) . 'forums/';
					elseif($post_type == 'bgc_event') $url = home_url() . '/' .  'events' . '/' . $author->user_login . '/' . $action . '/' . 'page' . '/' . $page . '/';
				}
				else { //action = list
					if($post_type == 'cn_job') $url = home_url() . '/' .  'jobs' . '/' . $author->user_login . '/' . $action . '/';
					elseif($post_type == 'post') $url = get_author_posts_url($author->ID);
					elseif($post_type == 'dlm_download') $url = home_url() . '/' .  'downloads' . '/' . $author->user_login . '/' . $action . '/';
					elseif($post_type == 'topic') $url = bp_core_get_user_domain($author->ID) . 'forums/';
					elseif($post_type == 'bgc_event') $url = home_url() . '/' .  'events' . '/' . $author->user_login . '/' . $action . '/';
				}
			}
			elseif ( $post_type == 'cn_job' ) $url = home_url() . '/' .  'jobs' . '/' . $action . '/';
			elseif ( $post_type == 'post' ) $url = home_url() . '/' .  'posts' . '/' . $action . '/';
			elseif ( $post_type == 'dlm_download' ) $url = home_url() . '/' .  'resources' . '/' . $action . '/';
			elseif ( $post_type == 'topic' ) $url = home_url() . '/' .  'questions' . '/' . $action . '/';
			elseif ( $post_type == 'bgc_event' ) $url = home_url() . '/' .  'events' . '/' . $action . '/';
		}

		if($id) return apply_filters('cn_getUrl', $url, get_post($id));
		else return apply_filters('cn_getUrl', $url, null);
	}
	
	//Get edit button for a post
	function cn_getEditButton( $post, $label = 'Edit', $before = '', $after = '', $override_permission = false ) {
		if(!$this->cn_userCanEdit($post) && !$override_permission) return;
		$output  = $before . '<a rel="nofollow" href="';
		$output .= $this->cn_getUrl( 'edit', $post->ID, null, $post->post_type );
		$output .= '"> ' . $label . '</a>' . $after;
		return $output;
	
	}
	
	//Determine if the specified user can edit a specific post
	function cn_userCanEdit( $post ) {
		
		if(is_int($post)) $post = get_post( $post, 'OBJECT' );
		if(empty($post) || empty($post->post_type)) return false;
		
		// admin override
		if ( current_user_can( 'administrator' ) || is_super_admin() ) {
			return true;
		}
		
		$post_type = $post->post_type;
		
		//if the capability wasn't set for a cpt, we need to check if the cpt has set a post type to model off of
		$post_type_obj = get_post_type_object( $post_type );
		$cap_type = $post_type_obj->capability_type;
		
		//pluralize post type if needed
		if ( substr( $post_type, -1 ) !== 's' )
			 $post_type = $post_type . 's';
		if ( substr( $cap_type, -1 ) !== 's' )
			 $cap_type = $cap_type . 's';
		
		//admin/high level user override
		if ( current_user_can( 'edit_published_' . $post_type ) && current_user_can( 'edit_others_' . $post_type ) )
			return true;
		if ( current_user_can( 'edit_published_' . $cap_type ) && current_user_can( 'edit_others_' . $cap_type ) )
			return true;
			
		if ( is_object( $post ) && get_current_user_id() == $post->post_author ) {
			if($post->post_type == 'topic' && current_user_can( 'edit_topic', $post->ID ) && !bbp_past_edit_lock( $post->post_date_gmt ) ){
				return true;
			}
			else{
				if ( ( $post->post_status == 'publish' && (current_user_can( 'edit_published_' . $post_type ) || current_user_can( 'edit_published_' . $cap_type )) ) || $post->post_status != 'publish' ) {
					return true;
				} else {
					return false;
				}
			}
		} else {
			return false;
		}
	}
	
	//Calculate the closing date for a job announcement from post publish date
	function cn_jobs_apply_by_date_calc($post_id){
		//get the published date fo the post
		$date = get_post_time( 'U', false, $post_id );
		return !$date ? false : date('y-m-d', strtotime("+60 days",$date));
	}
	
	//Filters a category link to the group's extension page
	function cn_category_link($termlink, $term_id){
		global $post, $bp;
		if(is_admin()) return $termlink;
		
		$group_id = $this->cn_get_group_from_cat($term_id);
		$group = groups_get_group( array( 'group_id' => $group_id ) );
		if(!$group) $group = groups_get_group( array( 'group_id' => 1 ) );
		
		$home = apply_filters('switch_group_link_to_home_or_component', false, $group, $term_id);
		
		//depending on the post type, we want to construct the link differently
		return $this->get_group_link_by_posttype($group, $post->post_type, $home);
		return $termlink;
	}
	
	//returns the permalink to a group component (or group homepage) depending on the post type
	function get_group_link_by_posttype($group = false, $post_type, $home = false){
		global $bp;
		if($group && !is_object($group)) $group = groups_get_group( array( 'group_id' => $group ) );
		if(!$group) $group = groups_get_group( array( 'group_id' => 1 ) );
		
		if($home) return bp_get_group_permalink($group);
		else{
			switch ($post_type) {
				case 'dlm_download': //downloads
					return bp_get_group_permalink($group) . 'group-library/'; //find a way to dynamically pull 'group-library'
					break;
				case 'bgc_event': //events
					return bp_get_group_permalink($group) . 'calendar/'; //find a way to dynamically pull 'calendar'
					break;
				case 'topic': //downloads
					return bp_get_group_permalink($group) .  'forum/'; //find a way to dynamically pull 'forum'
					break;
				default: //if !post_type || post_type == 'post' || post_type == 'cn_job', set the link to the main group page
					return bp_get_group_permalink($group);
			}
		}
	}
	
	//Gets all the groups a post is in (via categories) and lists them, with the current group unlinked
	//modified version of WP get_the_category_list()
	function cn_get_the_category_list($link_current_group = false, $separator = '', $parents = '', $post_id = false){
		global $wp_rewrite;
		if ( ! is_object_in_taxonomy( get_post_type( $post_id ), 'category' ) ) {
			/* This filter is documented in wp-includes/category-template.php */
			return apply_filters( 'the_category', '', $separator, $parents );
		}
	 
		$categories = get_the_category( $post_id );
		if ( empty( $categories ) ) {
			/* This filter is documented in wp-includes/category-template.php */
			return apply_filters( 'the_category', __( 'Uncategorized' ), $separator, $parents );
		}
	 
		$rel = ( is_object( $wp_rewrite ) && $wp_rewrite->using_permalinks() ) ? 'rel="category tag"' : 'rel="category"';
	 
		$thelist = '';
		if ( '' == $separator ) {
			$thelist .= '<ul class="post-groups">';
			foreach ( $categories as $category ) {
				$cg = $this->cn_set_group_id == $this->cn_get_group_from_cat($category->term_id) ? 'class="current_group" ' : ''; //current group?
				$thelist .= "\n\t<li>";
				switch ( strtolower( $parents ) ) {
					case 'multiple':
						if ( $category->parent )
							$thelist .= get_category_parents( $category->parent, true, $separator );
						$thelist .= (!$link_current_group && $cg) ?
							'<span class="current_group">' . $category->name . '</span></li>' : '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" title="' . esc_attr( sprintf( __( "View all posts in %s", 'chesnet' ), $category->name ) ) . '" ' . $cg . $rel . '>' . $category->name.'</a></li>';
						break;
					case 'single':
						$thelist .= (!$link_current_group && $cg) ?
							'<span class="current_group">' : '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" title="' . esc_attr( sprintf( __( "View all posts in %s", 'chesnet' ), $category->name ) ) . '" ' . $cg . $rel . '>';
						if ( $category->parent )
							$thelist .= get_category_parents( $category->parent, false, $separator );
						$thelist .= (!$link_current_group && $cg) ?
							$category->name.'</span></li>' : $category->name.'</a></li>';
						break;
					case '':
					default:
						$thelist .= (!$link_current_group && $cg) ?
							'<span class="current_group">' . $category->name.'</span></li>' : '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" title="' . esc_attr( sprintf( __( "View all posts in %s", 'chesnet' ), $category->name ) ) . '" ' . $cg . $rel . '>' . $category->name.'</a></li>';
				}
			}
			$thelist .= '</ul>';
		} else {
			$i = 0;
			foreach ( $categories as $category ) {
				$cg = $this->cn_set_group_id == $this->cn_get_group_from_cat($category->term_id) ? 'class="current_group" ' : ''; //current group?
				if ( 0 < $i )
					$thelist .= $separator;
				switch ( strtolower( $parents ) ) {
					case 'multiple':
						if ( $category->parent )
							$thelist .= get_category_parents( $category->parent, true, $separator );
						$thelist .= (!$link_current_group && $cg) ?
							'<span class="current_group">' . $category->name . '</span>' : '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" title="' . esc_attr( sprintf( __( "View all posts in %s", 'chesnet' ), $category->name ) ) . '" ' . $cg . $rel . '>' . $category->name.'</a>';
						break;
					case 'single':
						$thelist .= (!$link_current_group && $cg) ?
							'<span class="current_group">' : '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" title="' . esc_attr( sprintf( __( "View all posts in %s", 'chesnet' ), $category->name ) ) . '" ' . $cg . $rel . '>';
						if ( $category->parent )
							$thelist .= get_category_parents( $category->parent, false, $separator );
						$thelist .= (!$link_current_group && $cg) ?
							$category->name . "</span>" : $category->name . "</a>";
						break;
					case '':
					default:
						$thelist .= (!$link_current_group && $cg) ?
							'<span class="current_group">' . $category->name . '</span>' : '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" title="' . esc_attr( sprintf( __( "View all posts in %s", 'chesnet' ), $category->name ) ) . '" ' . $cg . $rel . '>' . $category->name.'</a>';
				}
				++$i;
			}
		}
	 
		return apply_filters( 'the_category', $thelist, $separator, $parents );
	}
	
	//returns the mime type for a file
	function _mime_content_type($filename) {
		if(class_exists('finfo')){
			$result = new finfo();
		
			if (is_resource($result) === true) {
				return $result->file($filename, FILEINFO_MIME_TYPE);
			}
		}
		else{
			$imagetype = exif_imagetype($filename);
			return image_type_to_mime_type($imagetype);
		}
	}
	
	function _get_the_tag_list($before, $sep, $after, $urls = true){
		if(!$urls){
			$posttags = get_the_tags();
			if ($posttags && is_array($posttags)) {
				$tags = array();
				foreach($posttags as $posttag) $tags[] = $posttag->name;
				if(empty($sep)) $sep = ', ';
				return $before . implode($sep, $tags) . $after;
			}
		}
		else return get_the_tag_list( $before, $sep, $after );
	}
	
	//returns a time_stamp in relative "ago" format
	// $time is in EST (UTC-5)
	function ago($time) {
		$periods = array(
			__('second', 'chesnet'),
			__('minute', 'chesnet'),
			__('hour', 'chesnet'),
			__('day', 'chesnet'),
			__('week', 'chesnet'),
			__('month', 'chesnet'),
			__('year', 'chesnet'),
			__('decade', 'chesnet'),
		);
		$lengths = array("60","60","24","7","4.35","12","10");
		
		$now = time();
		
		$difference = $now - $time;
		$tense = __('ago', 'chesnet');
		
		for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
			$difference /= $lengths[$j];
		}
		
		$difference = round($difference);
		
		if($difference != 1) {
			$periods[$j].= __('s', 'chesnet');
		}
		
		return $difference . ' ' . $periods[$j] . ' ' . $tense;
	}
	
	//Returns the difference between two dates
	//date args must be a unix timestamp
	function date_difference($date, $now=false){
		if(!$now) $now = time();
		$datediff = $date - $now;
		return ceil($datediff/(60*60*24));	
	}
	
	
}
$GLOBALS['chesnet'] = new CHESNET;












/* Helpers */
//returns an array of the groups a post is in (via its categories)
function cn_get_post_groups($post){
	$post = get_post($post);
	if(!$post) return false;
	$group_ids = $cat_ids = array();
	
	//get the post's categories
	$cat_slugs = wp_get_object_terms( $post->ID, 'category', array('fields' => 'slugs') );
	if(is_wp_error($cat_slugs) || empty($cat_slugs) || !is_array($cat_slugs)){
		 $group_ids = array(1); 
	}
	else{
		foreach($cat_slugs as $cat_slug){
			//get group of each cat_id and add to array
			$group_ids[] = cn_gid_from_cat_slug($cat_slug);
		}
	}
	
	$group_ids = empty($group_ids) ? array(1) : $group_ids;
	
	$args = array('include' => $group_ids, 'per_page' => false);
	return groups_get_groups( $args );
}

//return the group a post is in (via its category)
function cn_get_post_group($post){
	$post = get_post($post);
	if(!$post) return false;
	
	$group_id = cn_get_gid_from_post_id($post->ID);
	return groups_get_group(array('group_id' => $group_id));
}

//returns the group id from a given post
function cn_get_gid_from_post_id($post_id){
	$retval = false;
	$cats = wp_get_post_categories( $post_id, array('fields' => 'slugs') );
	if($cats && is_array($cats)){
		$cat = $cats[0];
		$retval = cn_gid_from_cat_slug($cat);
	}
	return $retval;
}

//Gets the group ID from a category ID or object
function cn_get_group_from_cat($cat_id){
	$cat = get_category( $cat_id );
	if(!$cat) return false; //if an invalid ID or object was provided, bail
	
	$group_id = cn_gid_from_cat_slug($cat->slug);
	return (int)$group_id;
}

//Gets the category slug from a group ID
function cn_cat_slug_from_gid($gid){
	return 'cn_cat_group_' . $gid;
}

//Gets the group ID from a category slug
function cn_gid_from_cat_slug($slug){
	//preg_match_all('!\d+!', $slug, $matches);
	//$m = max(array_keys($matches[0]));
	//return $matches[0][$m];
	return (int)str_replace('cn_cat_group_', '', $slug); //faster, but less generic and flexible if the slug format changes
}

//Gets the category term (object) from a group ID.
function cn_get_cat_from_group($gid){
	$cat_slug = cn_cat_slug_from_gid($gid);
	$term = get_term_by( 'slug', $cat_slug, 'category' );
	return $term;
}

//checks if a post is in a group by cross referencing the post's category
function cn_is_post_in_group($post, $group_id=false){
	$post = get_post($post);
	if(!$post) return false;
	
	if(!$group_id){
		global $bp;
		$group_id = (int)$bp->groups->current_group->id;
	}
	else $group_id = intval($group_id);
	
	//get the post's categories
	$cat_ids = array();
	$cat_ids = wp_get_object_terms( $post->ID, 'category', array('fields' => 'ids') );
	if(!is_wp_error($cat_ids)){
		foreach($cat_ids as $cat_id){
			if(cn_get_group_from_cat($cat_id) == $group_id) return true;
		}
	}
	return false;
}
















function dump_hook( $tag, $hook ) {
    ksort($hook);

    echo "<pre>>>>>>\t$tag<br>";

    foreach( $hook as $priority => $functions ) {

	echo $priority;

	foreach( $functions as $function )
	    if( $function['function'] != 'list_hook_details' ) {

		echo "\t";

		if( is_string( $function['function'] ) )
		    echo $function['function'];

		elseif( is_string( $function['function'][0] ) )
		     echo $function['function'][0] . ' -> ' . $function['function'][1];

		elseif( is_object( $function['function'][0] ) )
		    echo "(object) " . get_class( $function['function'][0] ) . ' -> ' . $function['function'][1];

		else
		    print_r($function);

		echo ' (' . $function['accepted_args'] . ') <br>';
		}
    }

    echo '</pre>';
}

function list_hooks( $filter = false ){
	global $wp_filter;

	$hooks = $wp_filter;
	ksort( $hooks );

	foreach( $hooks as $tag => $hook )
	    if ( false === $filter || false !== strpos( $tag, $filter ) )
			dump_hook($tag, $hook);
}

function list_hook_details( $input = NULL ) {
    global $wp_filter;

    $tag = current_filter();
    if( isset( $wp_filter[$tag] ) )
		dump_hook( $tag, $wp_filter[$tag] );

	return $input;
}

function list_live_hooks( $hook = false ) {
    if ( false === $hook )
		$hook = 'all';

    add_action( $hook, 'list_hook_details', -1 );
}
?>