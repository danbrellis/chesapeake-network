<?php do_action( 'bp_before_member_settings_template' ); ?>

<form action="<?php echo bp_displayed_user_domain() . bp_get_settings_slug() . '/general'; ?>" method="post" class="standard-form" id="settings-form">

    
	<?php if ( !is_super_admin() ) : ?>
        <label for="pwd"><?php _e( 'Current Password <span>(required to update email or change current password)</span>', 'buddypress' ); ?>
        	<input type="password" name="pwd" id="pwd" size="16" value="" class="settings-input small" aria-describedby="forgotpasswordtxt" />
            <span id="forgotpasswordtxt"><a href="<?php echo wp_lostpassword_url(); ?>" title="<?php esc_attr_e( 'Password Lost and Found', 'buddypress' ); ?>"><?php _e( 'Forgot password?', 'buddypress' ); ?></a></span>
        </label>
        
	<?php endif; ?>
	<div class="row">
    	<div class="large-14 columns">
            <label for="email"><?php _e( 'Account Email', 'buddypress' ); ?>
                <input type="text" name="email" id="email" value="<?php echo bp_get_displayed_user_email(); ?>" class="settings-input" />
            </label>
        </div>
    </div>
	<?php _e( 'Change Password <span>(leave blank for no change)</span>', 'buddypress' ); ?>
    <div class="row">
        <div class="large-7 columns">
            <label for="pass1"><?php _e( 'New Password', 'buddypress' ); ?>
                <input type="password" name="pass1" id="pass1" value="" class="error settings-input small password-entry" />
                <span id="pass-strength-result" class="error"></span>
            </label>
        </div>
        <div class="large-7 columns">
            <label for="pass2"><?php _e( 'Repeat New Password', 'buddypress' ); ?>
                <input type="password" name="pass2" id="pass2" size="16" value="" class="settings-input small password-entry-confirm" />
            </label>
        </div>
    </div>

	<?php do_action( 'bp_core_general_settings_before_submit' ); ?>

	<div class="submit">
		<input type="submit" name="submit" value="<?php esc_attr_e( 'Save Changes', 'buddypress' ); ?>" id="submit" class="auto" />
	</div>

	<?php do_action( 'bp_core_general_settings_after_submit' ); ?>

	<?php wp_nonce_field( 'bp_settings_general' ); ?>

</form>

<?php do_action( 'bp_after_member_settings_template' ); ?>
