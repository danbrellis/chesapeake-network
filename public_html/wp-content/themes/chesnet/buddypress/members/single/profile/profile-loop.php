<?php global $chesnet;
do_action( 'bp_before_profile_loop_content' ); ?>

<?php if ( bp_has_profile() ) :
	$chesnet->profile_tabs = array();
	$i = 0; ?>

	<div class="tabs-content">
		<?php while ( bp_profile_groups() ) : bp_the_profile_group(); ?>
    
            <?php if ( bp_profile_group_has_fields() ) : ?>
    
                <?php do_action( 'bp_before_profile_field_content' ); ?>
                <?php //add tabs to global chesnet to be used in the sidebar
				$chesnet->profile_tabs[$i++] = array('slug' => bp_get_the_profile_group_slug() . '-panel',
												     'name' => bp_get_the_profile_group_name());
				?>
    
                <div class="content bp-widget <?php bp_the_profile_group_slug(); if($i == 1) echo ' active'; ?>" id="<?php bp_the_profile_group_slug(); ?>-panel">
    
                    <h4><?php bp_the_profile_group_name(); ?></h4>
        
                    <div>

						<?php while ( bp_profile_fields() ) : bp_the_profile_field(); ?>
    
                            <?php if ( bp_field_has_data() ) : 
								$chesnet->add_css_classes_to_field(array('row')); ?>
    
                                <div<?php bp_field_css_class(); ?>>
    
                                    <div class="small-6 columns"><?php bp_the_profile_field_name(); ?></div>
    
                                    <div class="small-8 columns"><?php bp_the_profile_field_value(); ?></div>
    
                                </div>
    
                            <?php endif; ?>
    
                            <?php do_action( 'bp_profile_field_item' ); ?>
    
                        <?php endwhile; ?>
    
                    </div>
                </div>
    
                <?php do_action( 'bp_after_profile_field_content' ); ?>
    
            <?php endif; ?>
    
        <?php endwhile; ?>
    </div>

	<?php do_action( 'bp_profile_field_buttons' ); ?>

<?php endif; ?>

<?php do_action( 'bp_after_profile_loop_content' ); ?>
