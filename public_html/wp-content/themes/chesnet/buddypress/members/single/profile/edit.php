<?php do_action( 'bp_before_profile_edit_content' );

if ( bp_has_profile( 'profile_group_id=' . bp_get_current_profile_group_id() ) ) :
	while ( bp_profile_groups() ) : bp_the_profile_group(); ?>

<form action="<?php bp_the_profile_group_edit_form_action(); ?>" method="post" id="profile-edit-form" class="standard-form <?php bp_the_profile_group_slug(); ?>">

	<?php do_action( 'bp_before_profile_field_content' ); ?>

		<h4><?php printf( __( "Editing '%s' Profile Group", "buddypress" ), bp_get_the_profile_group_name() ); ?></h4>

		<?php if ( bp_profile_has_multiple_groups() ) : ?>
			<!--<ul class="button-nav hide">
				<?php //bp_profile_group_tabs(); ?>
			</ul>-->
		<?php endif; ?>

		<div class="clear"></div>
        <div class="row">
		<?php global $profile_template; $i = 0;
		while ( bp_profile_fields() ) : bp_the_profile_field(); ?>
			<div class="medium-<?php echo $profile_template->field_count > 1 ? '7' : '14'; ?> columns">
                <div<?php bp_field_css_class( 'editfield' ); ?>>

                    <?php $profile_field_description = bp_get_the_profile_field_description();
                    $field_type = bp_xprofile_create_field_type( bp_get_the_profile_field_type() );
                    $edit_field_html_args = $profile_field_description ? array('aria-describedby' => bp_get_the_profile_field_id().-'helptxt') : array();
                    $field_type->edit_field_html($edit_field_html_args); ?>
                    <?php if($profile_field_description): ?>
                        <span id="<?php bp_the_profile_field_id(); ?>-helptxt"><?php echo $profile_field_description; ?></span>
                    <?php endif;

                    do_action( 'bp_custom_profile_edit_fields_pre_visibility' );

                    if ( bp_current_user_can( 'bp_xprofile_change_field_visibility' ) ) : ?>
                            
                        <?php bp_profile_visibility_radio_buttons(array('before' => '<ul id="visdrop-'.bp_get_the_profile_field_input_name().'" data-dropdown-content class="f-dropdown sepfromtrigger" aria-hidden="false" aria-autoclose="true"><li class="dd-title">'.__( 'Who can see this field?', 'chesnet' ).'</li>')) ?>
                    <?php endif ?>

                    <?php do_action( 'bp_custom_profile_edit_fields' ); ?>
                </div>
            </div><!-- //.medium-14.columns -->
            <?php $i++;
            if($i % 2 == 0): ?>
            </div><div class="row">
            <?php endif; ?>

		<?php endwhile; ?>
    </div>

	<?php do_action( 'bp_after_profile_field_content' ); ?>

	<div class="submit">
		<input type="submit" name="profile-group-edit-submit" id="profile-group-edit-submit" value="<?php esc_attr_e( 'Save Changes', 'buddypress' ); ?> " />
	</div>

	<input type="hidden" name="field_ids" id="field_ids" value="<?php bp_the_profile_field_ids(); ?>" />

	<?php wp_nonce_field( 'bp_xprofile_edit' ); ?>

</form>

<?php endwhile; endif; ?>

<?php do_action( 'bp_after_profile_edit_content' ); ?>
