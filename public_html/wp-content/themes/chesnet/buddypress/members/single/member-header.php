<?php

/**
 * BuddyPress - Users Header
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 */

?>

<?php do_action( 'bp_before_member_header' );
global $bp, $chesnet; ?>

<div id="user-page-banner" class="page-banner">
	<?php if(bp_core_can_edit_settings()): ?><a href="<?php echo bp_core_get_user_domain(bp_displayed_user_id()) . 'profile/change-cover/'; ?>" data-tooltip aria-haspopup="true" class="has-tip edit-photo edit-cover" title="<?php _e( 'Change Cover Photo', 'chesnet' ); ?>"><i class="fi-camera"></i></a><?php endif; ?>
    <div id="item-header-avatar">
    	<?php if(bp_core_can_edit_settings()): ?>
        	<div class="row">
        		<div class="small-4 columns small-centered">
        			<a href="<?php echo bp_core_get_user_domain(bp_displayed_user_id()) . 'profile/change-avatar/'; ?>" data-tooltip aria-haspopup="true" class="has-tip edit-photo edit-avatar" title="<?php _e( 'Change Profile Photo', 'chesnet' ); ?>"><i class="fi-camera"></i></a>
                </div>
            </div>
        <?php endif; ?>
        <?php //check if user has title or org in profile
		$org = sanitize_text_field(xprofile_get_field_data( 'Organization', bp_displayed_user_id() ));
		$job = sanitize_text_field(xprofile_get_field_data( 'Job Title', bp_displayed_user_id() ));
		if(!empty($org) || !empty($job)):
		?>
        <a href="<?php bp_displayed_user_link(); ?>" data-tooltip aria-haspopup="true" class="has-tip tip-right" title="<?php echo $job; ?><br /><?php echo $org; ?><br />">
            <?php bp_displayed_user_avatar('type=full'); ?>
        </a>
        <?php else: ?>
        	<?php bp_displayed_user_avatar('type=full'); ?>
        <?php endif; ?>
    </div><!-- #item-header-avatar -->
    <div id="item-header-data">
        <h2><?php bp_displayed_user_fullname(); ?><?php if(is_super_admin()) printf(' (<span title="%s">%d</span>)', __('Member ID', 'chesnet'), bp_displayed_user_id()); ?></h2>
        <span class="highlight"><span class="activity"><?php bp_last_activity( bp_displayed_user_id() ); ?></span>
    </div>
    <div id="item-header-actions">
    	<ul class="button-group">
            <?php do_action( 'bp_member_header_actions' ); ?>
            <?php if ( bp_is_item_admin() ) :
				$manage_link = bp_displayed_user_domain() . 'settings';
				?>
				<li><a href="<?php echo $manage_link; ?>" <?php /*data-dropdown="drop1" aria-controls="drop1" aria-expanded="false" */ ?>class="group-button"><span class="dashicons dashicons-menu"></span></a>
					<?php /* @todo: show manage subnav
					<br><ul id="drop1" data-dropdown-content class="f-dropdown" aria-hidden="true" tabindex="-1">
						<li><a href="#">This is a link</a></li>
						<li><a href="#">This is another</a></li>
						<li><a href="#">Yet another</a></li>
					</ul>
					*/ ?>
				</li>
            <?php endif; ?>
        </ul>
    </div>
</div>

<!--
<div id="item-header-avatar">
	<a href="<?php bp_displayed_user_link(); ?>">

		<?php bp_displayed_user_avatar( 'type=full' ); ?>

	</a>
</div>

<div id="item-header-content">

	<?php if ( bp_is_active( 'activity' ) && bp_activity_do_mentions() ) : ?>
		<h2 class="user-nicename">@<?php bp_displayed_user_mentionname(); ?></h2>
	<?php endif; ?>

	<span class="activity"><?php bp_last_activity( bp_displayed_user_id() ); ?></span>

	<?php do_action( 'bp_before_member_header_meta' ); ?>

	<div id="item-meta">

		<?php if ( bp_is_active( 'activity' ) ) : ?>

			<div id="latest-update">

				<?php bp_activity_latest_update( bp_displayed_user_id() ); ?>

			</div>

		<?php endif; ?>

		<div id="item-buttons">

			<?php do_action( 'bp_member_header_actions' ); ?>

		</div>

		<?php
		/***
		 * If you'd like to show specific profile fields here use:
		 * bp_member_profile_data( 'field=About Me' ); -- Pass the name of the field
		 */
		 do_action( 'bp_profile_header_meta' );

		 ?>

	</div>

</div>
-->

<?php do_action( 'bp_after_member_header' ); ?>