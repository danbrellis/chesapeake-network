<div id="0buddypress">

	<?php global $chesnet, $field;
	do_action( 'bp_before_register_page' ); ?>

	<div class="page" id="register-page">

		<form action="" name="signup_form" id="signup_form" class="standard-form" method="post" enctype="multipart/form-data">

		<?php if ( 'registration-disabled' == bp_get_current_signup_step() ) : ?>
			<?php if(!did_action('template_notices')) do_action( 'template_notices' ); ?>
			<?php do_action( 'bp_before_registration_disabled' ); ?>

				<p><?php _e( 'User registration is currently not allowed.', 'buddypress' ); ?></p>

			<?php do_action( 'bp_after_registration_disabled' ); ?>
		<?php endif; // registration-disabled signup setp ?>

		<?php if ( 'request-details' == bp_get_current_signup_step() ) : ?>

			<?php if(!did_action('template_notices')) do_action( 'template_notices' ); ?>

			<p><?php _e( 'Registering for this site is easy. Just fill in the fields below, and we\'ll get a new account set up for you in no time.', 'buddypress' ); ?></p>

			<?php do_action( 'bp_before_account_details_fields' ); ?>

			<div class="register-section" id="basic-details-section">
            	<?php /***** Basic Account Details ******/ ?>
                <h4><?php _e( 'Account Details', 'chesnet' ); ?></h4>
                
                <div class="row">
                	<div class="large-14 columns">
                        <label for="signup_username" class="hide"><?php _e( 'Username', 'chesnet' ); ?> <small class="req"><?php _e( '(required)', 'chesnet' ); ?></small></label>
                        <?php //do_action( 'bp_signup_username_errors' ); ?>
                        <input type="hidden" name="signup_username" id="signup_username" value="" />
        
                        <label for="signup_email"><?php _e( 'Email Address', 'chesnet' ); ?> <small class="req"><?php _e( '(required)', 'chesnet' ); ?></small>
							<?php do_action( 'bp_signup_email_errors' ); ?>
                            <input type="text" name="signup_email" id="signup_email" value="<?php bp_signup_email_value(); ?>" />
                        </label>
                    </div>
                </div>
                <div class="row">
                	<div class="medium-7 columns">

                        <label for="signup_password"><?php _e( 'Choose a Password', 'chesnet' ); ?> <small class="req"><?php _e( '(required)', 'chesnet' ); ?></small>
                        	<input type="password" name="signup_password" id="signup_password" value="" class="error settings-input small password-entry" />
                        	<span id="pass-strength-result" class="error"></span>
                        </label>
                    </div>
                	<div class="medium-7 columns">
                        <label for="signup_password_confirm"><?php _e( 'Confirm Password', 'chesnet' ); ?> <small class="req"><?php _e( '(required)', 'chesnet' ); ?></small>
                        	<input type="password" name="signup_password_confirm" id="signup_password_confirm" value="" class="settings-input small password-entry-confirm" />
                        </label>
                     </div>
                 </div>
				<?php do_action( 'bp_account_details_fields' ); ?>

			</div><!-- #basic-details-section -->

			<?php do_action( 'bp_after_account_details_fields' ); ?>

			<?php /***** Extra Profile Details ******/ ?>

			<?php if ( bp_is_active( 'xprofile' ) ) : ?>

				<?php do_action( 'bp_before_signup_profile_fields' ); ?>

				<div class="register-section" id="profile-details-section">

				<?php /* Use the profile field loop to render input fields for the 'base' profile field group */			
				//if ( bp_is_active( 'xprofile' ) ) : if ( bp_has_profile( array( 'profile_group_id' => 1, 'fetch_field_data' => false ) ) ) : while ( bp_profile_groups() ) : bp_the_profile_group(); ?>
                <?php if ( bp_is_active( 'xprofile' ) ) : if ( bp_has_profile( array( 'exclude_groups' => '6,3', 'fetch_field_data' => false ) ) ) : ?>
                	<ul class="tabs hide-if-no-js" data-tab>
                	<?php global $profile_template; foreach($profile_template->groups as $key => $group): if(count($group->fields) > 0):
						$sanitized_name = sanitize_title($group->name); ?>
                        <li class="tab-title<?php if($key == 0){ echo ' active'; $active = $sanitized_name; } ?>"><a href="#<?php echo $sanitized_name; ?>"><?php echo $group->name; ?></a></li>
                    <?php endif; endforeach; ?>
                    </ul>

                	<div class="tabs-content">
               		<?php while ( bp_profile_groups() ) : bp_the_profile_group(); ?>
                    	<?php //if ( bp_profile_group_has_fields() ): 
						$i = 0; $sanitized_name = sanitize_title(bp_get_the_profile_group_name()); ?>
                        	<div id="<?php echo $sanitized_name; ?>" class="content<?php if($active == $sanitized_name) echo ' active'; ?>">
                                <h4 class="hide-if-js"><?php bp_the_profile_group_name(); ?></h4>
                                <div class="row">
                                    <?php while ( bp_profile_fields() ) : bp_the_profile_field(); ?>
                                        <div class="medium-<?php echo $profile_template->field_count > 1 ? '7' : '14'; ?> columns">
                                            <div<?php bp_field_css_class( 'editfield' ); ?>>
    
												<?php $profile_field_description = bp_get_the_profile_field_description();
												$field_type = bp_xprofile_create_field_type( bp_get_the_profile_field_type() );
												$edit_field_html_args = $profile_field_description ? array('aria-describedby' => bp_get_the_profile_field_id().-'helptxt') : array();
                                                $field_type->edit_field_html($edit_field_html_args); ?>
                                                <?php if($profile_field_description): ?>
                                                	<span id="<?php bp_the_profile_field_id(); ?>-helptxt"><?php echo $profile_field_description; ?></span>
                                                <?php endif;
                
												do_action( 'bp_custom_profile_edit_fields_pre_visibility' );
                
												if ( bp_current_user_can( 'bp_xprofile_change_field_visibility' ) ) : ?>
														
                                                    <?php bp_profile_visibility_radio_buttons(array('before' => '<ul id="visdrop-'.bp_get_the_profile_field_input_name().'" data-dropdown-content class="f-dropdown sepfromtrigger" aria-hidden="false" aria-autoclose="true"><li class="dd-title">'.__( 'Who can see this field?', 'chesnet' ).'</li>')) ?>
												<?php endif ?>
                
												<?php do_action( 'bp_custom_profile_edit_fields' ); ?>
                                            </div>
                                        </div><!-- //.medium-14.columns -->
                                        <?php $i++;
                                        if($i % 2 == 0): ?>
                                        </div><div class="row">
                                        <?php endif; ?>
                                    <?php endwhile; ?>
                                </div><!-- //.row -->
                            </div>
                            
                        <?php //endif;
					endwhile; ?>
                    </div><!-- //.tabs-content -->
                    <input type="hidden" name="signup_profile_field_ids" id="signup_profile_field_ids" value="<?php bp_the_profile_field_ids(); ?>" />
				<?php endif; endif; 
				remove_filter('bp_get_the_profile_field_name', 'cn_bp_get_the_profile_field_name'); ?>
                
				<?php do_action( 'bp_signup_profile_fields' ); ?>

				</div><!-- #profile-details-section -->

				<?php do_action( 'bp_after_signup_profile_fields' ); ?>

			<?php endif; ?>

			<?php if ( bp_get_blog_signup_allowed() ) : ?>
                
                                <?php do_action( 'bp_before_blog_details_fields' ); ?>
                
                                <?php /***** Blog Creation Details ******/ ?>
                
                                <div class="register-section" id="blog-details-section">
                
                                    <h4><?php _e( 'Blog Details', 'chesnet' ); ?></h4>
                
                                    <p><input type="checkbox" name="signup_with_blog" id="signup_with_blog" value="1"<?php if ( (int) bp_get_signup_with_blog_value() ) : ?> checked="checked"<?php endif; ?> /> <?php _e( 'Yes, I\'d like to create a new site', 'chesnet' ); ?></p>
                
                                    <div id="blog-details"<?php if ( (int) bp_get_signup_with_blog_value() ) : ?>class="show"<?php endif; ?>>
                
                                        <label for="signup_blog_url"><?php _e( 'Blog URL', 'chesnet' ); ?> <?php _e( '(required)', 'chesnet' ); ?></label>
                                        <?php do_action( 'bp_signup_blog_url_errors' ); ?>
                
                                        <?php if ( is_subdomain_install() ) : ?>
                                            http:// <input type="text" name="signup_blog_url" id="signup_blog_url" value="<?php bp_signup_blog_url_value(); ?>" /> .<?php bp_signup_subdomain_base(); ?>
                                        <?php else : ?>
                                            <?php echo home_url( '/' ); ?> <input type="text" name="signup_blog_url" id="signup_blog_url" value="<?php bp_signup_blog_url_value(); ?>" />
                                        <?php endif; ?>
                
                                        <label for="signup_blog_title"><?php _e( 'Site Title', 'chesnet' ); ?> <?php _e( '(required)', 'chesnet' ); ?></label>
                                        <?php do_action( 'bp_signup_blog_title_errors' ); ?>
                                        <input type="text" name="signup_blog_title" id="signup_blog_title" value="<?php bp_signup_blog_title_value(); ?>" />
                
                                        <span class="label"><?php _e( 'I would like my site to appear in search engines, and in public listings around this network.', 'chesnet' ); ?></span>
                                        <?php do_action( 'bp_signup_blog_privacy_errors' ); ?>
                
                                        <label><input type="radio" name="signup_blog_privacy" id="signup_blog_privacy_public" value="public"<?php if ( 'public' == bp_get_signup_blog_privacy_value() || !bp_get_signup_blog_privacy_value() ) : ?> checked="checked"<?php endif; ?> /> <?php _e( 'Yes', 'chesnet' ); ?></label>
                                        <label><input type="radio" name="signup_blog_privacy" id="signup_blog_privacy_private" value="private"<?php if ( 'private' == bp_get_signup_blog_privacy_value() ) : ?> checked="checked"<?php endif; ?> /> <?php _e( 'No', 'chesnet' ); ?></label>
                
                                        <?php do_action( 'bp_blog_details_fields' ); ?>
                
                                    </div>
                
                                </div><!-- #blog-details-section -->
                
                                <?php do_action( 'bp_after_blog_details_fields' ); ?>

			<?php endif; ?>

			<?php do_action( 'bp_before_registration_submit_buttons' ); ?>

            <div class="submit">
                <input type="submit" name="signup_submit" id="signup_submit" value="<?php esc_attr_e( 'Complete Sign Up', 'chesnet' ); ?>" />
            </div>

			<?php do_action( 'bp_after_registration_submit_buttons' ); ?>

			<?php wp_nonce_field( 'bp_new_signup' ); ?>

		<?php endif; // request-details signup step ?>

		<?php if ( 'completed-confirmation' == bp_get_current_signup_step() ) : ?>

			<?php if(!did_action('template_notices')) do_action( 'template_notices' ); ?>
			<?php do_action( 'bp_before_registration_confirmed' ); ?>

			<?php if ( bp_registration_needs_activation() ) : ?>
				<p class="h4"><?php _e( 'You have successfully created your account!', 'chesnet'); ?></p>
                <p class="h4"><?php _e( 'To begin using this site you will need to activate your account via the email we have just sent to your address (make sure to check your spam box).', 'chesnet' ); ?></p>
			<?php else : ?>
				<p class="h4"><?php _e( 'You have successfully created your account! Please log in using the username and password you have just created.', 'chesnet' ); ?></p>
			<?php endif; ?>

			<?php do_action( 'bp_after_registration_confirmed' ); ?>

		<?php endif; // completed-confirmation signup step ?>

		<?php do_action( 'bp_custom_signup_steps' ); ?>

		</form>

	</div>

	<?php do_action( 'bp_after_register_page' ); ?>

</div><!-- #buddypress -->
