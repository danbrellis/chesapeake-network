<?php
get_header(); ?>
<div class="row">
    <div class="small-14 medium-10 medium-push-4 large-7 large-push-3 columns" role="main">
		
        <?php do_action( 'bp_before_directory_activity_content' ); ?>
        
        <header>
            <h1 class="entry-title screen-reader-text"><?php the_title(); ?></h1>
        </header>
        <?php /*<form id="cn_tags_filter_activity_query">
        	<input name="tags" class="cn-tagit-input" id="activity_tags" value="Apple, Orange">
        </form>
		*/ ?>
        <div class="content">
        
			<?php while (have_posts()) : the_post(); ?>
				<?php the_content(); ?>
            <?php endwhile; ?>
    
        </div><!-- .activity -->
    
        <?php do_action( 'bp_after_directory_activity_list' ); ?>
    
        <?php do_action( 'bp_directory_activity_content' ); ?>
    
        <?php do_action( 'bp_after_directory_activity_content' ); ?>
    
        <?php do_action( 'bp_after_directory_activity' ); ?>
    </div>
    <?php get_sidebar('my-activity-left'); get_sidebar('right'); ?>
</div>
<?php get_footer(); ?>
