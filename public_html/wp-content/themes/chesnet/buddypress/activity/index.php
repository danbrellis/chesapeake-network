<?php do_action( 'bp_before_directory_activity' ); ?>

<div id="0buddypress">
	<?php do_action( 'bp_before_directory_activity_content' ); ?>

	<?php if ( is_user_logged_in() ) : ?>

		<?php //bp_get_template_part( 'activity/post-form' ); ?>

	<?php endif; ?>

	<div class="item-list-tabs activity-type-tabs" role="navigation">
        <ul class="sub-nav item-list-tabs activity-type-tabs" role="navigation">
            <li><?php _e('Filter:', 'chesnet'); ?></li>
            <?php do_action( 'bp_before_activity_type_tab_all' ); ?>
            <?php if ( is_user_logged_in() && bp_is_active( 'groups' ) ) : ?>
                <?php do_action( 'bp_before_activity_type_tab_groups' ); ?>
                <?php if ( bp_get_total_group_count_for_user( bp_loggedin_user_id() ) ) : ?>
                    
                    <li class="selected" id="activity-groups"><a href="<?php echo bp_loggedin_user_domain() . bp_get_activity_slug() . '/' . bp_get_groups_slug() . '/'; ?>" title="<?php esc_attr_e( 'The activity of groups I am a member of.', 'chesnet' ); ?>"><?php printf( __( 'My Groups %s', 'chesnet' ), '<span class="label round secondary">' . bp_get_total_group_count_for_user( bp_loggedin_user_id() ) . '</span>' ); ?></a></li>

                <?php endif; ?>
            
            <?php else: //if user isn't logged in, default to hide_sitewide = false?>
                <li class="selected" id="activity-all"><a href="<?php bp_activity_directory_permalink(); ?>" title="<?php esc_attr_e( 'The public activity for everyone on this site.', 'chesnet' ); ?>"><?php printf( __( 'All Members %s', 'chesnet' ), '<span class="label round secondary">' . bp_get_total_member_count() . '</span>' ); ?></a></li>
            <?php endif; ?>
            
            <?php if ( is_user_logged_in() ) : ?>
            
                <?php do_action( 'bp_before_activity_type_tab_favorites' ); ?>
                <?php if ( bp_get_total_favorite_count_for_user( bp_loggedin_user_id() ) ) : ?>
                    <li id="activity-favorites"><a href="<?php echo bp_loggedin_user_domain() . bp_get_activity_slug() . '/favorites/'; ?>" title="<?php esc_attr_e( "The activity I've marked as a favorite.", 'chesnet' ); ?>"><?php printf( __( 'My Favorites %s', 'chesnet' ), '<span class="label round secondary">' . bp_get_total_favorite_count_for_user( bp_loggedin_user_id() ) . '</span>' ); ?></a></li>
                <?php endif; ?>
    
                <?php do_action( 'bp_before_activity_type_tab_mentions' ); ?>
                <?php if ( bp_activity_do_mentions() ) : ?>
                    <li id="activity-mentions"><a href="<?php echo bp_loggedin_user_domain() . bp_get_activity_slug() . '/mentions/'; ?>" title="<?php esc_attr_e( 'Activity that I have been mentioned in.', 'chesnet' ); ?>"><?php _e( 'Mentions', 'chesnet' ); ?><?php if ( bp_get_total_mention_count_for_user( bp_loggedin_user_id() ) ) : ?> <strong><span class="label round secondary"><?php bp_get_total_mention_count_for_user( bp_loggedin_user_id() ); ?></span></strong><?php endif; ?></a></li>
                <?php endif; //bp_activity_do_mentions() ?>
    
                <?php do_action( 'bp_activity_type_tabs' ); ?>
            <?php endif; //is_user_logged_in() ?>
        </ul>
		<?php /* //old ul.activity-tab-type
		<ul>
			<?php do_action( 'bp_before_activity_type_tab_all' ); ?>

			<li class="selected" id="activity-all"><a href="<?php bp_activity_directory_permalink(); ?>" title="<?php esc_attr_e( 'The public activity for everyone on this site.', 'chesnet' ); ?>"><?php printf( __( 'All Members <span>%s</span>', 'chesnet' ), bp_get_total_member_count() ); ?></a></li>

			<?php if ( is_user_logged_in() ) : ?>

				<?php do_action( 'bp_before_activity_type_tab_friends' ); ?>

				<?php if ( bp_is_active( 'friends' ) ) : ?>

					<?php if ( bp_get_total_friend_count( bp_loggedin_user_id() ) ) : ?>

						<li id="activity-friends"><a href="<?php echo bp_loggedin_user_domain() . bp_get_activity_slug() . '/' . bp_get_friends_slug() . '/'; ?>" title="<?php esc_attr_e( 'The activity of my friends only.', 'chesnet' ); ?>"><?php printf( __( 'My Friends <span>%s</span>', 'chesnet' ), bp_get_total_friend_count( bp_loggedin_user_id() ) ); ?></a></li>

					<?php endif; ?>

				<?php endif; ?>

				<?php do_action( 'bp_before_activity_type_tab_groups' ); ?>

				<?php if ( bp_is_active( 'groups' ) ) : ?>

					<?php if ( bp_get_total_group_count_for_user( bp_loggedin_user_id() ) ) : ?>

						<li id="activity-groups"><a href="<?php echo bp_loggedin_user_domain() . bp_get_activity_slug() . '/' . bp_get_groups_slug() . '/'; ?>" title="<?php esc_attr_e( 'The activity of groups I am a member of.', 'chesnet' ); ?>"><?php printf( __( 'My Groups <span>%s</span>', 'chesnet' ), bp_get_total_group_count_for_user( bp_loggedin_user_id() ) ); ?></a></li>

					<?php endif; ?>

				<?php endif; ?>

				<?php do_action( 'bp_before_activity_type_tab_favorites' ); ?>

				<?php if ( bp_get_total_favorite_count_for_user( bp_loggedin_user_id() ) ) : ?>

					<li id="activity-favorites"><a href="<?php echo bp_loggedin_user_domain() . bp_get_activity_slug() . '/favorites/'; ?>" title="<?php esc_attr_e( "The activity I've marked as a favorite.", 'chesnet' ); ?>"><?php printf( __( 'My Favorites <span>%s</span>', 'chesnet' ), bp_get_total_favorite_count_for_user( bp_loggedin_user_id() ) ); ?></a></li>

				<?php endif; ?>

				<?php if ( bp_activity_do_mentions() ) : ?>

					<?php do_action( 'bp_before_activity_type_tab_mentions' ); ?>

					<li id="activity-mentions"><a href="<?php echo bp_loggedin_user_domain() . bp_get_activity_slug() . '/mentions/'; ?>" title="<?php esc_attr_e( 'Activity that I have been mentioned in.', 'chesnet' ); ?>"><?php _e( 'Mentions', 'chesnet' ); ?><?php if ( bp_get_total_mention_count_for_user( bp_loggedin_user_id() ) ) : ?> <strong><span><?php printf( _nx( '%s new', '%s new', bp_get_total_mention_count_for_user( bp_loggedin_user_id() ), 'Number of new activity mentions', 'chesnet' ), bp_get_total_mention_count_for_user( bp_loggedin_user_id() ) ); ?></span></strong><?php endif; ?></a></li>

				<?php endif; ?>

			<?php endif; ?>

			<?php do_action( 'bp_activity_type_tabs' ); ?>
		</ul>*/ ?>
	</div><!-- .item-list-tabs.row -->
    
	<?php
    /**
	 * Removing this for now because we do activity filtering in the sidebar for message types
	 *
	<div class="item-list-tabs no-ajax" id="subnav" role="navigation">
		<ul>
			<li class="feed"><a href="<?php bp_sitewide_activity_feed_link(); ?>" title="<?php esc_attr_e( 'RSS Feed', 'chesnet' ); ?>"><?php _e( 'RSS', 'chesnet' ); ?></a></li>

			<?php do_action( 'bp_activity_syndication_options' ); ?>

			<li id="activity-filter-select" class="last">
				<label for="activity-filter-by"><?php _e( 'Show:', 'chesnet' ); ?></label>
				<select id="activity-filter-by">
					<option value="-1"><?php _e( '&mdash; Everything &mdash;', 'chesnet' ); ?></option>

					<?php bp_activity_show_filters(); ?>

					<?php do_action( 'bp_activity_filter_options' ); ?>

				</select>
			</li>
		</ul>
	</div><!-- .item-list-tabs -->
	**/ ?>
	
	<?php do_action( 'bp_before_directory_activity_list' );	?>
	<?php if(!did_action('template_notices')) do_action( 'template_notices' ); ?>
	<div class="activity" id="activity-container" role="main">

		<?php bp_get_template_part( 'activity/activity-loop' ); ?>
        
	</div><!-- .activity -->

	<?php do_action( 'bp_after_directory_activity_list' ); ?>
	
	<?php do_action( 'bp_directory_activity_content' ); ?>

	<?php do_action( 'bp_after_directory_activity_content' ); ?>

	<?php do_action( 'bp_after_directory_activity' ); ?>

</div>
