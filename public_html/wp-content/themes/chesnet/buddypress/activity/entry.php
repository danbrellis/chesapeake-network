<?php

/**
 * BuddyPress - Activity Stream (Single Item)
 *
 * This template is used by activity-loop.php and AJAX functions to show
 * each activity.
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 */

?>

<?php do_action( 'bp_before_activity_entry' );
global $activities_template, $chesnet, $post;
$chesnet->cn_set_current_group_id($activities_template->activity->item_id);


if($activities_template->activity->secondary_item_id !== '0') {
	$post = get_post($activities_template->activity->secondary_item_id);
	setup_postdata( $post );
	$activity_type = $post->post_type;
}
if(!$post || $activities_template->activity->secondary_item_id == '0'){
	$activity_type = 'group';
}
?>

<li class="<?php bp_activity_css_class(); ?>" id="activity-<?php bp_activity_id(); ?>">
    <?php if(bp_get_activity_type() == 'created_group'): ?>
    	<div class="activity-body">
			<?php bp_activity_action(); ?>
        </div>
    <?php else: ?>
    	<?php //@todo: make this an anchor that links to a filtered activity stream ?>
        <span class="activity-type-icon activity-<?php echo $activity_type; ?>" title="<?php echo cn_get_activity_type($activity_type, 'new_name'); ?>"><span class="screen-reader-text"><?php echo cn_get_activity_type($activity_type, 'new_name'); ?></span></span>
        <div class="activity-type clearfix">
            <div class="activity-groups-avatars">
                <?php echo bp_get_activity_secondary_avatar(array('width' => 50, 'height' => 50)); ?>
            </div>
            <h5 class="uppercase"><?php echo cn_get_activity_type($activity_type, 'new_name'); ?></h5>
        </div><!-- //.activity-type -->
        <div class="activity-body">
            <div class="activity-header clearfix">
                <?php bp_activity_action(); ?>
            </div>
            <?php if ( bp_activity_has_content() ) : ?>
                <div class="activity-inner clearfix">
                    <?php bp_activity_content_body(); ?>
                </div>
            <?php endif; ?>
            <?php do_action( 'bp_activity_entry_content' ); ?>
        </div><!-- //.activity-body -->
        <div class="activity-meta clearfix">
            <div class="activity-more">
                <a href="<?php echo cn_bp_activity_get_permalink(null, $activities_template->activity); ?>"><?php _e('Read More', 'chesnet'); ?></a>
                
                <a href="<?php echo cn_bp_activity_get_permalink(null, $activities_template->activity); ?>" class="hide"><?php _e('Read More', 'chesnet'); ?></a>
            </div>
            <?php echo get_the_tag_list(sprintf('<div class="activity-tags left">%s: ', __('Tagged with', 'chesnet')), ', ', '</div>'); ?>
            <div class="activity-social right">
                <ul class="inline-list">
                    <li>
                        <?php
                        if ( bp_activity_can_favorite() && is_user_logged_in() ) {
                            $fav_class = 'favorite-activity left imgWrapLeft';
                            if(!bp_get_activity_is_favorite()){
                                $fav_href = bp_get_activity_favorite_link();
                                $fav_title = __( 'Mark as favorite.', 'chesnet' );
                            }
                            else{
                                $fav_href = bp_get_activity_unfavorite_link();
                                $fav_class .= ' favorited';
                                $fav_title = __( 'Remove favorite.', 'chesnet' );
                            }
                            printf('<a href="%s" class="%s" title="%s">
                                        <i class="fi-star"></i>
                                    </a>',
                                    $fav_href,
                                    $fav_class,
                                    $fav_title
                            );
                        }
                        else printf('<i class="fi-star left imgWrapLeft"></i>');
                        ?>
                        <span class="favorite-count right" id="activity-<?php bp_activity_id(); ?>-fc" title="<?php echo cn_favorites_number(); ?>">
                            <?php cn_bp_activity_favorite_count(); ?>
                        </span>
                    </li>
                    <?php if($activities_template->activity->secondary_item_id !== '0' && $activities_template->activity->component == 'groups' && comments_open()):  ?>
                        <?php if(is_user_logged_in() || $post->comment_count !== "0"): ?>
                        <li>
                            <?php if(is_user_logged_in()): ?>
                                <a href="<?php echo cn_bp_activity_get_permalink(null, $activities_template->activity); ?>#respond" class="comment-activity left imgWrapLeft" title="<?php _e( 'Post a comment.', 'chesnet' ); ?>">
                                    <i class="fi-comment"></i>
                                </a>
                            <?php else: ?>
                                <i class="fi-comment left imgWrapLeft"></i>
                            <?php endif; ?>
                            <span class="comment-count right" id="activity-<?php bp_activity_id(); ?>-cc">
                                <?php comments_number( '', '1', '%' ); ?>
                            </span>
                        </li>
                        <?php endif; ?>
                    <?php endif; ?>
                </ul>
            </div>
        </div><!-- //.activity-meta -->
        <div id="activity-<?php bp_activity_id(); ?>-modal" class="reveal-modal" data-reveal><a class="close-reveal-modal">&#215;</a></div>
    <?php endif; //if activity_type is created_group ?>
</li>

<?php do_action( 'bp_after_activity_entry' ); ?>
