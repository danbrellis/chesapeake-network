<?php do_action( 'bp_before_activity_loop' ); ?>

<?php global $chesnet, $bp;
$chesnet->showing_activity_stream = true;
if ( bp_has_activities( bp_ajax_querystring( 'activity' ) ) ) : ?>

	<?php if ( empty( $_POST['page'] ) ) : ?>

		<ul id="activity-stream" class="activity-list no-bullet">

	<?php endif; ?>

	<?php while ( bp_activities() ) : bp_the_activity(); ?>
		<?php 
		remove_filter( 'bp_get_activity_action', 'bp_activity_filter_kses', 1 );
		remove_filter( 'bp_get_activity_action', 'force_balance_tags' );
		remove_filter( 'bp_get_activity_action', 'wptexturize' );
		remove_filter( 'bp_get_activity_action', 'convert_chars' );
		remove_filter( 'bp_get_activity_action', 'wpautop' );
		remove_filter( 'bp_get_activity_action', 'make_clickable', 9 );
		remove_filter( 'bp_get_activity_action', 'stripslashes_deep', 5 );
		remove_all_filters('bp_get_activity_action_pre_meta', 10);
		
		remove_filter( 'bp_get_activity_content_body', 'bp_activity_filter_kses', 1 );
		remove_filter( 'bp_get_activity_content', 'bp_activity_filter_kses', 1 );
		remove_filter( 'bp_get_activity_content_body', 'wpautop' );
		remove_filter( 'bp_get_activity_content', 'wpautop' );
		remove_filter( 'bp_get_activity_content_body', 'bp_activity_truncate_entry', 5 );
		remove_filter( 'bp_get_activity_content', 'bp_activity_truncate_entry', 5 );
		
		remove_filter( 'comments_open', 'bp_comments_open', 10, 2 );
		bp_get_template_part( 'activity/entry' );
        wp_reset_postdata();
		add_filter( 'comments_open', 'bp_comments_open', 10, 2 ); ?>

	<?php endwhile; ?>

	<?php if ( bp_activity_has_more_items() ) : ?>

		<li class="load-more">
			<a href="<?php bp_activity_load_more_link() ?>"><?php _e( 'Load More', 'buddypress' ); ?></a>
		</li>

	<?php endif; ?>

	<?php if ( empty( $_POST['page'] ) ) : ?>

		</ul>

	<?php endif; ?>

<?php else : ?>

	<div id="message" class="info">
		<p><?php _e( 'Sorry, there was no activity found. Please try a different filter.', 'buddypress' ); ?></p>
	</div>

<?php endif; ?>

<?php do_action( 'bp_after_activity_loop' ); ?>

<?php if ( empty( $_POST['page'] ) ) : ?>

	<form action="" name="activity-loop-form" id="activity-loop-form" method="post">

		<?php wp_nonce_field( 'activity_filter', '_wpnonce_activity_filter' ); ?>

	</form>

<?php endif; ?>