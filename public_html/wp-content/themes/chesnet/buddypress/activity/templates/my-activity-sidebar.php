<?php

function show_my_activity_groups_list(){
	$list = cn_get_groups_for_my_activity_sidebar('<li>', '</li>');
	if(empty($list)) echo '<p>' . __('You have not joined any groups.', 'chesnet') . '</p>';
	else echo '<ul class="subtle-sidenav">' . $list . '</ul>';
}

function cn_get_groups_for_my_activity_sidebar($before='', $after=''){
	global $chesnet;
	$r = '';
	$groups = $chesnet->cn_get_user_groups();
	if(isset($groups['groups'])) return $chesnet->format_group_list_for_widget($groups['groups'], $before, $after);
}

?>