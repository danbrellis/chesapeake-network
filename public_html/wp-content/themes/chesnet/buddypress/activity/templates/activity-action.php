<?php
/*
 *
 * Template file for actiity action
 *
 */ 
?>

<div class="activity-avatar left">
	<a href="<?php echo $user_url; ?>">
		<?php echo $user_avatar; ?>   
	</a>
</div>
<div class="byline">
	<span class="author"><?php echo $user_link; ?></span>
	<span class="lowercase"><?php printf(__('added a %s in', 'chesnet'), cn_get_activity_type($post->post_type, 'new_name')); ?></span>
	<span class="group"><a href="<?php echo $group_url; ?>"><?php echo $group->name; ?></a></span>
</div>