<?php do_action( 'bp_before_create_group_page' ); 
global $bp; ?>

<div id="0buddypress">

	<?php do_action( 'bp_before_create_group_content_template' ); 
	
	if(!did_action('template_notices')) do_action( 'template_notices' );
	
	?>

	<form action="<?php bp_group_creation_form_action(); ?>" method="post" id="create-group-form" class="standard-form" enctype="multipart/form-data">

		<?php do_action( 'bp_before_create_group' ); 
		
		//render navigation
		$creation_steps = $bp->groups->group_creation_steps;
		if ( !bp_get_groups_current_create_step() ) {
			$keys = array_keys( $bp->groups->group_creation_steps );
			$bp->groups->current_create_step = array_shift( $keys );
		}
		$current_step_slug = bp_get_groups_current_create_step();
		$current_step = $creation_steps[bp_get_groups_current_create_step()];
		$no_steps_complete = 1;
		$total_no_steps = count((array) $creation_steps);
		
		while (key($creation_steps) !== $current_step_slug) { 
			$prev_slug = key($creation_steps);
			if (next($creation_steps) === false) throw new Exception('Invalid key'); 
			$no_steps_complete++;
		}
		next($creation_steps);
		$next_slug = key($creation_steps);
		?>
        
        <div class="progress success round" title="<?php printf(__('%d out of %d steps.', 'chesnet'), $no_steps_complete, $total_no_steps); ?>">
            <span class="meter" style="width: <?php echo ($no_steps_complete / $total_no_steps) * 100; ?>%"></span>
        </div>
        
        <div class="row">
        	<div class="small-7 medium-4 columns text-left">
            	<h5><a href="<?php bp_groups_directory_permalink(); ?>create/step/<?php echo $prev_slug; ?>">
					<?php echo isset($prev_slug) ? sprintf( __('&laquo; %s', 'chesnet'), $creation_steps[$prev_slug]['name']) : '&nbsp;'; ?>
                </a></h5>
            </div>
            <div class="small-7 medium-push-6 medium-4 columns text-right">
            	<?php if(bp_are_previous_group_creation_steps_complete( $next_slug )){
					$class = false;
					$a = '<a href="'.bp_get_groups_directory_permalink().'create/step/'.$next_slug.'">';
					$close_a = '</a>';
				}
				else {
					$class = ' class="subheader"';
					$a = $close_a = false;
				} ?>
            	<h5<?php echo $class; ?>><?php echo $next_slug ? $a . sprintf( __('%s &raquo;', 'chesnet'), $creation_steps[$next_slug]['name']) . $close_a : '&nbsp;'; ?></h5>
            </div>
            <div class="small-14 medium-pull-4 medium-6 columns text-center"><h4 title="<?php _e('Current step in group creation.', 'chesnet'); ?>"><?php echo $current_step['name']; ?></h4></div>
        </div>

		<?php /* removing this for now in lieu of above progress bar
		<div class="item-list-tabs no-ajax" id="group-create-tabs" role="navigation">
			<ul class="tabs" data-tab>

				<?php bp_group_creation_tabs(); ?>

			</ul>
		</div>
		*/ ?>

		<div class="tabs-content item-body" id="group-create-body">

			<?php /* Group creation step 1: Basic group details */ ?>
			<?php if ( bp_is_group_creation_step( 'group-details' ) ) : ?>

				<?php do_action( 'bp_before_group_details_creation_step' ); ?>

				<div>
					<label for="group-name"><?php _e( 'Group Name (required)', 'buddypress' ); ?></label>
					<input type="text" name="group-name" id="group-name" aria-required="true" value="<?php bp_new_group_name(); ?>" />
				</div>

				<div>
					<label for="group-desc"><?php _e( 'Group Description (required)', 'buddypress' ); ?></label>
					<textarea name="group-desc" id="group-desc" aria-required="true"><?php bp_new_group_description(); ?></textarea>
				</div>

				<?php
				do_action( 'bp_after_group_details_creation_step' );
				do_action( 'groups_custom_group_fields_editable' ); // @Deprecated

				wp_nonce_field( 'groups_create_save_group-details' ); ?>

			<?php endif; ?>

			<?php /* Group creation step 2: Group settings */ ?>
			<?php if ( bp_is_group_creation_step( 'group-settings' ) ) : ?>

				<?php do_action( 'bp_before_group_settings_creation_step' ); ?>

				<h4><?php _e( 'Privacy Options', 'buddypress' ); ?></h4>

				<div class="row">
                    <div class="large-14 columns">
						<input type="radio" name="group-status" value="public"<?php if ( 'public' == bp_get_new_group_status() || !bp_get_new_group_status() ) { ?> checked="checked"<?php } ?> /><label><strong><?php _e( 'This is a public group', 'buddypress' ); ?></strong></label>
                        <ul>
                            <li><?php _e( 'Any site member can join this group.', 'buddypress' ); ?></li>
                            <li><?php _e( 'This group will be listed in the groups directory and in search results.', 'buddypress' ); ?></li>
                            <li><?php _e( 'Group content and activity will be visible to any site member.', 'buddypress' ); ?></li>
                        </ul>
                        <input type="radio" name="group-status" value="private"<?php if ( 'private' == bp_get_new_group_status() ) { ?> checked="checked"<?php } ?> /><label><strong><?php _e( 'This is a private group', 'buddypress' ); ?></strong></label>
                        <ul>
                            <li><?php _e( 'Only users who request membership and are accepted can join the group.', 'buddypress' ); ?></li>
                            <li><?php _e( 'This group will be listed in the groups directory and in search results.', 'buddypress' ); ?></li>
                            <li><?php _e( 'Group content and activity will only be visible to members of the group.', 'buddypress' ); ?></li>
                        </ul>
                        <input type="radio" name="group-status" value="hidden"<?php if ( 'hidden' == bp_get_new_group_status() ) { ?> checked="checked"<?php } ?> /><label><strong><?php _e('This is a hidden group', 'buddypress' ); ?></strong></label>
                        <ul>
                            <li><?php _e( 'Only users who are invited can join the group.', 'buddypress' ); ?></li>
                            <li><?php _e( 'This group will not be listed in the groups directory or search results.', 'buddypress' ); ?></li>
                            <li><?php _e( 'Group content and activity will only be visible to members of the group.', 'buddypress' ); ?></li>
                        </ul>
                    </div>
                </div>

                <hr />

				<h4><?php _e( 'Group Invitations', 'buddypress' ); ?></h4>

				<p><?php _e( 'Which members of this group are allowed to invite others?', 'buddypress' ); ?></p>

				<div class="row">
                    <div class="large-14 columns">
						<input type="radio" name="group-invite-status" value="members"<?php bp_group_show_invite_status_setting( 'members' ); ?> /><label><strong><?php _e( 'All group members', 'buddypress' ); ?></strong></label>
                    </div>
                </div>
                <div class="row">
                    <div class="large-14 columns">
                    	<input type="radio" name="group-invite-status" value="mods"<?php bp_group_show_invite_status_setting( 'mods' ); ?> /><label><strong><?php _e( 'Group admins and mods only', 'buddypress' ); ?></strong></label>
                    </div>
                </div>
                <div class="row">
                    <div class="large-14 columns">
                    	<input type="radio" name="group-invite-status" value="admins"<?php bp_group_show_invite_status_setting( 'admins' ); ?> /><label><strong><?php _e( 'Group admins only', 'buddypress' ); ?></strong></label>
                    </div>
                </div>
                
                <hr />

				<?php if ( bp_is_active( 'forums' ) ) : ?>

					<h4><?php _e( 'Group Forums', 'buddypress' ); ?></h4>

					<?php if ( bp_forums_is_installed_correctly() ) : ?>

						<p><?php _e( 'Should this group have a forum?', 'buddypress' ); ?></p>

						<div class="checkbox">
							<label><input type="checkbox" name="group-show-forum" id="group-show-forum" value="1"<?php checked( bp_get_new_group_enable_forum(), true, true ); ?> /> <?php _e( 'Enable discussion forum', 'buddypress' ); ?></label>
						</div>
					<?php elseif ( is_super_admin() ) : ?>

						<p><?php printf( __( '<strong>Attention Site Admin:</strong> Group forums require the <a href="%s">correct setup and configuration</a> of a bbPress installation.', 'buddypress' ), bp_core_do_network_admin() ? network_admin_url( 'settings.php?page=bb-forums-setup' ) :  admin_url( 'admin.php?page=bb-forums-setup' ) ); ?></p>

					<?php endif; ?>

				<?php endif; ?>

				<?php do_action( 'bp_after_group_settings_creation_step' ); ?>

				<?php wp_nonce_field( 'groups_create_save_group-settings' ); ?>

			<?php endif; ?>

			<?php /* Group creation step 3: Avatar Uploads */ ?>
			<?php if ( bp_is_group_creation_step( 'group-avatar' ) ) : ?>

				<?php do_action( 'bp_before_group_avatar_creation_step' ); ?>

				<?php if ( 'upload-image' == bp_get_avatar_admin_step() ) : ?>

					<div class="left-menu">

						<?php bp_new_group_avatar(); ?>

					</div><!-- .left-menu -->

					<div class="main-column">
						<p><?php _e( "Upload an image to use as a profile photo for this group. The image will be shown on the main group page, and in search results.", 'buddypress' ); ?></p>

						<p>
							<input type="file" name="file" id="file" />
							<input type="submit" name="upload" id="upload" value="<?php esc_attr_e( 'Upload Image', 'buddypress' ); ?>" />
							<input type="hidden" name="action" id="action" value="bp_avatar_upload" />
						</p>

						<p><?php _e( 'To skip the group profile photo upload process, hit the "Next Step" button.', 'buddypress' ); ?></p>
					</div><!-- .main-column -->

				<?php endif; ?>

				<?php if ( 'crop-image' == bp_get_avatar_admin_step() ) : ?>

					<h4><?php _e( 'Crop Group Profile Photo', 'buddypress' ); ?></h4>

					<img src="<?php bp_avatar_to_crop(); ?>" id="avatar-to-crop" class="avatar" alt="<?php esc_attr_e( 'Profile photo to crop', 'buddypress' ); ?>" />

					<div id="avatar-crop-pane">
						<img src="<?php bp_avatar_to_crop(); ?>" id="avatar-crop-preview" class="avatar" alt="<?php esc_attr_e( 'Profile photo preview', 'buddypress' ); ?>" />
					</div>

					<input type="submit" name="avatar-crop-submit" id="avatar-crop-submit" value="<?php esc_attr_e( 'Crop Image', 'buddypress' ); ?>" />

					<input type="hidden" name="image_src" id="image_src" value="<?php bp_avatar_to_crop_src(); ?>" />
					<input type="hidden" name="upload" id="upload" />
					<input type="hidden" id="x" name="x" />
					<input type="hidden" id="y" name="y" />
					<input type="hidden" id="w" name="w" />
					<input type="hidden" id="h" name="h" />

				<?php endif; ?>

				<?php do_action( 'bp_after_group_avatar_creation_step' ); ?>

				<?php wp_nonce_field( 'groups_create_save_group-avatar' ); ?>

			<?php endif; ?>

			<?php /* Group creation step 4: Invite friends to group */ ?>
			<?php if ( bp_is_group_creation_step( 'group-invites' ) ) : ?>

				<?php do_action( 'bp_before_group_invites_creation_step' ); ?>

				<?php if ( bp_is_active( 'friends' ) && bp_get_total_friend_count( bp_loggedin_user_id() ) ) : ?>

					<div class="left-menu">

						<div id="invite-list">
							<ul>
								<?php bp_new_group_invite_friend_list(); ?>
							</ul>

							<?php wp_nonce_field( 'groups_invite_uninvite_user', '_wpnonce_invite_uninvite_user' ); ?>
						</div>

					</div><!-- .left-menu -->

					<div class="main-column">

						<div id="message" class="info">
							<p><?php _e('Select people to invite from your friends list.', 'buddypress' ); ?></p>
						</div>

						<?php /* The ID 'friend-list' is important for AJAX support. */ ?>
						<ul id="friend-list" class="item-list" role="main">

						<?php if ( bp_group_has_invites() ) : ?>

							<?php while ( bp_group_invites() ) : bp_group_the_invite(); ?>

								<li id="<?php bp_group_invite_item_id(); ?>">

									<?php bp_group_invite_user_avatar(); ?>

									<h4><?php bp_group_invite_user_link(); ?></h4>
									<span class="activity"><?php bp_group_invite_user_last_active(); ?></span>

									<div class="action">
										<a class="remove" href="<?php bp_group_invite_user_remove_invite_url(); ?>" id="<?php bp_group_invite_item_id(); ?>"><?php _e( 'Remove Invite', 'buddypress' ); ?></a>
									</div>
								</li>

							<?php endwhile; ?>

							<?php wp_nonce_field( 'groups_send_invites', '_wpnonce_send_invites' ); ?>

						<?php endif; ?>

						</ul>

					</div><!-- .main-column -->

				<?php else : ?>

					<div id="message" class="info">
						<p><?php _e( 'Once you have built up friend connections you will be able to invite others to your group.', 'buddypress' ); ?></p>
					</div>

				<?php endif; ?>

				<?php wp_nonce_field( 'groups_create_save_group-invites' ); ?>

				<?php do_action( 'bp_after_group_invites_creation_step' ); ?>

			<?php endif; ?>

			<?php do_action( 'groups_custom_create_steps' ); // Allow plugins to add custom group creation steps ?>

			<?php do_action( 'bp_before_group_creation_step_buttons' ); ?>

			<?php if ( 'crop-image' != bp_get_avatar_admin_step() ) : ?>

				<div class="submit" id="previous-next">

					<?php /* Previous Button */ ?>
					<?php if ( !bp_is_first_group_creation_step() ) : ?>

						<input type="button" class="button secondary left" value="<?php esc_attr_e( 'Back to Previous Step', 'buddypress' ); ?>" id="group-creation-previous" name="previous" onclick="location.href='<?php bp_group_creation_previous_link(); ?>'" />

					<?php endif; ?>

					<?php /* Next Button */ ?>
					<?php if ( !bp_is_last_group_creation_step() && !bp_is_first_group_creation_step() ) : ?>

						<input type="submit" class="right" value="<?php esc_attr_e( 'Next Step', 'buddypress' ); ?>" id="group-creation-next" name="save" />

					<?php endif;?>

					<?php /* Create Button */ ?>
					<?php if ( bp_is_first_group_creation_step() ) : ?>

						<input type="submit" value="<?php esc_attr_e( 'Create Group and Continue', 'buddypress' ); ?>" id="group-creation-create" name="save" />

					<?php endif; ?>

					<?php /* Finish Button */ ?>
					<?php if ( bp_is_last_group_creation_step() ) : ?>

						<input type="submit" class="right" value="<?php esc_attr_e( 'Finish', 'buddypress' ); ?>" id="group-creation-finish" name="save" />

					<?php endif; ?>
				</div>

			<?php endif;?>

			<?php do_action( 'bp_after_group_creation_step_buttons' ); ?>

			<?php /* Don't leave out this hidden field */ ?>
			<input type="hidden" name="group_id" id="group_id" value="<?php bp_new_group_id(); ?>" />

			<?php do_action( 'bp_directory_groups_content' ); ?>

		</div><!-- .item-body -->

		<?php do_action( 'bp_after_create_group' ); ?>

	</form>

	<?php do_action( 'bp_after_create_group_content_template' ); ?>

</div>

<?php do_action( 'bp_after_create_group_page' ); ?>