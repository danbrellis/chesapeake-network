<?php
get_header(); ?>
<div class="row">
    <div class="small-14 large-10 columns" role="main">
        <?php do_action( 'bp_before_directory_activity_content' ); ?>
        
		<?php while (have_posts()) : the_post(); ?>
            <?php the_content(); ?>
        <?php endwhile; ?>
    </div>
    <?php get_sidebar('right'); ?>
</div>
<?php get_footer(); ?>
