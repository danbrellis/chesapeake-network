<?php

/**
 * This template, which powers the group Send Invites tab when IA is enabled, can be overridden
 * with a template file at groups/single/invite-anyone.php
 *
 * @package Invite Anyone
 * @since 0.8.5
 */

global $bp, $members_template;
?>
<div id="invite-anyone">
	<?php do_action( 'bp_before_group_send_invites_content' ) ?>
    
	<form action="" method="get" id="search-members-form">
        <div class="row">
            <div class="large-14 columns">
                <div class="row collapse">
                    <div class="small-11 columns">
                        <label><input type="text" name="s" id="members_search" placeholder="<?php _e('Search Members...', 'chesnet'); ?>"></label>
                    </div>
                    <div class="small-3 columns">
                        <input type="submit" id="members_search_submit" name="members_search_submit" value="<?php _e('Search', 'chesnet'); ?>" class="button postfix">
                    </div>
                </div>
            </div>
        </div>
  
    </form>
      
    
    <?php if ( !bp_get_new_group_id() ) : ?>
        <form action="<?php invite_anyone_group_invite_form_action() ?>" method="post" id="send-invite-form">
    <?php endif; ?>
    
    <div id="members-dir-list" class="members dir-list article">
        <?php if ( bp_has_members( bp_ajax_querystring( 'members' ) . '&type=alphabetical' ) ) : ?>
        
            <div id="pag-top" class="pagination">
                <div class="pag-count" id="member-dir-count-top">
                    <?php bp_members_pagination_count(); ?>
                </div>
                <div class="pagination-links" id="member-dir-pag-top">
                    <?php bp_members_pagination_links(); ?>
                </div>
            </div>
            <?php do_action( 'bp_before_directory_members_list' ); ?>
        
            <ul id="members-list" class="item-list" role="main">
            <?php $site_admin = is_super_admin();
			while ( bp_members() ) : bp_the_member(); ?>
        
                <li>
                    <div class="item-avatar">
                        <a href="<?php bp_member_permalink(); ?>"><?php bp_member_avatar(); ?></a>
                    </div>
        
                    <div class="action">
                        <div class="_6a _6b" style="height:50px"></div>
                        <div class="item-title _6a _6b">
                        	<?php
							$user_id = bp_get_member_user_id();
							$group_id = bp_get_group_id();
							if(groups_check_user_has_invite( $user_id, $group_id)){
								printf(__('%s Invited', 'chesnet'), '<i class="fi-check"></i>');
							}
							elseif(groups_is_user_member($user_id, $group_id)) {
								printf(__('%s Member', 'chesnet'), '<i class="fi-check"></i>');
							}
							elseif($site_admin && groups_is_user_banned( $user_id, $group_id )){
								printf(__('%s Banned', 'chesnet'), '<i class="fi-x"></i>');
							}
							else { ?>
								<span class="spinner"></span><a href="<?php echo wp_nonce_url( add_query_arg('invite', $user_id), 'groups_invite_user', '_wpnonce_invite_user' ); ?>" class="button secondary invite-btn" style="margin-bottom:0" data-user-id="<?php echo $user_id; ?>"><?php _e('Invite', 'chesnet'); ?></a>
							<?php } ?>
                        </div>
                    </div>
                    <div class="item _6a">
                        <div class="_6a _6b" style="height:50px"></div>
                        <div class="item-title _6a _6b">
                            <a href="<?php bp_member_permalink(); ?>" target="_blank"><?php bp_member_name(); ?></a>  
                            <?php if(is_super_admin()) echo $members_template->member->user_email; ?>   
                        </div>
                    </div>
        
                    
        
                    <div class="clear"></div>
                </li>
        
            <?php endwhile; ?>
        
            </ul>
        
            <?php do_action( 'bp_after_directory_members_list' ); ?>
        
            <?php bp_member_hidden_fields(); ?>
            <div id="pag-bottom" class="pagination">
                <div class="pag-count" id="member-dir-count-bottom">
                    <?php bp_members_pagination_count(); ?>
                </div>
                <div class="pagination-links" id="member-dir-pag-bottom">
                    <?php bp_members_pagination_links(); ?>
                </div>
            </div>
            
            <?php
			if ( !bp_get_new_group_id() ) {
				?><input type="hidden" name="group_id" id="group_id" value="<?php bp_group_id(); ?>" /><?php
			} else {
				?><input type="hidden" name="group_id" id="group_id" value="<?php bp_new_group_id(); ?>" /><?php
			}
			?>
            <input type="hidden" name="current_user" id="current_user" value="<?php echo $bp->loggedin_user->id; ?>" />
        
        <?php else: ?>
        
            <div id="message" class="info">
                <p><?php _e( "Sorry, no members were found.", 'buddypress' ); ?></p>
            </div>
        
        <?php endif; ?>
    </div><!-- #members-dir-list -->
    
    
    <?php wp_nonce_field( 'groups_send_invites', '_wpnonce_send_invites') ?>
    
        <!-- Don't leave out this sweet field -->

    <?php if ( !bp_get_new_group_id() ) : ?>
        </form>
    <?php endif; ?>
    
    <?php do_action( 'bp_after_group_send_invites_content' ) ?>
</div>