<?php

do_action( 'bp_before_group_header' );
global $bp, $chesnet, $groups_template;
echo '<pre>';
//var_dump($bp->groups->current_group->user_has_access);
//var_dump($groups_template->group->avatar_thumb);
echo '</pre>';
?>

<div id="group-page-banner" class="page-banner">
	<?php if(ocp_can_user_access_group()): ?><a href="<?php echo trailingslashit( bp_get_group_permalink( groups_get_current_group() ) . 'change-cover' ); ?>" data-tooltip aria-haspopup="true" class="has-tip edit-photo edit-cover" title="<?php _e( 'Change Cover Photo', 'chesnet' ); ?>"><i class="fi-camera"></i></a><?php endif; ?>
    <div id="item-header-avatar">
        <?php if(bp_is_item_admin()): ?>
        	<div class="row">
        		<div class="small-4 columns small-centered">
        			<a href="<?php echo trailingslashit( bp_get_group_permalink( groups_get_current_group() ) . 'admin/group-avatar' ); ?>" data-tooltip aria-haspopup="true" class="has-tip edit-photo edit-avatar" title="<?php _e( 'Change Profile Photo', 'chesnet' ); ?>"><i class="fi-camera"></i></a>
                </div>
            </div>
        <?php endif; ?>
        <a href="<?php bp_group_permalink(); ?>" data-tooltip aria-haspopup="true" class="has-tip tip-right" title="<?php bp_group_description(); ?>">
            <?php bp_group_avatar(); ?>
        </a>
    </div><!-- #item-header-avatar -->
    <div id="item-header-data">
        <h2><?php bp_group_name(); ?><?php if(is_super_admin()) printf(' (<span title="%s">%d</span>)', __('Group ID', 'chesnet'), bp_get_group_id()); ?></h2>
        <span class="highlight"><?php bp_group_type(); ?></span> &middot; <span class="activity"><?php printf( __( 'active %s', 'buddypress' ), bp_get_group_last_active() ); ?></span>
    </div>
    <div id="item-header-actions">
    	<ul class="button-group">
            <?php do_action( 'bp_group_header_actions' ); ?>
            <?php if ( bp_is_item_admin() ) :
				//nav item found here $bp->bp_options_nav[$bp->groups->current_group->true_slug]
				$manage_link = bp_get_group_permalink( $bp->groups->current_group ) . 'admin';
				?>
				<li><a href="<?php echo $manage_link; ?>" <?php /*data-dropdown="drop1" aria-controls="drop1" aria-expanded="false" */ ?>class="group-button" title="<?php _e('Manage group settings', 'chesnet'); ?>"><span class="screen-reader-text"><?php _e('Manage group settings', 'chesnet'); ?></span><span class="dashicons dashicons-menu"></span></a>
					<?php /* @todo: show manage subnav
					<br><ul id="drop1" data-dropdown-content class="f-dropdown" aria-hidden="true" tabindex="-1">
						<li><a href="#">This is a link</a></li>
						<li><a href="#">This is another</a></li>
						<li><a href="#">Yet another</a></li>
					</ul>
					*/ ?>
				</li>
            <?php endif; ?>
        </ul>
    </div>
</div>
<?php
do_action( 'bp_after_group_header' );
?>