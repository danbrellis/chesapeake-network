<?php if ( $pendingposts->have_posts() ) : ?>
	<div id="pag-top" class="pagination">
		<div class="right">
			<?php $big = 999999999; // need an unlikely integer
            echo $pag_links = paginate_links( array(
                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                'format' => '?paged=%#%',
                'current' => max( 1, $paged ),
                'total' => $pendingposts->max_num_pages,
                'prev_text'          => __('&laquo; Previous', 'chesnet'),
                'next_text'          => __('Next &raquo;', 'chesnet'),
                'type'               => 'list'
            ) ); ?>
        </div>
		<div class="pag-count" id="group-announcement-moderation-count-top">
			<?php printf(__('Viewing %s announcements', 'chesnet'), $pendingposts->post_count == $pendingposts->found_posts ? __('all ', 'chesnet') . $pendingposts->post_count : $pendingposts->post_count . __(' of ', 'chesnet') . $pendingposts->found_posts); ?>
		</div>
	</div>
    
    <ul id="moderate-list" class="item-list">
		<?php while ( $pendingposts->have_posts() ): $pendingposts->the_post(); ?>

			<li>
				<?php /*<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>*/ ?>
                <h4><a href="<?php echo get_post_type() == 'topic' ? bbp_add_view_all(get_permalink(), true) : get_permalink(); ?>"><?php the_title(); ?></a></h4>
				<span class="activity"><?php printf(__('Added %s by %s', 'chesnet'), '<span title="'.get_the_date('d M Y - g:i a').'">' . bp_core_time_since(get_the_date('U'), bp_core_current_time( false, 'timestamp' )) . '</span>', bp_core_get_userlink( get_the_author_meta('ID') )); ?></span>

				<div class="action">
					<ul class="inline-list right">
						<?php bp_button( array(
                            'id' => 'approve-announcement-btn',
                            'component' => 'groups', 
                            'wrapper' => 'li', 
                            'link_href' => cn_get_group_pending_announcement_approve_link(get_the_ID()), 
                            'link_class' => 'button small', 
                            'link_text' => __('Approve', 'chesnet')
                        ) ); ?>
						<?php bp_button( array(
                            'id' => 'reject-announcement-btn',
                            'component' => 'groups', 
                            'wrapper' => 'li', 
                            'link_href' => cn_get_group_pending_announcement_reject_link(get_the_ID()), 
                            'link_class' => 'button alert small', 
                            'link_text' => __('Reject', 'chesnet')
                        ) ); ?>
                    </ul>

					<?php do_action( 'bp_group_membership_requests_admin_item_action' ); ?>

				</div>
			</li>

		<?php endwhile; //$pendingposts loop
		wp_reset_postdata(); ?>
	</ul>
    
    <div id="pag-bottom" class="pagination">
		<div class="right">
			<?php $big = 999999999; // need an unlikely integer
            echo $pag_links = paginate_links( array(
                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                'format' => '?paged=%#%',
                'current' => max( 1, $paged ),
                'total' => $pendingposts->max_num_pages,
                'prev_text'          => __('&laquo; Previous', 'chesnet'),
                'next_text'          => __('Next &raquo;', 'chesnet'),
                'type'               => 'list'
            ) ); ?>
        </div>
		<div class="pag-count" id="group-announcement-moderation-count-bottom">
			<?php printf(__('Viewing %s announcements', 'chesnet'), $pendingposts->post_count == $pendingposts->found_posts ? __('all ', 'chesnet') . $pendingposts->post_count : $pendingposts->post_count . __(' of ', 'chesnet') . $pendingposts->found_posts); ?>
		</div>
	</div>

<?php else: ?>

	<div id="message" class="info">
        <p><?php _e( 'There are no pending announcements awaiting moderation.', 'chesnet' ); ?></p>
    </div>

<?php endif; //if $pendingposts->have_posts() ?>