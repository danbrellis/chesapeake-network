<?php
global $chesnet, $bp;
?>
<aside id="sidebar" class="small-14 medium-3 columns medium-pull-11">
	<?php do_action('cn_before_sidebar'); ?>
    <?php do_action('cn_before_sidebar_single_member_left'); ?>
    <?php cn_the_activity_filters(true); ?>
    
    <?php if( bp_is_groups_component() && bp_current_action() == 'my-groups' && bp_user_can_create_groups() ) : ?>
        <?php 
		 $button_args = array(
		 	'id'				=> 'create_group',
			'component'			=> 'groups',
			'must_be_logged_in'	=> true,
			'block_self'		=> false,
			'link_text'			=> __( 'Create a Group', 'chesnet' ),
			'link_title'		=> __( 'Create a Group', 'chesnet' ),
			'link_class'		=> 'group-create no-ajax button',
			'link_href'			=> trailingslashit( bp_get_root_domain() ) . trailingslashit( bp_get_groups_root_slug() ) . trailingslashit( 'create' ),
			'wrapper'    => false,
		);
		bp_button( $button_args ); ?>
    <?php endif; ?>
    
    <?php if( bp_is_user_profile() && (bp_current_action() == 'public' || bp_current_action() == 'edit') && bp_is_active( 'xprofile' ) ) : ?>
        <div class="homeSideNav no-bg">
        	<?php if(bp_current_action() == 'public'): ?>
            	<ul class="subtle-sidenav" data-tab id="profile-group-tabs">
					<?php if(isset($chesnet->profile_tabs) && is_array($chesnet->profile_tabs)):
                        ksort($chesnet->profile_tabs); ?>
                        <?php foreach($chesnet->profile_tabs as $order => $tab): ?>
                            <li class="tab-title<?php if($order == 0) echo ' active'; ?>"><a href="#<?php echo $tab['slug']; ?>"><?php echo $tab['name']; ?></a></li>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>
            <?php else: ?>
            	<ul class="subtle-sidenav"><?php bp_profile_group_tabs(); ?></ul>
            <?php endif; ?>
        </div>
        <hr />
    <?php endif; ?>
    
    <?php if( bp_docs_is_bp_docs_page() && !bp_docs_is_doc_create() && current_user_can( 'bp_docs_create' ) ) : ?>
        <div class="homeSideNav no-bg">
			<?php bp_docs_create_button(); ?>
        </div>
    <?php endif; ?>
    
	<?php do_action('cn_after_sidebar'); ?>
</aside>