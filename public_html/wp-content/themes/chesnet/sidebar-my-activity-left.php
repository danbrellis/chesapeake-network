<?php


global $wp_post_types, $chesnet;
?>
<aside id="sidebar" class="small-14 medium-4 large-3 columns medium-pull-10 large-pull-7">
	<?php do_action('cn_before_sidebar'); ?>
	<div class="homeSideNav" id="cnLogin">
		<?php the_widget('Chesnet_Login_Widget'); ?>
    </div>
    <?php cn_the_activity_filters(true); ?>
    <div class="homesideNavcont" id="home-groups-container">
        <div class="clearfix">
        	<h5 class="navHandle left"><?php _e('My Groups', 'chesnet'); ?></h5>
            <span class="right"><a href="<?php echo trailingslashit( bp_get_root_domain() . '/' . bp_get_groups_root_slug() . '/'); ?>"><?php _e('All Groups', 'chesnet'); ?></a></span>
        </div>
        <div class="homeSideNav">
			<?php if(is_user_logged_in()): ?>
                <?php show_my_activity_groups_list(); ?>
            <?php endif; ?>
            <?php if(is_user_logged_in() && bp_user_can_create_groups()): ?>
                <a href="<?php echo trailingslashit( bp_get_root_domain() ) . trailingslashit( bp_get_groups_root_slug() ) . trailingslashit( 'create' ); ?>" title="<?php _e('Create new group', 'chesnet'); ?>"><i class="fi-plus imgWrapLeft"></i><?php _e('Create New Group', 'chesnet'); ?></a>
            <?php endif; ?>
        </div>
    </div>
	<?php do_action('cn_after_sidebar'); ?>
</aside>