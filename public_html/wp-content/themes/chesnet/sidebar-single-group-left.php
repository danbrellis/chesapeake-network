<?php
global $chesnet, $bp, $groups_template;
$group =& $groups_template->group;
?>
<aside id="sidebar" class="small-14 medium-3 columns medium-pull-11">
	<?php do_action('cn_before_sidebar'); ?>
    <?php do_action('cn_before_sidebar_single_group_left'); ?>
    <?php cn_the_activity_filters(true); ?>
    
	<?php if(bp_is_group_admin_page()) : ?>
        <div class="homeSideNav no-bg">
            <ul class="subtle-sidenav item-list-tabs no-ajax" role="navigation" id="subnav">
                <?php bp_group_admin_tabs(); ?>
            </ul>
        </div>
    <?php endif; ?>
    
    <?php do_action('bp_after_group_admin_tabs', $group); ?>
    
    <?php if ( bp_group_is_visible() ) : ?>
        <div class="homesideNavcont" id="group-admins-container">
            <h5 class="navHandle"><?php _e('Admins', 'chesnet'); ?><i class="fi-sheriff-badge imgWrapRight"></i></h5>
            <div class="homeSideNav no-bg">
				<?php $chesnet->bp_group_list_admins();
                do_action( 'bp_after_group_menu_admins' ); ?>
            </div>
        </div>
	<?php endif; ?>
    
    <?php if ( bp_group_is_visible() && bp_group_has_moderators() ) : ?>
        <div class="homesideNavcont" id="group-mods-container">
            <h5 class="navHandle"><?php _e('Moderators', 'chesnet'); ?></h5>
            <div class="homeSideNav no-bg">
				<?php $chesnet->bp_group_list_mods();
                do_action( 'bp_after_group_menu_mods' ); ?>
            </div>
        </div>
	<?php endif; ?>
    
	<?php if( class_exists( 'BP_Groups_Hierarchy' ) ) :
		$current_subgroup_permission = groups_get_groupmeta( $bp->groups->current_group->id, 'bp_group_hierarchy_subgroup_creators' );
		$nav_item = get_site_option( 'bpgh_extension_nav_item_name', __('Member Groups','bp-group-hierarchy') );
		$nav_item = trim(str_replace('%d', '', $nav_item));
		?>
		
		<?php if($bp_group_hierarchy_get_subgroups = bp_group_hierarchy_get_subgroups()):
			$subgroups = groups_get_groups( array(
				'type'				=> 'alphabetical',
				'include'			=> $bp_group_hierarchy_get_subgroups,
				'populate_extras'	=> false,
				'update_meta_cache'	=> false
			) );
			if(isset($subgroups['groups'])): ?>
				<div class="homesideNavcont" id="group-sub-groups-container">
					<h5 class="navHandle"><?php echo $nav_item; ?></h5>
					<div class="homeSideNav no-bg">
						<ul id="sub-groups" class="subtle-sidenav">
							<?php echo $chesnet->format_group_list_for_widget($subgroups['groups'], '<li>', '</li>'); ?>
						</ul>
					</div>
				</div>
			<?php endif; ?>
		
		<?php endif; ?>
    <?php endif; ?>
    
    <?php $group_emails = groups_get_groupmeta($bp->groups->current_group->id, 'group_email_address', false);
	if($group_emails && is_array($group_emails)): ?>
    	<div class="homesideNavcont">
            <h5 class="navHandle" title="<?php _e('Post to this group by sending an email to any of the below addresses.', 'chesnet'); ?>"><?php _e('Post By Email', 'chesnet'); ?><i class="fi-mail imgWrapRight"></i></h5>
            <div class="homeSideNav no-bg">
                <ul id="groupEmails" class="subtle-sidenav">

					<?php foreach($group_emails as $group_email){
						$email = sanitize_email($group_email);
						$email_parts = explode('@', $email);
						echo '<li><a href="mailto:'.$email.'" title="'.sprintf(__('Send an email to %s', 'chesnet'), $email).'">' . $email_parts[0] . '</a></li>';
					} ?>
                </ul>
        	</div>
    	</div>
    <?php endif; ?>
    
	<?php do_action('cn_after_sidebar'); ?>
</aside>