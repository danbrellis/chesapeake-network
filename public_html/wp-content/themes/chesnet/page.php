<?php
get_header();


if(isset($_GET['admin']) && $_GET['admin'] == 'rundatabasetransfers' && is_super_admin()):

global $wpdb, $bp, $chesnet;
define('WP_DEBUG', true);

//set up golightly database
$goli = new wpdb('chesadmin_db','wTYDSYqe2I','chesadmin_golightly','localhost');
$i = 0;

/** Member Import From CSV **/
/*
[0]  => Email 
[1]  => Email1 
[2]  => Email2 
[3]  => Title 
[4]  => FirstName 
[5]  => MiddleName 
[6]  => LastName 
[8]  => GoLightly_ID 
[10] => JobTitle 
[11] => Organization 
[12] => Website 
[13] => HomeAddressLine1 
[14] => HomeAddressLine2 
[15] => HomeAddressLine3 
[16] => HomeCity 
[17] => HomeState 
[18] => HomeZipCode 
[19] => HomeCountry 
[20] => AddressOrganizationLine 
[21] => AddressLine1 
[22] => AddressLine2 
[23] => AddressLine3 
[24] => City 
[25] => State 
[26] => ZipCode 
[27] => Country 
[28] => Phone 
[29] => FaxPhone 
[30] => HomePhone 
[31] => CellPhone 
[32] => PagerPhone 
[33] => LinkToProfile 
[34] => LinkToPhoto 
[35] => RegistrationTime 
[36] => FirstVisit 
[37] => LastVisit 
[38] => Facebook 
[39] => Skype 
[40] => LinkedIn 
[41] => YahooIM 
[42] => Twitter 
[43] => Flickr 
[44] => MySpace 
[45] => AIM 
[46] => MSN
*/

/*

//get the csv file 
$filepath = get_stylesheet_directory() . '/library/member-profile-import-5.csv';
$file = fopen($filepath,"r"); 
while(!feof($file)) {
	$row = fgetcsv($file);
	//bail if no golightly id
	if(empty($row[8]) || $row[8] == 'GoLightly_ID') continue;
	
	//first make sure email doesn't already exist
	$user = get_user_by( 'email', $row[0] );
	if($user) {
		print_r($row[0]  . " - error (aborted)<br />");
		continue;
	}
	
	//user row from db
	$goli_user = $goli->get_row( $goli->prepare("SELECT * FROM `gct_users` WHERE user_id = %d", $row[8]));
	
	//user table
	$user_login = $row[8];
	$user_pass = wp_hash_password($goli_user->user_password);
	$user_nicename = $row[8];
	$user_email = $row[0];
	$user_registered = date('Y-m-d H:i:s', strtotime($row[35]));
	$user_status = $goli_user->user_blocked;
	$display_name = $goli_user->user_latest_display_full_name;
	
	$wpdb->insert( 
		$wpdb->users,
		array( 
			'user_login' => $user_login,
			'user_pass' => $user_pass, 
			'user_nicename' => $user_nicename, 
			'user_email' => $user_email, 
			'user_registered' => $user_registered, 
			'user_status' => $user_status, 
			'display_name' => $display_name,
			'auto_join_complete' => 0 
		), 
		array( 
			'%s', 
			'%s',
			'%s',
			'%s',
			'%s',
			'%d',
			'%s',
		)
	);
	$user_id = $wpdb->insert_id;
	if(!$user_id) {
		echo "Error user with golightly id: " . $row[8] . "<br />";
		continue;
	}
	
	//user meta
	$md = array(); //meta data
	$md['first_name'] = $row[4];
	$md['last_name'] = $row[6];
	$md['nickname'] = $display_name;
	$md['rich_editing'] = 'true';
	$md['comment_shortcuts'] = 'false';
	$md['show_admin_bar_front'] = 'false';
	$md['wp_capabilities'] = wp_serialize(array('author' => 1, 'bbp_participant' => 1));
	$md['wp_user_level'] = 2;
	$md['golightly_id'] = $row[8];
	//$md['total_group_count'] = XXX; @done
	//$md['bp_docs_count'] = xxx; @todo
	
	foreach($md as $meta_key => $meta_value) {
		if(!add_user_meta( $user_id, $meta_key, $meta_value, true )){
			echo "Error adding User Meta for user: " . $user_id . ", meta_key: " . $meta_key . "<br />";
		}
	}
		
	//xprofile
	$xp_values = array();
	$xp_place_holders = array();
	$xp = array();
	$xp[1] = $display_name;
	if($row[4]) $xp[2] = $row[4];
	if($row[6]) $xp[3] = $row[6];
	if($row[10]) $xp[4] = $row[10];
	if($row[11]) $xp[5] = $row[11];
	if($row[12]) $xp[7] = $row[12];
	if($row[38]) $xp[8] = $row[38];
	if($row[42]) $xp[9] = $row[42];
	if($row[40]) $xp[10] = $row[40];
	if($row[43]) $xp[11] = $row[43];
	if($goli_user->user_bio) $xp[12] = trim(strip_tags(
				preg_replace("/<\/p>/", "\r\n\r\n", 
					preg_replace("/<br\W*?\/?>/", "\r\n", 
						preg_replace("/\r|\n/", "", $goli_user->user_bio)
					)
				)
			));
	if($row[28]) $xp[13] = $row[28];
	if($row[30]) $xp[14] = $row[30];
	if($row[31]) $xp[15] = $row[31];
	if($row[29]) $xp[16] = $row[29];
	if($row[1]) $xp[17] = $row[1];
	if($row[2]) $xp[18] = $row[2];
	
	foreach($xp as $field_id => $value){
		array_push($xp_values, $field_id, $user_id, $value);
		$xp_place_holders[] = "('%d', '%d', '%s')";
	}
	
	$xpq = "INSERT INTO ". $wpdb->prefix."bp_xprofile_data (`field_id`, `user_id`, `value`) VALUES " . implode(', ', $xp_place_holders);
	$xpr = $wpdb->query( $wpdb->prepare("$xpq ", $xp_values));
	if($xpr == false){
		echo "Error adding XProfile Fields for user: " . $user_id . "<br />";
		var_dump($wpdb->last_query); echo "<br />";
		var_dump($wpdb->last_error); echo "<br />";
	}
	else {
		echo "Success! GoLightly (".$row[8].") -> WP (".$user_id.")" . "<br />";
		$i++;
	}
	
	unset($row, $md, $xp, $xp_values, $xp_place_holders);
}
fclose($file);
echo '<p>Done. Total count: ' . $i . '</p>';
*/


/** Add Last Activity For Member **/
/*
$users = $wpdb->get_results(
	"
	SELECT      u.ID, um.meta_value as gid
	FROM        $wpdb->usermeta um
	INNER JOIN  $wpdb->users u 
	            ON u.ID = um.user_id
	WHERE		um.meta_key = 'golightly_id'
	AND			u.ID BETWEEN 4000 AND 4999
	"
); 
if($users){
	foreach($users as $user){
		//get unix last active time
		$unix = $goli->get_var( $goli->prepare("SELECT `user_lastvisit` FROM `gct_users` WHERE `user_id` = %d", $user->gid) );
		if(!$unix || $unix == 0) continue;
		$lastvisit = date('Y-m-d H:i:s', $unix);
		$wpdb->insert( 
			$wpdb->prefix . 'bp_activity', 
			array( 
				'user_id' => $user->ID, 
				'component' => 'members',
				'type' => 'last_activity',
				'date_recorded' => $lastvisit
			), 
			array( 
				'%d', 
				'%s',
				'%s',
				'%s'
			) 
		);
	}
}
*/

/** Add Groups **/
/*
$ggroups = $goli->get_results( 
	"
	SELECT DISTINCT i.*, e.email_address_email, ml.* 
	FROM gct_mailing_lists ml 
	JOIN gct_interest_groups i 
		ON i.interestgroups_ID = ml.maillists_IDOfOwner 
	JOIN gct_email_addresses e 
		ON e.email_address_id = ml.maillists_EmailAddressOfList
	AND i.interestgroups_deleted = 0
	AND i.interestgroups_archived = 0
	"
);

foreach($ggroups as $ggroup){
	$ggroup_id = (int)$ggroup->interestgroups_ID;
	if($ggroup_id == 3) continue; //skip CN group for now
	
	//create bp group
	$creator = get_users(array('meta_key' => 'golightly_id', 'meta_value' => $ggroup->interestgroups_PrimaryAdmin, 'fields' => 'id', 'number' => 1));
	$creator_id = $creator[0];
	$group_created = date('Y-m-d H:i:s', $ggroup->interestgroups_DateCreated);
	
	$gr = $wpdb->insert( 
		$wpdb->prefix . 'bp_groups',
		array( 
			'creator_id' => $creator_id,
			'name' => $ggroup->interestgroups_GroupName,
			'slug' => $ggroup->interestgroups_shortcut ? $ggroup->interestgroups_shortcut : sanitize_title($ggroup->interestgroups_GroupName), 
			'description' => $ggroup->interestgroups_summary, 
			'status' => $ggroup->interestgroups_WhoCanJoin == 2 ? 'public' : 'private', 
			'enable_forum' => $ggroup->interestgroups_forum_id ? 1 : 0, 
			'date_created' => $group_created,
			'auto_join' => 0
		), 
		array( 
			'%d', 
			'%s',
			'%s',
			'%s',
			'%s',
			'%d',
			'%s',
			'%b'
		)
	);
	if($gr == false){
		echo "Error adding golightly group ".$ggroup->interestgroups_GroupName." (ID: " . $ggroup_id . ")<br />";
		var_dump($wpdb->last_query); echo "<br />";
		var_dump($wpdb->last_error); echo "<br /><br />";
		continue;
	}
	else {
		$group_id = $wpdb->insert_id;
	}
	$group = groups_get_group(array('group_id' => $group_id));
	
	//Created Group Activity
	$ar = $wpdb->insert( 
		$wpdb->prefix . 'bp_activity',
		array( 
			'user_id' => $creator_id,
			'component' => 'groups',
			'type' => 'created_group',
			'action' => bp_core_get_userlink( $creator_id ) . ' created the group <a href="'.bp_get_group_permalink($group).'">'.bp_get_group_name($group) .'</a>',
			'primary_link' => bp_get_group_permalink($group),
			'item_id' => $group_id,
			'secondary_item_id' => 0,
			'date_recorded' => $group_created
		), 
		array( 
			'%d', 
			'%s',
			'%s',
			'%s',
			'%s',
			'%d',
			'%d',
			'%s'
		)
	);
	if($ar == false) echo "Error adding Created Group activity: " . $group_id . "<br />";
	
	//Group Meta Data
	$md = array(); //meta data
	//$md['total_member_count'] = XXX; @todo
	$md['last_activity'] = $group_created;
	$md['bp_group_hierarchy_subgroup_creators'] = 'group_admins';
	$md['ass_default_subscription'] = 'supersub';
	//$md['ass_subscribed_users'] = XXX; @todo
	if($ggroup->maillists_ListType == '2') $md['cn_moderate_new_announcements'] = 'yes';
	$md['invite_status'] = $ggroup->interestgroups_WhoCanJoin == 2 ? 'members' : 'mods';
	//$md['forum_id'] = XXX; @todo
	//$md['bpdocs'] = XXX; @todo
	if($ggroup->interestgroups_forum_id) $md['golightly_forum_id'] = $ggroup->interestgroups_forum_id;
	if($ggroup->maillists_SubjectPrefix) $md['maillists_SubjectPrefix'] = $ggroup->maillists_SubjectPrefix;
	if($ggroup->maillists_WelcomeListDescription)
		$md['ass_welcome_email'] = array(
			'enabled' => 'yes',
			'subject' => 'Welcome to ' . $group->name,
			'content' => $ggroup->maillists_WelcomeListDescription. "\r\n" . "\r\n" . $ggroup->maillists_ModeratorSignature
		);
	//$md['_bbp_forum_enabled_XXX'] = XXX; @todo
	$md['golightly_gid'] = $ggroup_id;
	if($ggroup->interestgroups_forum_id) $md['forum_exists_but_not_accessible'] = 'true';
	$md['cn_resources'] = 'a:2:{s:12:"group-enable";s:1:"1";s:10:"can-create";s:6:"member";}';
	$md['group_email_address'] = $ggroup->email_address_email;
	
	foreach($md as $meta_key => $meta_value) {
		if(!groups_add_groupmeta( $group_id, $meta_key, $meta_value, true )){
			echo "Error adding Group Meta for group: " . $group_id . ", meta_key: " . $meta_key . "<br />";
		}
	}
	
	//Add category
	$cat = wp_insert_term(
		$group->name, // the term 
		'category', // the taxonomy
		array( 'slug' => cn_cat_slug_from_gid($group_id), 'description' => 'Posts associated with the ' . $group->name . ' group')
	);
	if(is_wp_error($cat)) echo 'Error adding category for group: ' . $group_id . "(" . $cat->get_error_message() .")<br>";

	echo "Success! GoLightly (".$ggroup_id.") -> WP (".$group_id.")" . "<br />";
		$i++;
		
	echo 'Done: added ' . $i . ' groups';
}
*/

/** Add Members To Groups **/
/*
$users = $wpdb->get_results(
	"
	SELECT      u.ID, um.meta_value as gid
	FROM        $wpdb->usermeta um
	INNER JOIN  $wpdb->users u 
	            ON u.ID = um.user_id
	WHERE		um.meta_key = 'golightly_id'
	AND			u.ID = 1
	"
); 
$group_count = array();
$users_added = 0;
foreach($users as $user){
	if(!$user->gid){
		echo 'No golightly id. Skipping WP User: ' . $user->ID . '<br>';
		continue;
	}
	$user_id = $user->ID;
	
	//get golightly grants
	$grants = $goli->get_results($goli->prepare("
		SELECT * 
		FROM `gct_grant` 
		WHERE `grantee_object_id` = %d 
		AND `grantee_object_class_id` = %d 
		AND `controlled_object_class_id` = %d
	",
	(int)$user->gid,
	3,
	2
	));
	
	if(!$grants || !is_array($grants)){
		echo 'No groups for WP User: ' . $user_id . '<br>';
		continue;
	}
	
	$gugroups = array();
	foreach($grants as $grant){
		if($grant->controlled_object_id == 3) continue;
		switch ($grant->role_id){
			case 2:
				$gugroups[$grant->controlled_object_id]['is_admin'] = 1;
				$gugroups[$grant->controlled_object_id]['is_confirmed'] = 1;
				$gugroups[$grant->controlled_object_id]['confirmed_date'] = $grant->update_datetime;
				break;
			case 4:
				$gugroups[$grant->controlled_object_id]['is_confirmed'] = 1;
				$gugroups[$grant->controlled_object_id]['confirmed_date'] = $grant->update_datetime;
				break;
			case 3:
				$gugroups[$grant->controlled_object_id]['is_mod'] = 1;
				$gugroups[$grant->controlled_object_id]['is_confirmed'] = 1;
				$gugroups[$grant->controlled_object_id]['confirmed_date'] = $grant->update_datetime;
				break;
			case 7:
				$gugroups[$grant->controlled_object_id]['invite_sent'] = 1;
				$gugroups[$grant->controlled_object_id]['invited_date'] = $grant->create_datetime;
				
				$create_user = get_users(array('meta_key' => 'golightly_id', 'meta_value' => $grant->create_userid, 'fields' => 'id', 'number' => 1));
				$gugroups[$grant->controlled_object_id]['inviter_id'] = $create_user[0];
				break;
		}
	}
	
	$user_group_count = 0;
	foreach($gugroups as $gugroup_id => $gugroup_data){
		$date_modified = $user_title = false;
		$is_admin = $is_mod = 0;
		$group_id = $wpdb->get_var($wpdb->prepare("
			SELECT g.id FROM `wp_bp_groups` g 
			JOIN `wp_bp_groups_groupmeta` gm 
			ON g.id = gm.group_id 
			WHERE gm.meta_value = %d 
			AND gm.meta_key = 'golightly_gid'
		",
		$gugroup_id));
		if(!$group_id) continue;
		
		if(isset($gugroup_data['invite_sent']) && !isset($gugroup_data['is_confirmed']))
			$date_modified = $gugroup_data['invited_date'];
		
		if(isset($gugroup_data['is_confirmed']))
			$date_modified = $gugroup_data['confirmed_date'];
		
		if(isset($gugroup_data['is_mod'])){
			$is_mod = 1;
			$user_title = 'Group Moderator';
		}
		
		if(isset($gugroup_data['is_admin'])){
			$is_admin = 1;
			$is_mod = 0;
			$user_title = 'Group Admin';
		}
		
		//Created Group Activity
		$gmr = $wpdb->insert( 
			$wpdb->prefix . 'bp_groups_members',
			array( 
				'group_id' => $group_id,
				'user_id' => $user_id,
				'inviter_id' => isset($gugroup_data['inviter_id']) ? $gugroup_data['inviter_id'] : 0,
				'is_admin' => $is_admin,
				'is_mod' => $is_mod,
				'user_title' => $user_title,
				'date_modified' => $date_modified,
				'is_confirmed' => isset($gugroup_data['is_confirmed']) ? 1 : 0,
				'is_banned' => 0,
				'invite_sent' => isset($gugroup_data['invite_sent'])? 1 : 0
			), 
			array( 
				'%s', 
				'%d',
				'%d',
				'%d',
				'%d',
				'%s',
				'%s',
				'%d',
				'%d',
				'%d'
			)
		);
		if($gmr == false){
			echo "Error associating user (".$user_id.") to wp group (" . $group_id . ")<br>";
			continue;
		}
		else {
			if(!isset($group_count[$group_id])) $group_count[$group_id] = 0;
			$group_count[$group_id]++;
			$user_group_count++;
		}
	}

	update_user_meta($user_id, 'total_group_count', $user_group_count);	
	
	echo 'Added '.$user_group_count .' groups to user<br>';
	$users_added++;
}
foreach($group_count as $group_id => $count){
	//groups_update_groupmeta( $group_id, 'total_member_count', $count );
	$wpdb->query($wpdb->prepare(
		"UPDATE wp_bp_groups_groupmeta SET `meta_value` = `meta_value` + %d WHERE `group_id` = %s AND `meta_key` = 'total_member_count'",
		$count,
		$group_id
	));
}
echo 'Done! Added data for ' . $users_added . ' users';
*/

/** Add Patch For Group Slugs **/
/*
$groups = $wpdb->get_results( 
	"
	SELECT ID, name, slug 
	FROM wp_bp_groups
	"
);

foreach ( $groups as $group ) {
	if(is_numeric($group->slug)){
		$wpdb->update( 
			'wp_bp_groups', 
			array( 
				'slug' => sanitize_title($group->name),	// string
			), 
			array( 'ID' => $group->ID ), 
			array( 
				'%s',
			), 
			array( '%d' ) 
		);
	}
}
*/

/** Add Calendar Events **/
/*
//get golightly events no older than Jan 1 2013 (we can reimport more later
$gevents = $goli->get_results( 
	"
	SELECT e.* FROM `gct_events` e 
	JOIN gct_interest_groups g 
	ON g.interestgroups_ID = e.`event_interest_group_id` 
	WHERE `event_start_unixtime` > '1357012095'
	AND g.interestgroups_deleted = 0
	AND (g.interestgroups_archived = 0 OR e.`event_interest_group_id` IN(611,625,629,14616,1689,14594,607,615,1693))
	"
);
$added_events = 0;
foreach ( $gevents as $gevent ) {
	$post_author = 0;
	$creator = get_users(array('meta_key' => 'golightly_id', 'meta_value' => $gevent->event_started_by, 'fields' => 'id', 'number' => 1));
	$post_author = $creator[0];
	if(!$post_author) continue;
	
	$ed = '';
	if($gevent->event_description){
		$ed = stripcslashes($gevent->event_description);
		$ed = preg_replace("/<img[^>]+\>/i", "", $ed); //need to remove all images since they're lost on the old server
		$ed = preg_replace('/(<[^>]+) style=("|\').*?("|\')/i', '$1', $ed);
		$ed = str_replace('<p>&nbsp;</p>', '', $ed);
		$ed = trim($ed);
	}
	if(!$ed) continue;
	$last_modified = date('Y-m-d H:i:s', $gevent->event_last_processed);
	
	//add post
	$wpdb->insert( 
		$wpdb->posts, 
		array( 
			'post_author' => $post_author, 
			'post_date' => $last_modified,
			'post_date_gmt' => $last_modified,
			'post_content' => $ed,
			'post_title' => $gevent->event_name,
			'post_status' => 'publish', 
			'comment_status' => 'open',
			'ping_status' => 'closed',
			'post_name' => sanitize_title($gevent->event_name),
			'post_modified' => $last_modified,
			'post_modified_gmt' => get_gmt_from_date($last_modified),
			'post_parent' => 0,
			'post_type' => 'bgc_event'
		), 
		array( 
			'%s', 
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%d',
			'%s', 
		) 
	);
	$event_id = $wpdb->insert_id;
	
	if(!$event_id){
		echo 'Error adding event with golightly id: ' . $gevent->event_id . '<br>';
		continue;
	}
	$wpdb->query($wpdb->prepare("UPDATE `wp_posts` SET `guid`= concat('http://www.chesapeakenetwork.org/?p=',ID) WHERE ID = %d", $event_id));
	
	//Group
	$states = array(
		611 => 'maryland',
		625 => 'west-virginia',
		629 => 'new-york',
		14616 => 'new-york',
		1689 => 'delaware',
		14594 => 'delaware',
		607 => 'pennsylvania',
		615 => 'virginia',
		1693 => 'district-of-columbia'
	);
	$tagslug = $tag = false;
	if(isset($states[$gevent->event_interest_group_id])){
		$group = groups_get_group(array('group_id' => 1));
		$tagslug = $states[$gevent->event_interest_group_id];
		$tt = wp_set_object_terms( $event_id, array($tagslug), 'post_tag' );
		$tag = get_term_by('slug', $tagslug, 'post_tag');
	}
	else {
		//get group
		$group = $wpdb->get_row($wpdb->prepare("
			SELECT g.* FROM `wp_bp_groups` g 
			JOIN `wp_bp_groups_groupmeta` gm 
			ON g.id = gm.group_id 
			WHERE gm.meta_value = %d 
			AND gm.meta_key = 'golightly_gid'
		",
		$gevent->event_interest_group_id));
		
	}
	if(!$group) continue;
	$cat_slug = cn_cat_slug_from_gid($group->id);
	wp_set_object_terms( $event_id, array($cat_slug), 'category' );
	
	//Activity
	$action = '
	<div class="activity-avatar left">
		<a href="'.bp_core_get_user_domain( $post_author ).'">
			'.bp_core_fetch_avatar( array(
				'item_id' => $post_author,
				'object'  => 'user',
				'type'    => 'thumb',
				'class'   => 'avatar imgWrapLeft',
				'width'   => 50,
				'height'  => 50,
				'email'   => false
			) ).'   
		</a>
	</div>
	<div class="byline">
		<span class="author">'.bp_core_get_userlink( $post_author ).'</span> <span class="lowercase">added a new event in</span> <span class="group"><a href="'.bp_get_group_permalink( $group ).'">'. $group->name .'</a></span>
	</div>';

	$content = '
	<h3 class="activity-title">'.$gevent->event_name.'</h3>
	<div class="activity-content">'.$ed.'</div>
	';
	$status = bp_get_group_status( $group );

	$wpdb->insert( 
		$wpdb->prefix . 'bp_activity', 
		array( 
			'user_id' => $post_author,
			'component' => 'groups',
			'type' => 'bp_bgc_created',
			'action' => $action,
			'content' => $content,
			'primary_link' => add_query_arg('gid', $group->id, get_permalink($event_id)),
			'item_id' => $group->id,
			'secondary_item_id' => $event_id,
			'date_recorded' => $last_modified,
			'hide_sitewide' => bp_get_group_status( $group ) == 'public' ? 0 : 1,
		), 
		array( 
			'%s', 
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%d',
			'%d',
			'%s',
			'%d'
		) 
	);
	$aid = $wpdb->insert_id;
	if(!$aid){
		echo 'Error adding event activity (WP ID: ' . $event_id . '<br>';
		var_dump($wpdb->last_query); echo "<br />";
		var_dump($wpdb->last_error); echo "<br /><br />";
	}
	elseif($tag){
		bp_activity_add_meta( $aid, 'post_tag_id', $tag->term_id, false );
	}
	
	$location = '';
	if($gevent->event_location) $location .= $gevent->event_location . ', ';
	if($gevent->event_map_street) $location .= $gevent->event_map_street . ', ';
	if($gevent->event_map_city) $location .= $gevent->event_map_city . ', ';
	if($gevent->event_map_state) $location .= $gevent->event_map_state . ' ';
	if($gevent->event_map_zip) $location .= $gevent->event_map_zip . ', ';
	if($gevent->event_map_country) $location .= $gevent->event_map_country;
	$location = trim($location, ', ');
	
	$show_map = 0;
	if($location && $gevent->event_map_zoom_level) $show_map = 1;
	
	$md = array(); //meta data
	$md['_bgc_event_time'] = $gevent->event_start_date;
	if($location) $md['_bgc_event_location'] = $location;
	$md['_bgc_event_map'] = $show_map;
	$md['golightly_event_id'] = $gevent->event_id;
	$md['_cn_post_sharing'] = 'no';
	if($aid) $md['_cn_bp_activity'] = $aid;
	
	foreach($md as $meta_key => $meta_value) {
		if(!add_post_meta( $event_id, $meta_key, $meta_value, true )){
			echo "Error adding Post Meta for event: " . $event_id . ", meta_key: " . $meta_key . "<br />";
		}
	}

	$added_events++;
}
echo 'Done. Added ' . $added_events . ' events';
*/

/** Delete Members from Groups (patch) **/
/*
//Get WP Groups
$groups = $wpdb->get_results(
	"
	SELECT g.id, gm.meta_value as gid 
	FROM `wp_bp_groups` g 
	JOIN `wp_bp_groups_groupmeta` gm 
	ON g.id = gm.group_id 
	WHERE gm.meta_key = 'golightly_gid'
	"
);

foreach($groups as $group){
	//get golightly grants
	echo '<hr><hr><br>Starting Group: '.$group->id . '<br>';
	$group_grants = $goli->get_results($goli->prepare("
		SELECT *
		FROM `gct_grant` 
		WHERE `controlled_object_id` = %d 
		AND `controlled_object_id` != 3
		AND `grantee_object_class_id` = %d 
		AND `controlled_object_class_id` = %d
		AND `delete_flag` = %d
	",
	(int)$group->gid,
	3,
	2,
	1
	));
	
	if(!$group_grants || !is_array($group_grants)){
		echo 'No grants to be deleted in group: ' . $group->id . '<br>';
		continue;
	}
	
	$ggusers = array();
	foreach($group_grants as $group_grants){
		switch ($group_grants->role_id){
			case 2:
				$ggusers[$group_grants->grantee_object_id]['is_admin'] = 0;
				break;
			case 4:
				$ggusers[$group_grants->grantee_object_id]['is_member'] = 0;
				break;
			case 3:
				$ggusers[$group_grants->grantee_object_id]['is_mod'] = 0;
				break;
		}
	}
	//var_dump($group_grants);
	foreach($ggusers as $gguser => $gguser_data){
		if(empty($gguser_data)) continue;
		$user = get_users(array('meta_key' => 'golightly_id', 'meta_value' => $gguser, 'fields' => 'id', 'number' => 1));
		$user_id = $user[0];
		if(!$user_id) continue;
		echo 'starting user: ' . $user_id . '<br>';
		if(!isset($gguser_data['is_admin']) && !isset($gguser_data['is_mod']) &&
		   isset($gguser_data['is_member']) && $gguser_data['is_member'] === 0)  {
			groups_remove_member( $user_id, $group->id );
			echo 'Removed user ' . $user_id  . ' from group ' . $group->id . '<br>';
			continue;
		}
		else {
			
			
			$data = $data_format = array();
			if(isset($gguser_data['is_admin']) && $gguser_data['is_admin'] === 0 &&
		   	   isset($gguser_data['is_mod']) && $gguser_data['is_mod'] === 0){
				$data['is_admin'] = 0;
				$data['is_mod'] = 0;
				$data_format = array('%d','%d');
			}
			elseif(isset($gguser_data['is_admin']) && $gguser_data['is_admin'] === 0 &&
		   	   	   !isset($gguser_data['is_mod'])) {
				$data['is_admin'] = 0;
				$data_format = array('%d');
			}
			elseif(isset($gguser_data['is_mod']) && $gguser_data['is_mod'] === 0 &&
		   	   	   !isset($gguser_data['is_admin'])) {
				$data['is_mod'] = 0;
				$data_format = array('%d');
			}
			else {
				echo 'No action for user: ' . $user_id; continue;
			}
			
			
			//remove admin or mod attributes
			$wpdb->update( 
				'wp_bp_groups_members',  //table
				$data, 
				array( //where
					'group_id' => $group->id,
					'user_id' => $user_id ), 
				$data_format, 
				array( //where format
					'%d',
					'%d'
				) 
			);
		}
	}
	echo '<br><hr><br>';
	
}
*/

/** Add Group Subscription Data **/
/*$lists = $goli->get_results(
	"SELECT le.`listemails_LocalListID`, eo.email_owner_id, le.`listemails_DeliveryMethod` 
	FROM `gct_list_emails` le 
	JOIN gct_interest_groups g 
		ON g.interestgroups_ID = le.`listemails_LocalListID` 
	JOIN `gct_email_owners` eo 
		ON eo.email_owner_address_id = le.`listemails_LocalEmailAddressID`
	WHERE le.`listemails_LocalListID` != 3"
);

$gglist = array();
foreach($lists as $list){
	$gglist[$list->listemails_LocalListID][$list->email_owner_id] = $list->listemails_DeliveryMethod;
}

foreach($gglist as $ggroup_id => $gusers_data){
	if(empty($gusers_data)) continue;
	
	$group_id = $wpdb->get_var($wpdb->prepare("
			SELECT g.id FROM `wp_bp_groups` g 
			JOIN `wp_bp_groups_groupmeta` gm 
			ON g.id = gm.group_id 
			WHERE gm.meta_value = %d 
			AND gm.meta_key = 'golightly_gid'
		",
		$ggroup_id));
	if(!$group_id) continue;
	
	$users_data = array();
	array_walk($gusers_data, function($gsubscription_method, $guser_id) use (&$users_data) {
		$user = get_users(array('meta_key' => 'golightly_id', 'meta_value' => $guser_id, 'fields' => 'id', 'number' => 1));
		$user_id = $user[0];
		$sub_method = $gsubscription_method == 2 ? 'dig' : 'supersub';
		$users_data[$user_id] = $sub_method;
	});

	//echo 'Group: ' . $group_id . '<br><pre>'; var_dump($users_data); echo '</pre><br><hr><br>';
	
	groups_add_groupmeta( $group_id, 'ass_subscribed_users', $users_data, true );
}*/

/** Add Group Email Addresses to DirectAdmin Aliases **/
/* include($_SERVER['DOCUMENT_ROOT'] . '/directadmin.php');
$group = groups_get_group(array('group_id' => 131));
$group_id = $group->id;
$address = 'foo';

$Socket = new HTTPSocket; 

$Socket->connect("tcp://68.69.2.178", 2222); 
$Socket->set_login('chesadmin','BYGQxa5J'); 
$Socket->set_method('POST'); 

$emails = $wpdb->get_col(
	"SELECT `meta_value` FROM `wp_bp_groups_groupmeta` WHERE `meta_key` = 'group_email_address'"
);
foreach($emails as $email){
	$user_parts = explode('@', $email);
	$user = $user_parts[0];
	$Socket->query('/CMD_API_EMAIL_FORWARDERS', 
		array( 
			'domain' => 'chesapeakenetwork.org', 
			'action' => 'create', 
			'user' => $user, 
			'email' => '"|/home/chesadmin/domains/chesapeakenetwork.org/public_html/emailhandler.php"'
		)
	);
	echo '<pre>';
	var_dump($Socket->result);
	echo '</pre><br><br>';
	
}
*/
/*
$Socket->query('/CMD_API_EMAIL_FORWARDERS', 
	array( 
		'domain' => 'chesapeakenetwork.org', 
		'action' => 'delete', 
		'select0' => 'bar',
		'delete' => 'Delete'
	)
);
var_dump($Socket->result);
if(strpos($Socket->result, 'error=0') === false){
	mail(get_option("admin_email"), '['.get_bloginfo('name').'] Error creating group email alias', "There was an error creating an alias for the group '{$group->name}' (ID: {$group_id})." . "\r\n" . "\r\n" . print_r($Socket->result, true));
}
*/

/** Add Group Library Folders, Files & Aliases **/
//done through group_library_transfer.php
//'books' and 'stories' were added manually

/** Correct Duplicate dlm_download post_name Entries **/
/*
$allduplicates = $wpdb->get_results("
	SELECT `post_name`, COUNT(*) c 
	FROM `wp_posts` 
	WHERE `post_type` = 'dlm_download' 
	GROUP BY `post_name` 
	HAVING c > 1
");

foreach($allduplicates as $row){
	echo '-------------------------<br><br>Starting duplicates for: ' . $row->post_name . '<br>';
	$subset = $wpdb->get_results($wpdb->prepare("SELECT * FROM `wp_posts` WHERE `post_name` = %s && `post_type` = 'dlm_download' ORDER BY `post_date` ASC, `ID` ASC", $row->post_name));
	printf('Found %d duplicates<br>', count($subset));
	
	$map = array();
	foreach($subset as $dlm){
		$map[date('Y-m-d', strtotime($dlm->post_date))][$dlm->ID] = $dlm;
	}
		
	
	foreach($map as $date){
		$date_count = count($date);
		if($date_count < 2) {
			echo 'Skipping because only 1 post in the time frame<br>';
			continue;
		}
		else echo 'Change post_name for ' . $date_count - 1 . ' posts<br>';
		$i = 0;
		foreach($date as $id => $p){
			$i++;
			if($i == 1) continue;
			$chesnet->wp_insert_post_post_ID = $id;
			$chesnet->wp_insert_post_post_args = array('post_date' => $p->post_date);
			$slug = wp_unique_post_slug( $p->post_name, $id, 'publish', 'dlm_download' );
			if($slug != $p->post_name){
				$wpdb->update( $wpdb->posts, array('post_name' => $slug), array('ID' => $id), array('%s'), array('%d') );
				printf('Post ID (%d) was given a new post_name: %s', $id, $slug); echo '<br>';
			}
		}
	}
	echo '<br>';

}
*/

endif;


?>
<div class="row">
    <div class="small-14 large-10 columns" role="main">
        <?php do_action('cn_before_content'); ?>
        <?php while (have_posts()) : the_post(); ?>
            <article <?php post_class() ?> id="post-<?php the_ID(); ?>">
                <header>
                    <h1 class="entry-title"><?php the_title(); ?></h1>
                </header>
                <?php do_action('cn_page_before_entry_content'); ?>
                <div class="entry-content">
                	<?php if(!did_action('template_notices')) do_action( 'template_notices' ); ?>
					<?php the_content(); ?>
                </div>
                <footer>
                    <?php wp_link_pages(array('before' => '<nav id="page-nav"><p>' . __('Pages:', 'chesnet'), 'after' => '</p></nav>' )); ?>
                    <p><?php the_tags(); ?></p>
                </footer>
                <?php do_action('cn_page_before_comments'); ?>
                <?php comments_template(); ?>
                <?php do_action('cn_page_after_comments'); ?>
            </article>
        <?php endwhile;?>

        <?php do_action('cn_after_content'); ?>

    </div>
    <?php get_sidebar('right'); ?>
</div>
<?php get_footer(); ?>
