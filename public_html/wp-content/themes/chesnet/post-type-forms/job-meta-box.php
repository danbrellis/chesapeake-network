<?php

// Don't load directly
if ( !defined( 'ABSPATH' ) ) {
	die('-1' );
}

/**
 * The Jobs Meta Box
 */

?>

<?php //if( WP_DEBUG ) echo '<small>[DEBUG] Path to this template: '.__FILE__.'</small>'; ?>

<div id='jobDetails' class="inside jobForm">
    <?php do_action( 'cn_job_details_top', $postId, true ) ?>
	<?php wp_nonce_field( 'cn_job', 'cn_job_nonce' ); ?>
	<p><label for="cn_jobs_org">Enter the hiring organization.</label>
	<input type="text" id="cn_jobs_org" name="cn_jobs_org" value="<?php echo esc_attr( $cn_jobs_org ); ?>" style="width:99%;" />
	
	<p><label for="cn_jobs_orgweb">Enter the hiring organization's website.</label>
	<input type="text" id="cn_jobs_orgweb" name="cn_jobs_orgweb" value="<?php echo esc_attr( $cn_jobs_orgweb ); ?>" style="width:99%;" /></p>
    
   <?php if($cn_jobs_announcement_file): ?>
		<div><strong><?php echo basename($cn_jobs_announcement_file['file']); ?></strong> is currently attached.
		&nbsp;&nbsp;<span href="#" id="delete_cn_jobs_announcement_file" class="cn_delete_file delete button hide-if-no-js" title="Delete file">Delete</span>
			<div class="hide-if-js"><input type="checkbox" name="cn_delete_file" value="1" id="cn_delete_file" /> <label for="cn_delete_file">No attachment file.</label></div></div>
	<?php endif; ?>
	<p><label for="cn_jobs_announcement_file">Provide an attachment with the job announcement (max 1).</label><br />
	<input type="file" id="cn_jobs_announcement_file" name="cn_jobs_announcement_file" value="" /></p>
	
	<p><label for="cn_jobs_announcement_website">Enter the website for the position announcement (must begin with http:// or https://).</label>
	<input type="text" id="cn_jobs_announcement_website" name="cn_jobs_announcement_website" value="<?php echo esc_attr( $cn_jobs_announcement_website ); ?>" style="width:99%;" /></p>
    
    <p><label for="cn_jobs_apply_by_date">Enter the deadline for receiving applications.</label><br />
	<input type="text" id="cn_jobs_apply_by_date" name="cn_jobs_apply_by_date" value="<?php if($cn_jobs_apply_by_date) echo date('Y-m-d', strtotime($cn_jobs_apply_by_date)); ?>" size="25" class="datepickerclass" /><br />
	<span class="description">(YYYY-MM-DD)</span></p>
    
</div>
<?php do_action( 'cn_job_details_bottom', $postId, true ) ?>
