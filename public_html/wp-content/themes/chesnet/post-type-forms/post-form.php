<?php

// Don't load directly
if ( !defined( 'ABSPATH' ) ) {
	die('-1');
}


/**
 * The Submit and Edit job form
 */
?>
<p><a href="<?php echo $this->cn_getUrl( 'list', null, null, $post->post_type, null ); ?>">Cancel</a></p>
<form method="post" action="" enctype="multipart/form-data">
<?php do_action( 'cn_post_submission' ); ?>
	<?php wp_nonce_field( $post_type_obj->name . '_submission' ); ?>
    
    <?php echo $this->cn_get_user_groups_checkboxes($group_ids); ?>
    
	<?php do_action( 'cn_submission_form_before_post_title' ); ?>
	<div class="post-title">
		<label for='post_title' <?php if ( $_POST && empty( $post->post_title ) ) echo 'class="error"'; ?>>Post Title</label> <small class="req">(required)</small>
		<input type="text" tabindex="0" name="post_title" value="<?php echo ( isset( $post->post_title ) ) ? esc_html( stripslashes( $post->post_title ) ) : ''; ?>"/>
	</div>
	<?php do_action( 'cn_submission_form_after_post_title' ); ?>

	<?php do_action( 'cn_submission_form_before_post_content' ); ?>
	<div class="cn-post-content">
		<label for='post_content' <?php if ( $_POST && empty( $post->post_content)  ) echo 'class="error"'; ?>>Message</label> <small class="req">(required)</small>
		<?php $this->cn_formContentEditor( $post, true ); ?>
	</div>
	<?php do_action( 'cn_submission_form_after_post_content' ); ?>

	<p></p>


	<?php
	//cn_formPostDetails( $post, 'post' );
	$this->cn_formSpamControl();
	?>
	
	<?php do_action( 'cn_submission_form_before_post_submit_button' ); ?>	
	<div class="form-footer wp-admin">
	<input type='submit' id="post" class="button submit cn-submit" value="<?php
	if ( isset( $id ) && $id ) {
		echo 'Update Post';
	} else {
		echo 'Submit Post' ;
	}
	?>" name='cn-post-form' />
	</div>
<?php do_action( 'cn_submission_form_after_post_submit_button' ); ?>	
<?php do_action( 'cn_submission_form_bottom' ); ?>

</form>