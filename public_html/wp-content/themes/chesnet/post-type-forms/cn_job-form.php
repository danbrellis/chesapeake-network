<?php

// Don't load directly
if ( !defined( 'ABSPATH' ) ) {
	die('-1');
}


/**
 * The Submit and Edit job form
 */
?>
<p><a href="<?php echo $this->cn_getUrl( 'list', null, null, $post->post_type, null ); ?>">Cancel</a></p>
<form method="post" action="" enctype="multipart/form-data">
<?php do_action( 'cn_post_submission' ); ?>
	<?php wp_nonce_field( $post_type_obj->name . '_submission' ); ?>

	<?php do_action( 'cn_submission_form_before_post_title' ); ?>
	<div class="post-title">
		<label for='post_title' <?php if ( $_POST && empty( $post->post_title ) ) echo 'class="error"'; ?>>Position Title</label> <small class="req">(required)</small>
		<input type="text" tabindex="0" name="post_title" value="<?php echo ( isset( $post->post_title ) ) ? esc_html( stripslashes( $post->post_title ) ) : ''; ?>"/>
	</div>
	<?php do_action( 'cn_submission_form_after_post_title' ); ?>

	<?php do_action( 'cn_submission_form_before_post_content' ); ?>
	<div class="cn-post-content">
		<label for='post_content' <?php if ( $_POST && empty( $post->post_content)  ) echo 'class="error"'; ?>>Job Description</label> <small class="req">(required)</small>
		<?php $this->cn_formContentEditor( $_POST['post_content'] ? esc_attr($_POST['post_content']) : esc_attr($post->post_content), true ); ?>
	</div>
	<?php do_action( 'cn_submission_form_after_post_content' ); ?>

	<p></p>


	<?php
	$this->cn_formPostDetails( $post, 'cn_job' );
	$this->cn_formSpamControl();
	?>
	
	<?php do_action( 'cn_job_submission_form_before_post_submit_button' ); ?>	
	<div class="form-footer wp-admin">
	<input type='submit' id="post" class="button submit cn-submit" value="<?php
	if ( isset( $id ) && $id ) {
		echo 'Update Job';
	} else {
		echo 'Submit Job' ;
	}
	?>" name='cn-post-form' />
	</div>
<?php do_action( 'cn_submission_form_after_post_submit_button' ); ?>	
<?php do_action( 'cn_submission_form_bottom' ); ?>

</form>