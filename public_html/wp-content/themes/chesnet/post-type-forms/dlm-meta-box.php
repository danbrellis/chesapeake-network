<?php

// Don't load directly
if ( !defined( 'ABSPATH' ) ) {
	die('-1' );
}

/**
 * The Download Versions Box
 */

?>

<?php //if( WP_DEBUG ) echo '<small>[DEBUG] Path to this template: '.__FILE__.'</small>'; ?>

<?php
if ( is_string( $files ) ) {
	$files = array_filter( (array) json_decode( $files ) );
} elseif ( is_array( $files ) ) {
	$files = array_filter( $files );
} else {
	$files = array();
}
?>

<div id="dlmDetails" class="inside dlmForm">
    <?php do_action( 'dlm_details_top', $postId, true ); ?>
    
	<p><label for="dlm_new_upload"><?php _e('Upload a file.', 'chesnet'); ?></label><br />
	<input type="file" name="<?php echo $post->ID ? $post->ID : 'new_dlm_download_version'; ?>-dlm_new_upload" /><br />
    <span class="description"><?php _e('Or enter multiple files for mirrors, one file url per line.', 'chesnet'); ?></span><br />
    <textarea name="dlm_download_version[<?php echo $post->ID ? $post->ID : 'new_dlm_download_version'; ?>][files]" style="width:98%"><?php echo esc_textarea( implode( "\r", $files ) ); ?></textarea></p>
    
    <p><label><?php _e('Version', 'chesnet'); ?></label><br />
	<input type="text" name="dlm_download_version[<?php echo $post->ID ? $post->ID : 'new_dlm_download_version'; ?>][version]" value="<?php echo $version; ?>" /></p>
    <p><label><?php _e('Priority', 'chesnet'); ?></label><br /><input type="text" class="file_menu_order" name="dlm_download_version[<?php echo $post->ID ? $post->ID : 'new_dlm_download_version'; ?>][menu_order]" value="<?php echo $post->menu_order; ?>" /></p>
	    
</div>
<?php do_action( 'dlm_details_bottom', $postId, true ) ?>
