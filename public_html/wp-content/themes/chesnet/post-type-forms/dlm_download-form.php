<?php

// Don't load directly
if ( !defined( 'ABSPATH' ) ) {
	die('-1');
}

//get all dlm versions
if(isset($post->ID)) $versions =& get_posts( array(
	'post_parent'		=> $id,
	'post_type'			=> 'dlm_download_version',
	'orderby'			=> 'menu_order post_modified',
	'order'				=> 'ASC',
	'post_status'		=> 'any',
	'posts_per_page'	=> -1) );

/**
 * The Submit and Edit download form
 */
?>
<p><a href="<?php the_permalink(); ?>"title="<?php the_title_attribute(); ?>"><?php _e('Cancel', 'chesnet'); ?></a></p>
<form method="post" action="" enctype="multipart/form-data">
<?php do_action( 'cn_post_submission' ); ?>
	<?php wp_nonce_field( $post_type_obj->name . '_submission' ); ?>
    
    <p>Add to group libraries:</p>
    <?php echo $this->cn_get_user_groups_checkboxes($group_ids); ?>
    
    <?php echo $this->cn_get_tagit_form($post->ID, $this->get_dlm_tax($this->cn_set_group_id)); ?>
    <hr />

	<?php do_action( 'cn_submission_form_before_post_title' ); ?>
	<div class="post-title">
		<label for='post_title' <?php if ( $_POST && empty( $post->post_title ) ) echo 'class="error"'; ?>><?php _e('Download Title', 'chesnet'); ?></label> <small class="req">(required)</small>
		<input type="text" tabindex="0" name="post_title" value="<?php echo ( isset( $post->post_title ) ) ? esc_html( stripslashes( $post->post_title ) ) : ''; ?>"/>
	</div>
	<?php do_action( 'cn_submission_form_after_post_title' ); ?>

	<?php do_action( 'cn_submission_form_before_post_content' ); ?>
	<div class="cn-post-content">
		<label for='post_content' <?php if ( $_POST && empty( $post->post_content)  ) echo 'class="error"'; ?>>Download Description</label> <small class="req">(required)</small>
		<?php $this->cn_formContentEditor( $_POST['post_content'] ? esc_attr($_POST['post_content']) : esc_attr($post->post_content), true ); ?>
	</div>
	<?php do_action( 'cn_submission_form_after_post_content' ); ?>
    

	<p>Versions:</p>
	<?php
	//new version
	$this->cn_formPostDetails( null, 'dlm_download_version' );
	if($versions){
		//each existing one
		foreach($versions as $child) $this->cn_formPostDetails( $child, 'dlm_download_version' );
	}
	$this->cn_formSpamControl();
	?>
	
	<?php do_action( 'cn_dlm_submission_form_before_post_submit_button' ); ?>	
	<div class="form-footer wp-admin">
	<input type='submit' id="post" class="button submit cn-submit" value="<?php
	if ( isset( $cn_dlm_id ) && $cn_dlm_id ) {
		echo 'Update Download';
	} else {
		echo 'Submit Download' ;
	}
	?>" name='cn-post-form' />
	</div>
<?php do_action( 'cn_submission_form_after_post_submit_button', $post ); ?>	
<?php do_action( 'cn_submission_form_bottom', $post ); ?>

</form>