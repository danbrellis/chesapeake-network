<?php

/**
 * Define some contants
 */
 
define( 'BP_MESSAGES_AUTOCOMPLETE_ALL', true );

$this->error_codes = array( 
	0 => __("There is no error, the file uploaded with success", "chesnet"),
	1 => __("The uploaded file exceeds the upload_max_filesize directive in php.ini", "chesnet"),
	2 => __("The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form", 'chesnet'), 
	3 => __("The uploaded file was only partially uploaded", 'chesnet'), 
	4 => __("No file was uploaded", 'chesnet'), 
	6 => __("Missing a temporary folder", 'chesnet')
);

?>