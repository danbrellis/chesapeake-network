<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;
/**
 * Register chesnet widgets.
 */
function cn_register_widgets() {
	register_widget("Chesnet_Login_Widget");
	register_widget("Chesnet_Ads_Widget");
}
add_action( 'widgets_init', 'cn_register_widgets' );

/**
 * Chesnet Login Widget.
 *
 * @since Chesnet (1.0.0)
 */
class Chesnet_Login_Widget extends WP_Widget {

	/**
	 * Constructor method.
	 */
	public function __construct() {
		parent::__construct(
			false,
			_x( '(Chesnet) Log In', 'Title of the login widget', 'chesnet' ),
			array(
				'description' => __( 'Show a Log In form to logged-out visitors, and an avatar and edit profile link to those logged in.', 'chesnet' )
			)
		);
	}

	/**
	 * Display the login widget.
	 *
	 * @see WP_Widget::widget() for description of parameters.
	 *
	 * @param array $args Widget arguments.
	 * @param array $instance Widget settings, as saved by the user.
	 */
	public function widget( $args, $instance ) {
		$title = isset( $instance['title'] ) ? $instance['title'] : '';
		$title = apply_filters( 'widget_title', $title );

		echo $args['before_widget'];

		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
		}
		
		if ( is_user_logged_in() ) : ?>

			<?php do_action( 'cn_before_login_widget_loggedin' ); ?>
            
            <div class="bp-login-widget-user-avatar left imgWrapLeft">
                <a href="<?php echo bp_loggedin_user_domain(); ?>">
                    <?php bp_loggedin_user_avatar( 'type=thumb&width=50&height=50' ); ?>
                </a>
            </div>
    
            <div class="bp-login-widget-user-links">
                <span class="byline author"><?php echo bp_core_get_userlink( bp_loggedin_user_id() ); ?></span><br />
                <a class="edit-profile" href="<?php echo bp_loggedin_user_domain() . 'profile/edit/'; ?>"><?php _e( 'Edit Profile', 'chesnet' ); ?></a>
            </div>
        

			<?php do_action( 'cn_after_login_widget_loggedin' ); ?>

		<?php else : ?>

			<?php do_action( 'cn_before_login_widget_loggedout' ); ?>

			<form name="bp-login-form" id="bp-login-widget-form" class="standard-form" action="<?php echo esc_url( site_url( 'wp-login.php', 'login_post' ) ); ?>" method="post">
				<div class="row">
                	<div class="large-14 columns">
                    	<label for="bp-login-widget-user-login"><?php _e( 'Username or Email', 'chesnet' ); ?>
						<input type="text" name="log" id="bp-login-widget-user-login" class="input" value="" /></label>
                    </div>
                </div>
                <div class="row">
                	<div class="large-14 columns">
						<label for="bp-login-widget-user-pass"><?php _e( 'Password', 'chesnet' ); ?>
                        <input type="password" name="pwd" id="bp-login-widget-user-pass" class="input" value=""  /></label>
                    </div>
                </div>

				<div class="row">
                	<div class="large-14 columns forgetmenot">
                    	<input name="rememberme" type="checkbox" id="bp-login-widget-rememberme" value="forever" checked /><label for="bp-login-widget-rememberme"><?php _e( 'Remember Me', 'chesnet' ); ?></label>
                    </div>
                </div>
                <div class="row">
                	<div class="large-7 columns">
                    	<input type="submit" name="wp-submit" class="button small" id="bp-login-widget-submit" value="<?php esc_attr_e( 'Log In', 'chesnet' ); ?>" />
                    </div>
                    <div class="large-7 columns">
						<?php if ( bp_get_signup_allowed() ) : ?>
                        	<label class="bp-login-widget-register-link inlinewithbutton"><?php printf( __( '<a href="%s" title="Register for a new account">Register</a>', 'chesnet' ), bp_get_signup_page() ); ?></label>
						<?php endif; ?>
                    </div>
                </div>
			</form>

			<?php do_action( 'cn_after_login_widget_loggedout' ); ?>

		<?php endif; ?>
		
        <ul class="subtle-sidenav">
            <li><a class="fi-torsos-female-male" href="<?php echo trailingslashit( bp_get_root_domain() . '/' . bp_get_members_root_slug() . '/'); ?>"><?php _e('Members', 'chesnet'); ?></a></li>
            <?php $cn = groups_get_group( array( 'group_id' => 1 ) );
            
            if(class_exists('BP_Group_Calendar_Extension')):
                $BP_Group_Calendar_Extension = new BP_Group_Calendar_Extension;
                $BP_Group_Calendar_Extension->bp_group_calendar_extension();
                $cal_href =  bp_get_root_domain() . '/' . bp_get_groups_root_slug() . '/' . $cn->slug . '/' . $BP_Group_Calendar_Extension->slug . '/'; ?>
                <li><a class="fi-calendar" href="<?php echo trailingslashit($cal_href); ?>"><?php echo $BP_Group_Calendar_Extension->name; ?></a></li>
            <?php endif; ?>
            <?php if(class_exists('bbpress')):
                $forum_href =  bp_get_root_domain() . '/' . bbp_get_forum_slug() . '/group-forums'; ?>
                <li><a class="fi-comment-quotes" href="<?php echo trailingslashit($forum_href); ?>"><?php _e('Forums'); ?></a></li>
            <?php endif; ?>
        </ul>

		<?php echo $args['after_widget'];
	}

	/**
	 * Update the login widget options.
	 *
	 * @param array $new_instance The new instance options.
	 * @param array $old_instance The old instance options.
	 * @return array $instance The parsed options to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance             = $old_instance;
		$instance['title']    = isset( $new_instance['title'] ) ? strip_tags( $new_instance['title'] ) : '';

		return $instance;
	}

	/**
	 * Output the login widget options form.
	 *
	 * @param $instance Settings for this widget.
	 */
	public function form( $instance = array() ) {

		$settings = wp_parse_args( $instance, array(
			'title' => '',
		) ); ?>

		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'chesnet' ); ?>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $settings['title'] ); ?>" /></label>
		</p>

		<?php
	}
}


/**
 * Chesnet Advertising Widget.
 *
 * @since Chesnet (1.0.0)
 */
class Chesnet_Ads_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'chesnet_ads_widget', // Base ID
			__( '(Chesnet) Simple Ad Widget', 'chesnet' ), // Name
			array( 'description' => __( 'A widget for simple advertising displays', 'chesnet' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		if(is_array($instance['ads'])){
			if(count($instance['ads']) > 1)
				$ad = $instance['ads'][mt_rand(0, count($instance['ads']) - 1)];
			else $ad = $instance['ads'][0];
		}
		else $ad = $instance['ads'];

		printf('<div id="cn_track_ad_click" onclick="cn_track_ad_click(\'%s\', \'%s\'); return false;">', $ad['name'], $ad['link']); //https://support.google.com/analytics/answer/1136920?hl=en
		if(isset($ad['link']) && !empty($ad['link']))
			printf('<a href="%s" target="_blank">%s</a>', esc_url($ad['link']), $ad['content']);
		else echo $ad['content'];
		echo '</div>';

		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		
		$ad_fields_template = '<div class="ad-data-field">
			<p>
				<label for="%8$s">'.__('Ad Name', 'chesnet').':</label>
				<input class="widefat" id="%8$s" name="%9$s" type="url" value="%1$s">
			</p>
			<p>
				<label for="%6$s">'.__('Content', 'chesnet').':</label>
				<textarea class="widefat" rows="16" cols="15" id="%6$s" name="%4$s">%2$s</textarea>
			</p>
			<p>
				<label for="%7$s">'.__('URL', 'chesnet').':</label>
				<input class="widefat" id="%7$s" name="%5$s" type="url" value="%3$s">
			</p>
			<hr />
    </div>';

		$ads = isset ( $instance['ads'] ) ? $instance['ads'] : array();
		$ads_num = count( $ads );
		$ads[$ads_num] = array('name' => '', 'content'=>'', 'link'=>'http://'); //add a new, blank fieldset
		$i = 0;
		
		foreach ( $ads as $id => $ad ) {
			printf(
				$ad_fields_template,
				isset($ad['name']) ? esc_attr($ad['name']) : '', //1
				esc_textarea($ad['content']), //2
				isset($ad['link']) ? esc_url($ad['link']) : 'http://', //3
				$this->get_field_name('ads['.$i.'][content]'), //4
				$this->get_field_name('ads['.$i.'][link]'), //5
				$this->get_field_id('ads-'.$i.'-content'), //6
				$this->get_field_id('ads-'.$i.'-link'), //7
				$this->get_field_id('ads-'.$i.'-name'), //8
				$this->get_field_name('ads['.$i.'][name]') //9
				
			);
			$i++;
		}
  
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance['ads'] = array();
		$i = 0;
		if ( isset ( $new_instance['ads'] ) ) {
			foreach ( $new_instance['ads'] as $ad ) {
				if(!is_array($ad)) continue;
				
				if ( '' == trim( $ad['content'] ) || '' == trim( $ad['name'] ) || '' == trim( $ad['link'] || $ad['link'] == 'http://' ) ) continue;
				else {
					$instance['ads'][$i]['content'] = stripslashes($ad['content']);
					$instance['ads'][$i]['name'] = stripslashes($ad['name']);
					$instance['ads'][$i]['link'] = $ad['link'];
				}
					
				$i++;
			}
		}
		return $instance;
	}

} // class CN_Ads_Widget


?>