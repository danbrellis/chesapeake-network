<?php

function cn_sidebar_widgets() {
	register_sidebar(array(
		'id' => 'sidebar-widgets',
		'name' => __('Sidebar widgets', 'chesnet'),
		'description' => __('Drag widgets to this sidebar container.', 'chesnet'),
		'before_widget' => '<div id="%1$s" class="homeSideNav">',
		'after_widget' => '</div>',
		'before_title' => '<h6>',
		'after_title' => '</h6>'
	));

	register_sidebar(array(
		'id' => 'footer-widgets',
		'name' => __('Footer widgets', 'chesnet'),
		'description' => __('Drag widgets to this footer container', 'chesnet'),
		'before_widget' => '<article id="%1$s" class="large-4 columns widget %2$s">',
		'after_widget' => '</article>',
		'before_title' => '<h6>',
		'after_title' => '</h6>'      
	));
	register_sidebar(array(
		'name' => __( 'Right Hand Sidebar', 'chesnet' ),
		'id' => 'right-sidebar',
		'before_widget' => '<div id="%1$s" class="sidebar-module sidebar-module-inset panel">',
		'after_widget' => '</div>',
		'description' => __( 'Widgets in this area will be shown on the right-hand side.', 'chesnet' ),
		'before_title' => '<h4>',
		'after_title' => '</h4>'
	));
	register_sidebar(array(
		'name' => __( 'Right Ads', 'chesnet' ),
		'id' => 'right-sidebar-ads',
		'before_widget' => '<div id="%1$s" class="sidebar-module sidebar-module-inset sponsored-module">',
		'after_widget' => '</div>',
		'description' => __( 'Widgets in this area will be shown as ads on the right sidebar.', 'chesnet' ),
		'before_title' => '<h4>',
		'after_title' => '</h4>'
	));
	register_sidebar(array(
		'name' => __( 'Footer', 'chesnet' ),
		'id' => 'footer-sidebar',
		'before_widget' => '<div id="%1$s" class="footer-widget">',
		'after_widget' => '</div>',
		'description' => __( 'Widgets in this area will be shown in the footer.', 'chesnet' ),
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	));
}

add_action( 'widgets_init', 'cn_sidebar_widgets' );

?>