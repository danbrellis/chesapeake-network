<?php

/**
 * Start cleanup functions
 * ----------------------------------------------------------------------------
 */


add_action('after_setup_theme','start_cleanup');

function start_cleanup() {

    // launching operation cleanup
    add_action('init', 'cleanup_head');

    // remove WP version from RSS
    add_filter('the_generator', 'remove_rss_version');

    // remove pesky injected css for recent comments widget
    add_filter( 'wp_head', 'remove_wp_widget_recent_comments_style', 1 );

    // clean up comment styles in the head
    add_action('wp_head', 'remove_recent_comments_style', 1);

    // clean up gallery output in wp
    add_filter('gallery_style', 'gallery_style');
    
    // additional post related cleaning
    add_filter('get_image_tag_class', 'image_tag_class', 0, 4);
    add_filter('get_image_tag', 'image_editor', 0, 4);
    //add_filter( 'the_content', 'img_unautop', 30 );

} 

/**
 * Clean up head
 * ----------------------------------------------------------------------------
 */

function cleanup_head() {

    // EditURI link
    remove_action( 'wp_head', 'rsd_link' );

    // Category feed links
    remove_action( 'wp_head', 'feed_links_extra', 3 );

    // Post and comment feed links
    remove_action( 'wp_head', 'feed_links', 2 );
    
    // Windows Live Writer
    remove_action( 'wp_head', 'wlwmanifest_link' );

    // Index link
    remove_action( 'wp_head', 'index_rel_link' );

    // Previous link
    remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );

    // Start link
    remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );

    // Canonical
    remove_action('wp_head', 'rel_canonical', 10, 0 );

    // Shortlink
    remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );

    // Links for adjacent posts
    remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );

    // WP version
    remove_action( 'wp_head', 'wp_generator' );

    // Remove WP version from css
    add_filter( 'style_loader_src', 'remove_wp_ver_css_js', 9999 );

    // Remove WP version from scripts
    add_filter( 'script_loader_src', 'remove_wp_ver_css_js', 9999 );

    // Prevent unneccecary info from being displayed
    add_filter('login_errors',create_function('$a', "return null;"));

} 


// remove WP version from RSS
function remove_rss_version() { return ''; }

// remove WP version from scripts
function remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}

// remove injected CSS for recent comments widget
function remove_wp_widget_recent_comments_style() {
   if ( has_filter('wp_head', 'wp_widget_recent_comments_style') ) {
      remove_filter('wp_head', 'wp_widget_recent_comments_style' );
   }
}

// remove injected CSS from recent comments widget
function remove_recent_comments_style() {
  global $wp_widget_factory;
  if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
    remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));
  }
}

// remove injected CSS from gallery
function gallery_style($css) {
  return preg_replace("!<style type='text/css'>(.*?)</style>!s", '', $css);
}

/**
 * Clean up image tags
 * ----------------------------------------------------------------------------
 */


// Remove default inline style of wp-caption
add_shortcode('wp_caption', 'fixed_img_caption_shortcode');
add_shortcode('caption', 'fixed_img_caption_shortcode');
function fixed_img_caption_shortcode($attr, $content = null) {
    if ( ! isset( $attr['caption'] ) ) {
        if ( preg_match( '#((?:<a [^>]+>\s*)?<img [^>]+>(?:\s*</a>)?)(.*)#is', $content, $matches ) ) {
            $content = $matches[1];
            $attr['caption'] = trim( $matches[2] );
        }
    }
    $output = apply_filters('img_caption_shortcode', '', $attr, $content);
    if ( $output != '' )
        return $output;
    extract(shortcode_atts(array(
        'id'    => '',
        'align' => 'alignnone',
        'width' => '',
        'caption' => '',
        'class'   => ''
    ), $attr));
    if ( 1 > (int) $width || empty($caption) )
        return $content;

    $markup = '<figure';
    if ($id) $markup .= ' id="' . esc_attr($id) . '"';
    if ($class) $markup .= ' class="' . esc_attr($class) . '"';
    $markup .= '>';
    $markup .= do_shortcode( $content ) . '<figcaption>' . $caption . '</figcaption></figure>';
    return $markup;
}


// Clean the output of attributes of images in editor
function image_tag_class($class, $id, $align, $size) {
    $align = 'align' . esc_attr($align);
    return $align;
} 

// Remove width and height in editor, for a better responsive world.
function image_editor($html, $id, $alt, $title) {
    return preg_replace(array(
            '/\s+width="\d+"/i',
            '/\s+height="\d+"/i',
            '/alt=""/i'
        ),
        array(
            '',
            '',
            '',
            'alt="' . $title . '"'
        ),
        $html);
} 

// Wrap images with figure tag - Credit: Robert O'Rourke - http://bit.ly/1q0WHFs
function img_unautop($pee) {
	$pee = preg_replace('/<p>\\s*?(<a .*?><img.*?><\\/a>|<img.*?>)?\\s*<\\/p>/s', '<figure>$1</figure>', $pee);
    return $pee;
}

//make sure all activity items have the same tags as their respective post
function cleanup_activity_post_tags($span = '30 hours ago'){
	global $chesnet;
	$taxonomies = array( 
		'post_tag'
	);
	
	$term_args = array(
		'hide_empty'        => true, 
		'fields'            => 'id', 
	);
	$terms = get_terms('post_tag', $term_args);
	
	$args = array(
		'date_query' => array(
			array(
				'column' => 'post_modified',
				'after'  => $span,
			),
		),
		'posts_per_page' => -1,
		'post_type' => $chesnet->user_can_add,
		'tag__in' => $terms
	);
	$query = new WP_Query( $args );
	if($query->have_posts()){
		while ( $query->have_posts() ) {
			$query->the_post();
			$post_id = $query->post->ID;
			//get tag for post
			$tag_ids = wp_get_object_terms( $post_id, 'post_tag', array('fields' => 'ids') );
			
			//get activity ideas for post
			$activity_ids = get_post_meta($post_id, '_cn_bp_activity');
			foreach($activity_ids as $activity_id){
				
				//add the post_tag id to the activity meta
				if($tag_ids && is_array($tag_ids)){
					$activity_tags_old = $tags_to_delete = $tags_to_add = array();
					//get activity's tags
					$activity_tags_old = bp_activity_get_meta($activity_id, 'post_tag_id', false);
					$tags_to_delete = array_diff($activity_tags_old, $tag_ids);
					$tags_to_add = array_diff($tag_ids, $activity_tags_old);
					
					if(!empty($tags_to_delete) && is_array($tags_to_delete)) {
						foreach($tags_to_delete as $tag_to_delete){
							bp_activity_delete_meta($activity_id, 'post_tag_id', $tag_to_delete);
						}
					}
			
					if(!empty($tags_to_add) && is_array($tags_to_add)) {
						foreach($tags_to_add as $tag_to_add){
							bp_activity_add_meta($activity_id, 'post_tag_id', $tag_to_add);
						}
					}
				}
				else bp_activity_delete_meta($activity_id, 'post_tag_id');
			}
		}
	}
	
	// Restore original Post Data
	wp_reset_postdata();
}

?>
