<?php

/**
 * Register Menus
 * http://codex.wordpress.org/Function_Reference/register_nav_menus#Examples
 */
register_nav_menus(array(
    'top-bar-l' => __('Left Top Bar', 'chesnet'), // registers the menu in the WordPress admin menu editor
    'top-bar-r' => __('Right Top Bar', 'chesnet'),
	'top-bar-r-lo' => __('Right Top Bar (logged out)', 'chesnet'),
    'mobile-off-canvas' => __('Mobile', 'chesnet'),
	'footer_menu' => __('Footer Navigation', 'chesnet')
));


/**
 * Left top bar
 * http://codex.wordpress.org/Function_Reference/wp_nav_menu
 */
function cn_top_bar_l() {
	add_filter( 'wp_nav_menu_objects', 'cn_top_bar_l_filter', 10, 2 );
	
	wp_nav_menu(array( 
		'container' => false,                          // remove nav container
		'container_class' => '',                        // class of container
		'menu' => '',                                   // menu name
		'menu_class' => 'top-bar-menu left',            // adding custom nav class
		'theme_location' => 'top-bar-l',                // where it's located in the theme
		'before' => '',                                 // before each link <a> 
		'after' => '',                                  // after each link </a>
		'link_before' => '',                            // before each link text
		'link_after' => '',                             // after each link text
		'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
		'depth' => 5,                                   // limit the depth of the nav
		'fallback_cb' => false,                         // fallback function (see below)
		'walker' => new cn_top_bar_walker()
	));
	
	remove_filter( 'wp_nav_menu_objects', 'cn_top_bar_l_filter' );
	
}

/**
 * Right top bar
 */
function cn_top_bar_r() {
	if ( is_user_logged_in() ) {
		global $bp;

		//settings
		$bp_settings = $bp->settings;
		$setcom = '';
		
		// Setup the logged in user variables
		$user_domain   = bp_loggedin_user_domain();
		$settings_link = trailingslashit( $user_domain . $bp_settings->slug );
		
		// General Account
		$settings_nav[] = array(
			'id'     => 'my-account-' . $bp_settings->id . '-general',
			'title'  => __( 'General', 'chesnet' ),
			'href'   => trailingslashit( $settings_link . 'general' )
		);
		
		if(bp_is_active( 'notifications' )){
			// Notifications
			$settings_nav[] = array(
				'id'     => 'my-account-' . $bp_settings->id . '-notifications',
				'title'  => __( 'Notifications', 'chesnet' ),
				'href'   => trailingslashit( $settings_link . 'notifications' )
			);
		}
		
		if(bp_is_active( 'groups' )){
			// My Groups
			$settings_nav[] = array(
				'id'     => 'my-account-'.$bp->groups->id.'-memberships',
				'title'  => __( 'My Groups', 'chesnet' ),
				'href'   => trailingslashit( $user_domain . $bp->groups->slug )
			);
		}
		
		//Log out
		$settings_nav[] = array(
			'id'     => 'my-logout',
			'title'  => __( 'Log Out', 'chesnet' ),
			'href'   => str_replace("%", "%%", wp_logout_url())
		);
		
		$avatar = bp_core_fetch_avatar( array( 'item_id' => $bp->loggedin_user->id, 'type' => 'thumb', 'alt' => $bp->displayed_user->fullname, 'width' => 35, 'height' => 35 ) );
	
	
		$setcom .= '<li class="has-dropdown" id="my-account-' . $bp_settings->id.'">
			<a href="'.bp_core_get_user_domain(get_current_user_id()).'" title="'.__('My Account', 'chesnet').'"><span class="screen-reader-text">'.__('My Account', 'chesnet').'</span>'.$avatar.'</a>';
		$setcom .= '<ul class="dropdown">';
			foreach($settings_nav as $nav){
				$setcom .= '<li id="'.$nav['id'].'"><a href="'.$nav['href'].'">'.$nav['title'].'</a></li>';
			}
			$setcom .= '</ul>';
		$setcom .= '</li>';
	
		wp_nav_menu(array( 
			'container' => false,                           // remove nav container
			'container_class' => '',                        // class of container
			'menu' => '',                                   // menu name
			'menu_class' => 'top-bar-menu right',           // adding custom nav class
			'theme_location' => 'top-bar-r',                // where it's located in the theme
			'before' => '',                                 // before each link <a> 
			'after' => '',                                  // after each link </a>
			'link_before' => '',                            // before each link text
			'link_after' => '',                             // after each link text,
			'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s'.$setcom.'</ul>',
			'depth' => 5,                                   // limit the depth of the nav
			'fallback_cb' => false,                         // fallback function (see below)
			//'walker' => new cn_top_bar_walker()
		));
	}
	else {
		wp_nav_menu(array( 
			'container' => false,                           // remove nav container
			'container_class' => '',                        // class of container
			'menu' => '',                                   // menu name
			'menu_class' => 'top-bar-menu right',           // adding custom nav class
			'theme_location' => 'top-bar-r-lo',                // where it's located in the theme
			'before' => '',                                 // before each link <a> 
			'after' => '',                                  // after each link </a>
			'link_before' => '',                            // before each link text
			'link_after' => '',                             // after each link text,
			'depth' => 5,                                   // limit the depth of the nav
			'fallback_cb' => false,                         // fallback function (see below)
			//'walker' => new cn_top_bar_walker()
		));
	}
}

/**
 * Mobile off-canvas
 */
function cn_mobile_off_canvas() {
	wp_nav_menu(array( 
		'container' => false,                           // remove nav container
		'container_class' => '',                        // class of container
		'menu' => '',                                   // menu name
		'menu_class' => 'off-canvas-list',              // adding custom nav class
		'theme_location' => 'mobile-off-canvas',        // where it's located in the theme
		'before' => '',                                 // before each link <a> 
		'after' => '',                                  // after each link </a>
		'link_before' => '',                            // before each link text
		'link_after' => '',                             // after each link text
		'depth' => 5,                                   // limit the depth of the nav
		'fallback_cb' => false,                         // fallback function (see below)
		'walker' => new top_bar_walker()
	));
}

/**
 * Footer Menu
 */
function cn_footer_menu() {
	$before_items = sprintf(__('&copy; %d Chesapeake Network', 'chesnet'), date('Y')); 
	wp_nav_menu(array( 
		'container' => 'div',                         // remove nav container
		'container_class' => 'panel fm_nav',            // class of container
		'menu' => '',                                   // menu name
		'menu_class' => 'fm',            				// adding custom nav class
		'theme_location' => 'footer_menu',        		// where it's located in the theme
		'before' => '',                                 // before each link <a> 
		'after' => '',                                  // after each link </a>
		'link_before' => '',                            // before each link text
		'link_after' => '',                             // after each link text
		'items_wrap' => '<ul id="%1$s" class="%2$s"><li>'.$before_items.'</li>%3$s</ul>',
		'depth' => 1,                                   // limit the depth of the nav
		'fallback_cb' => false,                          // fallback function (see below)
		//'walker' => new cn_footer_walker()
	));
}

/** 
 * Add support for buttons in the top-bar menu: 
 * 1) In WordPress admin, go to Apperance -> Menus. 
 * 2) Click 'Screen Options' from the top panel and enable 'CSS CLasses' and 'Link Relationship (XFN)'
 * 3) On your menu item, type 'has-form' in the CSS-classes field. Type 'button' in the XFN field
 * 4) Save Menu. Your menu item will now appear as a button in your top-menu
*/
if ( ! function_exists( 'add_menuclass') ) {
	function add_menuclass($ulclass) {
	    $find = array('/<a rel="button"/', '/<a title=".*?" rel="button"/');
	    $replace = array('<a rel="button" class="button"', '<a rel="button" class="button"');
	    
	    return preg_replace($find, $replace, $ulclass, 1);
	}
	add_filter('wp_nav_menu','add_menuclass');
}

/**
 * Filter Functions
 */
function cn_top_bar_l_filter( $sorted_menu_items, $args ){
	if ( ! is_user_logged_in() ) {
		return $sorted_menu_items;
	}

	//Notifications
	if(bp_is_active( 'notifications' )){
		$notifications = bp_notifications_get_notifications_for_user( bp_loggedin_user_id(), 'object' );
		$count         = ! empty( $notifications ) ? count( $notifications ) : 0;
		$alert   	   = (int) $count > 0 ? sprintf('<span class="round label">%d</span>', number_format_i18n($count)) : '';
		$menu_title    = '<span id="ab-pending-notifications"><span class="screen-reader-text">'.__('Notifications', 'chesnet').'</span><i class="fi-flag"></i>' . $alert . '</span>';
		$menu_link     = trailingslashit( bp_loggedin_user_domain() . bp_get_notifications_slug() );
		$notification_rnd_id = 9999991234;
		
		$notify_menu_item = array (
			'title'					=> $menu_title,
			'menu_item_parent'		=> 0,
			'ID'					=> $notification_rnd_id,
			'db_id'					=> $notification_rnd_id,
			'url'					=> $menu_link,
			'current'				=> false,
			'current_item_ancestor'	=> false,
			'classes'				=> array('menu-item no-caret'),
			'object'				=> 'custom',
			'attr_title'			=> __('Notifications', 'chesnet')
		);
		//var_dump($sorted_menu_items);
		$sorted_menu_items[] = (object) $notify_menu_item;
	
		if ( !empty( $notifications ) ) {
			foreach ( (array) $notifications as $notification ) {
				$sorted_menu_items[] = (object) array (
					'title'					=> $notification->content,
					'menu_item_parent'		=> $notification_rnd_id,
					'ID'					=> 'notification-' . $notification->id,
					'db_id'					=> '',
					'url'					=> $notification->href,
					'current'				=> false,
					'object'				=> 'custom',
					'current_item_ancestor'	=> false
				);
			}
		} else {
			$sorted_menu_items[] = (object) array (
				'title'					=> __( 'No new notifications', 'chesnet' ),
				'menu_item_parent'		=> $notification_rnd_id,
				'ID'					=> 'no-notifications',
				'db_id'					=> '',
				'url'					=> $menu_link,
				'current'				=> false,
				'object'				=> 'custom',
				'current_item_ancestor'	=> false
			);
		}
	}
	
	//Messages	
	if(bp_is_active( 'messages' )){
		$mcount    = bp_get_total_unread_messages_count();
		$malert    = (int) $mcount > 0 ? sprintf('<span class="round label">%d</span>', number_format_i18n($mcount)) : '';
		$mmenu_title    = '<span id="ab-messages"><span class="screen-reader-text">'.__('Messages', 'chesnet').'</span><i class="fi-comments"></i>' . $malert . '</span>';
		
		$messages = array (
			'title'					=> $mmenu_title,
			'menu_item_parent'		=> 0,
			'ID'					=> '',
			'db_id'					=> '',
			'url'					=> bp_loggedin_user_domain() . bp_get_messages_slug(),
			'current'				=> false,
			'current_item_ancestor'	=> false,
			'object'				=> 'custom',
			'attr_title'			=> __('Messages', 'chesnet')
		);
			
		$sorted_menu_items[] = (object) $messages;
	}

    return $sorted_menu_items;
}
?>
