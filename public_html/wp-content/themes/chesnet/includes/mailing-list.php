<?php

/* Chesapeake Network Mailing List (CNML) */
/* creates the table `mail_outgoing` to store pending outgoing emails
 * columns: 
 **
  * ID
	* sender_id (user ID of post author)	
	* date_recorded (timestamp of initial recording)
	* activity_id (ie trigger by what activity)
	* post_id
	* group_id (of corresponding post_id)
	* subject (post title)
	* email body (filtered post_content with additional meta data added)
	* check performed to send to individual group members (bool)
	* check performed to send to digest group members (bool)
 */
function cnml_create_db_tables(){
	global $wpdb;
	
	$charset_collate = $wpdb->get_charset_collate();
	
	$out_table_name = $wpdb->prefix . 'cn_mail_outgoing';
	$out_sql = "CREATE TABLE $out_table_name (
	  ID bigint(20) NOT NULL AUTO_INCREMENT,
	  sender_id bigint(20) NOT NULL,
	  date_recorded datetimes NOT NULL,
	  activity_id bigint(20) NOT NULL,
	  post_id bigint(20) NOT NULL,
	  group_id bigint(20) NOT NULL,
	  subject text NOT NULL,
	  email_body longtext NOT NULL,
	  check_ind_sub TINYINT(1) DEFAULT 0 NOT NULL,
	  check_dig_sub TINYINT(1) DEFAULT 0 NOT NULL,
	  UNIQUE KEY id (ID)
	) $charset_collate;";
	
	$sent_table_name = $wpdb->prefix . 'cn_mail_sent';
	$sent_sql = "CREATE TABLE $sent_table_name (
	  ID bigint(20) NOT NULL AUTO_INCREMENT,
	  sender_id bigint(20) NOT NULL,
	  date_recorded datetimes NOT NULL,
	  activity_id bigint(20) NOT NULL,
	  post_id bigint(20) NOT NULL,
	  group_id bigint(20) NOT NULL,
	  subject text NOT NULL,
	  email_body longtext NOT NULL,
	  check_ind_sub TINYINT(1) DEFAULT 0 NOT NULL,
	  check_dig_sub TINYINT(1) DEFAULT 0 NOT NULL,
	  UNIQUE KEY id (ID)
	) $charset_collate;";
	
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $out_sql );
	dbDelta( $sent_sql );
	
	
}
add_action("after_switch_theme", "cnml_create_db_tables");




?>