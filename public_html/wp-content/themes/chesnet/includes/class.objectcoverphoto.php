<?php

/*
 * Adds the ability to change the cover photo for groups and profiles
 * @todo use native cover photo component built into buddypress (since buddypress 2.4)
 * @todo Make a BP component (add step for group/profile creation)
 * maybe thsi wil help: https://codex.buddypress.org/plugindev/how-to-edit-group-meta-tutorial/
 */
 
function ocp_can_user_access_group(){
	if(current_user_can( 'bp_moderate' ) || is_super_admin() || bp_is_item_admin()) return true;
}

function ocp_can_user_access_profile(){
	return (bp_is_my_profile() || is_super_admin());
}

class ObjectCoverPhoto {
	
	function __construct() {
		//setup nav
		add_action( 'bp_xprofile_setup_nav', array( $this, 'setup_nav' ) );
		add_action( 'bp_groups_setup_nav', array( $this, 'setup_nav' ) );
		
		//add css for background change
		add_action( 'wp_head', array( $this, 'inject_css' ) );
		
		//add_action( 'wp_print_scripts', array( $this, 'inject_js' ) );
		add_action( 'wp_ajax_cncp_delete_cover', array( $this, 'ajax_delete_cover_photo' ) );
	}
		
	function setup_nav(){
		global $bp;
		$access = false;
		if(!bp_is_user() && !bp_is_group()) return false;
		if(bp_is_user()) {
			// Determine user to use
			if ( bp_displayed_user_domain() ) {
				$user_domain = bp_displayed_user_domain();
			} elseif ( bp_loggedin_user_domain() ) {
				$user_domain = bp_loggedin_user_domain();
			} else {
				return;
			}
			$access = ( bp_is_my_profile() || is_super_admin() );
			$parent_url = trailingslashit( $user_domain . $bp->profile->slug );
			$no_access_url = $parent_url;
			$parent_slug = $bp->profile->slug;
		}
		if(bp_is_group()) {
			$group_permalink = bp_get_group_permalink( groups_get_current_group() );

			$access = ocp_can_user_access_group();
			$parent_slug = bp_get_current_group_slug();
			$parent_url = $no_access_url = $group_permalink;
		}
		
		bp_core_new_subnav_item( 
			array( 
				'name' => _x( 'Change Cover Photo', 'Object header sub menu', 'chesnet' ),
				'slug' => 'change-cover',
				'parent_url' => $parent_url,
				'parent_slug' => $parent_slug, 
				'screen_function' =>array( $this, 'screen_change_cover' ),
				'user_has_access'   => $access,
				'position' => 40,
				'no_access_url'  => $no_access_url
			)
		);
			
	}
	
	function screen_change_cover(){
		global $bp;
		//if the form was submitted, update here
		if( isset($_POST['cncp_itemID']) ){
			$valid = false;
			
			$itemID = $_POST['cncp_itemID'];
			$type = $_POST['cncp_type'];
			if($type == ($bp->groups->slug || $bp->profile->slug)){
				if($type == $bp->profile->slug){
					//make sure profile id exists
					if($user_id = get_user_by( 'id', $itemID )) $valid = true;	
				}
				else{
					
					if(groups_get_group(array('group_id' => $itemID))) $valid = true;
				}
			}
			
			if(!wp_verify_nonce( $_POST['_wpnonce'], 'bp_upload_cover_photo' )) $valid = false;

			if($valid){
				if(isset($_POST['cncp_del_image'])){
					if($this->delete_cover_for_item($itemID, $type))
						bp_core_add_message( __('Cover photo deleted.', 'chesnet') );
					else bp_core_add_message( __('Unable to delete cover photo.', 'chesnet'), 'error' );
				}
				if(!empty( $_POST['cncp_save_submit'] ) ){
					if( $this->handle_upload($itemID, $type, $_FILES))
						bp_core_add_message( __('Cover photo uploaded successfully!', 'chesnet') );
				}
			}
		}
		
		//hook the content
		add_action( 'bp_template_title', array( $this,'bp_template_title' ));
		add_action( 'bp_template_content',array( $this, 'bp_template_content') );
		bp_core_load_template( apply_filters( 'bp_core_template_plugin', 'members/single/plugins' ) );
		
	}
	
	//Change Background Page title
	function bp_template_title(){
		echo '<h3>'.__('Cover Photo', 'chesnet').'</h3>';
	}
	
	//Upload page content
	function bp_template_content(){
		global $bp;
		if(bp_is_user()){
			$itemID = $bp->displayed_user->id;
			$type = $bp->profile->slug;
		}
		elseif(bp_is_group()){
			$itemID = bp_get_current_group_id();
			$type = $bp->groups->slug;
		}
		else return;
		?>
		<form name="cncp_change" id="cncp_change" method="post" class="standard-form" enctype="multipart/form-data">
			
			<?php $image_url = cncp_get_image();
			if(!empty($image_url)):?>
                <input type="submit" id="cncp_del_image" name="cncp_del_image" class="button tiny" value="<?php _e('Delete','chesnet') ?>" />
                <div id="bg-delete-wrapper">
                    <div class="current-bg">
                    	<img src="<?php echo $image_url;?>" alt="current background" />
                    </div>
               </div>
		   <?php endif;?> 
           
			<p><?php _e( 'Click below to select a JPG, GIF or PNG format photo from your computer and then click \'Upload Image\' to proceed.', 'chesnet' ); ?></p>
			<label for="cncp_upload">
                <input type="file" name="file" id="cncp_upload" class="settings-input" />
            </label>
			
			<?php wp_nonce_field("bp_upload_cover_photo");?>
            <input type="hidden" name="cncp_type" id="cncp_type" value="<?php echo $type; ?>" />
            <input type="hidden" name="cncp_itemID" id="cncp_itemID" value="<?php echo $itemID; ?>" />
            <input type="hidden" name="action" id="action" value="bp_upload_cover_photo" />
            <p class="submit"><input type="submit" id="cncp_save_submit" name="cncp_save_submit" class="button" value="<?php _e('Save','chesnet') ?>" /></p>
		</form>
		<?php
	}
	
	//handles upload, a modified version of bp_core_avatar_handle_upload(from bp-core/bp-core-avatars.php)
	function handle_upload($itemID, $type, $file) {
		global $bp, $chesnet;
	
		//include core files
		require_once( ABSPATH . '/wp-admin/includes/file.php' );
		$max_upload_size = $this->get_max_upload_size();
		$max_upload_size = $max_upload_size*1024;//convert kb to bytes
		if(!$file) $file = $_FILES;
	
		$uploadErrors = $chesnet->error_codes;
	
		if ( isset($file['error']) && $file['error']) {
			bp_core_add_message( sprintf( __( 'Your upload failed, please try again. Error was: %s', 'chesnet' ), $uploadErrors[$file['file']['error']] ), 'error' );
			return false;
		}
	
		if ( ! ($file['file']['size']<$max_upload_size) ) {
			bp_core_add_message( sprintf( __( 'The file you uploaded is too big. Please upload a file under %s', 'chesnet'), size_format($max_upload_size) ), 'error' );
			return false;
		}
	
		if ( ( !empty( $file['file']['type'] ) && !preg_match('/(jpe?g|gif|png)$/i', $file['file']['type'] ) ) || !preg_match( '/(jpe?g|gif|png)$/i', $file['file']['name'] ) ) {
			bp_core_add_message( __( 'Please upload only JPG, GIF or PNG photos.', 'chesnet' ), 'error' );
			return false;
		}
		
		// @todo should these photos go into "uploads/cover-photos/users" / "uploads/cover-photos/groups"
		$uploaded_file = wp_handle_upload( $file['file'], array( 'action'=> 'bp_upload_cover_photo' ) );
		
		//if file was not uploaded correctly
		if ( !empty($uploaded_file['error'] ) ) {
			bp_core_add_message( sprintf( __( 'Upload Failed! Error was: %s', 'chesnet' ), $uploaded_file['error'] ), 'error' );
			return false;
		}
	
		//assume that the file uploaded succesfully
		//delete any previous uploaded image
		self::delete_cover_for_item($itemID, $type);
		
		//save in meta
		if($type == $bp->profile->slug){
			bp_update_user_meta( $itemID, 'cover_photo', $uploaded_file['url'] );
			bp_update_user_meta( $itemID, 'cover_photo_file_path', str_replace('\\', '/', $uploaded_file['file']) );
		}
		else {
			groups_update_groupmeta( $itemID, 'cover_photo', $uploaded_file['url'] );
			groups_update_groupmeta( $itemID, 'cover_photo_file_path', str_replace('\\', '/', $uploaded_file['file']) );
		}
	
		do_action( 'cncp_cover_uploaded', $uploaded_file['url'] ); //allow to do some other actions when a new background is uploaded
		return true;
	}

	//get the allowed upload size
	//there is no setting on single wp, on multisite, there is a setting, we will adhere to both
	function get_max_upload_size(){
		$max_file_sizein_kb = get_site_option( 'fileupload_maxk' ); //it wil be empty for standard wordpress
		
		
		if( empty( $max_file_sizein_kb ) ){//check for the server limit since we are on single wp
			$max_upload_size = (int)(ini_get('upload_max_filesize'));
			$max_post_size = (int)(ini_get('post_max_size'));
			$memory_limit = (int)(ini_get('memory_limit'));
			$max_file_sizein_mb = min( $max_upload_size, $max_post_size, $memory_limit );
			$max_file_sizein_kb =$max_file_sizein_mb*1024;//convert mb to kb
		}
		return apply_filters( 'cn_max_upload_size', $max_file_sizein_kb );
	}
	
	//inject css
	function inject_css(){
		if(bp_is_user() || bp_is_group()){
			$image_url = cncp_get_image();
			if(empty($image_url) || apply_filters( 'cncp_iwilldo_it_myself', false ) ) return;
			?>
			<style type="text/css">
				.page-banner#<?php echo bp_is_user() ? 'user-page-banner' : 'group-page-banner'; ?>{
					background-image: url(<?php echo $image_url;?>);
				}
			</style>  
		<?php
		}
	
	}
	
	//ajax delete the existing image
	function ajax_delete_cover_photo(){
		//validate nonce
		if( !wp_verify_nonce($_POST['_wpnonce'], "bp_upload_cover_photo") ) die('what!');
		
		self::delete_bg_for_user();
		$message='<p>'.__('Cover photo deleted successfully!', 'chesnet').'</p>';//feedback but we don't do anything with it yet, should we do something
		echo $message;
		exit(0);
	}
	
	//reuse it
	function delete_cover_for_item($item_id, $type){
		global $bp;
		//delete the associated image and send a message
		if($type == $bp->profile->slug){
			$old_file_path = get_user_meta( $item_id, 'cover_photo_file_path', true );

			bp_delete_user_meta( $item_id, 'cover_photo_file_path' );
			bp_delete_user_meta( $item_id, 'cover_photo' );
		}
		else{
			$old_file_path = groups_get_groupmeta( $item_id, 'cover_photo_file_path', true );

			groups_delete_groupmeta( $item_id, 'cover_photo_file_path' );
			groups_delete_groupmeta( $item_id, 'cover_photo' );
		}
		
		if( $old_file_path ) {
			if( @unlink ( $old_file_path ) ) return true; //remove old files with each new upload
		}
		return false;
	}
	
}

//only load if bp is active
if(function_exists('bp_is_active')) $_cncp = new ObjectCoverPhoto();

function cncp_get_image( $type = false, $item_id = false ){
    global $bp;
	$image_url = null;
	if(!$type){
		$type = bp_is_user() ? $bp->profile->slug : $bp->groups->slug;
	}
	
	if($type == $bp->profile->slug){
		if(!$item_id) $item_id = bp_displayed_user_id();
		if( empty( $item_id ) ) return false;
    	$image_url = bp_get_user_meta( $item_id, 'cover_photo', true );
	}
	else { //group
		if(!$item_id) $item_id = $bp->groups->current_group->id;
		if( empty( $item_id ) ) return false;
		$image_url = groups_get_groupmeta( $item_id, 'cover_photo', true );
	}
	return apply_filters( 'cncp_get_image', $image_url, $type, $item_id );
}