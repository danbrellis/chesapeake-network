<?php

//mark notification as seen
function cn_mod_announcements_mark_notifications() {
	if ( isset($_GET['action']) && 'cn_pending_seen' == $_GET['action'] && isset( $_GET['gid'] ) ){

		// Get required data
		$user_id  = bp_loggedin_user_id();
		$group_id = intval( $_GET['gid'] );
		$error = false;
		// Check nonce
		if ( !wp_verify_nonce( $_REQUEST['_wpnonce'], 'cn_mark_pending_seen_' . $group_id ) ) {
			bp_core_add_message( __( '<strong>ERROR</strong>: Are you sure you wanted to do that?', 'chesnet' ), 'error' );
			$error = true;
	
		// Check current user's ability to edit the user
		} elseif ( !current_user_can( 'edit_user', $user_id ) ) {
			bp_core_add_message( __( '<strong>ERROR</strong>: You do not have permission to mark notifications for that user.', 'chesnet' ), 'error' );
			$error = true;
		}
	
		// Bail if we have errors
		if ( $error === false ) {
	
			// Attempt to clear notifications for the current user from this topic
			$success = bp_notifications_mark_notifications_by_item_id( $user_id, $group_id, cn_mod_get_component_name(), cn_mod_posts_notification_action($group_id) );
		}
	
		// Redirect to the topic
		//$redirect = bp_get_group_admin_permalink( $group ) . 'announcement-moderation';
	
		// Redirect
		//wp_safe_redirect( $redirect );
		
		// For good measure
		//exit();
		
	}
}
add_action( 'init', 'cn_mod_announcements_mark_notifications' );

function cn_mod_get_group_approvers($group_id){
	$approvers_ids = array();
	$admins = groups_get_group_admins($group_id);
	$mods = groups_get_group_mods($group_id);
	
	$approvers = array_merge($admins, $mods);
	foreach($approvers as $approver) $approvers_ids[] = $approver->user_id;
	return array_unique($approvers_ids);
}

function cn_mod_user_can_approve($user_id, $group_id = false, $post_id = false){
	global $bp;
	
	if(!$group_id){
		if(!$post_id) $group_id = $bp->groups->current_group->id;
		else {
			global $chesnet;
			$group_id = $chesnet->cn_get_gid_from_post_id($post_id);
		}
	}
	$approvers = cn_mod_get_group_approvers($group_id);
	return in_array($user_id, $approvers);
}

function cn_get_group_pending_announcement_approve_link($post_id){
	global $chesnet;
	$gid = $chesnet->cn_get_gid_from_post_id($post_id);
	$group = groups_get_group(array('group_id' => $gid));
	return apply_filters( 'cn_get_group_pending_announcement_approve_link', wp_nonce_url( bp_get_group_admin_permalink($group) . 'announcement-moderation/approve/' . $post_id, 'groups_accept_pending_announcement' ) );
}

function cn_get_group_pending_announcement_reject_link($post_id){
	global $chesnet;
	$gid = $chesnet->cn_get_gid_from_post_id($post_id);
	$group = groups_get_group(array('group_id' => $gid));
	return apply_filters( 'cn_get_group_pending_announcement_reject_link', wp_nonce_url( bp_get_group_admin_permalink($group) . 'announcement-moderation/reject/' . $post_id, 'groups_reject_pending_announcement' ) );
}

function cn_mod_posts_notification_action($group_id){
	//group_id needed so notifications aren't grouped if the pending announcements are in diff groups
	return 'new_pending_announcement_' . $group_id;
}

function cn_mod_get_component_name(){
	return 'cn_announcements';
}

/**
 * The class_exists() check is recommended, to prevent problems during upgrade
 * or when the Groups component is disabled
 */
if ( class_exists( 'BP_Group_Extension' ) ) :
 
class CN_Group_Moderation extends BP_Group_Extension {
	
	var $settings;
	var $slug = 'announcement-moderation';
	var $tab_name;
	var $setting_name = 'cn_moderate_new_announcements';
	var $moderate = false;

	var $visibility;
	var $enable_nav_item;

	// This is so I can get a reliable group id even during group creation
	var $maybe_group_id;

	/**
	 * Constructor
	 *
	 * @package BuddyPress Docs
	 * @since 1.0-beta
	 */
	function __construct() {
		global $bp;

		$this->tab_name = 'Moderate';
		
		if ( !empty( $bp->groups->current_group->id ) )
			$this->maybe_group_id	= $bp->groups->current_group->id;
		else if ( !empty( $bp->groups->new_group_id ) )
			$this->maybe_group_id	= $bp->groups->new_group_id;
		else
			$this->maybe_group_id	= false;

		// Load the setting for the group, for easy access
		$this->moderate	= groups_get_groupmeta( $this->maybe_group_id, $this->setting_name ) == 'yes' ? true : false;

		$args = array(
			'slug'				=> $this->slug,
			'name'				=> $this->tab_name,
			'show_tab'			=> 'noone',
			'enable_nav_item'	=> false,
			'nav_item_name'		=> $this->cn_resources_tab_name,
			'nav_item_position'	=> 41
		);
		
		if($this->moderate == 'yes') parent::init( $args );
		
		//settings
		add_action( 'groups_group_after_save', array($this, 'groups_group_after_save') );
		add_action( 'bp_after_group_settings_creation_step', array($this, 'bp_group_settings') );
		add_action( 'bp_after_group_settings_admin', array($this, 'bp_group_settings') );
		
		//approve/reject
		add_action( 'cn_before_post_entry_meta', array($this, 'before_post_entry_meta') );
		add_action( 'bbp_template_before_replies_loop', array($this, 'before_post_entry_meta') );
		add_action( 'bp_screens', array($this, 'groups_screen_group_admin_moderations') );
		//add_action( 'after_groups_admin_announcement_moderation_approved', 'cn_after_groups_admin_announcement_moderation_approved', 10, 2 );
		add_filter( 'user_has_cap', array($this, 'show_pending_to_mods'), 10, 4 );
		
		//notification
		add_filter( 'bp_notifications_get_registered_components', array($this, 'bp_notifications_get_registered_components'), 10 );
		add_filter( 'bp_notifications_get_notifications_for_user', array($this, 'bp_notifications_get_notifications_for_user'), 10, 5 );
		add_action( 'added_post_meta', array($this, 'notify_group_moderators_pending_announcement'), 10, 4 );
		
		//bbpress weirdness
		add_filter( 'bbp_new_topic_pre_insert', array($this, 'bbp_new_topic_pre_insert'), 10, 1 );
		add_filter( 'bbp_after_has_topics_parse_args', array($this, 'bbp_add_pending_status_to_query_args'), 10, 1 );
		add_filter( 'bbp_after_has_replies_parse_args', array($this, 'bbp_add_pending_status_to_query_args'), 10, 1 );

	}
	
	function has_moderation($group_id = null){
		if(!$group_id) $group_id = $this->maybe_group_id;
		
		return groups_get_groupmeta( $group_id, $this->setting_name ) == 'yes' ? true : false;
	}
	 
	/**
	 * Determines what shows up on the Resources panel of the Group Admin
	 *
	 * @package BuddyPress Docs
	 * @since 1.0-beta
	 */
	function edit_screen( $group_id = null ) {
		if ( !bp_is_group_admin_screen( $this->slug ) )
			return false;

		$this->admin_markup($group_id);

		//need to do this so that bp doesn't add it's own... annoying
		echo '<input type="submit" class="hide" />';
	}

	/**
	 * Runs when the admin panel is saved
	 *
	 * @package BuddyPress Docs
	 * @since 1.0-beta
	 */
	function edit_screen_save( $group_id = null ) {
		return false;
	}
	
	/**
	 * Admin markup used on the edit and create admin panels
	 *
	 * @package BuddyPress Docs
	 * @since 1.0-beta
	 */
    function admin_markup( $group_id ){
		global $chesnet;
		
		$cat = $chesnet->cn_get_cat_from_group($group_id);
		$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
		$args = array(
			'cat'			=> $cat->term_id,
			'post_status'	=> 'pending',
			'post_type'		=> $chesnet->user_can_add,
			'posts_per_page'	=> 10,
			'paged'				=> $paged,
			'suppress_filters'	=> true
		);
		$pendingposts = new WP_Query( $args );
		do_action( 'bp_before_group_announcement_moderation_admin' ); ?>

		<div class="requests" id="pending-announcements-list">

			<?php include(locate_template( 'buddypress/groups/single/moderation-loop.php' )); ?>

		</div>

		<?php do_action( 'bp_before_group_announcement_moderation_admin' );
    }
	 	
	/**
	 * Markup for the frontend page
	 *
	 * @package ChesNet
	 * @since 1.0-beta
	 */
    function display($group_id = null) {
		return false;
    }
	
	function before_post_entry_meta($post){
		global $chesnet, $bp, $post;
		if($post->post_status !== 'pending') return; ?>
        
        <div data-alert class="alert-box warning"><?php _e('This message is awaiting moderation and is therefore only visible to the author or group moderators.', 'chesnet'); ?><a href="#" class="close hide-if-no-js">&times;</a></div>
        
        <?php //add approve/reject links
        $group_id = $chesnet->cn_get_gid_from_post_id($post->ID);
		$group = groups_get_group(array('group_id' => $group_id));
		if(empty($group)) return;
	
		$allowed_ids = array();
		$admins = groups_get_group_admins($group_id);
		$mods = groups_get_group_mods($group_id);
		$approvers = array_merge($admins, $mods);
		
		foreach($approvers as $approver) $allowed_ids[] = $approver->user_id;
		$allowed_ids = array_unique($allowed_ids);
		
		if(in_array($bp->loggedin_user->id, $allowed_ids)): ?>
			<ul class="inline-list right">
				<?php bp_button( array(
					'id' => 'approve-announcement-btn',
					'component' => 'groups', 
					'wrapper' => 'li', 
					'link_href' => cn_get_group_pending_announcement_approve_link($post->ID), 
					'link_class' => 'button', 
					'link_text' => __('Approve', 'chesnet')
				) ); ?>
				<?php bp_button( array(
					'id' => 'reject-announcement-btn',
					'component' => 'groups', 
					'wrapper' => 'li', 
					'link_href' => cn_get_group_pending_announcement_reject_link($post->ID), 
					'link_class' => 'button alert', 
					'link_text' => __('Reject', 'chesnet')
				) ); ?>
			</ul>
		<?php endif;
	}
	
	/**
	 * Handle the display of Admin > Membership Requests.
	 */
	function groups_screen_group_admin_moderations() {
		global $chesnet;
		$bp = buddypress();
	
		if ( $this->slug != bp_get_group_current_admin_tab() ) {
			return false;
		}
	
		if ( !bp_is_item_admin() || !$this->moderate ) {
			return false;
		}
		
		$group_id = $bp->groups->current_group->id;
		
		//since the user is on the mod admin page, mark any 'unseen' notifications as read
		bp_notifications_mark_notifications_by_item_id( bp_loggedin_user_id(), $group_id, cn_mod_get_component_name(), cn_mod_posts_notification_action($group_id) );
		
		$pending_action = (string) bp_action_variable( 1 );
		$post_id  = (int) bp_action_variable( 2 );

		if ( !empty( $pending_action ) && !empty( $post_id ) ) {
			if($pending_action == 'approve' || $pending_action == 'reject'){
				$post = get_post($post_id);
				if ( 'approve' == $pending_action && $post ) {
					// Check the nonce first.
					if ( !check_admin_referer( 'groups_accept_pending_announcement' ) ) return false;
		
					// Accept the pending announcement
					$post_args = array(
						'ID' => $post_id,
						'post_status' => 'publish'
					);
					
					if ( !wp_update_post( $post_args ) )
						bp_core_add_message( __( 'There was an error accepting the pending announcement. Please try again.', 'chesnet' ), 'error' );
					else {
						bp_core_add_message( __( 'The announcement was successully approved.', 'chesnet' ) );
						do_action( 'after_groups_admin_announcement_moderation_approved', $bp->groups->current_group->id, $post_id );
					}
		
				} elseif ( 'reject' == $pending_action && $post ) {
					/* Check the nonce first. */
					if ( !check_admin_referer( 'groups_reject_pending_announcement' ) )
						return false;
		
					// Reject the membership request
					$cat = $chesnet->cn_get_cat_from_group($bp->groups->current_group->id);
					
					if ( !wp_remove_object_terms( $post_id, (int)$cat->term_id, 'category' ) )
						bp_core_add_message( __( 'There was an error rejecting the pending announcement. Please try again.', 'chesnet' ), 'error' );
					else {
						bp_core_add_message( __( 'The announcement was successully rejected.', 'chesnet' ) );
						do_action( 'after_groups_admin_announcement_moderation_rejected', $bp->groups->current_group->id, $post_id );
					}
				}
			
				bp_core_redirect( trailingslashit(bp_get_group_admin_permalink( $bp->groups->current_group ) . $this->slug ));
			}
		}
		
		bp_core_load_template( apply_filters( 'groups_template_group_admin_moderation', 'groups/single/home' ) );
	}
	
	//temporarily grant editor priveledges to group admins & mods when viewing pending post in their respective group
	function show_pending_to_mods($allcaps, $cap, $args, $wp_user){
		// Bail out if we're not asking about a post:
		if ( 'edit_post' != $args[0] ) return $allcaps;
		
		// Bail out for users who can already edit others posts:
		if ( isset($allcaps['edit_others_posts']) ) return $allcaps;
		
		// Bail out for users who can't publish posts:
		if ( !isset( $allcaps['publish_posts'] ) || !$allcaps['publish_posts'] ) return $allcaps;
		
		// Load the post data:
		$post = get_post( $args[2] );
		
		// Bail out if the user is the post author:
		if ( $args[1] == $post->post_author ) return $allcaps;
		
		// Bail out if the post isn't pending or published:
		if ( ( 'pending' != $post->post_status ) ) return $allcaps;
		
		global $chesnet;
		
		$group = $chesnet->cn_get_post_group($post);
		if(empty($group)) return $allcaps;
		
		if($this->has_moderation($group->id)) {
			$approver_ids = cn_mod_get_group_approvers($group->id);
			if(in_array($args[1], $approver_ids)) $allcaps[$cap[0]] = true;
		}
		
		return $allcaps;
	}
	
	//add group setting if new posts ina group should be moderated
	function bp_group_settings(){
		global $wpdb, $current_site, $groups_template;

		$moderate = $this->moderate ? 'yes' : 'no';
	
		?>
		<h4><?php _e('Announcement Moderation', 'chesnet') ?></h4>
		<p><?php _e('When a new post, event, question, resource or job announcement is added to the group, should it be held in review by a group moderator before being published?', 'chesnet'); ?><br />
        <strong class="small"><?php _e('Note: This enabling this option requires action from the group moderator(s) or administrator(s) for each item added to this group.', 'chesnet'); ?></strong></p>
		<div class="row">
			<div class="large-14 columns">
				<ul class="no-bullet">
                	<li><input value="no" name="cn-moderate-new-announcements" id="cn-moderate-new-announcements-no" type="radio" <?php checked( $moderate, 'no' ); ?> /><label for="cn-moderate-new-announcements-no"><?php _e('Not moderated.', 'chesnet') ?></label></li>
                    <li><input value="yes" name="cn-moderate-new-announcements" id="cn-moderate-new-announcements-yes" type="radio" <?php checked( $moderate, 'yes' ); ?> /><label for="cn-moderate-new-announcements-yes"><?php _e('Moderator approval required.', 'chesnet') ?></label></li>
                </ul>
			</div>
		</div>
		<hr />
		<?php
	}
	
	//save group setting
	function groups_group_after_save($group){
		global $wpdb;

		if ( isset($_POST['cn-moderate-new-announcements']) ) {
			groups_update_groupmeta( $group->id, $this->setting_name, $_POST['cn-moderate-new-announcements']);
		}
	}
	
	//hacky way to add component to  list of registered components
	function bp_notifications_get_registered_components($component_names){
		// Force $component_names to be an array
		if ( ! is_array( $component_names ) ) {
			$component_names = array();
		}
	
		array_push( $component_names, cn_mod_get_component_name() );
	
		// Return component's with 'forums' appended
		return $component_names;
	}
	
	//format notification
	function bp_notifications_get_notifications_for_user($action, $item_id, $secondary_item_id, $total_items, $format = 'string'){
		// New reply notifications
		if ( cn_mod_posts_notification_action($item_id) === $action ) {
			$group_id = $item_id;
			$group = groups_get_group(array('group_id' => $group_id));
			$p = get_post($secondary_item_id);
			$title_attr  = sprintf(__( 'Pending announcements in %s awaiting moderation', 'chesnet' ), $group->name);
			$anchor = '#pending-announcements-list';
			if ( (int) $total_items > 1 ) {
				
				$text   = sprintf( __( '%d pending announcements in %s', 'chesnet' ), (int)$total_items, $group->name );
				$filter = 'cn_mod_format_notification_multiple_pending_announcement';
			} else {
				$text = sprintf( __( '%d pending announcement in %s', 'chesnet' ), (int)$total_items, $group->name );
				
				if ( !empty( $p ) ) {
					$title_attr = sprintf( __( '%d pending announcement in %2$s from %3$s', 'chesnet' ), (int) $total_items, $group->name, bp_core_get_user_displayname( $p->post_author ) );
					$anchor = '#' . $p->ID;
				} else {
					
				}
				$filter = 'cn_mod_format_notification_single_pending_announcement';
			}
			$url = bp_get_group_admin_permalink( $group ) . $this->slug . $anchor;
			$link = wp_nonce_url( add_query_arg( array( 'action' => 'cn_pending_seen', 'gid' => $group_id ), $url ), 'cn_mark_pending_seen_' . $group_id );
	
			// WordPress Toolbar
			if ( 'string' === $format ) {
				$return = apply_filters( $filter, '<a href="' . esc_url( $link ) . '" title="' . esc_attr( $title_attr ) . '">' . esc_html( $text ) . '</a>', (int) $total_items, $text, $link, $group_id );
	
			// Deprecated BuddyBar
			} else {
				$return = apply_filters( $filter, array(
					'text' => $text,
					'link' => $link
				), $link, (int) $total_items, $text, $group_id );
			}
	
			do_action( 'cn_mod_format_buddypress_notifications', $action, $item_id, $secondary_item_id, $total_items );
	
			return $return;
		}
		return $action;
	}
		
	function notify_group_moderators_pending_announcement($mid, $id, $meta_key, $meta_value){
		//email moderator when new post is added and needs approval
		if($meta_key == '_cn_post_sharing' && $meta_value == 'yes'){
			global $chesnet;
			
			$group_id = $chesnet->cn_get_gid_from_post_id($id);
			
			$group = groups_get_group(array('group_id' => $group_id));
			if(empty($group)) return;
			
			if($this->has_moderation($group_id)) {
				$to_ids = array();
				$admins = groups_get_group_admins($group_id);
				$mods = groups_get_group_mods($group_id);
				
				$p = get_post($id);
				if($p->post_status !== 'pending') return;
				
				$approvers = array_merge($admins, $mods);
				foreach($approvers as $approver) $to_ids[] = $approver->user_id;
				$to_ids = array_unique($to_ids);

				//email
				$to = $headers = array();
				foreach($to_ids as $to_id){
					$to[] = bp_core_get_user_email($to_id);
					
					//add notification for each user as well
					$this->groups_new_pending_announcement_notification($to_id, $p, $group_id);
				}
				if(empty($to)) return;
				
				$subject = sprintf(__('[%s] MODERATE: %s', 'chesnet'), get_bloginfo('name'), $p->post_title);
				
				//create accept and reject email forwards
				$accept = 'accept-'.$p->ID.'@chesapeakenetwork.org';
				$reject = 'reject-'.$p->ID.'@chesapeakenetwork.org';
				$sock = $chesnet->open_DA_socket();
				$chesnet->add_email_alias($sock, $accept);
				$chesnet->add_email_alias($sock, $reject);
				
				ob_start();
				include(locate_template( 'views/email/pending-moderation-notification.php' ));
				$message = ob_get_clean();
				//var_dump($message); exit();
				$headers[] = 'From: notifications@chesapeakenetwork.org';
				$headers[] = 'Reply-to: ' . $accept;
				$headers[] = 'Content-Type: text/html; charset=UTF-8';
				
				return wp_mail( $to, $subject, $message, $headers );
			}
		}
	}
	
	//add notification
	function groups_new_pending_announcement_notification( $receiver_user_id, $p, $group_id ) {
		if ( bp_is_active( 'notifications' ) ) {
			bp_notifications_add_notification( array(
				'user_id'           => $receiver_user_id,
				'item_id'           => $group_id, //group id
				'secondary_item_id' => $p->ID, //post id
				'component_name'    => 'cn_announcements',
				'component_action'  => cn_mod_posts_notification_action($group_id),
				'date_notified'     => bp_core_current_time(false)
			) );
		}
	}
	
	//filter post_status of new topics (over rides multistep post form)
	function bbp_new_topic_pre_insert($args){
		if($args['post_parent']) {
			$forum_id = $args['post_parent'];
			
			$group_ids = bbp_get_forum_group_ids( $forum_id );
			if(!$group_ids || !isset($group_ids[0]) || empty($group_ids)) return $args;
			$group_id = $group_ids[0];
			$args['post_status'] = $this->has_moderation($group_id) ? 'pending' : 'publish';
		}
		return $args;
	}
	
	//add pending status to bbp_topic query
	function bbp_add_pending_status_to_query_args($args){
		if(isset($args['post_status']) && bbp_get_view_all() && cn_mod_user_can_approve(bp_loggedin_user_id()))
			$args['post_status'] = $args['post_status'] . ',' . bbp_get_pending_status_id();
			
		return $args;
		
	}
	
}
bp_register_group_extension( 'CN_Group_Moderation' ); 

endif; // if ( class_exists( 'BP_Group_Extension' ) )

?>