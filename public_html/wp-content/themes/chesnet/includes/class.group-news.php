<?php

/*
 * Add the sub page to each group for the news/announcements
 */


/**
 * The class_exists() check is recommended, to prevent problems during upgrade
 * or when the Groups component is disabled
 */
if ( class_exists( 'BP_Group_Extension' ) ) :
 
class CN_News_Group_Extension extends BP_Group_Extension {
	
	var $slug = 'group-news';

	/**
	 * Constructor
	 *
	 * @package BuddyPress Docs
	 * @since 1.0-beta
	 */
	function __construct() {
		global $bp;

		$cn_news_tab_name = 'News';

		$args = array(
			'slug'				=> $this->slug,
			'name'				=> $cn_news_tab_name,
			'visibility'		=> 'public',
			'nav_item_position'	=> 11,
			'enable_nav_item'	=> true,
			'nav_item_name'		=> $cn_news_tab_name,
			'display_hook'		=> 'groups_custom_group_boxes',
			'screens'			=> array(
				'create' => array(
					'enabled' => false
				),
				'edit' => array(
					'enabled' => false
				),
				'admin' => array(
					'enabled' => false
				)
			)
		);
		
		parent::init( $args );
	}	
 	 	
	/**
	 * Markup for the frontend page
	 *
	 * @package BuddyPress Docs
	 * @since 1.0-beta
	 */
    function display() {
		global $chesnet;
		$group_cat = $chesnet->cn_cat_slug_from_gid($this->get_group_id());
		
		?>
        <p><a href="<?php echo $GLOBALS['chesnet']->cn_getUrl( 'add', null, null, 'post', null ); ?>">Add News</a></p>
		<?php
		
		//Query all posts in group category
		//Get pagination
		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		$args = array(
			'post_type'		=> 'post',
			'category_name'	=> $group_cat,
			'posts_per_page'=> 25,
			'paged'			=> $paged
		);
		$gposts = new WP_Query( $args ); ?>
        
        <?php if ( $gposts->have_posts() ) {
        
            //the loop
            while ( $gposts->have_posts() ){
				$gposts->the_post(); ?>
				<?php
                get_template_part( 'content', get_post_format() );
			} //end of the loop
        
            //pagination here
        	next_posts_link('&laquo; Older Entries', $gposts->max_num_pages);
			previous_posts_link('Newer Entries &raquo;');
			
            wp_reset_postdata();
			
		}
		else { ?>
            <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
        <?php
        }		
    }
}
bp_register_group_extension( 'CN_News_Group_Extension' );
 
endif; // if ( class_exists( 'BP_Group_Extension' ) )

?>