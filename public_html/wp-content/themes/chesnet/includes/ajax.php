<?php

$nopriv_actions = array();
$actions = array('load_my_activity_groups');

foreach($nopriv_actions as $nopriv_action){
	add_action( 'wp_ajax_nopriv_' . str_replace('_', '-', $nopriv_action), $nopriv_action );
}

foreach($actions as $action){
	add_action( 'wp_ajax_' . str_replace('_', '-', $action), $action );
}

//AJAX Functions
function load_my_activity_groups(){
	global $chesnet;
	$return = array();
	$return['type'] = 'error';
	
	$groups = $chesnet->cn_get_user_groups(array('order' => 'ASC'));
	
	if($groups && is_array($groups)){
		$return['type'] = 'success';
		$return['content'] = array();
		foreach($groups['groups'] as $group){
			//get group avatar
			$avatar_args = array(
				'item_id'	=> $group->id,
				'object'	=> 'group',
				'type'		=> 'thumb',
				'width'		=> 20,
				'height'	=> 20,
				'html'		=> true,
				'class'		=> 'alignleft imgWrapLeft'
			);
			$avatar = bp_core_fetch_avatar($avatar_args);
			$group_url = bp_get_group_permalink($group);
			$return['content'][$group->slug]['group_url'] = $group_url;
			$return['content'][$group->slug]['group_name'] = $group->name;
			$return['content'][$group->slug]['avatar'] = $avatar;
		}
		$return['content'] = $return['content'];
	}
	echo json_encode($return);
	die();
}
?>