<?php

/*
 * Add the sub page to each group for the resources
 */
 
//remove a dlm from a group via it's category
function bp_group_resources_remove_dlm_from_group($dlm_id, $group_id){
	global $bp, $chesnet;
	$dlm_pt = get_post_type_object( 'dlm_download' );
	//get cat for group id
	$group_cat = $chesnet->cn_cat_slug_from_gid($group_id);
	$result = wp_remove_object_terms( $dlm_id, $group_cat, 'category' );
	if(is_wp_error($result)){
		bp_core_add_message(
			sprintf(
				__('Unable to remove %s: %s', 'chesnet'),
				$dlm_pt->labels->singular_name,
				$result->get_error_message()
			),
			'error'
		);
		return false;	
	}
	elseif($result === false){
		bp_core_add_message(sprintf(__('Unable to remove resource.', 'chesnet'), $dlm_pt->labels->singular_name));
		return false;
	}
	else return true;
}

function get_manage_dlm_labels_url($dlm, $group = false){
	global $bp, $chesnet;
	if(is_int($dlm)) $dlm_id = $dlm;
	elseif ( is_a( $dlm, 'WP_Post' ) ) {
		$dlm_id = $dlm->ID;
	}
	else return false;
	
	//get dlm's group
	if(!$group) $group = $chesnet->cn_get_post_group($dlm);
	//http://localhost/chesnet/groups/chesapeake-network/group-library/edit/resource/140/labels
	$url = bp_get_group_permalink( $group ) . 'group-library';
	$url = $url . '/edit/resource/' . $dlm_id . '/labels/';
	return $url;
}

function cn_resources_get_label_link($label, $group){
		global $chesnet;
		$tax = $chesnet->get_dlm_tax($group->id);
		$ancestors = $ancestor_slugs = array();
		$ancestors = get_ancestors($label->term_id, $tax);
		$ancestors = array_reverse($ancestors);
		 if(!empty($ancestors) && is_array($ancestors)){
			foreach($ancestors as $ancestor){
				$a = get_term_by('id', $ancestor, $tax);
				$ancestor_slugs[] = $a->slug;
			}
		 }
		$ancestor_slugs[] = $label->slug;
		return trailingslashit(bp_get_group_permalink( $group ) . 'group-library') . implode('/', $ancestor_slugs);
	}

/**
 * The class_exists() check is recommended, to prevent problems during upgrade
 * or when the Groups component is disabled
 */
if ( class_exists( 'BP_Group_Extension' ) ) :
 
class CN_Resources_Group_Extension extends BP_Group_Extension {
	
	var $group_enable;
	var $settings;
	var $slug = 'group-library';
	var $gl_permalink;
	var $cn_resources_tab_name;
	var $display_screen;
	var $label_blacklist = array(
			'add-label',
			'delete-label',
			'edit-label',
			'edit',
			'resource',
			'page',
			'label'
		);
	var $group;
	var $dlm_post_type_obj;

	var $visibility;
	var $enable_nav_item;
	var $enable_create_step;

	// This is so I can get a reliable group id even during group creation
	var $maybe_group_id;
	
	var $label = false;
	var $dlm = false;

	/**
	 * Constructor
	 *
	 * @package BuddyPress Docs
	 * @since 1.0-beta
	 */
	function __construct() {
		global $bp;

		$this->cn_resources_tab_name = 'Resources';
		
		if ( !empty( $bp->groups->current_group->id ) )
			$this->maybe_group_id	= $bp->groups->current_group->id;
		else if ( !empty( $bp->groups->new_group_id ) )
			$this->maybe_group_id	= $bp->groups->new_group_id;
		else
			$this->maybe_group_id	= false;

		// Load the resources setting for the group, for easy access
		$this->settings			= groups_get_groupmeta( $this->maybe_group_id, 'cn_resources' );
		$this->group_enable		= !empty( $this->settings['group-enable'] ) ? true : false;

		$args = array(
			'slug'				=> $this->slug,
			'name'				=> $this->cn_resources_tab_name,
			'visibility'		=> 'public',
			'nav_item_position'	=> 45,
			'enable_nav_item'	=> $this->enable_nav_item(),
			'nav_item_name'		=> $this->cn_resources_tab_name,
			'display_hook'		=> 'groups_custom_group_boxes'
		);
		
		parent::init( $args );

		// Create some default settings if the create step is skipped
		if ( apply_filters( 'cn_resources_force_enable_at_group_creation', false ) ) {
			add_action( 'groups_created_group', array(&$this, 'enable_at_group_creation') );
		}
		$this->group = groups_get_group( array( 'group_id' => $this->get_group_id() ) );
		$this->gl_permalink = trailingslashit(bp_get_group_permalink( $this->group ) . $this->slug);
		add_filter( 'cn_possible_groups', array($this, 'cn_possible_groups'), 10, 2 );
		
		$this->golightly = groups_get_groupmeta($this->get_group_id(), 'golightly_gid');
		$this->library_transfered = groups_get_groupmeta($this->get_group_id(), 'resource_library_transfered');
		if($this->golightly && $this->library_transfered !== 'yes' && !is_super_admin()) return;
		if ( bp_is_current_action( $this->slug ) ) {
			add_action( 'cn_before_sidebar_single_group_left', array(&$this, 'single_group_sidebar_left') );
			add_action( 'wp_loaded', array(&$this, 'wp_loaded') );
		}
		

	}
	
	//used before download is added or edited to filter which groups a user can add the dlm to
	function cn_possible_groups($possible_groups, $data){
		if(!$this->dlm_post_type_obj)
			$this->dlm_post_type_obj = get_post_type_object( 'dlm_download' );

		if($data['post_type'] !== $this->dlm_post_type_obj->name) return $possible_groups;
		if(!isset($possible_groups['groups']) || !is_array($possible_groups['groups'])) return $possible_groups;
		
		//remove any groups due to permissions
		foreach($possible_groups['groups'] as $key => $group){
			$user_can = false;
			
			$group_settings = groups_get_groupmeta( $group->id, 'cn_resources' );
			if ( empty( $group_settings['can-create'] ) ) {
				$group_settings['can-create'] = 'member';
			}
			$user_id = bp_loggedin_user_id();
			
			if(!empty($group_settings['group-enable'])){
				switch ( $group_settings['can-create'] ) {
					case 'admin' :
						if ( groups_is_user_admin( $user_id, $group->id ) || is_super_admin())
							$user_can = true;
						break;
					case 'mod' :
						if ( groups_is_user_mod( $user_id, $group->id ) || groups_is_user_admin( $user_id, $group->id ) )
							$user_can = true;
						break;
					case 'member' :
					default :
						if ( groups_is_user_member( $user_id, $group->id ) )
							$user_can = true;
						break;
				}
			}
			if($user_can === false){ //remove group from array and update count
				unset($possible_groups['groups'][$key]);
			}
		}
		$possible_groups['total'] = count($possible_groups['groups']);
		
		return $possible_groups;	
	}
 
	/**
	 * Set some default settings for a group
	 *
	 * This function is only called if you're forcing Docs enabling on group creation
	 *
	 * @package BuddyPress_Docs
	 * @since 1.1.18
	 */
	function enable_at_group_creation( $group_id ) {
		$settings = apply_filters( 'cn_resources_default_group_settings', array(
			'group-enable'	=> 1,
			'can-create' 	=> 'member'
		) );

		groups_update_groupmeta( $group_id, 'cn_resources', $settings );
	}

	/**
	 * Determines what shows up on the Resources panel of the Create process
	 *
	 * @package BuddyPress Docs
	 * @since 1.0-beta
	 */
	function create_screen( $group_id = null ) {
		if ( !bp_is_group_creation_step( $this->slug ) )
			return false;

		$this->admin_markup($group_id);

		wp_nonce_field( 'groups_create_save_' . $this->slug );
	}

	/**
	 * Runs when the create screen is saved
	 *
	 * @package BuddyPress Docs
	 * @since 1.0-beta
	 */
	function create_screen_save( $group_id = null ) {
		global $bp;

		check_admin_referer( 'groups_create_save_' . $this->slug );

		$success = $this->settings_save( $bp->groups->new_group_id );
	}

	/**
	 * Determines what shows up on the Resources panel of the Group Admin
	 *
	 * @package BuddyPress Docs
	 * @since 1.0-beta
	 */
	function edit_screen( $group_id = null ) {
		if ( !bp_is_group_admin_screen( $this->slug ) )
			return false;

		$this->admin_markup($group_id);

		// On the edit screen, we have to provide a save button
		?>
		<p>
			<input type="submit" value="Save Changes" class="button" id="save" name="save" />
		</p>
		<?php

		wp_nonce_field( 'groups_edit_save_' . $this->slug );
	}

	/**
	 * Runs when the admin panel is saved
	 *
	 * @package BuddyPress Docs
	 * @since 1.0-beta
	 */
	function edit_screen_save( $group_id = null ) {
		global $bp;

		if ( !isset( $_POST['save'] ) )
			return false;

		check_admin_referer( 'groups_edit_save_' . $this->slug );

		$success = $this->settings_save();

		/* To post an error/success message to the screen, use the following */
		if ( !$success )
			bp_core_add_message( __( 'There was an error saving, please try again', 'buddypress' ), 'error' );
		else
			bp_core_add_message( __( 'Settings saved successfully', 'buddypress' ) );

		bp_core_redirect( bp_get_group_permalink( $bp->groups->current_group ) . 'admin/' . $this->slug );
	}
	
	/**
	 * Saves group settings. Called from edit_screen_save() and create_screen_save()
	 *
	 * @package BuddyPress Docs
	 * @since 1.0-beta
	 */
	function settings_save( $group_id = false ) {
		$success = false;

		if ( !$group_id )
			$group_id = $this->maybe_group_id;

		$settings = !empty( $_POST['cn-resources'] ) ? $_POST['cn-resources'] : array();

		if ( $this->settings == $settings ) {
			// No need to resave settings if they're the same
			$success = true;
		} else if ( groups_update_groupmeta( $group_id, 'cn_resources', $settings ) ) {
			$success = true;
		}

		return $success;
	}
	
	/**
	 * Admin markup used on the edit and create admin panels
	 *
	 * @package BuddyPress Docs
	 * @since 1.0-beta
	 */
    function admin_markup( $group_id ){
		$group_enable = empty( $this->settings['group-enable'] ) ? false : true;
		$can_create = empty( $this->settings['can-create'] ) ? 'member' : $this->settings['can-create'];

		?>

		<h3 class="hidden-for-small-up"><?php _e( 'Resource Library', 'chesnet' ) ?></h3>

		<p><?php _e('Use this file upload library to share and store spreadsheets, documents, photos and more. Select below settings for your group\'s library, including, folders and permissions.', 'chesnet'); ?></p>

		<p>
			 <input type="checkbox" name="cn-resources[group-enable]" id="cn-resources-group-enable" value="1" <?php checked( $group_enable, true ) ?> /><label for="cn-resources-group-enable"><?php _e('Enable Resource Library for this group.', 'chesnet'); ?></label>
             
		</p>
        
		<div id="cn-resources-options" <?php if ( !$group_enable ) : ?>class="hide"<?php endif; ?>>
			<h3><?php _e('Options', 'chesnet'); ?></h3>
            
            
            <div class="row">
                <div class="large-14 columns">
                    <label for="can-create"><?php _e('Minimum role to upload files to this group library:', 'chesnet'); ?>
                        <select name="cn-resources[can-create]" id="can-create">
							<option value="admin" <?php selected( $can_create, 'admin' ) ?> /><?php _e('Group admin', 'chesnet'); ?></option>
							<option value="mod" <?php selected( $can_create, 'mod' ) ?> /><?php _e('Group moderator', 'chesnet'); ?></option>
							<option value="member" <?php selected( $can_create, 'member' ) ?> /><?php _e('Group member', 'chesnet'); ?></option>
						</select>
                    </label>
                </div>
            </div>
            

		</div>

		<?php
    }
	
	/**
	 * Determine whether the group nav item should show up for the current user
	 *
	 * @package BuddyPress Docs
	 * @since 1.0-beta
	 */
	function enable_nav_item() {
		global $bp;

		$enable_nav_item = false;

		// The nav item should only be enabled when BP Docs is enabled for the group
		if ( $this->group_enable ) {
			if ( !empty( $bp->groups->current_group->status ) && $status = $bp->groups->current_group->status ) {
				// Docs in public groups are publicly viewable.
				if ( 'public' == $status ) {
					$enable_nav_item = true;
				} else if ( groups_is_user_member( bp_loggedin_user_id(), $bp->groups->current_group->id ) ) {
					// Docs in private or hidden groups visible only to members
					$enable_nav_item = true;
				}
			}

			// Super admin override
			if ( is_super_admin() ) $enable_nav_item = true;
		}

		return $enable_nav_item;
	}
	
	/**
	 *
	 *
	 */
	function set_display_screen(){
		global $chesnet;
		
		$url = $_SERVER['REQUEST_URI'];
		$parsed_url = parse_url($url);
		$path_arr = explode($this->slug, $parsed_url['path']);
		$path_arr[1] = rtrim(ltrim($path_arr[1], '/'), '/');
		if(empty($path_arr[1])) return false;
		$args = explode('/', $path_arr[1]);
		
		if(count($args)){
			if(isset($args[1]) && isset($args[2])){
				if($args[1] == 'resource' && is_numeric($args[2])){
					$proposed_id = (int)$args[2];
					//make sure the dlm exists & belongs to the current group
					$dlm = get_post($proposed_id);
					if($dlm && $chesnet->cn_is_post_in_group($dlm)){
						if(!$chesnet->cn_userCanEdit($dlm)) {
							bp_core_add_message( sprintf( __('You do not have permission to edit this %s', 'chesnet'), $this->dlm_post_type_obj->labels->singular_name), 'error' );
							return false;
						}
						else {
							
							if (strpos($url, "/edit/resource/".$dlm->ID."/labels/")){
								$this->dlm = $dlm;
								$this->display_screen = 'manage-dlm-labels';
								return;
							}
						}
					}
					else bp_core_add_message( sprintf( __('%s not found', 'chesnet'), $this->dlm_post_type_obj->labels->singular_name), 'error' );
				}
			}

			if($args[0] == 'page') return;
			
			if($args[0] == 'add-label') {
				$this->display_screen = 'add-label';
				return;
			}
			
			$adding = $editing = $deleting = false;
			$rargs = array_reverse($args);

			if($rargs[0] == 'add-label') {
				$adding = true;
				array_shift($rargs);
			}
			elseif($rargs[0] == 'edit-label'){
				$editing = true;
				array_shift($rargs);
			}
			elseif($rargs[0] == 'delete-label'){
				$deleting = true;
				array_shift($rargs);
			}
			
			//validate label
			if(isset($rargs[1])){
				if($rargs[1] == 'page'){
					$label_slug = $rargs[2];
					if(isset($rargs[3])) $parent_slug = $rargs[3];
				}
				else {
					$label_slug = $rargs[0];
					$parent_slug = $rargs[1];
				}
			}
			else $label_slug = $rargs[0];
			
			if(isset($parent_slug)){
				$parent = get_term_by('slug', $parent_slug, $chesnet->get_dlm_tax($this->get_group_id()));
			}
			$label = get_terms(array($chesnet->get_dlm_tax($this->get_group_id())), array(
				'hide_empty'        => false, 
				'number'            => 1, 
				'fields'            => 'all', 
				'slug'              => $label_slug, 
				'parent'            => isset($parent) ? intval($parent->term_id) : '0',
				'hierarchical'      => true
			));

			if($label){
				if($adding) $this->display_screen = 'add-label';
				elseif($editing) $this->display_screen = 'edit-label';
				elseif($deleting) $this->display_screen = 'delete-label';
				else $this->display_screen = 'view-label';
				
				$this->label = $label[0];
				return;
			}
		}
		return false;
		
	}
	
	function get_label_link($label){
		global $chesnet;
		$tax = $chesnet->get_dlm_tax($this->get_group_id());
		$ancestors = $ancestor_slugs = array();
		$ancestors = get_ancestors($label->term_id, $tax);
		$ancestors = array_reverse($ancestors);
		 if(!empty($ancestors) && is_array($ancestors)){
			foreach($ancestors as $ancestor){
				$a = get_term_by('id', $ancestor, $tax);
				$ancestor_slugs[] = $a->slug;
			}
		 }
		$ancestor_slugs[] = $label->slug;
		return trailingslashit($this->gl_permalink . implode('/', $ancestor_slugs));
	}
	
	function get_manage_dlm_labels_url($dlm, $group = false){
		if(!$group) $group = $this->group;
		$url = get_manage_dlm_labels_url($dlm, $group);
		if($url){
			if(isset($this->label) && $this->label) $url = add_query_arg('referer-label', $this->label->term_id, $url);
			
			return $url;
		}
		return false;
	}
		
	function wp_loaded(){
		global $chesnet, $bp;
		
		$this->dlm_post_type_obj = get_post_type_object( 'dlm_download' );
		
		//Save Manage Labels
		if(isset($_POST['cn-manage-dlm-labels']) && $_POST['cn-manage-dlm-labels'] == 1){
			//verify nonce
			if ( !isset( $_POST['manage_dlm_labels_nonce'] ) || !wp_verify_nonce( $_POST['manage_dlm_labels_nonce'], 'manage_dlm_labels' )) {
				bp_core_add_message( __('The security certificate didn\'t verify.', 'chesnet'), 'error' );
				return false;
			}
			
			$gid = $_POST['gid'];
			$label_tax_name = $chesnet->get_dlm_tax($gid);
			
			//form submitted
			if(isset($_POST['dlm-id'])){
				$proposed_id = (int)$_POST['dlm-id'];
				//make sure the dlm exists & belongs to the current group
				$dlm = get_post($proposed_id);
				if($dlm && $chesnet->cn_is_post_in_group($dlm)){
					if(!$chesnet->cn_userCanEdit($dlm)) {
						bp_core_add_message( sprintf( __('You do not have permission to edit this %s.', 'chesnet'), $this->dlm_post_type_obj->labels->singular_name), 'error' );
						return false;
					}
					else{ //process tags
						if($gid !== $bp->groups->current_group->id){
							bp_core_add_message( __('There was an error with the specified group.', 'chesnet'), 'error' );
							return false;
						}

						if(isset($_POST['tax_input'][$label_tax_name])){
							$labels = $_POST['tax_input'][$label_tax_name];
						
							$labels = array_unique($labels);
							$labels = array_map('intval', $labels);
						}
						else $labels = array();
						
						$r = wp_set_object_terms( $dlm->ID, $labels, $label_tax_name, false );
						if(is_array($r)){
							bp_core_add_message( __('Successfully updated the folders.', 'chesnet') );
							$redirect_url = isset($_POST['redirect-url']) ? $_POST['redirect-url'] : $this->gl_permalink;
							wp_redirect( $redirect_url );
							exit();
						}
						else bp_core_add_message( __('There was an error editing the folders.', 'chesnet'), 'error' );
						return false;
					}
				}
			}
			else { //adding or editting a label
				//validate label against blacklist
				if(isset($_POST['cn_new_terms'])){
					$submitted_label = trim(sanitize_text_field($_POST['cn_new_terms']));
					if(in_array($submitted_label, $this->label_blacklist) || empty($submitted_label) || is_numeric($submitted_label)) {
						bp_core_add_message( __('Invalid folder name.', 'chesnet'), 'error' );
						return false;
					}
				}
			
				if(isset($_POST['label'])){ //editting existing label
					//label actually exists?
					$label = get_term_by('id', (int)$_POST['label'], $label_tax_name);
		
					if(!$label){
						bp_core_add_message( __('Unable to edit folder.', 'chesnet'), 'error' );
						return false;
					}
					
					$parent = (int)$_POST['cn_parent_label'];
					$update = wp_update_term( (int)$label->term_id, $label_tax_name, array(
						'name' => $submitted_label,
						//'slug' => $label->slug, //Not sure if we need to keep slug static
						'parent' => $parent == '-1' ? '0' : $parent
					) );
					if(is_wp_error($update)){
						bp_core_add_message( sprintf(__('Error editing folder: %s', 'chesnet'), $update->get_error_message()), 'error' );
						return false;
					}
					
					bp_core_add_message( __('Successfully edited folder.', 'chesnet') );
					wp_redirect( $this->get_label_link(get_term_by('id', $update['term_id'], $label_tax_name)) );
					exit();
					
				}
				else { //adding new label
				
					$args = array();
					if(isset($_POST['parent-label'])){
						$args['parent'] = intval($_POST['parent-label']);
					}
					
					$inserted_term = wp_insert_term( $submitted_label, $label_tax_name, $args);
					if(is_wp_error($inserted_term)){
						bp_core_add_message( __('Unable to add folder.', 'chesnet'), 'error' );
						return false;
					}
					
					bp_core_add_message( __('Successfully added new folder.', 'chesnet') );
					wp_redirect( $this->get_label_link(get_term_by('id', $inserted_term['term_id'], $label_tax_name)) );
					exit();
					
				}
			}
		}
		return false;
	}
		
	
	
	
	 	
	/**
	 * Markup for the frontend page
	 *
	 * @package ChesNet
	 * @since 1.0-beta
	 */
    function display($group_id = null) {
		if($this->golightly && $this->library_transfered !== 'yes' && !is_super_admin()): ?>
            <div data-alert class="alert-box warning">
                <h4>Group Library Is On It's Way.</h4>
                <p style="margin-bottom:0">Don't worry, we haven't lost the resource library from your group. We're working hard to transfer everything over to the new site, and we'll get these up as soon as possible.</p>
            </div>
           <?php return;
		else: 
            
            
		global $bp, $chesnet;
		$group_cat = $chesnet->cn_cat_slug_from_gid($this->get_group_id());
		$label_tax_name = $chesnet->get_dlm_tax($this->get_group_id());
		$folders = $dl_args = array();
		
		$this->set_display_screen();
				
		if (isset($_GET['cn_dlm_remove_item_nonce']) && wp_verify_nonce($_GET['cn_dlm_remove_item_nonce'], 'remove_dlm_from_group_' . $this->get_group_id())) {
			// remove a dlm from a group
			$dlm_id = $_GET['dlm_id'];
			if(bp_group_resources_remove_dlm_from_group($dlm_id, $this->get_group_id()))
				bp_core_add_message(sprintf(__('%s successfully removed from this group.', 'chesnet'), $this->dlm_post_type_obj->labels->singular_name));
		}
		if (isset($_GET['cn_dlm_remove_label_nonce']) && wp_verify_nonce($_GET['cn_dlm_remove_label_nonce'], 'remove_label')) {
			//only delete a folder if there is nothing in it!
			$label_id = intval($_GET['lid']);
			$label = get_term_by('id', $label_id, $label_tax_name);
			if($label){
				//check if subfolders exist
				$children = get_terms($label_tax_name, array(
					'hide_empty'        => false,
					'child_of'			=> (int) $label_id
				));
				
				if($children)
					bp_core_add_message(__('A folder must be empty in order to be deleted.', 'chesnet'), 'error');
				else {
					//check if the label has any files in in
					$ldlm_args = array(
						'post_type' => 'dlm_download',
						'tax_query' => array(
							array(
								'taxonomy' => 'category',
								'field' => 'slug',
								'terms' => array( $group_cat )
							),
							array(
								'taxonomy' => $label_tax_name,
								'field' => 'id',
								'terms' => array( (int) $label_id ),
								'operator' => 'IN'
							),
							'relation' => 'AND'
						)
					);
					$ldlm = new WP_Query( $ldlm_args );

					// The 2nd Loop
					if( $ldlm->have_posts() )
						bp_core_add_message(__('A folder must be empty in order to be deleted.', 'chesnet'), 'error');
					else {
						//delete the label
						if(wp_delete_term( (int) $label_id, $label_tax_name ))
							bp_core_add_message(__('Folder successfully deleted.', 'chesnet'));
					}
					
					// Restore original Post Data
					wp_reset_postdata();
				}
			}
			
			
			//@todo: add ability to delete folder and subfolders and dlms (have to check permissions)
			/*
			$label_id = intval($_GET['lid']);
			$label = get_term_by('id', $label_id, $label_tax_name);
			$bundle = array();
			if($label){
				$children = get_terms($label_tax_name, array(
					'hide_empty'        => false,
					'child_of'			=> $label_id
				));
				echo '<pre>';
				var_dump($children);
				echo '</pre>';
				if(is_array($children)){
					foreach($children as $child){
						//delete each child term
						if(wp_delete_term($child->term_id, $label_tax_name))
							$bundle[] = $child->term_id;
					}
				}
				
				//remove any dlms from group (only if they dont' have another term)
				$dl_args = array(
					'post_type' => 'dlm_download',
					'tax_query' => array(
						array(
							'taxonomy' => 'category',
							'field' => 'slug',
							'terms' => array( $group_cat )
						),
						array(
							'taxonomy' => $label_tax_name,
							'field' => 'id',
							'terms' => array( $this->label->slug ),
							'operator' => 'NOT IN'
						),
						'relation' => 'AND'
					)
				);
				
				$parent = get_term_by('id', intval($label->parent), $label_tax_name);
				if(wp_delete_term($label->term_id, $label_tax_name)){
					bp_core_add_message(__('Folder successfully deleted.', 'chesnet'));
					//wp_redirect($this->get_label_link($parent));
				}
			}
			*/
		}
		?>
        
        <?php if(!did_action('template_notices')) do_action( 'template_notices' ); ?>
        
		<?php //Breadcrumbs
		if(!empty($this->label)): ?>
			<ul class="breadcrumbs">
                    <li><a href="<?php echo $this->gl_permalink; ?>"><?php echo $this->cn_resources_tab_name; ?></a></li>
                    <?php
                    $ancestors = get_ancestors( $this->label->term_id, $label_tax_name );
                    if($ancestors && is_array($ancestors)){
                        krsort($ancestors);
                        foreach($ancestors as $ancestor){
                            $item = get_term_by('id', $ancestor, $label_tax_name);
                            echo '<li><a href="'.$this->get_label_link($item).'">'.$item->name.'</a></li>';
                        }
                    }
                    ?>
                    <li class="current"><a href="#"><?php echo $this->label->name; ?></a></li>
                </ul>
                
        <?php endif;
		
		//show screen
		$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
		switch ($this->display_screen):
			case 'add-label': ?>
            	<h4><?php _e('Add New Folder', 'chesnet'); ?></h4>
            	<div class="article clearfix">
                    <form action="<?php echo $this->gl_permalink; ?>" method="post" id="manage-dlm-labels-form">
                        <div class="row">
                            <div class="large-14 columns">
                                <label><?php _e('Enter the name of the folder', 'chesnet'); ?>
                                    <input type="text" name="cn_new_terms" id="cn_new_terms" value="<?php echo isset($_POST['cn_new_terms']) ? esc_attr($_POST['cn_new_terms']) : ''; ?>"/>
                                </label>
                            </div>
                        </div>
    
                        <input type="hidden" name="gid" value="<?php echo $this->get_group_id(); ?>" />
                        <input type="hidden" name="parent-label" value="<?php echo $this->label->term_id; ?>" />
                        <input type="hidden" name="cn-manage-dlm-labels" value="1" />
                        <?php wp_nonce_field( 'manage_dlm_labels', 'manage_dlm_labels_nonce' ); ?>
                        <input type="submit" class="button" value="Submit" /><a href="<?php echo $this->gl_permalink ?>" class="right"><?php _e('Cancel', 'chesnet'); ?></a>
                    </form>
                </div>
				<?php
				
				
				
				
				break;
			case 'edit-label': ?>
                <h4><?php printf('%s <em>' . $this->label->name . '</em>', __('Edit folder', 'chesnet')); ?></h4>
                <div class="article clearfix">
                    <form action="<?php echo $this->gl_permalink; ?>" method="post" id="manage-dlm-labels-form">
                        <div class="row">
                            <div class="large-14 columns">
                                <label><?php _e('Edit folder name', 'chesnet'); ?>
                                    <input type="text" name="cn_new_terms" id="cn_new_terms" value="<?php echo isset($_POST['cn_new_terms']) ? esc_attr($_POST['cn_new_terms']) : esc_attr($this->label->name); ?>" />
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-14 columns">
                                <label><?php _e('Select folder parent', 'chesnet'); ?>
                                    <?php  wp_dropdown_categories($args = array(
                                        'show_option_all'    => false,
                                        'show_option_none'   => __('No parent', 'chesnet'),
                                        'orderby'            => 'NAME', 
                                        'hide_empty'         => 0, 
                                        'exclude_tree'		 => (int) $this->label->term_id,
                                        'selected'           => (int) $this->label->parent,
                                        'hierarchical'       => 1, 
                                        'name'               => 'cn_parent_label',
                                        'id'                 => 'cn_parent_label',
                                        'taxonomy'           => $label_tax_name,
                                        'hide_if_empty'      => false,
                                    )); ?>
                                </label>
                            </div>
                        </div>
                          
    
                        <input type="hidden" name="gid" value="<?php echo $this->get_group_id(); ?>" />
                        <input type="hidden" name="label" value="<?php echo $this->label->term_id; ?>" />
                        <input type="hidden" name="cn-manage-dlm-labels" value="1" />
                        <?php wp_nonce_field( 'manage_dlm_labels', 'manage_dlm_labels_nonce' ); ?>
                        <input type="submit" class="button" value="Submit" /><a href="<?php echo $this->get_label_link($this->label); ?>" class="right"><?php _e('Cancel', 'chesnet'); ?></a>
                    </form>
                </div>
                <?php
				
				
				
				
				
				break;
			case 'delete-label': ?>
				<h4><?php printf('%s <em>' . $this->label->name . '</em>', __('Delete folder', 'chesnet')); ?></h4>
				<div class="article clearfix">
                    <p><?php printf(__('Note: A folder must be empty in order to delete it (including %s and subfolders).', 'chesnet'), $this->dlm_post_type_obj->labels->name); ?></p>
                    <p><strong><?php _e('Are you sure you want to delete this folder?', 'chesnet'); ?></strong></p>
                    <?php //label's parent
                    $parent = get_term_by('id', (int) $this->label->parent, $label_tax_name); ?>
                    <a href="<?php echo add_query_arg('lid', $this->label->term_id, wp_nonce_url($parent ? $this->get_label_link($parent) : $this->gl_permalink, 'remove_label', 'cn_dlm_remove_label_nonce')); ?>" class="button alert"><?php _e('Delete', 'chesnet'); ?></a><a href="<?php echo $this->get_label_link($this->label); ?>" class="right"><?php _e('Cancel', 'chesnet'); ?></a>
				</div>
				<?php
				
				
				break;
			case 'view-label': ?>
                
				<?php
				//child labels
				$folders = get_terms(array($label_tax_name), array(
					'orderby'           => 'name', 
					'order'             => 'ASC',
					'parent'			=> intval($this->label->term_id),
					'hide_empty'        => 0, 
					'fields'            => 'all',
					'hierarchical'		=> true
				));
				
				$dl_args = array(
					'post_type' => 'dlm_download',
					'tax_query' => array(
						array(
							'taxonomy' => 'category',
							'field' => 'slug',
							'terms' => array( $group_cat )
						),
						array(
							'taxonomy' => $label_tax_name,
							'field' => 'slug',
							'terms' => array( $this->label->slug ),
							'operator' => 'IN',
							'include_children' => false
						),
						'relation' => 'AND'
					),
					'post_status' => 'publish',
					'posts_per_page' => 25,
					'suppress_filters' => true,
					'paged' => $paged
				);
                
                
				break;
			case 'manage-dlm-labels':
                $dlm = get_post($this->dlm); ?>
                
                <h4><?php _e('Manage Folders', 'chesnet'); ?></h4>
                <div class="article clearfix">
                    <p><?php printf(__('Edit Folders for %s', 'chesnet'), '<strong><span class="title">' . $dlm->post_title . '</span></strong>'); ?></p>
                    
                    <form action="<?php echo $this->gl_permalink; ?>" method="post" id="manage-dlm-labels-form">
                    
                        <?php //get all terms in this taxonomy (these are folders)
                        $labels = get_terms(array($label_tax_name), array(
                            'orderby'           => 'name', 
                            'order'             => 'ASC',
                            'hide_empty'        => false, 
                            'fields'            => 'all', 
                            'hierarchical'      => true
                        ));
        
                        if($labels && is_array($labels)){					
                            include ABSPATH . 'wp-admin/includes/template.php';
                            echo '<ul class="rlchecklist">';
                            wp_terms_checklist( $dlm->ID, array(
                                'taxonomy'              => $label_tax_name,
                                'checked_ontop'         => false
                            ) );
                            echo '</ul>';
                        }
                        else printf('<p>%s <a href="'. $this->gl_permalink . 'add-label">%s</a></p>', __('No available folders found.', 'chesnet'), __('Add Folder', 'chesnet')); ?>
                        
                        <input type="hidden" name="gid" value="<?php echo $this->get_group_id(); ?>" />
                        <input type="hidden" name="dlm-id" value="<?php echo $dlm->ID; ?>" />
                        <input type="hidden" name="cn-manage-dlm-labels" value="1" />
                        <?php wp_nonce_field( 'manage_dlm_labels', 'manage_dlm_labels_nonce' );
                        //cancel url
                        if(isset($_GET['referer-label'])){
                            $rl = get_term_by('id', $_GET['referer-label'], $label_tax_name);
                            if($rl) $cancel_url = $this->get_label_link($rl) . '#dlm-' . $dlm->ID;
                        }
                        if(!isset($cancel_url)) $cancel_url = $this->gl_permalink;
                        ?>
                        <input type="hidden" name="redirect-url" value="<?php echo $cancel_url; ?>" />
                        <input type="submit" class="button" value="Submit" /><a href="<?php echo $cancel_url; ?>" class="right"><?php _e('Cancel', 'chesnet'); ?></a>
                    
                    </form>
                </div>
                <?php





				break;
			default:
				$folders = get_terms(array($label_tax_name), array(
					'orderby'           => 'name', 
					'order'             => 'ASC',
					'parent'			=> 0,
					'hide_empty'        => 0, 
					'fields'            => 'all'
				));
				$terms = array();
				if(is_wp_error($folders)) $folders = array();
				if(!empty($folders) && !is_wp_error($folders)) foreach($folders as $f) $terms[] = (int)$f->term_id;
				
				$dl_args = array(
					'post_type' => 'dlm_download',
					'tax_query' => array(
						array(
							'taxonomy' => 'category',
							'field' => 'slug',
							'terms' => array( $group_cat )
						),
						array(
							'taxonomy' => $label_tax_name,
							'operator' => 'NOT IN',
							'terms' => $terms
						)
					),
					'post_status' => 'publish',
					'posts_per_page' => 25,
					'suppress_filters' => true,
					'paged' => $paged
				);
			
			
			
		endswitch;
		
		
		
		
		
			
		//display folders
		if(!empty($folders)){
			echo '<ul class="small-block-grid-3 resource-folders">';
			foreach($folders as $folder){
				
				echo '<li><a class="th" title="'. $folder->name .'" href="' . $this->get_label_link($folder) . '">' . $folder->name . '</a></li>';
			}
			echo '</ul>';
		}
		
		//Display Downloads
		if(!empty($dl_args)){
			$dls = new WP_Query( $dl_args );
			//Iterate through the posts and print out the list
			if($dls->have_posts()): ?>
            	<?php $chesnet->wp_query_pagination($dls); ?>
				<table role="grid" style="width:100%">
				  <thead>
					<tr><th><!--checkbox--></th><th><?php _e('Name', 'chesnet'); ?></th><th><?php _e('Uploaded By', 'chesnet'); ?></th><th><?php _e('Last Modified', 'chesnet'); ?></th></tr>
				  </thead>
				  <tbody>
					<?php while( $dls->have_posts() ) :
						$dls->next_post();
						$post_modified = strtotime($dls->post->post_modified);
						$modified = date('Y',$post_modified) == date('Y') ? date('j M',$post_modified) : date(get_option('date_format', 'n/j/y'),$post_modified); ?>
						<tr id="dlm-<?php echo $dls->post->ID; ?>">
							<td><!--<?php if($chesnet->cn_userCanEdit($dls->post) ): ?><input type="checkbox" name="" id="" /><?php endif; ?>--></td>
							<td>
								<a href="<?php echo get_post_permalink($dls->post->ID); ?>"><?php echo get_the_title( $dls->post->ID ); ?></a>
								<div class="small">
									<?php if($chesnet->cn_userCanEdit($dls->post) ): ?>
										<a href="<?php echo $this->get_manage_dlm_labels_url($dls->post); ?>"><?php _e('Manage Folders', 'chesnet'); ?></a>&nbsp;|&nbsp;<a href="<?php print add_query_arg('dlm_id', $dls->post->ID, wp_nonce_url($this->gl_permalink, 'remove_dlm_from_group_' . $this->get_group_id(), 'cn_dlm_remove_item_nonce'));?>">Remove from Group</a>
									<?php endif; ?>
								</div>
							</td>
							<td><?php echo bp_core_get_userlink((int)$dls->post->post_author); ?></td>
							<td><?php echo $modified; ?></td>
					<?php endwhile; ?>
				  </tbody>
				</table>
			<?php  //end if anything
			else: echo $this->dlm_post_type_obj->labels->not_found;
			
			endif;
			wp_reset_postdata();
		}
		endif; //if golightly
    }
	
	function single_group_sidebar_left(){
		global $chesnet;
		
		$label_link = $this->label ? $this->get_label_link($this->label) : $this->gl_permalink; ?>
        
     	<?php if(empty($this->display_screen) || $this->display_screen == 'view-label'): ?>
            
            
            <div class="homeSideNav no-bg">
                <a class="button" href="<?php echo $chesnet->cn_getUrl( 'add', null, null, 'dlm_download', null ); ?>"><?php _e('Add Download', 'chesnet'); ?></a>
                <ul class="subtle-sidenav">
                    <li><a class="fi-folder-add" href="<?php echo $label_link . 'add-label'; ?>"><?php _e('Add Folder', 'chesnet'); ?></a></li>
                    <?php if($this->label): ?>
                    <li><a class="fi-page-edit" href="<?php echo $label_link . 'edit-label'; ?>"><?php _e('Edit Folder', 'chesnet'); ?></a></li>
                    <li><a class="fi-trash" href="<?php echo $label_link . 'delete-label'; ?>"><?php _e('Delete Folder', 'chesnet'); ?></a></li>
                    <?php endif; ?>
                </ul>
            </div>
            <hr />
        <?php endif;
	}
	
}
bp_register_group_extension( 'CN_Resources_Group_Extension' );
 
endif; // if ( class_exists( 'BP_Group_Extension' ) )

?>