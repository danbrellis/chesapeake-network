<aside id="sidebar" class="small-14 large-4 columns">
	<?php do_action('cn_before_sidebar'); ?>
    <?php dynamic_sidebar("right-sidebar"); ?>
    <?php global $bp; if(is_active_sidebar("right-sidebar-ads") && $bp->current_component == 'activity'): ?>
		<div class="panel widget">
			<div class="clearfix">
            	<span class="h5 left"><?php _e('Sponsored', 'chesnet'); ?></span>
                <a href="<?php echo trailingslashit( bp_get_root_domain() . '/' . 'request-advertising' . '/'); ?>" class="right size14"><?php _e('Display your ad', 'chesnet'); ?></a>
            </div>
			<?php dynamic_sidebar("right-sidebar-ads"); ?>
		</div>
	<?php endif; ?>
	<?php cn_footer_menu(); ?>
	<?php do_action('cn_after_sidebar'); ?>
</aside>
