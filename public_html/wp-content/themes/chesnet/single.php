<?php get_header();
global $post, $chesnet, $activities_template, $bp; ?>

<div class="row">
	<div class="small-14 large-10 columns" role="main">

	<?php do_action('cn_before_content');
	$group = groups_get_group( array( 'group_id' => $chesnet->cn_set_group_id ) );
	if($group) $chesnet->cn_the_set_group = $group; ?>
	
	<?php while (have_posts()) : the_post(); ?>
    	<?php 
		//$post_type_obj = get_post_type_object( get_post_type() );
		//$posttype_singular_name = $post->post_type == 'post' ? __('News &amp; Announcements', 'chesnet') : $post_type_obj->labels->singular_name;
		$posttype_singular_name = cn_get_activity_type(get_post_type(), 'singular_name');

		//Set Activity
		//$pto = get_post_type_object( $post->post_type );
		$chesnet->cn_set_current_activity_id();
		$activity_id = $chesnet->cn_the_set_activity_id;

		if(isset($activity_id) && $activity_id){
			$template_args = array(
				'include'           => $activity_id,
				'display_comments'  => false,
				'update_meta_cache' => false,
				'show_hidden'		=> true
			);
	
			$activities_template = new BP_Activity_Template( $template_args );
			//$activities_template->has_activities();
			$activities_template->the_activity();
		}
		?>
		<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
            <header class="clearfix single-pageheader">
                <h3><?php echo $posttype_singular_name; ?></h3>
                <div><a href="<?php echo $chesnet->get_group_link_by_posttype($group, get_post_type()); ?>"><?php echo $group->name . ' ' .  cn_get_activity_type(get_post_type(), 'plural_name'); ?></a> &middot; <a href="<?php echo bp_core_get_user_domain($post->post_author); ?>"><?php printf(__('More from %s', 'chesnet'), xprofile_get_field_data(1, $post->post_author)); ?></a></div>
                <?php
                	/*
					 * Previous and Next eliminated for the time being
					 * Need to either send to previous/next posts
					 * or figure out a way to find the previous/next activity ID
					 
					 <div class="right"><a>Previous</a> &middot; <a>Next</a></div>
					 
					 */
					 
				if(!did_action('template_notices')) do_action( 'template_notices' ); ?>
            </header>
            <div class="post-header">
				<?php 
				add_filter('switch_group_link_to_home_or_component', '__return_true');
				$chesnet->post_entry_meta();
				remove_filter('switch_group_link_to_home_or_component', '__return_true'); ?>
                <h2 class="entry-title"><?php the_title(); ?></h2>
			</div>
			
            <?php if ( bp_group_is_visible($group) ): ?>
				<?php do_action('cn_post_before_entry_content'); ?>
                <div class="entry-content clearfix">
                    <?php the_content(); ?>
                </div>
                <div class="row" id="ses<?php /* #single-entry-sharing */?>">
                    <div class="medium-4 medium-push-10 small-14 columns">
                        <ul class="post-sharing-side-nav">
                            <?php if(isset($activities_template->activity)): ?>
                            <li>
								<?php if ( bp_activity_can_favorite() && is_user_logged_in() ) {
                                    $fav_class = 'favorite-activity';
                                    if(!bp_get_activity_is_favorite()){
                                        $fav_href = bp_get_activity_favorite_link();
                                        $fav_title = __( 'Favorite', 'chesnet' );
                                    }
                                    else{
                                        $fav_href = bp_get_activity_unfavorite_link();
                                        $fav_class .= ' favorited';
                                        $fav_title = __( 'Unfavorite', 'chesnet' );
                                    }
                                    printf('<a href="%1$s" class="%2$s" title="%3$s"><i class="fi-star imgWrapLeft"></i>%3$s</a>',
                                            $fav_href,
                                            $fav_class,
                                            $fav_title
                                    );
                                } ?>
                                </li>
                            <?php endif; ?>
                            <li>
                            <?php if(comments_open() && is_user_logged_in()):  ?>
                                <a href="#respond" class="comment-activity" title="<?php _e( 'Post a comment.', 'chesnet' ); ?>"><i class="fi-comment imgWrapLeft"></i><?php _e('Comment', 'chesnet'); ?></a>
                            <?php endif; ?>
                            </li>
                            <?php /* add Subscribe, Email, Share (social media), Embed, and Report */ ?>
                            
                            <?php echo $chesnet->cn_getEditButton( $post, sprintf('%s'.__('Edit', 'chesnet'), '<i class="fi-page-edit imgWrapLeft"></i>'),'<li class="divider"></li><li>', '<li>' ); ?>
                        </ul>
                    </div>
                    <div class="medium-10 medium-pull-4 small-14 columns">
                        <ul class="no-bullet">
                            <li><?php the_tags(sprintf('<i class="fi-pricetag-multiple imgWrapLeft"></i><span class="post-tags">%s: ', __('Tagged with', 'chesnet')), ', ', '</span>'); ?></li>
                            <?php if( isset($activities_template->activity) && bp_activity_can_favorite() ) :
                                $cn_favorites_number = cn_favorites_number(); ?>
                                <li>
                                    <i class="fi-star imgWrapLeft"></i><span class="favorite-count" id="activity-<?php bp_activity_id(); ?>-fc" title="<?php echo $cn_favorites_number; ?>"><?php echo $cn_favorites_number; ?></span>
                                </li>
                            <?php endif; ?>
                            
                            <?php if(comments_open()):  ?>
                                <li>
                                    <i class="fi-comment imgWrapLeft"></i><span class="comment-count" id="activity-<?php bp_activity_id(); ?>-cc" title="<?php comments_number( '0 comments', '1 comment', '% comments' ); ?>"><?php comments_number( '0 comments', '1 comment', '% comments' ); ?></span>
                                </li>
                            <?php endif; ?>
                        </ul>
                        <div class="comments-block">
                            <?php if((comments_open() || get_comments_number())){
                                do_action('cn_post_before_comments');
                                comments_template();
                                do_action('cn_post_after_comments');
                            } ?>
                        </div>
                    </div>
                    
                </div>
                
            <?php else:
				//bp_do_404();
				do_action( 'bp_before_group_status_message' ); ?>

                <div class="alert-box alert" data-alert>
                    <?php bp_group_status_message($group); ?>
                    <a href="#" class="close hide-if-no-js">&times;</a>
                </div>

                <?php do_action( 'bp_after_group_status_message' );
			
			endif; ?>
            
            
			<footer>
				
				
				<?php //wp_link_pages(array('before' => '<nav id="page-nav"><p>' . __('Pages:', 'FoundationPress'), 'after' => '</p></nav>' )); ?>
                <!--
                <div class="clearfix">
                    <div class="left"><?php previous_post_link( '%link', '<span class="meta-nav">' . _x( '&larr;', 'Previous post link', 'chesnet' ) . '</span> %title' ); ?></div>
                    <div class="right"><?php next_post_link( '%link', '%title <span class="meta-nav">' . _x( '&rarr;', 'Next post link', 'chesnet' ) . '</span>' ); ?></div>
                </div>
				-->
                
			</footer>
		</article>
	<?php endwhile;?>

	<?php do_action('foundationPress_after_content'); ?>

	</div>
	<?php 
	function single_post_sidebar_meta(){
		global $chesnet, $post;
		if ( bp_group_is_visible($chesnet->cn_the_set_group) ) {
			
			if($post->post_type == 'dlm_download') get_template_part('views/dlm_download/meta', 'right-sidebar');
			
		}
	}
	add_action('cn_before_sidebar', 'single_post_sidebar_meta');
	get_sidebar('right');
	remove_action('cn_before_sidebar', 'single_post_sidebar_meta'); ?>
</div>
<?php get_footer(); ?>
