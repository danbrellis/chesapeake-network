<?php 
global $chesnet;
function cn_list_comments($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment;
	$ctimestamp = strtotime($comment->comment_date); ?>
	<li <?php comment_class(); ?>>
		<div id="comment-<?php comment_ID(); ?>">
			<a class="left" href="<?php echo bp_core_get_user_domain( $comment->user_id ); ?>" title="<?php echo bp_core_get_user_displayname( $comment->user_id ); ?>">
				<?php echo bp_core_fetch_avatar ( array( 'item_id' => $comment->user_id, 'type' => 'thumb', 'class' => 'imgWrapLeft' ) ); ?>
            </a>
            <div class="comment-data hide-over">
                <header class="author-box">
                    <span class="byline author"><?php echo bp_core_get_userlink( $comment->user_id ); ?></span> &middot; <time class="" datetime="<?php echo date('c', $ctimestamp); ?>" title="<?php printf(__('%s at %s', 'chesnet'), date('l, F jS, Y', $ctimestamp), date('g:ia', $ctimestamp)); ?>"><?php printf(__('Posted %s', 'chesnet'), bp_core_time_since( strtotime($comment->comment_date_gmt), bp_core_current_time( false, 'timestamp' ) )); ?></time>
                </header>
    
                <?php if ($comment->comment_approved == '0') : ?>
                    <div data-alert class="alert-box warning">
                    	<?php _e('Your comment is awaiting moderation.', 'chesnet') ?>
                    </div>
                <?php endif; ?>
    
                <section class="comment-content">
                    <?php comment_text() ?>
                    <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
                </section>
            </div>

		</div>
<?php } ?>

<?php
// Do not delete these lines
	if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die (__('Please do not load this page directly. Thanks!', 'chesnet'));

	if ( post_password_required() ) { ?>
	<section id="comments">
		<div class="notice">
			<p class="bottom"><?php _e('This post is password protected. Enter the password to view comments.', 'chesnet'); ?></p>
		</div>
	</section>
	<?php
		return;
	}
?>
<?php if ( comments_open() ) : ?>
    <section>
        <h3 class="hidden-for-small-up"><?php comment_form_title( __('Leave a Reply', 'chesnet'), __('Leave a Reply to %s', 'chesnet') ); ?></h3>
		<?php if ( get_option('comment_registration') && !is_user_logged_in() ) : ?>
            <div data-alert class="alert-box warning">
            	<?php printf( __('You must be <a href="%s">logged in</a> to post a comment.', 'chesnet'), wp_login_url( get_permalink() ) ); ?>
                <a href="#" class="close">&times;</a>
            </div>
            
            
		<?php else : 
			$commenter = wp_get_current_commenter();
			$req = get_option( 'require_name_email' );
			$aria_req = ( $req ? " aria-required='true'" : '' );
			
			ob_start();
			$chesnet->wp_editor_placeholder = __('Write a comment...', 'chesnet');
			get_template_part( 'views/comment', 'field' );
			$comment_field = ob_get_clean();
			
			$comments_args = array(
				'title_reply'			=> null,
				'cancel_reply_link'		=> null,
				'label_submit'			=> __( 'Submit', 'cheset' ),
				'comment_field'			=> $comment_field,
				'logged_in_as'			=> null,
				'comment_notes_before'	=> null,
				'comment_notes_after'	=> null,
				'submit_field'			=> '<input type="submit" class="button right small" name="%s" id="%s" value="%s" />'
			);
			
			$chesnet->comment_form($comments_args);
		endif; // If registration required and not logged in ?>
    </section>
<?php endif; // if you delete this the sky will fall on your head ?>

<?php if ( have_comments() ) : ?>
	<section id="comments">
		<h3 class="hidden-for-small-up"><?php comments_number(__('No Responses to', 'chesnet'), __('One Response to', 'chesnet'), __('% Responses to', 'FoundationPress') ); ?> &#8220;<?php the_title(); ?>&#8221;</h3>
		<ul class="commentlist no-bullet">
		<?php wp_list_comments('type=comment&callback=cn_list_comments'); ?>

		</ul>
		<footer>
			<nav id="comments-nav">
				<div class="comments-previous"><?php previous_comments_link( __( '&larr; Older comments', 'chesnet' ) ); ?></div>
				<div class="comments-next"><?php next_comments_link( __( 'Newer comments &rarr;', 'chesnet' ) ); ?></div>
			</nav>
		</footer>
	</section>
<?php endif; ?>