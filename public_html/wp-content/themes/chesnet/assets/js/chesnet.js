// JavaScript Document
// @version 1.0.0
var ajaxurl = "http://www.chesapeakenetwork.org/wp-admin/admin-ajax.php";
//Initiate Foundation JS
jQuery(document).foundation({
	reveal : {
		css : {
			open : {
				'opacity': 0,
				'visibility': 'visible',
				'display' : 'block'
			},
			close : {
				'opacity': 1,
				'visibility': 'hidden',
				'display': 'none'
			}
		}
	}	
});

jQuery(document).ready(function($) {
	
	//Joyride
	$(document).foundation({
		'joyride': { 
			'cookie_monster': !$.cookie('joyride') ? false : true,// not cookieMonster
			post_ride_callback : function () {
				!$.cookie('joyride') ? $.cookie('joyride', 'ridden') : null;
			}    
		} // * edited original answer, missing closing bracket
	}).foundation('joyride', 'start');

	//Add geocomplete to inputs with the class
	if ( $( ".geocompletei" ).length ) {
		$( ".geocompletei" ).each(function( index ) {
			var $this = $(this);
			
			var mapdetscont = $this.closest('div');
			var mapcanvas = mapdetscont.find("#" + $this.attr("id") + "-map");
			var showmapchk = mapdetscont.find('.show-map');
			
			if(mapcanvas.length) {
				$this.geocomplete({
					map: "#" + mapcanvas.attr('id'),
					markerOptions: {'draggable': true},
					mapOptions: {'scrollwheel': true},
					location: $this.val()
				})
				.bind("geocode:dragged", function(event, result){
					var geocoder = new google.maps.Geocoder();
					console.log(result['D'])
					var latlng = new google.maps.LatLng(result['k'], result['D']);
					geocoder.geocode({'latLng': latlng}, function(results, status) {
						if (status == google.maps.GeocoderStatus.OK) {
							if (results[0]) {
								$this.val(results[0].formatted_address);
							}
						} else {
							alert("Geocoder failed due to: " + status);
						}
					});
				});
				
				//if checkbox, chk first to see if we show the map
				if(showmapchk.length == 0 || (showmapchk.length && showmapchk.is(':checked'))){
					//make map visible
					mapcanvas.addClass('googlemapcanvas');
					mapcanvas.css('width', '100%');
				}
				
				if(showmapchk.length){
					showmapchk.click(function(){
						if(showmapchk.is(':checked')){
							mapcanvas.addClass('googlemapcanvas');
							mapcanvas.css('width', '100%');
						}
						else{
							mapcanvas.removeClass('googlemapcanvas');
						}
					});
				}
				
				
				
			}
			else {
				$this.geocomplete();
			}
			
			/*
			$(this).geocomplete().bind("geocode:result", function(event, result){
				$(this).geocomplete()
			});
			*/
		});
	}
	
	//Add selected file to input when 'file upload' is clicked
	$('.fileUpload input').change(function(){
		var $this = $(this);
		var filepath = $this.val();
		var filename = filepath.split('\\').pop();
		$this.closest('.row').find('input:disabled').val(filename);
	});
	$('.fileUpload').closest('div').removeClass('left');
	
	//Add tooltip to explain "Add Media" button in annoucnement form
	if($('#wp-cnpostcontent-media-buttons').length){
		var btn = $('#wp-cnpostcontent-media-buttons');
		console.log(btn);
		btn.attr( "data-dropdown", "media_btn_explain");
		btn.attr( "data-options", "is_hover:true; align:right");
		btn.attr( "aria-expanded", "true");
		
		$(document).foundation('dropdown', 'reflow');
	}
	
	//Delete file when clicked in announcement form
	$('.cn_delete_file').on( "click", function(){
		var btn = $(this);
		var post_ID = $('#post_ID').val();
		var btn_id = btn.attr('id'); //ie delete__92__cn_jobs_announcement_file
		var btn_data = btn_id.substring(8); //ie 92__cn_jobs_announcement_file
		var btn_data_array = btn_data.split('__');
		
		var data = {
			action: 'cn_set_to_delete_file_and_meta',
			security: $('#cn_delete_file_nonce').val(),
			delete_file: true,
			key: '_' + btn_data_array[1],
			post_id: btn_data_array[0]
		};
		//console.log(data);
		$.post(ajaxurl, data, function(response) {
			var r = $.parseJSON( response );
			if(r.type == 'success'){
				//delete file text
				//console.log(btn);
				btn.closest('.row').next('.row').find('input:disabled').attr('placeholder', '');
				
				btn.closest('.row').css('background-color', 'rgb(255, 170, 170)');
				btn.closest('.row').fadeOut("slow");	
					
			}
		});
	});
	
	
	 /*********************************************
	 ******************* Posts *******************
	 *********************************************/
	
	//initialize map for bgc_event
	if ( typeof cn_data['bgc_map'] !== 'undefined' ) {
		var map;
		var geocoder;
		var location = cn_data['bgc_map']['loc'];
		var canvas = cn_data['bgc_map']['canvas'];
		
		geocoder = new google.maps.Geocoder();
		
		geocoder.geocode( { 'address': location}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) { //address is valid, so create the map
				var jcanvas = $("#" + canvas);

				var mapOptions = {
					zoom: 10,
					center: results[0].geometry.location
				}
				map = new google.maps.Map(document.getElementById(canvas), mapOptions);
				var marker = new google.maps.Marker({
					url: 'http://maps.google.com/?q=' + location,
					map: map,
					position: results[0].geometry.location
				});
				google.maps.event.addListener(marker, 'click', function() {
					window.open( marker.url, '_blank' );
				});
				
				//console.log(results[0]);
				
				//format divs
				var jcp = jcanvas.parent( "div" ); //jcanvas parent
				jcp.addClass("small-14 large-7 medium-push-7 columns");
				jcanvas.addClass("googlemapcanvas");
				jcanvas.css('width', '100%');
				jcp.next().addClass("large-7 medium-pull-7");
				
				//remove map link
				$("#gmap_link_for_" + cn_data['post_id']).hide();
				
			} else {
				console.log("Geocode was not successful for the following reason: " + status);
			}
		});
	}
	
	//Add button class to 'Comment' submit
	$('#commentform input#submit').addClass('button');
	
	
	/*********************************************
	 ************** Home / Activity **************
	 *********************************************/
	
	//add tag-it plugin
	if($('.cn-tagit-input').length){
		$('.cn-tagit-input').tagit({
			availableTags: cn_data['activity_tags'],
			showAutocompleteOnFocus: true,
			caseSensitive: false,
			placeholderText: cn_data['tagit_placeholder'],
			afterTagAdded: function(event, ui) {
				cn_bp_activity_request();
			},
			afterTagRemoved: function(event, ui) {
				cn_bp_activity_request();
			}
		});
	}
	
	cn_bp_init_activity();
	
	//BP AJAX Activity
	//clicking on message type
	$( ".cn-activity-filter-by" ).click(function() {
		event.preventDefault();

		//collect filters
		var filters = [];
		var filter;
		filters = cn_getActivityFilters();
		filters.push( $( this ).attr('id').replace('activity-filter-by-', ''));
		filter = $.unique(filters).join(',');
		var selected_tab = jq( 'div.activity-type-tabs li.selected' );
		if ( !selected_tab.length )
			var scope = null;
		else
			var scope = selected_tab.attr('id').substr( 9, selected_tab.attr('id').length );

		bp_activity_request(scope, filter);
		
		//add selected class to button
		cn_bp_add_filter_selected($(this));
		
		return false;
	});
	
	//clicking on X to delete message type filter
	$( ".cn-activity-filter-by" ).on( "click", "span", function(event) {
		if($(event.target).is( $('.fi-x') )){
			event.stopPropagation();
			event.preventDefault();
			
			$( this ).parent().removeClass('selected');
			
			//get filters
			cn_bp_activity_request();
			
			//remove icons
			$(this).remove();
		}
	});
	
	//clicking on a tag in the activity item	
	$( ".activity" ).on( "click", "a", function(event) {
		//console.log($(this).closest('.activity-tags'));
		if($(this).closest('.activity-tags').length){
			event.stopPropagation();
			event.preventDefault();
			$('.cn-tagit-input').tagit('createTag', $(this).text());
			return false;
		}
	});
	
	//load my activity sidebar
	//not true.. don't really need this since we can use the css "text-overflow: ellipsis;"
	if($('#home-groups-container').length){
		var magc = $('#home-groups-container');
		$.post(ajaxurl, {'action': 'load-my-activity-groups'}, function(response) {
			var r = $.parseJSON( response );
			if(r.type == 'success'){
				var glist = $('<ul/>')
					.addClass('subtle-sidenav');
				magc.find("ul:last").replaceWith(glist);
				//console.log(glist);
				$.each( r.content, function( key, values ) {
					//console.log(values);
					var li = $('<li/>')
						.appendTo(glist);
					var aaa = $('<a/>')
						.html(values.avatar)
						.attr("href", values.group_url)
						.attr("title", values.group_name)
						.appendTo(li);
					var aaaimg = aaa.find('img');
					aaa.append(fitStringToWidth(values.group_name, magc.find('ul a').width() - 15 - aaaimg.outerWidth(true)))
				});
			}
		});
	}
	
	//expand first button in .button-group to fill remaining width on l=page load and window resize
	$( window ).resize(function() {
		expand_first_button_in_group();
	});
	
	//activity loop: make content max-height to fit img stretch
	if($('#activity-stream').length){
		$.each($('.activity-item'), function(index){
			//get height of .activity-content
			var originalContent = $(this).find( '.activity-content' );
			
			var imgHeight = $(this).find('.activity-inner img').height();
			if(imgHeight > originalContent.height()){
				originalContent.css('max-height', imgHeight);
			}
		});
	}
	
	//adds site banner image
	add_site_banner_img();
	$( window ).resize(function() {
		add_site_banner_img();
	});
	
	//alert users they can send an announcement to the group via the form when they clikc on the group's email address
	if($('.homesideNavcont #groupEmails').length){
		$( ".homesideNavcont #groupEmails" ).on( "click", "a", function() {
			var email = confirm(cn_data['post_by_email_confirm_txt']);
			if(email == true) return true;
			else {
				event.stopPropagation();
				event.preventDefault();
				window.location.href = cn_data['announcement_link'];
			}
			return false;
		});
	}
	
	
	/*********************************************
	 *************** Group Settings **************
	 *********************************************/
	if($('#group-admin-add-addt-email-field').length){
		$( "#group-admin-add-addt-email-field" ).click(function() {
			var row = $(this).closest('div.row');	
			var clonable = row.children('.columns').eq(-2);
			
			var newclone = clonable.clone();
			var inp = newclone.find('input');
			var label = newclone.find('label');
			var n = parseInt(/group-email-address-(\d+)/.exec(inp.attr('id'))[1], 10);
			var nid = inp.attr('id').replace(n, n+1);
			console.log(nid);
			
			inp.val("");
			inp.attr('id', nid);
			label.attr('for', nid);
			label.text(label.text().replace(n, n+1));
			
			newclone.insertAfter(clonable);
		});
		
	}
	
	/*********************************************
	 *********** Group Calendar Plugin ***********
	 *********************************************/
	if($('.datepickerclass').length){
		$('input').filter('.datepickerclass').datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});	
	}
	
	
	
	/*********************************************
	 ****************** Profile ******************
	 *********************************************/
	
	/* @todo REVIST THIS TO GENERATE QUESRY STRING FOR DIFF TABS
	 * https://github.com/balupton/jquery-history
	
	if($('#profile-group-tabs').length){
		//check if hash is set
		var hash = window.location.hash;
		console.log(hash.length);
		
		if(hash.length){
			
			
			var hashData = hash.queryStringToJSON();
			var section = hashData['section'];

		
			if(section != undefined){
				
				$("#profile-group-tabs li").each(function( index ) {
					$(this).removeClass('active');
					if($(this).children('a').first().attr('href').replace('#', '') == section){
						$(this).addClass('active'); //add to tab
					}
				});
				$(".tabs-content div").removeClass('active');
				$(".tabs-content #" + section ).addClass('active'); //add to content
			}
		}
		
		$("#profile-group-tabs li a").click(function(event) {
			event.preventDefault();
			
		});
		
		//update hash on click
		
	}
	*/
	
	//profile fields for registration
	$('.editfield ul').each(function(){
		var cont = $(this).closest('div.editfield');
		var spantip = cont.find('span.has-tip');
		$(this).find('label').on('click', 'input', function(event){
			var inpval = $(this).val();
			var visibility = $(this).next('span').text();
			var ttsel = spantip.attr('data-selector');
			$('span#' + ttsel + ' strong').text(visibility);
			
			//fix icon
			var icon; 
			if(inpval == 'public') icon = 'unlock';
			else if(inpval == 'loggedin') icon = 'lock';
			else icon = 'x';
			
			var i = cont.find('i');
			i.removeClass('fi-lock fi-unlock fi-x');
			console.log(icon);
			i.addClass('fi-' + icon);  
		});
	});
		
}); //end jquery

//do everythign we need to do to make the activity work and look good!
function cn_bp_init_activity(){
	if ( null != $.cookie('bp-activity-filter') && jq('#activity-actions-container').length ) {
		//console.log($.cookie('bp-activity-filter'));
		var filters = [];
		filters = $.cookie('bp-activity-filter').split(',');
		
		$.each(filters, function( index, value ){
			if(/bp_tagged_item/i.test(value)){
				//get individual tags
				var tags_str = value.replace('bp_tagged_item:', '');
				var tags = tags_str.split(';');
				//add the tags to the tag-it
				$.each(tags, function(index, tag) {
					$('.cn-tagit-input').tagit('createTag', tag);
				});
				return true;
			}
			
			if($('#activity-filter-by-' + value).length)
				cn_bp_add_filter_selected($('#activity-filter-by-' + value));
		});
	}
	
	//expand first button in .button-group to fill remaining width on l=page load and window resize
	expand_first_button_in_group();
	setTimeout(function () { 
		$('.tagit-new input').focus();
		$('.tagit-new input').blur();
	}, 10);
	
	
}

//load the image on the site banner
function add_site_banner_img(){
	if($('#cn-site-banner').is(':visible')){
		$('#cn-site-banner').css('background-image', 'url(http://www.chesapeakenetwork.org/wp-content/uploads/2016/03/16579986031_f6bc99ba43_k.jpg)');
	}
}

//expand first button in .button-group
function expand_first_button_in_group(){
	$( "ul.button-group.expand-first" ).each(function( index ) {
		var group = $( this ); //ul dom
		var cumwidth = 0;
		$( this ).children("li").each(function( index ) {
			//console.log($(this));
			cumwidth = cumwidth + $(this).outerWidth(true);
			
		});
		var contwidth = $( this ).parent().width();
		var makeup = contwidth - cumwidth;
		var first = $( this ).find('li').first().find('a');
		first.width(first.width() + makeup - 1);
	});
}

function cn_bp_add_filter_selected(i){
	i.addClass('selected');
	var link_txt = i.text();
	i.html('<span class="remove-activity-filter"><i class="fi-check"></i><i class="fi-x"></i></span>' + link_txt);
}

//Handler for bp_activity_request()
function cn_bp_activity_request(scope, filter){
	if(!scope){
		var selected_tab = jq( 'div.activity-type-tabs li.selected' );
		if ( !selected_tab.length )
			scope = null;
		else
			scope = selected_tab.attr('id').substr( 9, selected_tab.attr('id').length );
	}
	if(!filter){
		filter = cn_getActivityFilters();
	}
	if(!filter){
		filter = -1;
	}
	else filter = filter.join(',');
	bp_activity_request(scope, filter);
}

//gather activity filters
function cn_getActivityFilters(){
	var filters = [];
	var tags = [];
	//do message types
	$.each($('.cn-activity-filter-by'), function(index){
		if( $(this).hasClass('selected')){
			filters.push($( this ).attr('id').replace('activity-filter-by-', ''));
		}
	});
	
	//get tags
	if($('.bp-filter-activity-by-tags').length){
		tags = $('.bp-filter-activity-by-tags').val().split(',');
		tags = $.unique(tags).join(';');
		if(tags.length !== 0){
			var tag_str = 'bp_tagged_item:' + tags;
			filters.push(tag_str);
		}
	}
	return filters;
}

//from http://stackoverflow.com/questions/282758/truncate-a-string-nicely-to-fit-within-a-given-pixel-width
function fitStringToWidth(str,width,className) {
  // str    A string where html-entities are allowed but no tags.
  // width  The maximum allowed width in pixels
  // className  A CSS class name with the desired font-name and font-size. (optional)
  // ----
  // _escTag is a helper to escape 'less than' and 'greater than'
  function _escTag(s){ return s.replace("<","&lt;").replace(">","&gt;");}

  //Create a span element that will be used to get the width
  var span = document.createElement("span");
  //Allow a classname to be set to get the right font-size.
  if (className) span.className=className;
  span.style.display='inline';
  span.style.visibility = 'hidden';
  span.style.padding = '0px';
  document.body.appendChild(span);

  var result = _escTag(str); // default to the whole string
  span.innerHTML = result;
  // Check if the string will fit in the allowed width. NOTE: if the width
  // can't be determinated (offsetWidth==0) the whole string will be returned.

  if (span.offsetWidth > width) {
    var posStart = 0, posMid, posEnd = str.length, posLength;
    // Calculate (posEnd - posStart) integer division by 2 and
    // assign it to posLength. Repeat until posLength is zero.
    while (posLength = (posEnd - posStart) >> 1) {
      posMid = posStart + posLength;
      //Get the string from the begining up to posMid;
      span.innerHTML = _escTag(str.substring(0,posMid)) + '&hellip;';

      // Check if the current width is too wide (set new end)
      // or too narrow (set new start)
      if ( span.offsetWidth > width ) posEnd = posMid; else posStart=posMid;
    }

    result = _escTag(str.substring(0,posStart)) +
      '&hellip;';
  }
  document.body.removeChild(span);
  return result;
}

//that tracks a click on an outbound link in Google Analytics.
// https://support.google.com/analytics/answer/1136920?hl=en
function cn_track_ad_click(name, url) {
	ga('send', 'event', 'CN Ad Click', 'click', name, {
     'transport': 'beacon',
     'hitCallback': function(){window.open(url);}
   });
}