// JavaScript Document
// @version 1.0.0

jQuery(document).ready(function($) {
	/*********************************************
	 *********** Group Calendar Plugin ***********
	 *********************************************/
	if($('#bgc_more_info #event-date').length){
		$('#bgc_more_info #event-date').datepicker({dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true});	
	}
});