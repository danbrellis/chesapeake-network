jQuery(document).ready( function() {
	"use strict";
	var j = jQuery;
	
	
	j("ul#members-list a.invite-btn").on( "click", function(event) {
		event.preventDefault();
		var btn = j(this);
		var spinner = btn.prev('.spinner');
		btn.addClass('disabled');
		spinner.css('visibility', 'visible');

		//var user_id = btn.data( "user-id" );
		var params = get_query_args(btn.attr( "href" ));
		j.post( ajaxurl, {
			action: 'invite_anyone_groups_invite_user',
			'cookie': encodeURIComponent(document.cookie),
			'_wpnonce_invite_user': params._wpnonce_invite_user,
			'user_id': params.invite,
			'group_id': j("input#group_id").val(),
			'inviter': j("input#current_user").val()
		},
		function(response) {
			var r = j.parseJSON(response);
			if(r.status === 'success'){
				btn.replaceWith(r.msg);
			}
			else {
				alert("Error: " + r.msg);
				btn.removeClass('disabled');
			}
			spinner.hide();
		});
	});
	
	
	

});

function get_query_args(url){
	var vars = [], hash;
	if(url == undefined) url = document.URL;
	var q = url.split('?')[1];
	if(q !== undefined){
		q = q.split('&');
		for(var i = 0; i < q.length; i++){
			hash = q[i].split('=');
			//vars.push(hash[1]);
			vars[hash[0]] = hash[1];
		}
	}
	return vars;
}