<?php get_header(); ?>

<div class="row">
    <div class="small-14 medium-10 large-7 large-push-3 medium-push-4 columns" role="main">

		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<header>
				<h1 class="entry-title"><?php _e('File Not Found', 'chesnet'); ?></h1>
			</header>
			<div class="entry-content">
				<div class="error">
					<p class="bottom"><?php _e('The page you are looking for might have been removed, had its name changed, or is temporarily unavailable.', 'chesnet'); ?></p>
				</div>
				<p><?php _e('Please try the following:', 'chesnet'); ?></p>
				<ul>
					<li><?php _e('Check your spelling', 'chesnet'); ?></li>
					<li><?php printf(__('Return to the <a href="%s">home page</a>', 'chesnet'), home_url()); ?></li>
					<li><?php _e('Click the <a href="javascript:history.back()">Back</a> button', 'chesnet'); ?></li>
				</ul>
			</div>
		</article>

	</div>
	<?php get_sidebar(); get_sidebar('right'); ?>
</div>
<?php get_footer(); ?>
