<?php
/*
Plugin Name: (DO NOT UPDATE) BuddyPress Group Email Subscription
Plugin URI: http://wordpress.org/extend/plugins/buddypress-group-email-subscription/
Description: Allows group members to receive email notifications for group activity and forum posts instantly or as daily digest or weekly summary.
Author: Deryk Wenaus, boonebgorges, r-a-y
Revision Date: December 1, 2014
Version: 3.5
*/

/* The following edits made by Dan Brellis (dbrellis)

  line		page									description
  391		bp-activity-subscription-functions.php	gets the post object of the activity so we can skip if the activity
  													doesn't refer to a post and we can use the post id as the reply-to
													address for emailing comments
								
  421-422	bp-activity-subscription-functions.php	Changed $subject variable to include post's title
  
  453-454	bp-activity-subscription-functions.php	Added ass_filter_message_content to filter email content
  
  558-561	bp-activity-subscription-functions.php	Add headers to wp_mail()
  
  575-576	bp-activity-subscription-functions.php	allows action hook after ass_group_notification_activity() has done it's work
  
  868-911	bp-activity-subscription-functions.php	simple reformatting to align with foundation 5 framework, no major change.
  
  375		bp-activity-subscription-digest.php 	remove <span> tag around $action
  
*/

/**
 * Main loader for the plugin.
 *
 * This function is hooked to bp_include, which is the recommended method for loading BP plugins
 * since BP 1.2.5 or so. When this function is loaded properly, it will unhook
 * activitysub_load_buddypress(). If bp_include is not fired (because you are running a legacy
 * version of BP), the legacy function will load the plugin normally.
 */
function ass_loader() {
	if ( bp_is_active( 'groups' ) && bp_is_active( 'activity' ) ) {
		require_once( dirname( __FILE__ ) . '/bp-activity-subscription-main.php' );
	}

	remove_action( 'plugins_loaded', 'activitysub_load_buddypress', 11 );
}
add_action( 'bp_include', 'ass_loader' );

/**
 * Legacy loader for BP < 1.2
 *
 * This function will be unhooked by ass_loader() when possible
 */
function activitysub_load_buddypress() {
	global $ass_activities;
	if ( function_exists( 'bp_core_setup_globals' ) ) {
		// Don't load the plugin if activity and groups are not both active
		if ( function_exists( 'bp_is_active' ) && ( !bp_is_active( 'groups' ) || !bp_is_active( 'activity' ) ) )
			return false;

		require_once( dirname( __FILE__ ) . '/bp-activity-subscription-main.php' );
		return true;
	}
	/* Get the list of active sitewide plugins */
	$active_sitewide_plugins = maybe_unserialize( get_site_option( 'active_sitewide_plugins' ) );

	if ( !isset( $active_sidewide_plugins['buddypress/bp-loader.php'] ) )
		return false;

	if ( isset( $active_sidewide_plugins['buddypress/bp-loader.php'] ) && !function_exists( 'bp_core_setup_globals' ) ) {
		require_once( WP_PLUGIN_DIR . '/buddypress/bp-loader.php' );
		// Don't load the plugin if activity and groups are not both active
		if ( function_exists( 'bp_is_active' ) && ( !bp_is_active( 'groups' ) || !bp_is_active( 'activity' ) ) )
			return false;

		require_once( dirname( __FILE__ ) . '/bp-activity-subscription-main.php' );
		return true;
	}

	return false;
}
add_action( 'plugins_loaded', 'activitysub_load_buddypress', 11 );


function activitysub_textdomain() {
	load_plugin_textdomain( 'bp-ass', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
}
add_action( 'init', 'activitysub_textdomain' );


function activitysub_setup_digest_defaults() {
	require_once( dirname( __FILE__ ) . '/bp-activity-subscription-digest.php' );
	ass_set_daily_digest_time( '05', '00' );
	ass_set_weekly_digest_time( '4' );
}
register_activation_hook( __FILE__, 'activitysub_setup_digest_defaults' );

function activitysub_unset_digests() {
	wp_clear_scheduled_hook( 'ass_digest_event' );
	wp_clear_scheduled_hook( 'ass_digest_event_weekly' );
}
register_deactivation_hook( __FILE__, 'activitysub_unset_digests' );


?>
