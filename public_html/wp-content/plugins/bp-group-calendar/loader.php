<?php
/*
Plugin Name: (DO NOT UPDATE) BuddyPress Group Calendar
Version: 1.3.9
Plugin URI: http://premium.wpmudev.org/project/buddypress-group-calendar/
Description: Adds event calendar functionality to BuddyPress Groups. Maintain, update and share upcoming group events with really swish calendar functionality.
Author: WPMU DEV
Author URI: http://premium.wpmudev.org/
Network: true
WDP ID: 109

Copyright 2009-2014 Incsub (http://incsub.com)
Author - Aaron Edwards
Contributors -

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License (Version 2 - GPLv2) as published by
the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* The following edits made by Dan Brellis (dbrellis)

  line		page									description
  73		loader.php								commented out line to include wpmudev-dash-notification.php
  
  23		groupcalendar/bp-group-calendar.php		comment out admin_init action to run bp_group_calendar_make_current()
  25		groupcalendar/bp-group-calendar.php		add hook to register bgc_event CPT
  69		groupcalendar/bp-group-calendar.php		comment out bp_group_calendar_global_install()
  105		groupcalendar/bp-group-calendar.php		add bp_group_calendar_register_post_type() to register bgc_event
  308		groupcalendar/bp-group-calendar.php		gets the category ID relative for the group
  333		groupcalendar/bp-group-calendar.php		stores the event object as $event for manipulation
  334		groupcalendar/bp-group-calendar.php		adds a check to if(!$event) to see if we have the right category & post_type
  342		groupcalendar/bp-group-calendar.php		set $creator_id to $event->post_author
  350-389	groupcalendar/bp-group-calendar.php		remove $wpdb query and updates event & meta via CPT functions
  394-440	groupcalendar/bp-group-calendar.php		remove $wpdb query and inserts event & meta via CPT functions
  671		groupcalendar/bp-group-calendar.php		stores the event object as $event for manipulation
  672		groupcalendar/bp-group-calendar.php		performs a check to see if the event ID is valid
  678		groupcalendar/bp-group-calendar.php		replaces $event_id with $event->ID
  693		groupcalendar/bp-group-calendar.php		set $creator_id to $event->post_author
  703		groupcalendar/bp-group-calendar.php		uses wp_delete_post() to delete the event
  861		groupcalendar/bp-group-calendar.php		gets the category object relative for the group
  870-916	groupcalendar/bp-group-calendar.php		replace wpdb SELECT with WP_Query
  920		groupcalendar/bp-group-calendar.php		set $range default & added $owner to bp_group_calendar_list_events() args
  925		groupcalendar/bp-group-calendar.php		gets the category object relative for the group
  926		groupcalendar/bp-group-calendar.php		sets default $event_args
  937		groupcalendar/bp-group-calendar.php		comment out $filter
  949-957	groupcalendar/bp-group-calendar.php		add $event_args and remove $filter
  964-972	groupcalendar/bp-group-calendar.php		add $event_args and remove $filter
  976		groupcalendar/bp-group-calendar.php		changed else if statement to default to range = upcoming
  995-1007	groupcalendar/bp-group-calendar.php		set $event-args['post_author'] based on $owner
  1009-1010	groupcalendar/bp-group-calendar.php		removed $limit
  1012-1015	groupcalendar/bp-group-calendar.php		removed $wpdb SELECT and added WP_Query
  1021-1035	groupcalendar/bp-group-calendar.php		fixed object values
  1023		groupcalendar/bp-group-calendar.php		set $event_time from get_post_meta)
  1042		groupcalendar/bp-group-calendar.php		add wp_reset_postdata()
  1205		groupcalendar/bp-group-calendar.php		added updated args to bp_group_calendar_list_events()
  1215		groupcalendar/bp-group-calendar.php		gets the category object relative for the group
  1219		groupcalendar/bp-group-calendar.php		gets event object from get_posts instead of wpdb SELECT
  1222		groupcalendar/bp-group-calendar.php		adds a check to if(!$event) to see if we're in the right category
  1225		groupcalendar/bp-group-calendar.php		sets $event_location from get_post_meta()
  1228-1274	groupcalendar/bp-group-calendar.php		fixed object values
  1306-1357	groupcalendar/bp-group-calendar.php		adds conditional loader of CHESNET theme form template
  1368		groupcalendar/bp-group-calendar.php		gets the category object relative for the group
  1382		groupcalendar/bp-group-calendar.php		gets event object from get_posts instead of wpdb SELECT
  1393		groupcalendar/bp-group-calendar.php		adds a check to if(!$event) to see if we have the right category & post_type
  1387-1400	groupcalendar/bp-group-calendar.php		sets event object from get_post() and get_post_meta()
  1436-1440	groupcalendar/bp-group-calendar.php		checks for redundancies when building $event_meta
	1524		groupcalendar/bp-group-calendar.php		updated WP_Widget __contructor function to be in line with codex
  1560-1577	groupcalendar/bp-group-calendar.php		removed $wpdb SELECT and added WP_Query
  1585		groupcalendar/bp-group-calendar.php		sets event object from get_post() and get_post_meta()
  1600-1604	groupcalendar/bp-group-calendar.php		gets the group and bails on any private groups
  1608		groupcalendar/bp-group-calendar.php		removes groups_get_group()
	1629		groupcalendar/bp-group-calendar.php		updated WP_Widget __contructor function to be in line with codex
  1662-1682	groupcalendar/bp-group-calendar.php		removed $wpdb SELECT and added WP_Query
  1690		groupcalendar/bp-group-calendar.php		sets event object from get_post() and get_post_meta()
	1743		groupcalendar/bp-group-calendar.php		updated WP_Widget __contructor function to be in line with codex
  1786-1792	groupcalendar/bp-group-calendar.php		gets all the category ids relative to the specified groups
  1794-1813	groupcalendar/bp-group-calendar.php		removed $wpdb SELECT and added WP_Query
  1821		groupcalendar/bp-group-calendar.php		sets event object from get_post() and get_post_meta()
  
  59		groupcalendar/group_calendar.css		added min-height:16em;
  
  103		groupcalendar/calendar.class.php		add link to month in calendar widget
    	
*/

//default permissions for existing groups. Choose: full, limited, or none
if (!defined('BGC_MODERATOR_DEFAULT'))
	define('BGC_MODERATOR_DEFAULT', 'full');

if (!defined('BGC_MEMBER_DEFAULT'))
	define('BGC_MEMBER_DEFAULT', 'limited');

//default for sending email notifications for new events. Group admins can overwrite.
if (!defined('BGC_EMAIL_DEFAULT'))
	define('BGC_EMAIL_DEFAULT', 'yes'); //yes or no

$bp_group_calendar_current_version = '1.3.9';

/* Only load code that needs BuddyPress to run once BP is loaded and initialized. */
function bp_group_calendar_init() {
	require( dirname( __FILE__ ) . '/groupcalendar/bp-group-calendar.php' );
}
add_action( 'bp_include', 'bp_group_calendar_init' );

function bp_group_calendar_localization() {
	global $bgc_locale;
	// Load up the localization file if we're using WordPress in a different language
	// Place it in this plugin's "languages" folder and name it "groupcalendar-[value in wp-config].mo"
	load_plugin_textdomain( 'groupcalendar', FALSE, '/bp-group-calendar/languages' );
	if (get_locale())
		setlocale(LC_TIME, get_locale()); //for date translations in php

	//get display settings
	$temp_locales = explode('_', get_locale());
	$bgc_locale['code'] = ($temp_locales[0]) ? $temp_locales[0] : 'en';
	$bgc_locale['time_format'] = (get_option('time_format')=='H:i') ? 24 : 12;
	$bgc_locale['week_start'] = (get_option('start_of_week')=='0') ? 7 : get_option('start_of_week');
}
add_action( 'plugins_loaded', 'bp_group_calendar_localization' );

//include_once( dirname( __FILE__ ) . '/groupcalendar/dash-notice/wpmudev-dash-notification.php' );