<?php

//Load bp-deafult theme
//add_filter( 'bp_do_register_theme_directory', '__return_true' );

/********************************************
 * FUNCTIONS RELATED TO BUDDYPRESS ACTIVITY *
 ********************************************/
 
/**
 * Remove certain BP Activity abilities
 *
 * @since 1.0
 * @author dbrellis
 * @return void
 */
add_filter( 'bp_activity_can_comment', '__return_false'); //removes commenting on activity; comments only allowed on posts
add_filter( 'bp_activity_do_mentions', '__return_false' ); //disables the "mention" feature

/**
 * Returns the activity type by post type
 */
function cn_get_activity_type($post_type, $scope = 'new'){
	$types = array(
		'post' => array(
			'new'			=> 'bp_news_created',
			'reminder'		=> 'bp_news_reminder',
			'new_name'		=> __('New Announcement', 'chesnet'),
			'singular_name'	=> __('Announcement', 'chesnet'),
			'plural_name'	=> __('News &amp; Announcements', 'chesnet')
		),
		'topic' => array(
			'new'		=> 'bp_topic_created',
			'reminder'	=> 'bp_topic_reminder',
			'new_name'	=> __('New Question', 'chesnet'),
			'singular_name'	=> __('Question', 'chesnet'),
			'plural_name'	=> __('Questions', 'chesnet')
		),
		'dlm_download' => array(
			'new'		=> 'bp_dlm_created',
			'reminder'	=> 'bp_dlm_reminder',
			'new_name'	=> __('New Resource', 'chesnet'),
			'singular_name'	=> __('Resource', 'chesnet'),
			'plural_name'	=> __('Resources', 'chesnet')
		),
		'bgc_event' => array(
			'new'		=> 'bp_bgc_created',
			'reminder'	=> 'bp_bgc_reminder',
			'new_name' 	=> __('New Event', 'chesnet'),
			'singular_name'	=> __('Event', 'chesnet'),
			'plural_name'	=> __('Events', 'chesnet')
		),
		'cn_job' => array(
			'new'		=> 'bp_job_created',
			'reminder'	=> 'bp_job_reminder',
			'new_name'	=> __('New Job Announcement', 'chesnet'),
			'singular_name'	=> __('Job Posting', 'chesnet'),
			'plural_name'	=> __('Job Postings', 'chesnet')
		),
		'group' => array(
			'new'		=> 'created_group',
			'reminder'	=> 'bp_job_reminder',
			'new_name'	=> __('New Group', 'chesnet'),
			'singular_name'	=> __('Group', 'chesnet'),
			'plural_name'	=> __('Groups', 'chesnet')
		)
	);
	if(!isset($types[$post_type])){
		return cn_get_activity_type('post');
	}
	if(!isset($types[$post_type][$scope])){
		return cn_get_activity_type($post_type, 'new');
	}
	else return $types[$post_type][$scope];
}

/**
 * Registering the Activity actions for your component
 *
 * The registered actions will also be available in Administration
 * screens
 *
 * @uses bp_activity_set_action()
 */
function cn_register_activity_actions() {
	
    bp_activity_set_action('groups', cn_get_activity_type('post'),  __('New Announcement', 'chesnet') );
	bp_activity_set_action('groups', cn_get_activity_type('topic'),  __('New Question', 'chesnet') );
	bp_activity_set_action('groups', cn_get_activity_type('dlm_download'),  __('New Resource', 'chesnet') );
	bp_activity_set_action('groups', cn_get_activity_type('bgc_event'),  __('New Event', 'chesnet') );
	bp_activity_set_action('groups', cn_get_activity_type('cn_job'),  __('New Job', 'chesnet') );
}
add_action( 'bp_register_activity_actions', 'cn_register_activity_actions' );

/**
 * Adds options to activity filter dropdowns
 *
 * @since 1.0
 * @author dbrellis
 */
function cn_activity_filter_options() {
	echo '<option value="'.cn_get_activity_type('post').'">' . __('News &amp; Announcements', 'chesnet') . '</option>';
	echo '<option value="'.cn_get_activity_type('topic').'">' . __('Questions', 'chesnet') . '</option>';
	echo '<option value="'.cn_get_activity_type('dlm_download').'">' . __('Resources', 'chesnet') . '</option>';
	echo '<option value="'.cn_get_activity_type('bgc_event').'">' . __('Events', 'chesnet') . '</option>';
	echo '<option value="'.cn_get_activity_type('cn_job').'">' . __('Job Postings', 'chesnet') . '</option>';
}
add_action( 'bp_activity_filter_options', 'cn_activity_filter_options' );
add_action( 'bp_group_activity_filter_options', 'cn_activity_filter_options' );
add_action( 'bp_member_activity_filter_options', 'cn_activity_filter_options' );


/**
 * Filters the default bp show activity filter dropdown
 *
 * @since 1.0
 * @author dbrellis
 */
function cn_bp_get_activity_show_filters($filters, $context){
	if(in_array($context, array('activity', 'group', 'member'))){
		unset($filters['new_member']);
		unset($filters['updated_profile']);
		//unset($filters['created_group']);
		unset($filters['joined_group']);
		unset($filters['activity_update']);
		unset($filters['new_blog_post']);
		unset($filters['new_blog_comment']);
	}
	/*
	$filters = array(7) {
		["new_member"]=> string(11) "New Members"
		["updated_profile"]=> string(15) "Profile Updates"
		["created_group"]=> string(10) "New Groups"
		["joined_group"]=> string(17) "Group Memberships"
		["activity_update"]=> string(7) "Updates"
		["new_blog_post"]=> string(5) "Posts"
		["new_blog_comment"]=>  string(8) "Comments"
	}
	*/
	return $filters;
}
add_action('bp_get_activity_show_filters', 'cn_bp_get_activity_show_filters', 10, 2);


/**
 * Removes bp_activity actions
 *
 * @since 1.0
 * @author dbrellis
 */

function kill_anonymous_example() {
	remove_action( 'bp_activity_filter_options', 'bp_docs_activity_filter_options' );
	remove_action( 'bp_group_activity_filter_options', 'bp_docs_activity_filter_options' );
	remove_action( 'bp_member_activity_filter_options', 'bp_docs_activity_filter_options' );
	
    remove_anonymous_object_filter(
        'bp_activity_filter_options',
        'BBP_BuddyPress_Activity',
        'activity_filter_options'
    );
	remove_anonymous_object_filter(
        'bp_group_activity_filter_options',
        'BBP_BuddyPress_Activity',
        'activity_filter_options'
    );
	remove_anonymous_object_filter(
        'bp_member_activity_filter_options',
        'BBP_BuddyPress_Activity',
        'activity_filter_options'
    );
	remove_anonymous_object_filter(
        'bp_get_activity_action_pre_meta',
        'BP_Legacy',
        'secondary_avatars'
    );
}
//add_action( 'bp_get_activity_action_pre_meta', 'kill_anonymous_example', 10 );
add_action( 'bp_activity_filter_options', 'kill_anonymous_example', 0 );
add_action( 'bp_member_activity_filter_options', 'kill_anonymous_example', 0 );
add_action( 'bp_member_activity_filter_options', 'kill_anonymous_example', 0 );

function remove_anonymous_object_filter( $tag, $class, $method ) {
	$filters = $GLOBALS['wp_filter'][ $tag ];

	if ( empty ( $filters ) )
	{
		return;
	}

	foreach ( $filters as $priority => $filter )
	{
		foreach ( $filter as $identifier => $function )
		{
			if ( is_array( $function)
				and is_a( $function['function'][0], $class )
				and $method === $function['function'][1]
			)
			{
				remove_filter(
					$tag,
					array ( $function['function'][0], $method ),
					$priority
				);
			}
		}
	}
}

/**
 * Filters the css class for the activity stream <li>
 *
 * @since 1.0
 * @author dbrellis
 * @param str $class The classes for the <li> tag
 	@from bp_get_activity_css_class filter
 * @return str The classes for the <li> tag
 */
function cn_filter_activity_css_class($class){
	global $activities_template;
	$class = str_replace(' mini', '', $class);
	if($activities_template->activity->component == 'groups' && $activities_template->activity->secondary_item_id !== '0')
		$post_type = get_post_type($activities_template->activity->secondary_item_id);
	else $post_type = 'page';
		
	$class .= ' activity-'.$post_type.' clearfix';
	
	return $class;
}
add_filter( 'bp_get_activity_css_class', 'cn_filter_activity_css_class', 10, 1);


/* Activity Display */

function cn_bp_get_activity_action( $action ){
	return stripslashes( $action );
}
function cn_bp_get_activity_content_body( $content ){
	return stripslashes( $content );
}
add_filter('bp_get_activity_action', 'cn_bp_get_activity_action');
add_filter('bp_get_activity_content_body', 'cn_bp_get_activity_content_body');

function cn_bp_activity_permalink($permalink, $activity){
	return sprintf( '%1$s<div class="view activity-time-since">%5$s <a href="%2$s" title="%3$s">%4$s</a></div>', $activity->action, cn_bp_activity_get_permalink(null, $activity), esc_attr__( 'View Entire', 'buddypress' ), cn_bp_activity_time_since(null, $activity), __('Posted', 'chesnet') );	
}
add_filter('bp_activity_permalink', 'cn_bp_activity_permalink', 10, 2);

function cn_bp_activity_time_since ($time_since=false, $activity){
	return '<span class="activity-time">'.bp_core_time_since( $activity->date_recorded ).'</span>';
}
add_filter ('bp_activity_time_since', 'cn_bp_activity_time_since', 10, 2);

function bp_dtheme_activity_secondary_avatars( $action, $activity ) {
	switch ( $activity->component ) {
	case 'friends' :
		// Only insert avatar if one exists
		if ( $secondary_avatar = bp_get_activity_secondary_avatar() ) {
			$reverse_content = strrev( $action );
			$position = strpos( $reverse_content, 'a<' );
			$action = substr_replace( $action, $secondary_avatar, -$position - 2, 0 );
		}
		break;
	}

	return $action;
}
add_filter( 'bp_get_activity_action_pre_meta', 'bp_dtheme_activity_secondary_avatars', 10, 2 );


/* Activity Publishing */

//allow kses in activity
function cn_bp_activity_admin_load($doaction){
	if($doaction == 'save'){
		remove_filter( 'bp_activity_content_before_save', 'bp_activity_filter_kses', 1 );
		remove_filter( 'bp_activity_action_before_save', 'bp_activity_filter_kses', 1 );
	}
}
add_action('bp_activity_admin_load', 'cn_bp_activity_admin_load');

//return to the status quo
function cn_bp_activity_admin_edit_after(){
	add_filter( 'bp_activity_content_before_save', 'bp_activity_filter_kses', 1 );
	add_filter( 'bp_activity_action_before_save', 'bp_activity_filter_kses', 1 );
}
add_action('bp_activity_admin_edit_after', 'cn_bp_activity_admin_edit_after');

function cn_bp_record_activity($post_id, $post){
	global $chesnet;
	
	//return if it's not an activity post
	if(!in_array($post->post_type, $chesnet->user_can_add)) return;
	
	// Set the action & content. Filterable so that other integration pieces can alter it
	
	$action 	= '';
	$content	= '';
	$user_url	= bp_core_get_user_domain( $post->post_author );
	$user_link 	= bp_core_get_userlink( $post->post_author );
	$user_avatar	= bp_core_fetch_avatar( array(
		'item_id' => $post->post_author,
		'object'  => 'user',
		'type'    => 'thumb',
		'class'   => 'avatar imgWrapLeft',
		'width'   => 50,
		'height'  => 50,
		'email'   => false
	) );
	$post_url	= get_post_permalink( $post->ID );
	
	// Get the group. If none is set, default to group 1
	$cat_slugs = wp_get_post_categories( $post->ID, array('fields' => 'slugs') );
	$group_ids = array();
	$group_ids[] = $chesnet->cn_get_gid_from_post_id($post->ID);
	
	if(empty($group_ids)) $group_ids = array(1);  //if no categories, set it to group 1 (default)

	//loop through all groups and add an activity
	$all_activities = array();
	foreach($group_ids as $group_id){
		$args = array(
			'user_url'		=> $user_url,
			'user_link'		=> $user_link,
			'user_avatar'	=> $user_avatar,

			'user_id'		=> $post->post_author,
			'primary_link'	=> $post_url,
			'item_id'		=> $group_id,
			'secondary_item_id'	=> $post->ID // The id of the post itself
		);
		
		$activity_id = cn_bp_do_record_activity($group_id, $post, $args);
		$all_activities[] = $activity_id;
	
	}
	do_action( 'cn_after_all_activity_save', $all_activities );
}

function cn_bp_do_record_activity($group_id, $post, $args=false){
	$defaults = array(
		//used to create content & action
		'user_url'			=> bp_core_get_user_domain( $post->post_author ),
		'user_link'			=> bp_core_get_userlink( $post->post_author ),
		'user_avatar'		=> bp_core_fetch_avatar( array(
									'item_id' => $post->post_author,
									'object'  => 'user',
									'type'    => 'thumb',
									'class'   => 'avatar imgWrapLeft',
									'width'   => 50,
									'height'  => 50,
									'email'   => false
								) ),
		
		//used in bp_activity_add()
		'user_id'			=> $post->post_author,
		'primary_link'		=> get_post_permalink( $post->ID ),
		'component'			=> 'groups',
		'type'				=> cn_get_activity_type($post->post_type),
		'item_id'			=> $group_id,
		'recorded_time'		=> $post->post_modified,
		'secondary_item_id'	=> $post->ID // The id of the post itself
	);
	
	$args = wp_parse_args( $args, $defaults );
	
	extract($args);
	$action = $content = ''; //reset
	
	//action
	ob_start();
	$group = groups_get_group( array( 
					'group_id' => (int)$group_id,
					'populate_extras'   => false,
					'update_meta_cache' => false
				  ) );
	$group_url	= bp_get_group_permalink( $group );
	include(locate_template( 'buddypress/activity/templates/activity-action.php' ));
	$action = ob_get_clean();
		
	//content
	$activity_img = '';
	$activity_content = str_replace('</p>', "</p>\r\n", $post->post_content);
	$activity_content = wpautop(wp_strip_all_tags( $activity_content, false ));
	
	$imgPattern = '~<img [^\>]*\ />~';
	preg_match_all( $imgPattern, $post->post_content, $imgs );
	$no_imgs = count($imgs[0]);
	if ( $no_imgs > 0 ) {
		$img = $imgs[0][0];
		$activity_img = cn_bp_activity_content_img($img);
	};
	
	ob_start();
	include(locate_template( 'buddypress/activity/templates/activity-content.php' ));
	$content = ob_get_clean();
	
	$hide_sitewide = $group->status == 'public' ? false : true;
	
	$activity_args = array(
		'user_id'		=> $user_id,
		'action'		=> $action,
		'content'		=> $content,
		'primary_link'	=> $primary_link,
		'component'		=> $component,
		'type'			=> $type,
		'item_id'		=> $item_id,
		'secondary_item_id'	=> $secondary_item_id, // The id of the post itself
		'recorded_time'		=> isset($recorded_time) ? $recorded_time : bp_core_current_time(),
		'hide_sitewide'		=> apply_filters( 'cn_activity_hide_sitewide', $hide_sitewide, $post, $item = $post->post_author, $component, $group ) // Filtered to allow plugins and integration pieces to dictate
	);
	
	
	do_action( 'cn_before_activity_save', $activity_args, $post );
	
	remove_filter( 'bp_activity_content_before_save', 'bp_activity_filter_kses', 1 );
	remove_filter( 'bp_activity_action_before_save', 'bp_activity_filter_kses', 1 );
	$activity_id = bp_activity_add( apply_filters( 'cn_activity_args', $activity_args, $post ) );
	add_filter( 'bp_activity_content_before_save', 'bp_activity_filter_kses', 1 );
	add_filter( 'bp_activity_action_before_save', 'bp_activity_filter_kses', 1 );
	
	do_action( 'cn_after_activity_save', $activity_id, $activity_args );
	
	return $activity_id;
}

function cn_bp_update_activity($post){
	global $chesnet;
	return;
	//first get all activities with this post->ID as the secondary_item_id
	$activities = array();
	$activities = bp_activity_get(
		array(
			'show_hidden' => true,
			'filter' => array(
				'secondary_id' => $post->ID,
				'object' => 'groups',
			)	
		)
	);

	if(empty($activities['activities'])) {
		cn_bp_record_activity( $post->ID, $post );
	}

	//post's categories (groups)
	$groups = cn_get_post_groups($post);
	$group_ids = array();
	foreach( (array)$groups as $g){
		$group_ids[] = $g->id;
	}
	
	//if there are any group_ids NOT accounted for the activity array, we need to add an activity for that!
	$groups_needing_activity = $group_ids;
	
	$action 	= '';
	$content	= '';
	$user_url	= bp_core_get_user_domain( $post->post_author );
	$user_link 	= bp_core_get_userlink( $post->post_author );
	$user_avatar	= bp_core_fetch_avatar( array(
		'item_id' => $post->post_author,
		'object'  => 'user',
		'type'    => 'thumb',
		'class'   => 'avatar imgWrapLeft',
		'width'   => 50,
		'height'  => 50,
		'email'   => false
	) );
	$post_url	= get_post_permalink( $post->ID );
	$component	= 'groups';
	
	
	foreach ( $activities['activities'] as $activity ) {
		//first, if the post is no longer in the group, delete it and move on
		if(!in_array($activity->item_id, $group_ids)){
			bp_activity_delete_by_activity_id( $activity->id );
			do_action('cn_after_activity_delete', $activity);
			continue;
		}
		
		//action
		ob_start();
		$group = groups_get_group( array( 
						'group_id' => (int)$activity->item_id,
						'populate_extras'   => false,
						'update_meta_cache' => false
					  ) );
		$group_url	= bp_get_group_permalink( $group );
		include(locate_template( 'buddypress/activity/templates/activity-action.php' ));
		$action = ob_get_clean();
			
		//content
		$activity_img = '';
		$activity_content = str_replace('</p>', "</p>\r\n", $post->post_content);
		$activity_content = wpautop(wp_strip_all_tags( $activity_content, false ));
		
		$imgPattern = '~<img [^\>]*\ />~';
		preg_match_all( $imgPattern, $post->post_content, $imgs );
		$no_imgs = count($imgs[0]);
		if ( $no_imgs > 0 ) {
			$img = $imgs[0][0];
			$activity_img = cn_bp_activity_content_img($img);
		};
		
		ob_start();
		include(locate_template( 'buddypress/activity/templates/activity-content.php' ));
		$content = ob_get_clean();
	
		$action	= apply_filters( 'cn_activity_action', $action, $user_link, $post_url, $post, $group, $group_url );
		$content = apply_filters( 'cn_activity_content', $content, $user_link, $post_url, $post, $group, $group_url );
	
		//author, action, content, recorded_time
		$args = array(
			'id'				=> (int)$activity->id,
			'user_id'			=> $post->post_author,
			'action'			=> $action,
			'content'			=> $content,
			'component'			=> $activity->component,
			'type'				=> $activity->type,
			'primary_link'		=> $post_url,
			'item_id'			=> $activity->item_id,
			'secondary_item_id'	=> $post->ID, // The id of the post itself
			'recorded_time'		=> $activity->date_recorded
		);
			
		do_action( 'cn_before_activity_save', $args );
			
		remove_filter( 'bp_activity_content_before_save', 'bp_activity_filter_kses', 1 );
		remove_filter( 'bp_activity_action_before_save', 'bp_activity_filter_kses', 1 );
		remove_action( 'bp_activity_after_save' , 'ass_group_notification_activity', 50 );
		
		$activity_id = bp_activity_add( apply_filters( 'cn_activity_args', $args, $post ) );
		
		add_action( 'bp_activity_after_save' , 'ass_group_notification_activity', 50 );
		add_filter( 'bp_activity_content_before_save', 'bp_activity_filter_kses', 1 );
		add_filter( 'bp_activity_action_before_save', 'bp_activity_filter_kses', 1 );
		
		do_action( 'cn_after_activity_update', $activity_id, $args );

		$groups_needing_activity = array_diff($groups_needing_activity, array($activity->item_id));
		$all_activities[] = $activity_id;
	}
	
	//if there are any groups that still need an activity, let's make them
	foreach($groups_needing_activity as $group_id){
		$activity_id = cn_bp_do_record_activity($group_id, $post);
		$all_activities[] = $activity_id;
	}
	
	do_action( 'cn_after_all_activity_save', $all_activities );
}

/**
 * Handles activity on post status transition
 *
 * @since 1.0
 * @author dbrellis
 * @param str $new_status The status of the post after transition
 * @param str $old_status The status of the post before transition
 * @param obj $post Post object
 * @return void
 */
function cn_bp_catch_transition_post_status( $new_status, $old_status, $post ) {
	global $chesnet;
	
	if(!in_array($post->post_type, $chesnet->user_can_add)) return;
	
	// This is an edit
	if ( $new_status === $old_status ) {
		if ( $new_status == 'publish' ) {
			cn_bp_update_activity( $post );
			return;
		}
	}

	// Publishing a previously unpublished post
	if ( 'publish' === $new_status ) {
		// Untrashing the post
		// Nothing here yet
		//if ( 'trash' == $old_status ) {}
		
		//add reply-to email
		$sock = $chesnet->open_DA_socket();
		$chesnet->add_email_alias($sock, 'reply-to-' . $post->ID);
		
		// Record the post
		cn_bp_record_activity( $post->ID, $post );

	// Unpublishing a previously published post
	} else if ( 'publish' === $old_status ) {
		// Some form of pending status
		// Only remove the activity entry
		
		$activities = bp_activity_get(
			array(
				'show_hidden' => true,
				'filter' => array(
					'secondary_id' => $post->ID,
					'object' => 'groups'
				),
			)
		);
	
		foreach ( $activities['activities'] as $activity ) {
			bp_activity_delete_by_activity_id( $activity->id );
			do_action('cn_after_activity_delete', $activity);
		}
		
	}

	if('pending' != $new_status){
		//Remove accept/reject alias cmds
		$sock = $chesnet->open_DA_socket();
		$chesnet->delete_email_alias($sock, array('accept-' . $post->ID, 'reject-' . $post->ID));
	}
	
}
add_action( 'transition_post_status', 'cn_bp_catch_transition_post_status', 10, 3 );

function cn_bp_activity_content_img($img){
	$xpath = new DOMXPath(@DOMDocument::loadHTML($img));
	$imgsrc = $xpath->evaluate("string(//img/@src)");
	$imgalt = $xpath->evaluate("string(//img/@alt)");
	list($imgwidth, $imgheight, $imgtype, $imgattr) = getimagesize($imgsrc);
	if(!$imgwidth || !$imgheight) return;
	$ratio = $imgwidth / $imgheight;
	$class = 'right';
	if($ratio >= 1){ //landscape
		if($imgwidth > 548){
			$newwidth = 548;
			$newheight = $newwidth / $ratio;
			$class = 'center';
		}
		else {
			$newwidth = $imgwidth;
			$newheight = $imgheight;
			
			if($imgwidth > 300) $class = 'center';
			elseif($imgwidth > 250){
				$newwidth = 250;
				$newheight = $newwidth / $ratio;
			}
		}
	}
	else{ //portrait
		if($imgheight > 550){
			$newheight = 550;
			$newwidth = $newheight * $ratio;
			$class = 'center';
		}
		else {
			$newwidth = $imgwidth;
			$newheight = $imgheight;
			
			if($imgheight > 300) $class = 'center';
			elseif($imgheight > 140){
				$newheight = 140;
				$newwidth = $newheight * $ratio;
			}
		}
	}
	
	//sizes taken care of, now create the tag
	$imgcontclass = $class == 'right' ? 'right' : 'text-center';
	return '<div class="activity-img '.$imgcontclass.'"><img src="'.$imgsrc.'" width="'.$newwidth.'" height="'.$newheight.'" alt="'.$imgalt.'" class="'.$imgclass.'" /></div>';
}

function cn_activity_exists_by_id( $aid ) {
	$a = bp_activity_get( array(
		'in' => $aid,
		'show_hidden' => true
	) );
		
	return !empty( $a['activities'] );
}

function cn_bp_activity_get_permalink($link=false, $activity){
	//we basically never want someone to view an activity item, so we'll send them to the respective post
	if($activity->component != 'groups') return $link;
	
	$post = $activity->secondary_item_id == 0 ? false : get_post($activity->secondary_item_id);
	
	if($post){
		$link = get_post_permalink($post->ID);
		$link = add_query_arg('aid', $activity->id, $link);
		return $link;
	}
	else
		$link = add_query_arg('aid', $activity->id, $activity->primary_link) . '#activity-' . $activity->id;
	
	return $link;
	
}
add_filter('bp_activity_get_permalink', 'cn_bp_activity_get_permalink', 10, 2);

//replaced by cn_bp_activity_before_save()
function cn_bp_activity_component_before_save($component, $activity){
	return $component == 'blogs' ? false : $component;
}
//add_action('bp_activity_component_before_save', 'cn_bp_activity_component_before_save', 10, 2);

//replaced by cn_bp_activity_before_save()
function cn_bp_activity_type_before_save($type, $activity){
	$only_activities = array(
		'bp_news_created',
		'bp_topic_created',
		'bp_dlm_created',
		'bp_bgc_created',
		'bp_job_created',
		'created_group'
	);
	return in_array($type, $only_activities) ? $type : false;
	
	$activity_to_banish = array(
		'new_blog_post',
		'bp_doc_edited',
		'edit_calendar_event',
		'last_activity',
		'new_calendar_event',
		'new_avatar',
		'joined_group',
		'new_member',
		'activity_liked',
		'updated_profile',
		'group_details_updated'
	);
	return in_array($type, $activity_to_banish) ? false : $type;
}
//add_action('bp_activity_type_before_save', 'cn_bp_activity_type_before_save', 10, 2);

function cn_bp_activity_before_save($args){
	$only_activities = array(
		'bp_news_created',
		'bp_topic_created',
		'bp_dlm_created',
		'bp_bgc_created',
		'bp_job_created',
		'created_group'
	);
	if(!in_array($args->type, $only_activities)) $args->type = false;
	
	if($args->component == 'blogs') $args->component = false;

	
	if($args->type == 'created_group'){
		//do some additional maniplation
		$group = groups_get_group(array('group_id' => $args->item_id));
		if($group) $args->primary_link = bp_get_group_permalink($group);
		//if we need to, look into bp_activity_generate_action_string() for further customization of the 'action'
	}
	
	return $args;
}
add_action('bp_activity_before_save', 'cn_bp_activity_before_save' );


/* Activity Meta */

function cn_after_activity_save( $activity_id, $args ) {
	$post_id = $args['secondary_item_id'];
	
	//we want to make sure each post has any activity ids in it's meta
	$activity_ids = get_post_meta($post_id, '_cn_bp_activity');
	if($activity_ids && is_array($activity_ids)){
		if(!in_array($activity_id, $activity_ids))	
			add_post_meta($post_id, '_cn_bp_activity', $activity_id);
	}
	elseif ($activity_id !== $activity_ids) add_post_meta($post_id, '_cn_bp_activity', $activity_id);
		
	
	
	//add the post_tag id to the activity meta
	$tag_ids = wp_get_object_terms( $post_id, 'post_tag', array('fields' => 'ids') );
	if($tag_ids && is_array($tag_ids)){
		//get activity's tags
		$activity_tags_old = bp_activity_get_meta($activity_id, 'post_tag_id', false);
		$tags_to_delete = array_diff($activity_tags_old, $tag_ids);
		$tags_to_add = array_diff($tag_ids, $activity_tags_old);
		
		if(!empty($tags_to_delete) && is_array($tags_to_delete)) {
			foreach($tags_to_delete as $tag_to_delete){
				$deleted = bp_activity_delete_meta($activity_id, 'post_tag_id', $tag_to_delete);
			}
		}

		if(!empty($tags_to_add) && is_array($tags_to_add)) {
			foreach($tags_to_add as $tag_to_add){
				$added_activity_meta_id = bp_activity_add_meta($activity_id, 'post_tag_id', $tag_to_add);
			}
		}
	}
}

function cn_after_activity_delete($activity){
	//delete the activity meta on the post
	delete_post_meta($activity->secondary_item_id, '_cn_bp_activity', $activity->id);
}
add_action( 'cn_after_activity_save', 'cn_after_activity_save', 10, 2 );
add_action( 'cn_after_activity_update', 'cn_after_activity_save', 10, 2 );
add_action( 'cn_after_activity_delete', 'cn_after_activity_delete', 10, 1);

function cn_after_activity_update( $activity_id, $args ){
	//update activity meta with editted
	if(!bp_activity_add_meta($activity_id, '_cn_bp_activity_editted', 'yes', true)){
		//meta alread exists?
		bp_activity_update_meta( $activity_id, '_cn_bp_activity_editted', 'yes');
	}
}
add_action( 'cn_after_activity_update', 'cn_after_activity_update', 10, 2 );

function cn_bp_activity_favorite_count($activity_id=null){
	echo cn_bp_get_activity_favorite_count($activity_id);
}
	function cn_bp_get_activity_favorite_count($activity_id=null){
		if(!$activity_id){
			global $activity_template;
			$activity_id = bp_get_activity_id();
		}
		$fc = bp_activity_get_meta( $activity_id, 'favorite_count' );
		return !$fc ? 0 : $fc;
	}

function cn_favorites_number($zero=false, $one=false, $more=false){
	$number = cn_bp_get_activity_favorite_count();
	
	if ( $number > 1 ) {
        $output = str_replace( '%', number_format_i18n( $number ), ( false === $more ) ? __( '% people have favorited this' ) : $more );
    } elseif ( $number == 0 ) {
        $output = ( false === $zero ) ? __( '0 people have favorited this' ) : $zero;
    } else { // must be one
        $output = ( false === $one ) ? __( '1 person has favorited this' ) : $one;
    }
	return $output;
}

function cn_users_total_favorite_count($favorite_activity_entries, $user_id){
	global $wpdb, $bp;
	//check if all the activities exist
	$activities = $exists = array();
	$activities = maybe_unserialize( $favorite_activity_entries );
	if(!is_array($activities)) $activities = array($activities);
	$alist = implode(',',$activities);
	if($alist == '') return $favorite_activity_entries;
	$exists = $wpdb->get_results( 
		"SELECT `id`
		 FROM {$bp->activity->table_name}
		 WHERE `id` IN ({$alist})",
		 'ARRAY_N'
	);
	if(!empty($exists))	$favorite_activity_entries = call_user_func_array('array_merge', $exists);
		
	//if necessary, update the user meta for bp_favorite_activities with the existing activities
	if(count($favorite_activity_entries) !== count($activities)) bp_update_user_meta( $user_id, 'bp_favorite_activities', $favorite_activity_entries );
	
	return $favorite_activity_entries;
}
add_filter( 'users_total_favorite_count', 'cn_users_total_favorite_count', 10, 2 );

/* Activity Stream */
function cn_bp_legacy_theme_ajax_querystring($query_string, $object, $object_filter, $object_scope, $object_page, $object_search_terms, $object_extras){
	if(is_user_logged_in()){
		if(empty($object_scope) || $object_scope == 'all'){
			$object_scope = 'groups';
			$query_string = $query_string . '&scope=' . $object_scope;
		}
	}
	//var_dump(explode(';', $_SERVER['HTTP_COOKIE']));	
	return $query_string;
}
add_filter('bp_legacy_theme_ajax_querystring', 'cn_bp_legacy_theme_ajax_querystring', 10, 7);

function cn_bp_init(){
	if(is_user_logged_in())
		setcookie('bp-activity-scope', 'groups', 0, '/');
	else setcookie('bp-activity-scope', '', 0, '/');
}
add_action('bp_init', 'cn_bp_init');

function cn_the_activity_filters($only_on_activity = true, $before = '', $after = ''){
	global $chesnet, $bp;
	if($only_on_activity === false || ($only_on_activity ===true && $chesnet->showing_activity_stream === true)):
		echo $before;
		$pts = get_post_types( array('user_can_add' => true), 'objects' );
		if($pts && is_array($pts)): ?>
        <div class="homesideNavcont">
            <h5 class="navHandle" title="<?php _e('Filter the activity stream.', 'chesnet'); ?>"><?php _e('Activity', 'chesnet'); ?><i class="fi-filter imgWrapRight"></i></h5>
            <div id="activity-actions-container" class="homeSideNav">
                <?php foreach($pts as $pt): ?>
                    <ul class="button-group expand-first size18">
                        <li><a href="<?php echo '#'//add_query_arg(array('atype' => cn_get_activity_type($pt->name)),  bp_get_root_domain());?>" id="activity-filter-by-<?php echo cn_get_activity_type($pt->name); ?>" class="cn-activity-filter-by"><?php echo $pt->labels->name; ?></a></li>
                        <li><a href="<?php echo $chesnet->cn_getUrl( 'add', null, null, $pt->name, null ); ?>" title=""><i class="fi-plus"></i></a></li>
                    </ul>
                <?php endforeach; ?>
            </div>
        </div>
		<?php endif;
		echo $after;
	endif;
}

/**
 * Removes the button on a member page to send them a private message
 *
 * @since 1.0
 * @author dbrellis
 * @return void
 */
function cn_remove_public_message_button() {
	remove_filter( 'bp_member_header_actions', 'bp_send_public_message_button', 20);
}
add_action( 'bp_member_header_actions', 'cn_remove_public_message_button' );
?>